import matplotlib
matplotlib.use('Agg')
import matpy as mp
import numpy as np
import spinspy as spy
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import sys, os, shutil, tempfile
import subprocess, glob, cmocean
from mpl_toolkits.axes_grid1 import make_axes_locatable

try: # Try using mpi
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_procs = comm.Get_size()
except:
    rank = 0
    num_procs = 1

## USER CHANGE THIS SECTION
out_direct    = os.getcwd() + '/Videos' # Where to put the movies
the_name      = 'results_slices_'   # What to call the movies
out_suffix    = 'mp4'               # Movie type
mov_fps       = 10                  # Framerate for movie
pert_prefix   = 'bg_'               # Prefix for basic states. e.g. pret_prefix + 'u.0'
div_cmap      = cmocean.cm.balance  # Which divergent colourmap to use
non_div_cmap  = cmocean.cm.deep     # Which non-divergent colourmap to use
frames_direct = ''
dpi           = 250

# Determine which fields exist
plt_var = ['b', 'epv', 'u', 'v', 'w']

plot_perts_l = [[False]]*len(plt_var)
for ii in range(len(plt_var)):
    if os.path.isfile(pert_prefix + plt_var[ii] + '.0'):
        plot_perts_l[ii] = [False,True]

if (rank == 0):
    pstr = 'Producing videos for: '
    for ii in range(len(plt_var)):
        if plot_perts_l[ii] == [False,True]:
            pstr += plt_var[ii] + '(full,pert) '
        else:
            pstr += plt_var[ii] + '(full) '
    print(pstr)

# If the the out_directory doesn't exist, create it
if (rank == 0) and not(os.path.exists(out_direct)):
    os.makedirs(out_direct)

# Load some information
dat = spy.get_params()
x,y,z = spy.get_grid()
dt = dat.plot_interval/(24.*3600)

Nx = dat.Nx
Ny = dat.Ny
Nz = dat.Nz

X = np.zeros(Nx+1)
Y = np.zeros(Ny+1)
Z = np.zeros(Nz+1)

dx = x[1] - x[0]
dy = y[1] - y[0]
dz = z[1] - z[0]

X[1:] = x + dx/2.
X[0]  = x[0] - dx/2.
Y[1:] = y + dy/2.
Y[0]  = y[0] - dy/2.
Z[1:] = z + dz/2.
Z[0]  = z[0] - dz/2.

# Prepare directories
if rank == 0:
    print('Video files will be saved in {0:s}'.format(out_direct))
    if frames_direct == '':
        tmp_dir = tempfile.mkdtemp(dir=out_direct)
    else:
        tmp_dir = out_direct + '/' + frames_direct
        if not(os.path.exists(tmp_dir)):
            os.makedirs(tmp_dir)
    fig_prefix = tmp_dir + '/' + the_name # path for saving frames
    out_prefix = out_direct + '/' + the_name # path for saving videos
    for proc in range(1,num_procs):
        comm.send(tmp_dir,dest=proc,tag=1)
        comm.send(fig_prefix,dest=proc,tag=2)
        comm.send(out_prefix,dest=proc,tag=3)
else:
    tmp_dir = comm.recv(source=0,tag=1)
    fig_prefix = comm.recv(source=0,tag=2)
    out_prefix = comm.recv(source=0,tag=3)

# Determine which frames are already done
if not(frames_direct == ''):
    list_of_frames_already_done = glob.glob(fig_prefix + '*')
else:
    list_of_frames_already_done = []

if rank == 0:
    print(list_of_frames_already_done)

# Initialize the three meshes
fig = plt.figure(figsize=(12,7))

sp1 = plt.subplot(1,3,1)
plt.title('60% Depth')
sp2 = plt.subplot(1,3,2)
plt.title('50% Depth')
sp3 = plt.subplot(1,3,3)
plt.title('50% x')

sps = [sp1,sp2,sp3]

for sp in sps: # Modify positions to make room for colourbar ticklabels.
    pos1 = sp.get_position()
    pos2 = [pos1.x0, pos1.y0 + 0.1*pos1.height, pos1.width, pos1.height*0.9] 
    sp.set_position(pos2)

# Initial pcolormesh objects
QM1 = sp1.pcolormesh(X/1e3,Y/1e3,np.zeros((Ny,Nx)))
QM2 = sp2.pcolormesh(X/1e3,Y/1e3,np.zeros((Ny,Nx)))
QM3 = sp3.pcolormesh(Y/1e3,Z/1e3,np.zeros((Nz,Ny)))
QMs = [QM1,QM2,QM3]

for sp in sps:
    sp.axis('tight')
    sp.locator_params(axis='both', nbins=6)

# Initialize colorbars
div1 = make_axes_locatable(sp1)
div2 = make_axes_locatable(sp2)
div3 = make_axes_locatable(sp3)

cax1 = div1.append_axes("bottom", size="5%", pad=0.25)
cax2 = div2.append_axes("bottom", size="5%", pad=0.25)
cax3 = div3.append_axes("bottom", size="5%", pad=0.25)

cbar1 = plt.colorbar(QM1, cax=cax1, format="%.2e", orientation='horizontal')
cbar2 = plt.colorbar(QM2, cax=cax2, format="%.2e", orientation='horizontal')
cbar3 = plt.colorbar(QM3, cax=cax3, format="%.2e", orientation='horizontal')

tick_locator = matplotlib.ticker.MaxNLocator(nbins=7)
cbar1.locator = tick_locator
cbar2.locator = tick_locator
cbar3.locator = tick_locator

cax1.yaxis.set_visible(False)
cax1.set_xticklabels(cax1.get_xticklabels(),rotation=90,ha='center')
cax2.yaxis.set_visible(False)
cax2.set_xticklabels(cax2.get_xticklabels(),rotation=90,ha='center')
cax3.yaxis.set_visible(False)
cax3.set_xticklabels(cax3.get_xticklabels(),rotation=90,ha='center')

var_figs = ['']*len(plt_var)
var_figs_pert = ['']*len(plt_var)
fig_ttl = fig.suptitle('Title', fontsize=16)
fig.set_tight_layout({'rect':[0,0,1,0.97]})

# Determine how many outputs there are
var_xy = np.fromfile('{0:s}_slice_xy_0.bin'.format(plt_var[0]))
Nt = len(var_xy)/(Nx*Ny)
print(Nt)

Ntpp = Nt/num_procs
base_Nt = Ntpp*rank
if rank == 0:
    print('Most', Ntpp)
if rank == num_procs - 1:
    Ntpp = Nt - (num_procs-1)*Ntpp
    print('Last', Ntpp)
print(rank, base_Nt)

# Start at var0 and keep going until we run out.
for var_ind in range(len(plt_var)):
    var = plt_var[var_ind]
    plot_perts = plot_perts_l[var_ind]

    f1 = open('{0:s}_slice_xy_0.bin'.format(var), 'rb')
    f1.seek(base_Nt*Nx*Ny*8) # *8 for double
    var_xy = np.fromfile(f1,count=Ntpp*Nx*Ny).reshape(Ntpp,Nx,Ny)
    f1.close()

    f1 = open('{0:s}_slice_xy_1.bin'.format(var), 'rb')
    f1.seek(base_Nt*Nx*Ny*8) # *8 for double
    var_xz = np.fromfile(f1,count=Ntpp*Nx*Ny).reshape(Ntpp,Nx,Ny)
    f1.close()

    f1 = open('{0:s}_slice_yz_0.bin'.format(var), 'rb')
    f1.seek(base_Nt*Ny*Nz*8) # *8 for double
    var_yz = np.fromfile(f1,count=Ntpp*Ny*Nz).reshape(Ntpp,Ny,Nz)
    f1.close()

    # Load background state if needed
    if np.any(plot_perts):
        bg_xy = spy.reader(pert_prefix+var,0,[0,-1],[0,-1],int(0.4*Nz))
        bg_xz = spy.reader(pert_prefix+var,0,[0,-1],[0,-1],int(0.5*Nz))
        bg_yz = spy.reader(pert_prefix+var,0,int(0.5*Nx),[0,-1],[0,-1])
    else:
        bg_xy = 0.
        bg_xz = 0.
        bg_yz = 0.

    cv_xys = np.zeros((2,))
    cv_xzs = np.zeros((2,))
    cv_yzs = np.zeros((2,))

    for ii in range(Ntpp):

        for plot_pert in plot_perts:

            # Determine which colourmap to use
            # When plotting perturbations or velocities, use
            # a divergent colourmap and centre the colourbar
            if plot_pert or var == 'u' or var == 'v' or var == 'w' or var == 'b':
                cmap = div_cmap
                centre_cbar = True
            else:
                cmap = non_div_cmap
                centre_cbar = False 

            if (isinstance(cmap, basestring)):
                QM1.cmap = getattr(matplotlib.cm,cmap)
                QM2.cmap = getattr(matplotlib.cm,cmap)
                QM3.cmap = getattr(matplotlib.cm,cmap)
            else:
                QM1.cmap = cmap
                QM2.cmap = cmap
                QM3.cmap = cmap

            if plot_pert:
                frame_name = '{0:s}{1:s}-{2:05d}_pert.png'.format(fig_prefix,var,ii+base_Nt)
            else:
                frame_name = '{0:s}{1:s}-{2:05d}.png'.format(fig_prefix,var,ii+base_Nt)

            if ii % 1 == 0:
                print('Processor {0:d} accessing {1:s}.{2:d}'.format(rank,var,ii+base_Nt) + ' pert'*plot_pert)

            plt_xy = var_xy[ii,:,:] - plot_pert*bg_xy
            plt_xz = var_xz[ii,:,:] - plot_pert*bg_xz
            plt_yz = var_yz[ii,:,:] - plot_pert*bg_yz

            QM1.set_array(np.ravel(plt_xy.T))
            QM2.set_array(np.ravel(plt_xz.T))
            QM3.set_array(np.ravel(plt_yz.T))

            if centre_cbar:

                cv_xy = np.max(np.abs(np.ravel(plt_xy)))
                cv_xz = np.max(np.abs(np.ravel(plt_xz)))
                cv_yz = np.max(np.abs(np.ravel(plt_yz)))

                if cv_xy == 0:
                    cv_xy = 1
                if cv_xz == 0:
                    cv_xz = 1
                if cv_yz == 0:
                    cv_yz = 1
                
                if ii == 0:
                    cv_xys[0] = -cv_xy
                    cv_xys[1] =  cv_xy
                    cv_xzs[0] = -cv_xz
                    cv_xzs[1] =  cv_xz
                    cv_yzs[0] = -cv_yz
                    cv_yzs[1] =  cv_yz
                else:
                    # Only update colourbar if changed by more than 50%
                    if True:#not(0.5 <= cv_xy/(cv_xys[1]) <= 1.5):
                        cv_xys[0] = -cv_xy
                        cv_xys[1] =  cv_xy
                    if True:#not(0.5 <= cv_xz/(cv_xzs[1]) <= 1.5):
                        cv_xzs[0] = -cv_xz
                        cv_xzs[1] =  cv_xz
                    if True:#not(0.5 <= cv_yz/(cv_yzs[1]) <= 1.5):
                        cv_yzs[0] = -cv_yz
                        cv_yzs[1] =  cv_yz

            else:

                mn_xy = np.min(np.ravel(plt_xy))
                mn_xz = np.min(np.ravel(plt_xz))
                mn_yz = np.min(np.ravel(plt_yz))

                mx_xy = np.max(np.ravel(plt_xy))
                mx_xz = np.max(np.ravel(plt_xz))
                mx_yz = np.max(np.ravel(plt_yz))
                
                if ii == 0:
                    cv_xys[0] = mn_xy
                    cv_xys[1] = mx_xy
                    cv_xzs[0] = mn_xz
                    cv_xzs[1] = mx_xz
                    cv_yzs[0] = mn_yz
                    cv_yzs[1] = mx_yz
                else:
                    # Only update colourbar if changed by more than 50%
                    if True:#not(0.5 <= mn_xy/(cv_xys[0]) <= 1.5):
                        cv_xys[0] = mn_yz
                    if True:#not(0.5 <= mx_xy/(cv_xys[1]) <= 1.5):
                        cv_xys[1] = mx_yz
                    if True:#not(0.5 <= mn_xz/(cv_xzs[0]) <= 1.5):
                        cv_xzs[0] = mn_xz
                    if True:#not(0.5 <= mx_xz/(cv_xzs[1]) <= 1.5):
                        cv_xzs[1] = mx_xz
                    if True:#not(0.5 <= mn_yz/(cv_yzs[0]) <= 1.5):
                        cv_yzs[0] = mn_yz
                    if True:#not(0.5 <= mx_yz/(cv_yzs[1]) <= 1.5):
                        cv_yzs[1] = mx_yz

            QM1.set_clim((cv_xys))
            QM2.set_clim((cv_xzs))
            QM3.set_clim((cv_yzs))

            QM1.changed()
            QM2.changed()
            QM3.changed()

            if plot_pert:
                fig_ttl.set_text('{0:s} pert : t = {1:07.3f} days'.format(var,(ii+base_Nt+1)*dt))
            else:
                fig_ttl.set_text('{0:s} : t = {1:07.3f} days'.format(var,(ii+base_Nt+1)*dt))
            plt.draw()
            fig.savefig(frame_name, dpi=dpi)

    if rank == 0:    
        for plot_pert in plot_perts:
            if plot_pert:
                var_figs_pert[var_ind] = '{0:s}{1:s}-%05d_pert.png'.format(fig_prefix,var)
            else:
                var_figs[var_ind] = '{0:s}{1:s}-%05d.png'.format(fig_prefix,var)

# Have processor 0 wait for the others
if num_procs > 1:
    if rank > 0:
        isdone = True
        comm.send(isdone, dest=0, tag=rank)
        print('Processor {0:d} done.'.format(rank))
    elif rank == 0:
        isdone = False
        for proc in range(1,num_procs):
            isdone = comm.recv(source=proc,tag=proc)

# Now that the individual files have been written, we need to parse them into a movie.
if rank == 0:
    for var_ind in range(len(plt_var)):
        var = plt_var[var_ind]
        for plot_pert in plot_perts:
            if plot_pert:
                in_name  = var_figs_pert[var_ind]
                out_name = '{0:s}{1:s}_pert.{2:s}'.format(out_prefix,var,out_suffix)
            else:
                in_name  = var_figs[var_ind]
                out_name = '{0:s}{1:s}.{2:s}'.format(out_prefix,var,out_suffix)
            cmd = ['ffmpeg', '-framerate', str(mov_fps), '-r', str(mov_fps),
                '-i', in_name, '-y', '-q', '1', '-pix_fmt', 'yuv420p', out_name]
            subprocess.call(cmd)

if rank == 0:
    print('--------')
    if frames_direct == '':
        print('Deleting directory of intermediate frames.')
        shutil.rmtree(tmp_dir)
    print('Video creation complete.')
    print('Processor {0:d} done.'.format(rank))


