import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np
import spinspy as spy
import matpy as mp
import os
import scipy.interpolate as spi
from scipy.fftpack import fft, fftfreq
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

params = spy.get_params()
diags = spy.get_diagnostics()
TE = diags.KE + diags.APE

# Get time units
deltaT = diags.time.max() - diags.time.min()
if (deltaT/86400. > 4):
    time_units = ' (day)'
    time = diags.time/86400.
elif (deltaT/3600. > 4):
    time_units = ' (hour)'
    time = diags.time/3600.
elif (deltaT/60. > 4):
    time_units = ' (min)'
    time = diags.time/60.
else:
    time_units = ' (sec)'
    time = diags.time.copy()

smoothing = True

# The diagnostics are (in order):
# time, iteration, norm_u, norm_v, norm_w, del_b, 
# KE, APE, BPE, step_time, KE_aniso, PE_aniso, 
# rms_div_adv, rms_zeta_z, rms_lap_p, 
# ener_weighted_cyclo, ener_weighted_geo

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

def moving_average(x,y,window):
    out = np.zeros(y.shape)
    Nt, = y.shape
    for ii in range(Nt):
        out[ii,] = np.mean(y[abs(x - x[ii]) <= window], axis=0)
    return out

# Plot norms
plt.figure()
sp1 = plt.subplot(2,1,1)
plt.plot(time, diags.norm_u, label='$||u||_2$')
plt.plot(time, diags.norm_v, label='$||v||_2$')
plt.axis('tight')
plt.legend(loc='best')

plt.subplot(2,1,2)
sp2 = plt.plot(time, diags.norm_w)
plt.ylabel('$||w||_2$')
plt.xlabel('time' + time_units)
plt.axis('tight')

plt.tight_layout(True)
plt.savefig('{0:s}/vel_norms.pdf'.format(prefix))
plt.close()

if False:
    # Frequency-analyse the w norms
    # this should diagnose the wave-field
    # Need to interpolate to uniform time first
    f_int = spi.interp1d(time, diags.norm_w)
    new_t = np.arange(time[0],time[-1],np.mean(time[1:]-time[:-1]))
    new_w = f_int(new_t)
    Nt = len(new_t)
    w_hat = fft(new_w, n=Nt)
    omega = fftfreq(Nt,d=new_t[1]-new_t[0])
    plt.figure()
    plt.plot(omega[:Nt//2], abs(w_hat)[:Nt//2], label='$\\hat{||w||}$')
    plt.axis('tight')
    fr_f0 = 86400*params.rot_f/(2*np.pi)
    fr_N0 = 86400*params.N0/(2*np.pi)
    plt.plot([fr_f0, fr_f0], plt.gca().get_ylim(), label='$f_0$')
    plt.plot([fr_N0, fr_N0], plt.gca().get_ylim(), label='$N_0$')
    plt.yscale('log')
    plt.xscale('log')
    plt.axis('tight')
    plt.legend(loc='best')
    plt.xlabel('Frequency (per ' + time_units[2:])
    plt.tight_layout(True)
    plt.savefig('{0:s}/w_norm_hat.pdf'.format(prefix))
    plt.close()

# Plot energetics
plt.figure()
plt.plot(time, diags.KE, label='KE')
plt.plot(time, diags.APE, label='APE')
plt.plot(time, TE/2., label='$\\frac{1}{2}$TE')
plt.legend(loc='best')
plt.xlabel('time' + time_units)
plt.ylabel('Joules')
plt.axis('tight')
plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 4))
leg1 = plt.gca().legend(bbox_to_anchor=(0., -0.185, 1., .08), 
        loc='upper left', borderaxespad=0., ncol = 3, mode='expand')
plt.tight_layout(rect=[0,0.075,1,0.98], pad=1)
plt.savefig('{0:s}/Dimensional_energetics.pdf'.format(prefix))
plt.close()


if (TE[0] > 0):
    plt.figure()
    plt.plot(time, diags.KE/(TE[0]), label='KE/TE$_0$')
    plt.plot(time, diags.APE/(TE[0]), label='APE/TE$_0$')
    plt.plot(time, TE/(2.*TE[0]), label='$\\frac{1}{2}$TE/TE$_0$')
    #plt.legend(loc='best')
    leg1 = plt.gca().legend(bbox_to_anchor=(0., -0.185, 1., .08), 
            loc='upper left', borderaxespad=0., ncol = 3, mode='expand')
    plt.xlabel('time' + time_units)
    plt.ylabel('Proportion of Initial Total Energy')
    plt.axis('tight')
    plt.ylim(0,1)
    plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 4))
    plt.tight_layout(rect=[0,0.075,1,0.98], pad=1)
    plt.savefig('{0:s}/Nondimensional_energetics.pdf'.format(prefix))
    plt.close()
else:
    print('Cannot plot nondimensional energetics, TE0 = 0.')

if (TE[0] > 0):
    plt.figure()
    plt.plot(time, (TE - TE[0])/(TE[0]))
    plt.xlabel('time' + time_units)
    plt.ylabel('Relative change in total energy')
    plt.axis('tight')
    plt.gca().xaxis.get_major_formatter().set_powerlimits((0, 4))
    plt.tight_layout(True)
    plt.savefig('{0:s}/Relative_energy_change.pdf'.format(prefix))
    plt.close()
else:
    print('Cannot plot relative energy change, TE0 = 0.')

# Plot RMS of flux terms
try:
    plt.figure()
    plt.plot(time, diags.rms_div_adv, label='$\\nabla_H\\cdot(\\vec{u}_H\\cdot\\nabla_H\\vec{u}_H)$')
    plt.plot(time, diags.rms_zeta_z, label='$f\\zeta^z$')
    plt.plot(time, diags.rms_lap_p, label='$\\rho_0^{-1}\\nabla^2p$')
    plt.yscale('log')
    plt.legend(loc='best')
    plt.xlabel('time' + time_units)
    plt.ylabel('Root Mean Squared')
    plt.axis('tight')
    plt.ylim(plt.gca().get_ylim()[0]*0.9, plt.gca().get_ylim()[1]*1.1)
    plt.tight_layout(True)
    plt.savefig('{0:s}/RMS_comparison.pdf'.format(prefix))
    plt.close()

    total_rms = diags.rms_div_adv + diags.rms_zeta_z + diags.rms_lap_p
    plt.figure()
    plt.plot(time, diags.rms_div_adv/total_rms, label='$\\nabla_H\\cdot(\\vec{u}_H\\cdot\\nabla_H\\vec{u}_H)$')
    plt.plot(time, diags.rms_zeta_z/total_rms, label='$f\\zeta^z$')
    plt.plot(time, diags.rms_lap_p/total_rms, label='$\\rho_0^{-1}\\nabla^2_Hp$')
    plt.legend(loc='best')
    plt.xlabel('time' + time_units)
    plt.ylabel('Relative Root Mean Squared')
    plt.axis('tight')
    plt.ylim(0,1)
    plt.tight_layout(True)
    plt.savefig('{0:s}/RMS_normalized.pdf'.format(prefix))
    plt.close()
except:
    print('Failed to plot component RMS')

# Plot energy-weighted epsila
try:
    plt.figure()
    plt.plot(time, diags.ener_weighted_cyclo, label='Cyclostrophic')
    plt.plot(time, diags.ener_weighted_geo,   label='Geostrophic')
    plt.legend(loc='best')
    plt.xlabel('time' + time_units)
    plt.ylabel('Epsilon Balance')
    plt.yscale('log')
    plt.axis('tight')
    m1 = np.percentile(diags.ener_weighted_cyclo,95)
    m2 = np.percentile(diags.ener_weighted_geo,95)
    tmp = plt.gca().get_ylim()
    plt.ylim(0,min(tmp[1],1.2*max(m1,m2)))
    plt.tight_layout(True)
    plt.savefig('{0:s}/Balance_epsila.pdf'.format(prefix))
    plt.close()
except:
    print('Failed to plot balance epsila.')

# Plot time per time-step
try:
    plt.figure()
    ax1 = host_subplot(111, axes_class=AA.Axes)
    plt.subplots_adjust(right=0.75)
    ax1 = plt.gca()

    dt = diags.time[1:] - diags.time[:-1]
    step_time = diags.step_time[1:] - diags.step_time[:-1]

    iters = diags.iteration[1:] - diags.iteration[:-1]
    dt = dt/iters
    step_time = step_time/iters

    ax1.plot(time[1:], step_time, '.r', markersize=1)
    ax1.set_ylabel('Physical Seconds per time-step')
    ax1.set_xlabel('Time' + time_units)
    ax1.set_ylim((0, ax1.get_ylim()[1]))

    ax2 = ax1.twinx()
    ax2.plot(time[1:], dt/step_time, '.b', markersize=1)
    ax2.set_ylabel('Simulated seconds per physical second')
    ax2.set_ylim((0, ax2.get_ylim()[1]))

    ax3 = ax1.twinx()
    ax3.plot(time[1:], dt, '.g', markersize=1)
    ax3.set_ylabel('Simulated seconds per time step')
    ax3.set_ylim((0, ax3.get_ylim()[1]))

    offset = 60
    new_fixed_axis = ax3.get_grid_helper().new_fixed_axis
    ax3.axis["right"] = new_fixed_axis(loc="right", axes=ax3, offset=(offset, 0))
    ax3.axis["right"].toggle(all=True)

    ax1.axis["left"].label.set_color('r')
    ax2.axis["right"].label.set_color('b')
    ax3.axis["right"].label.set_color('g')

    ax1.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
    ax2.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
    ax3.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))

    plt.savefig('Diagnostics/ComputationTime.pdf')

    plt.close()
except:
    print('Failed to plot computation time.')



if ( hasattr(diags, 'b_pert_norm')):
    if smoothing:
        Nt = time.shape[0]
        newt  = np.linspace(time[0], time[-1], Nt//4)

        bpi   = spi.interp1d(time,diags.b_pert_norm,kind='slinear')
        b_pert_norm = bpi(newt)

        if ( hasattr(diags, 'norm_b')):
            bnorm  = spi.interp1d(time,diags.norm_b,kind='slinear')
            b_norm = bnorm(newt)

        b_pert_norm = moving_average(newt, b_pert_norm, (time[-1] - time[0])/(300.))

        time = newt
        Dt = mp.FiniteDiff(time,4,spb=True,uniform=True,Periodic=False)
        print('After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,time.shape[0]))
    else:
        b_pert_norm = diag.b_pert_norm
        Dt = mp.FiniteDiff(time,4,spb=True,uniform=False,Periodic=False)
        if ( hasattr(diags, 'norm_b')) :
            b_norm  = diags.norm_b

    gr    = Dt.dot(np.log(b_pert_norm))
    gr    = moving_average(time, gr, (time[-1] - time[0])/300.)

    plt.figure()
    ax1 = plt.gca()
    l1, = ax1.plot(time, b_pert_norm, 'r',label='$||b-B||_2$')
    ax1.set_xlabel('$t$' + time_units)
    ax1.set_yscale('log')
    plt.axis('tight')

    ax2 = ax1.twinx()
    if ( hasattr(params, 'Ro')):
        l2, = ax2.plot(time, gr/np.abs(params.Ro), 'b',
                label='$\\frac{1}{Ro}\\cdot\\frac{d}{dt}(\\log(||b-B||_2))$')
    else:
        l2, = ax2.plot(time, gr, 'b', label='$\\frac{d}{dt}(\\log(||b-B||_2))$')
    plt.axis('tight')
    plt.axis('tight')
    plt.ylim(0, min(2,plt.gca().get_ylim()[1]))

    leg1 = ax1.legend(bbox_to_anchor=(0., -0.15, 1., .08), loc='upper left', borderaxespad=0.)
    leg2 = ax2.legend(bbox_to_anchor=(0., -0.15, 1., .08), loc='upper right', borderaxespad=0.)

    ax1.xaxis.get_major_formatter().set_powerlimits((0, 4))

    plt.tight_layout(rect=[0,0.05,1,0.98], pad=1)
    plt.savefig('{0:s}/b_pert_norm.pdf'.format(prefix))
    plt.close()

    # Plot normalized perturbation norm
    if ( hasattr(diags, 'norm_b') ):
        # Won't work yet, don't have b_norm output
        bb = spy.reader(params.bg_b_filename,0,force_name=True)
        bb_norm = np.sqrt(np.sum(np.ravel(bb)**2))*(params.Lx*params.Ly*params.Lz)/(params.Nx*params.Ny*params.Nz)

        plt.figure()
        ax1 = plt.gca()
        l1, = ax1.plot(time, b_pert_norm/(b_norm + bb_norm),'r',label='$\\frac{||b - B||_2}{||b||_2 + ||B||_2}$')
        ax1.set_xlabel('$t$' + time_units)
        ax1.set_yscale('log')
        plt.axis('tight')
    
        ax2 = ax1.twinx()
        if ( hasattr(params, 'Ro')):
            l2, = ax2.plot(newt, gr/np.abs(params.Ro), 'b', label='$\\frac{1}{Ro}\\cdot\\frac{d}{dt}(\\log(||b-B||_2))$')
            cvp = np.percentile(gr/np.abs(params.Ro),99)*1.1
            cvm = max(0, np.percentile(gr/np.abs(params.Ro),1)*0.9)
        else:
            l2, = ax2.plot(newt, gr, 'b', label='$\\frac{d}{dt}(\\log(||b-B||_2))$')
            cvp = np.percentile(gr,99)*1.1
            cvm = max(0, np.percentile(gr,1)*0.9)
        plt.axis('tight')
        tmp = plt.gca().get_ylim()
        plt.ylim(cvm,cvp)
        ax2.grid(True)

        leg1 = ax1.legend(bbox_to_anchor=(0., -0.15, 1., .08), loc='upper left', borderaxespad=0.)
        leg2 = ax2.legend(bbox_to_anchor=(0., -0.15, 1., .08), loc='upper right', borderaxespad=0.)
        
        ax1.xaxis.get_major_formatter().set_powerlimits((0, 4))
    
        plt.tight_layout(rect=[0,0.05,1,0.98], pad=1)
        plt.savefig('{0:s}/b_pert_norm_normalized.pdf'.format(prefix))
        plt.close()

