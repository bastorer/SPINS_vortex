import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import spinspy as spy
import matpy as mp
from scipy.fftpack import fftfreq
import scipy.interpolate as spi

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

smoothing = True

# Load and define some things
diags = spy.get_diagnostics()
params = spy.get_params()

# Get time units
deltaT = diags.time.max() - diags.time.min()
if (deltaT/86400. > 4):
    time_units = ' (day)'
    t = diags.time/86400.
elif (deltaT/3600. > 4):
    time_units = ' (hour)'
    t = diags.time/3600.
elif (deltaT/60. > 4):
    time_units = ' (min)'
    t = diags.time/60.
else:
    time_units = ' (sec)'
    t = diags.time.copy()

Nx = params.Nx
Lx = params.Lx
Ny = params.Ny
Ly = params.Ly
Nz = params.Nz
Lz = params.Lz

vol = Lx*Ly*Lz/(Nx*Ny*Nz)

print('Processing anisotropy')

N = min(params.Nx,params.Ny)//2
d = min(params.Lx/params.Nx, params.Ly/params.Ny)

kcut = params.f_cutoff/(2.*d)
k = fftfreq(2*N,d=d)[:N]
dk = k[1] - k[0]

KK = np.zeros(N+1)
KK[:-1] = k
KK[-1] = k[-1] + dk
KK[0] = dk/2. # Don't actually go to zero, for log scales

aniso_KE = np.fromfile('KE_aniso.bin')
aniso_PE = np.fromfile('PE_aniso.bin')

dat_KE = np.fromfile('KE_1d_spect.bin')
dat_PE = np.fromfile('PE_1d_spect.bin')

Nt = dat_KE.shape[0]//N

orig_time = t.copy()

# SPINS returns depth-integrated spectra
aniso_KE = aniso_KE.reshape((Nt,N))
aniso_PE = aniso_PE.reshape((Nt,N))

dat_KE = dat_KE.reshape((Nt,N))*vol/(Nx*Ny)
dat_PE = dat_PE.reshape((Nt,N))*vol/(Nx*Ny)

# Wavenumber-integrated anisotropy
aniso_KE_int = np.sum(aniso_KE*dat_KE, axis=1)/np.sum(dat_KE, axis=1)
aniso_PE_int = np.sum(aniso_PE*dat_PE, axis=1)/np.sum(dat_PE, axis=1)

# Compute flux
Dt = mp.FiniteDiff(t, 8, spb=True, uniform=False)
flux_KE = Dt.dot(aniso_KE)
flux_PE = Dt.dot(aniso_PE)

# Try a bit of smoothing
if smoothing:
    newt = np.linspace(t[0],t[-1],Nt//10)
    NewNt = newt.shape[0]

    #Dt = mp.FiniteDiff(newt,4,spb=True,uniform=True)

    proj = spi.interp1d(t, aniso_KE, kind='slinear',axis=0)
    aniso_KE = proj(newt)

    proj = spi.interp1d(t, aniso_PE, kind='slinear',axis=0)
    aniso_PE = proj(newt)

    proj = spi.interp1d(t, flux_KE, kind='slinear',axis=0)
    flux_KE = proj(newt)

    proj = spi.interp1d(t, flux_PE, kind='slinear',axis=0)
    flux_PE = proj(newt)

    t = newt
    print('   After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,t.shape[0]))
    Nt = t.shape[0]
else:
    #Dt = mp.FiniteDiff(t,8,spb=True,uniform=False)
    pass

TT = np.zeros(Nt+1)
TT[1:] = t
TT[0] = 0.

#flux_KE = Dt.dot(aniso_KE)
#flux_PE = Dt.dot(aniso_PE)

# Now save everything that we computed
np.savez('Diagnostics/anisotropy_diagnostics.npz', \
        KK=KK, TT=TT, kcut=kcut, k=k, t=orig_time,\
        flux_PE = flux_PE,   flux_KE = flux_KE,\
        aniso_PE = aniso_PE, aniso_KE = aniso_KE,\
        aniso_PE_int = aniso_PE_int, aniso_KE_int = aniso_KE_int,
        time_units = time_units)

