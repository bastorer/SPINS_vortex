COL='\033[1;37m'
NC='\033[0m' # No Color
#echo -e "I ${COL}love${NC} Stack Overflow"

echo -e "${COL}Processing diagnostics.txt${NC}"
python "${SPINS_SCRIPTS_PE}/process_diagnostics.py" 
echo -e "${COL}    done processing diagnostics.txt${NC}"

if [[ -e KE_1d_spect.bin  ]]; then
    echo -e "${COL}Processing spectra${NC}"
    python "${SPINS_SCRIPTS_PE}/process_spectra.py"
    echo -e "${COL}    done processing spectra${NC}"

    echo -e "${COL}Plotting spectra${NC}"
    python "${SPINS_SCRIPTS_PE}/plot_spectra.py"
    echo -e "${COL}    done plotting spectra${NC}"
else
    echo -e "${COL}Skipping spectra${NC}"
fi

if [[ -e KE_aniso.bin  ]]; then
    echo -e "${COL}Processing anisotropy${NC}"
    python "${SPINS_SCRIPTS_PE}/process_aniso.py"
    echo -e "${COL}    done processing anisotropy${NC}"

    echo -e "${COL}Plotting anisotropy${NC}"
    python "${SPINS_SCRIPTS_PE}/plot_aniso.py"
    echo -e "${COL}    done plotting anisotropy${NC}"
else
    echo -e "${COL}Skipping anisotropy${NC}"
fi

# Determine number of processors for movie making
if [ -z "${SLURM_NTASKS+x}" ]; then 
    echo "SLURM_NTASKS is not set. Likely not using salloc. Will only use one processor for movie production."; 
    npc=1;
else 
    npc=${SLURM_NTASKS};
    echo "Will use ${npc} processors for movie production."
fi


echo -e "${COL}Making movies${NC}"
mpirun -n ${npc} python "${SPINS_SCRIPTS_PE}/make_movies.py" 
echo -e "${COL}    done making movies${NC}"

