#!/bin/bash
#SBATCH --nodes=1               # number of MPI processes
#SBATCH --ntasks-per-node=32
#SBATCH --mem=128000M   # memory; default unit is megabytes
#SBATCH --time=06-12:00          # time (DD-HH:MM)
#SBATCH --mail-user=bastorer@uwaterloo.ca
#SBATCH --job-name=AC_Bu5_Ro0p1_Alp2b25
#SBATCH --output=sim-%J.log
#SBATCH --account=ctb-mmstastn
#SBATCH -e sim-%j.err

# Update job summary with initial information
updateJobSummary

# Update spins.conf with accruate run-time information
updateSPINSruntime

# run spins
mpiexec ./vortex_reader.x

# Update the job summary with the final time
updateJobSummary
