#!/bin/bash
# bash script for submitting a job to the sharcnet queue
# PPN = processors per node
# MPP = memory per processor

#### Options ####
QUEUE=kglamb
NUM_PROCS=32
MPP=500M
RUNTIME=2.5h
DELAY=false
DELAY_FOR=
NAME=Matt_test
EXE_NAME=vortex_reader.x

#### OTHER INFO ####
DATE=`date +%Y-%m-%d_%H\h%M`
LOG_NAME="${DATE}.log"
ERR_NAME="${DATE}.err"

#### Submit ####
if [[ -x $EXE_NAME ]]; then
    if [[ "$DELAY" = false ]]; then
        sqsub -q ${QUEUE} \
              -f mpi --nompirun \
              -n ${NUM_PROCS} \
              --mpp=${MPP} \
              -r ${RUNTIME} \
              -j ${NAME} \
              -o ${LOG_NAME} \
              -e ${ERR_NAME} \
              mpirun -mca mpi_leave_pinned 0 ${EXE_NAME}
    elif [[ "$DELAY" = true ]]; then
        sqsub -q ${QUEUE} \
              -f mpi --nompirun \
              -n ${NUM_PROCS} \
              --mpp=${MPP} \
              -r ${RUNTIME} \
              -j ${NAME} \
              -o ${LOG_NAME} \
              -e ${ERR_NAME} \
              -w ${DELAY_FOR} \
              mpirun -mca mpi_leave_pinned 0 ${EXE_NAME}
    fi
    echo "Submitted ${EXE_NAME} with ${NUM_PROCS} processors"
    echo "          Requested memory:  ${MPP}"
    echo "          Requested runtime: ${RUNTIME}"
    echo "          Log (stdout) file: ${LOG_NAME}"
    echo "          Err (stderr) file: ${ERR_NAME}"
else
    echo "Couldn't find $EXE_NAME - Try again."
fi
