import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import spinspy as spy
import matpy as mp
import os, cmocean, sys
from scipy.fftpack import fftfreq
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Load stuff
prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

aniso = np.load('Diagnostics/anisotropy_diagnostics.npz')
aniso_KE = aniso['aniso_KE']
aniso_PE = aniso['aniso_PE']

aniso_KE_int = aniso['aniso_KE_int']
aniso_PE_int = aniso['aniso_PE_int']

aniso_KE_flux = aniso['flux_KE']
aniso_PE_flux = aniso['flux_PE']

TT   = aniso['TT']
KK   = aniso['KK']
time = aniso['t']
k    = aniso['k']
kcut = aniso['kcut']

spect = np.load('Diagnostics/spectral_diagnostics_1d.npz')
dat_KE = spect['dat_KE']
dat_PE = spect['dat_PE']

## Wavenumber-integrated anisotropy 
plt.figure()
ax1 = plt.subplot(1,1,1)
ax2 = ax1.twinx()

ax1.plot(time, aniso_KE_int, 'r')
ax2.plot(time, aniso_PE_int, 'b')

ax1.set_ylabel('KE Anisotropy')
ax2.set_ylabel('PE Anisotropy')

ax1.spines['left'].set_color('red')
ax1.spines['right'].set_color('blue')
ax2.spines['left'].set_color('red')
ax2.spines['right'].set_color('blue')
ax1.yaxis.label.set_color('red')
ax2.yaxis.label.set_color('blue')
ax1.tick_params(axis='y', colors='red')
ax2.tick_params(axis='y', colors='blue')

ax1.set_xlabel('Time' + str(aniso['time_units']))
ax2.set_xlabel('Time' + str(aniso['time_units']))
plt.axis('tight')
plt.tight_layout(True)
plt.savefig('Diagnostics/Aniso.pdf')
plt.close()

# Lines of wavenumber-dependent anisotropy

plt.figure()
ax1 = plt.subplot(2,1,1)
ax2 = plt.subplot(2,1,2)

cm_subsection = np.linspace(0., 1., 11.) 
colors = [ mp.darkjet(III) for III in cm_subsection ]
pnum = 0;

tmpk = k
tmpk[0] = (k[1]-k[0])//2

for ii in range(0,len(TT)-1,int(np.floor((len(TT)-1)//10))):
        
    if (ii == 0):
        ii = 1

    ax1.plot(tmpk, aniso_KE[ii,:], color=colors[pnum])
    ax2.plot(tmpk, aniso_PE[ii,:], color=colors[pnum])

    pnum += 1


ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.axis('tight')
ax1.plot([kcut, kcut], ax1.get_ylim(), 'k')
ax1.grid(True)

ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.axis('tight')
ax2.set_xlim(ax1.get_xlim())
ax2.plot([kcut, kcut], ax2.get_ylim(), 'k')
ax2.grid(True)

ax1.set_ylabel('KE anisotropy')
ax2.set_ylabel('PE anisotropy')
ax2.set_xlabel('Inverse Wavelength')

plt.tight_layout(True)
plt.savefig('Diagnostics/Aniso_lines.pdf')


# Plot KE
plt.figure(figsize=(6,10))
ax1 = plt.subplot(1,1,1)

cv = np.log10(np.max(aniso_KE.ravel()))

vmin = np.min(aniso_KE.ravel())
vmax = min(10, np.max(aniso_KE.ravel()))
q = plt.pcolormesh(KK, TT, aniso_KE, cmap=cmocean.cm.dense, vmin=vmin, vmax=vmax, linewidth=0, rasterized=True)
q.set_edgecolor('face')

cbar = plt.colorbar(q, pad = 0.15)
cbar.solids.set_rasterized(True)
cbar.solids.set_edgecolor("face")
ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

plt.xscale('log')
plt.axis('tight')
plt.xlabel('Inverse Lengthscale')
plt.ylabel('Time' + str(aniso['time_units']))
plt.title('KE Anisotropy')
plt.plot([kcut, kcut], ax1.get_ylim(), 'k')

plt.tight_layout(True)
plt.savefig('Diagnostics/KE_anisotropy.pdf')
plt.close()

# Plot PE
plt.figure(figsize=(6,10))
ax1 = plt.subplot(1,1,1)

cv = np.log10(np.max(aniso_PE.ravel()))

vmin = np.min(aniso_PE.ravel())
vmax = min(10, np.max(aniso_PE.ravel()))
q = plt.pcolormesh(KK, TT, aniso_PE, cmap=cmocean.cm.dense, vmin=vmin, vmax=vmax, linewidth=0, rasterized=True)
q.set_edgecolor('face')

cbar = plt.colorbar(q, pad = 0.15)
cbar.solids.set_rasterized(True)
cbar.solids.set_edgecolor("face")
ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

plt.xscale('log')
plt.axis('tight')
plt.xlabel('Inverse Lengthscale')
plt.ylabel('Time' + str(aniso['time_units']))
plt.title('PE Anisotropy')
plt.plot([kcut, kcut], ax1.get_ylim(), 'k')

plt.tight_layout(True)
plt.savefig('Diagnostics/PE_anisotropy.pdf')
plt.close()

### PE Aniso Delta
fig = plt.figure(figsize=(6,10))
ax1 = plt.subplot(1,1,1)

#cv = min(0.1, np.abs(np.max(aniso_PE_flux)))
cv = min(1, np.percentile(np.max(aniso_PE_flux), 80))
q = plt.pcolormesh(KK, TT, aniso_PE_flux, vmin=-cv, vmax=cv, cmap=cmocean.cm.balance)
q.set_edgecolor('face')
cbar = plt.colorbar(q, pad = 0.15)
cbar.solids.set_rasterized(True)
cbar.solids.set_edgecolor("face")

ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

ax1.set_xscale('log')
ax1.axis('tight')
ax1.set_xlabel('Inverse Lengthscale')
ax1.set_ylabel('Time' + str(aniso['time_units']))
ax1.set_title('$\\frac{d}{dt}$PE Anisotropy (per ' + str(aniso['time_units']) + ')')
ax1.plot([kcut, kcut], ax1.get_ylim(), 'k')

plt.tight_layout(True)
plt.savefig('Diagnostics/PE_anisotropy_delta.png', dpi=750)
plt.close()


### KE Aniso Delta
fig = plt.figure(figsize=(6,10))
ax1 = plt.subplot(1,1,1)

#cv = min(0.1, np.abs(np.max(aniso_KE_flux)))
cv = min(1, np.percentile(np.max(aniso_KE_flux), 80))
q = plt.pcolormesh(KK, TT, aniso_KE_flux, vmin=-cv, vmax=cv, cmap=cmocean.cm.balance)
q.set_edgecolor('face')
cbar = plt.colorbar(q, pad = 0.15)
cbar.solids.set_rasterized(True)
cbar.solids.set_edgecolor("face")

ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

ax1.set_xscale('log')
ax1.axis('tight')
ax1.set_xlabel('Inverse Lengthscale')
ax1.set_ylabel('Time (days)')
ax1.set_title('$\\frac{d}{dt}$KE Anisotropy (per day)')
ax1.plot([kcut, kcut], ax1.get_ylim(), 'k')

plt.tight_layout(True)
plt.savefig('Diagnostics/KE_anisotropy_delta.png', dpi=750)
plt.close()
