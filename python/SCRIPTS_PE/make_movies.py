import matplotlib
matplotlib.use('Agg')
import matplotlib.font_manager
matplotlib.font_manager._rebuild()
import matpy as mp
import numpy as np
import cmocean
import spinspy as spy
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import sys, os, shutil, tempfile
import subprocess, glob
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm, Normalize


try: # Try using mpi
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_procs = comm.Get_size()
except:
    rank = 0
    num_procs = 1

## USER CHANGE THIS SECTION
out_direct    = os.getcwd() + '/Videos'           # Where to put the movies
the_name      = 'results_'    # What to call the movies
out_suffix    = 'mp4'           # Movie type
mov_fps       = 10              # Framerate for movie
pert_prefix   = 'bg_'               # Prefix for basic states. e.g. pret_prefix + 'u.0'
div_cmap      = cmocean.cm.balance  # Which divergent colourmap to use
non_div_cmap  = cmocean.cm.deep     # Which non-divergent colourmap to use
frames_direct = ''
dpi           = 250
##

# Determine which fields exist
vs = glob.glob('*.1')
plt_var = [v[:-2] for v in vs]

plot_perts_l = [[False]]*len(plt_var)
for ii in range(len(plt_var)):
    if os.path.isfile(pert_prefix + plt_var[ii] + '.0'):
        plot_perts_l[ii] = [False,True]

if (rank == 0):
    pstr = 'Producing videos for: '
    for ii in range(len(plt_var)):
        if plot_perts_l[ii] == [False,True]:
            pstr += plt_var[ii] + '(full,pert) '
        else:
            pstr += plt_var[ii] + '(full) '
    print(pstr)

# If the the out_directory doesn't exist, create it
if (rank == 0) and not(os.path.exists(out_direct)):
    os.makedirs(out_direct)

# Load some information
dat = spy.get_params()
x,y,z = spy.get_grid()

# Indices for the three frames
indx1  = [0,-1] 
indy1  = [0,-1] 
indz1  = [int(0.6*dat.Nz)]
title1 = '40\% Depth'

indx2  = [0,-1] 
indy2  = [0,-1]
indz2  = [int(0.5*dat.Nz)]
title2 = '50\% Depth'

indx3  = [0,-1]
indy3  = [int(0.5*dat.Ny)]
indz3  = [0,-1]
title3 = 'Half y'

##

## USER SHOULDN'T NEED TO CHANGE ANYTHING AFTER THIS
def Determine_Grids(indx,indy,indz,x,y,z):
    
    if len(indx) > 1:
        gx = x
        xlabel = 'x'
        if len(indy) > 1:
            gy = y
            ylabel = 'y'
        else:
            gy = z
            ylabel = 'z'
    else:
        gx = y
        xlabel = 'y'
        gy = z
        ylabel = 'z'

    # Determine scale
    if gx.max() - gx.min() > 5e3:
        xlabel += ' (km)'
        gx = gx*1e-3
    elif gx.max() - gx.min() < 1e-2:
        xlabel += ' (mm)'
        gx = gx*1e3
    elif gx.max() - gx.min() < 2:
        xlabel += ' (cm)'
        gx = gx*1e2
    else:
        xlabel += ' (m)'

    if gy.max() - gy.min() > 5e3:
        ylabel += ' (km)'
        gy = gy*1e-3
    elif gy.max() - gy.min() < 1e-2:
        ylabel += ' (mm)'
        gy = gy*1e3
    elif gy.max() - gy.min() < 2:
        ylabel += ' (cm)'
        gy = gy*1e2
    else:
        ylabel += ' (m)'

    # Compute grids for plotting
    dx = gx[1] - gx[0]
    grx = np.zeros(len(gx)+1)
    grx[1:] = gx + dx/2
    grx[0]  = gx[0] - dx/2

    dy = gy[1] - gy[0]
    gry = np.zeros(len(gy)+1)
    gry[1:] = gy + dy/2
    gry[0]  = gy[0] - dy/2

    # Aspect ratio
    aspect_ratio = (gx.max() - gx.min())/(gy.max() - gy.min())

    return grx, gry, xlabel, ylabel, aspect_ratio

# Determine time scales
TF = dat.final_time
DT = dat.plot_interval
if hasattr(dat, 'plot_write_ratio'):
    DT *= dat.plot_write_ratio
if (TF/86400. > 4):
    time_units = ' (day)'
    dt = DT/86400.
elif (TF/3600. > 4):
    time_units = ' (hour)'
    dt = DT/3600.
elif (TF/60. > 4):
    time_units = ' (min)'
    dt = DT/60.
else:
    time_units = ' (sec)'
    dt = DT

# Prepare directories
if rank == 0:
    print('Video files will be saved in {0:s}'.format(out_direct))
    if frames_direct == '':
        tmp_dir = tempfile.mkdtemp(dir=out_direct)
    else:
        tmp_dir = out_direct + '/' + frames_direct
        if not(os.path.exists(tmp_dir)):
            os.makedirs(tmp_dir)
    fig_prefix = tmp_dir + '/' + the_name # path for saving frames
    out_prefix = out_direct + '/' + the_name # path for saving videos
    for proc in range(1,num_procs):
        comm.send(tmp_dir,dest=proc,tag=1)
        comm.send(fig_prefix,dest=proc,tag=2)
        comm.send(out_prefix,dest=proc,tag=3)
else:
    tmp_dir = comm.recv(source=0,tag=1)
    fig_prefix = comm.recv(source=0,tag=2)
    out_prefix = comm.recv(source=0,tag=3)

# Determine which frames are already done
if not(frames_direct == ''):
    list_of_frames_already_done = glob.glob(fig_prefix + '*')
else:
    list_of_frames_already_done = []

if rank == 0:
    print(list_of_frames_already_done)

# Initialize the three meshes
fig = plt.figure(figsize=(12,8))

sp1 = plt.subplot(1,3,1)
sp2 = plt.subplot(1,3,2)
sp3 = plt.subplot(1,3,3)

sps = [sp1,sp2,sp3]

for sp in sps: # Modify positions to make room for colourbar ticklabels.
    pos1 = sp.get_position()
    pos2 = [pos1.x0, pos1.y0 + 0.2*pos1.height, pos1.width, pos1.height*0.8] 
    sp.set_position(pos2)

# Initial pcolormesh objects
gridx1, gridy1, xlab1, ylab1, ar1 = Determine_Grids(indx1, indy1, indz1, x, y, z)
gridx2, gridy2, xlab2, ylab2, ar2 = Determine_Grids(indx2, indy2, indz2, x, y, z)
gridx3, gridy3, xlab3, ylab3, ar3 = Determine_Grids(indx3, indy3, indz3, x, y, z)

QM1 = sp1.pcolormesh(gridx1, gridy1, np.ones((len(gridy1)-1, len(gridx1)-1)))
QM2 = sp2.pcolormesh(gridx2, gridy2, np.ones((len(gridy2)-1, len(gridx2)-1)))
QM3 = sp3.pcolormesh(gridx3, gridy3, np.ones((len(gridy3)-1, len(gridx3)-1)))
QMs = [QM1,QM2,QM3]

sp1.set_title(title1)
sp1.set_xlabel(xlab1)
sp1.set_ylabel(ylab1)

sp2.set_title(title2)
sp2.set_xlabel(xlab2)
sp2.set_ylabel(ylab2)

sp3.set_title(title3)
sp3.set_xlabel(xlab3)
sp3.set_ylabel(ylab3)

for sp in sps:
    sp.locator_params(axis='both', nbins=6)

# Initialize colorbars
div1 = make_axes_locatable(sp1)
div2 = make_axes_locatable(sp2)
div3 = make_axes_locatable(sp3)

cax1 = div1.append_axes("bottom", size="5%", pad=0.5)
cax2 = div2.append_axes("bottom", size="5%", pad=0.5)
cax3 = div3.append_axes("bottom", size="5%", pad=0.5)

cbar1 = plt.colorbar(QM1, cax=cax1, format="%.2e", orientation='horizontal')
cbar2 = plt.colorbar(QM2, cax=cax2, format="%.2e", orientation='horizontal')
cbar3 = plt.colorbar(QM3, cax=cax3, format="%.2e", orientation='horizontal')

tick_locator = matplotlib.ticker.MaxNLocator(nbins=7)
cbar1.locator = tick_locator
cbar2.locator = tick_locator
cbar3.locator = tick_locator

cax1.yaxis.set_visible(False)
cax1.set_xticklabels(cax1.get_xticklabels(),rotation=90,ha='center')
cax2.yaxis.set_visible(False)
cax2.set_xticklabels(cax2.get_xticklabels(),rotation=90,ha='center')
cax3.yaxis.set_visible(False)
cax3.set_xticklabels(cax3.get_xticklabels(),rotation=90,ha='center')

# If the aspect ratios isn't too extreme, use true aspect ratio
if (0.5 <= ar1 <= 2.):
    sp1.set_aspect('equal')
if (0.5 <= ar2 <= 2.):
    sp2.set_aspect('equal')
if (0.5 <= ar3 <= 2.):
    sp3.set_aspect('equal')
for sp in sps:
    sp.autoscale(tight=True)

var_figs = ['']*len(plt_var)
var_figs_pert = ['']*len(plt_var)
fig_ttl = fig.suptitle('Title', fontsize=16)
fig.set_tight_layout({'rect':[0,0,1,0.97]})

# Start at var0 and keep going until we run out.
for var_ind in range(len(plt_var)):
    var = plt_var[var_ind]
    plot_perts = plot_perts_l[var_ind]

    ii = rank # parallel, so start where necessary
    cont = True

    # Use a log scale for the balance epsila
    #if (var == 'eps_qg') or (var == 'eps_cyclo'):
    #    QM1.set_norm(LogNorm(vmin=1e-3, vmax=1))
    #    QM2.set_norm(LogNorm(vmin=1e-3, vmax=1))
    #    QM3.set_norm(LogNorm(vmin=1e-3, vmax=1))
    #else:
    #    QM1.set_norm(Normalize())
    #    QM2.set_norm(Normalize())
    #    QM3.set_norm(Normalize())


    # Load background state if needed
    if np.any(plot_perts):
        bg_xy = spy.reader(pert_prefix+var,0,indx1,indy1,indz1,ordering='matlab')
        bg_xz = spy.reader(pert_prefix+var,0,indx2,indy2,indz2,ordering='matlab')
        bg_yz = spy.reader(pert_prefix+var,0,indx3,indy3,indz3,ordering='matlab')
    else:
        bg_xy = 0.
        bg_xz = 0.
        bg_yz = 0.

    while cont:

        try:
            var_xy = spy.reader(var,ii,indx1,indy1,indz1,ordering='matlab')
            var_xz = spy.reader(var,ii,indx2,indy2,indz2,ordering='matlab')
            var_yz = spy.reader(var,ii,indx3,indy3,indz3,ordering='matlab')

            for plot_pert in plot_perts:

                # Determine which colourmap to use
                # When plotting perturbations or velocities, use
                # a divergent colourmap and centre the colourbar
                if plot_pert or var == 'u' or var == 'v' or var == 'w' or var == 'b':
                    cmap = div_cmap
                    centre_cbar = True
                else:
                    cmap = non_div_cmap
                    centre_cbar = False 

                if (isinstance(cmap, str)):
                    QM1.cmap = getattr(matplotlib.cm,cmap)
                    QM2.cmap = getattr(matplotlib.cm,cmap)
                    QM3.cmap = getattr(matplotlib.cm,cmap)
                else:
                    QM1.cmap = cmap
                    QM2.cmap = cmap
                    QM3.cmap = cmap
                cbar1.update_normal(QM1)
                cbar2.update_normal(QM2)
                cbar3.update_normal(QM3)

                if plot_pert:
                    frame_name = '{0:s}{1:s}-{2:05d}_pert.png'.format(fig_prefix,var,ii)
                else:
                    frame_name = '{0:s}{1:s}-{2:05d}.png'.format(fig_prefix,var,ii)

                if ii % 1 == 0:
                    print('Processor {0:d} accessing {1:s}.{2:d}'.format(rank,var,ii) + ' pert'*plot_pert)

                plt_xy = var_xy - plot_pert*bg_xy
                plt_xz = var_xz - plot_pert*bg_xz
                plt_yz = var_yz - plot_pert*bg_yz

                QM1.set_array(np.ravel(plt_xy))
                QM2.set_array(np.ravel(plt_xz))
                QM3.set_array(np.ravel(plt_yz))

                if centre_cbar:

                    cv_xy = np.max(np.abs(np.ravel(plt_xy)))
                    cv_xz = np.max(np.abs(np.ravel(plt_xz)))
                    cv_yz = np.max(np.abs(np.ravel(plt_yz)))

                    if cv_xy == 0:
                        cv_xy = 1
                    if cv_xz == 0:
                        cv_xz = 1
                    if cv_yz == 0:
                        cv_yz = 1
            
                    QM1.set_clim((-cv_xy,cv_xy))
                    QM2.set_clim((-cv_xz,cv_xz))
                    QM3.set_clim((-cv_yz,cv_yz))

                else:

                    mn_xy = np.min(np.ravel(plt_xy))
                    mn_xz = np.min(np.ravel(plt_xz))
                    mn_yz = np.min(np.ravel(plt_yz))

                    mx_xy = np.max(np.ravel(plt_xy))
                    mx_xz = np.max(np.ravel(plt_xz))
                    mx_yz = np.max(np.ravel(plt_yz))
            
                    QM1.set_clim((mn_xy,mx_xy))
                    QM2.set_clim((mn_xz,mx_xz))
                    QM3.set_clim((mn_yz,mx_yz))

                QM1.changed()
                QM2.changed()
                QM3.changed()

                if plot_pert:
                    #fig_ttl.set_text('{0:s} pert : t = {1:.3g} days'.format(var.replace('_', ' '),ii*dt))
                    fig_ttl.set_text('{0:s} pert : t = {1:.3g}'.format(var.replace('_', ' '),ii*dt) + time_units)
                else:
                    #fig_ttl.set_text('{0:s} : t = {1:.3g} days'.format(var.replace('_', ' '),ii*dt))
                    fig_ttl.set_text('{0:s} : t = {1:.3g}'.format(var.replace('_', ' '),ii*dt) + time_units)
                plt.draw()
                fig.savefig(frame_name, dpi=dpi)

            ii += num_procs # Parallel, so skip a `bunch'
        except:
            cont = False
            # We don't know which one failed, so assume they both did.
            for plot_pert in plot_perts:
                if plot_pert:
                    var_figs_pert[var_ind] = '{0:s}{1:s}-%05d_pert.png'.format(fig_prefix,var)
                else:
                    var_figs[var_ind] = '{0:s}{1:s}-%05d.png'.format(fig_prefix,var)

# Have processor 0 wait for the others
if num_procs > 1:
    if rank > 0:
        isdone = True
        comm.send(isdone, dest=0, tag=rank)
        print('Processor {0:d} done.'.format(rank))
    elif rank == 0:
        isdone = False
        for proc in range(1,num_procs):
            isdone = comm.recv(source=proc,tag=proc)

# Now that the individual files have been written, we need to parse them into a movie.
if rank == 0:
    for var_ind in range(len(plt_var)):
        var = plt_var[var_ind]
        plot_perts = plot_perts_l[var_ind]
        for plot_pert in plot_perts:
            if plot_pert:
                in_name  = var_figs_pert[var_ind]
                out_name = '{0:s}{1:s}_pert.{2:s}'.format(out_prefix,var,out_suffix)
            else:
                in_name  = var_figs[var_ind]
                out_name = '{0:s}{1:s}.{2:s}'.format(out_prefix,var,out_suffix)
            cmd = ['ffmpeg', '-framerate', str(mov_fps), '-r', str(mov_fps),
                '-i', in_name, '-y', '-q', '1', '-pix_fmt', 'yuv420p', 
                '-vf', 'scale=-1:600', out_name]
            #cmd = ['ffmpeg', '-framerate', str(mov_fps), '-r', str(mov_fps),
            #    '-i', in_name, '-y', '-vcodec', 'libx264',
            #    '-pix_fmt', 'yuv420p', out_name]
            subprocess.call(cmd)

if rank == 0:
    print('--------')
    if frames_direct == '':
        print('Deleting directory of intermediate frames.')
        shutil.rmtree(tmp_dir)
    print('Video creation complete.')
    print('Processor {0:d} done.'.format(rank))


