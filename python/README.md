# Welcome!
This directory contains the python tools necessary for processing SPINS simulations.
These were originally developed by Ben Storer (bastorer@uwaterloo.ca).

If you make contributions to the python scripts, please add yourself to the authors list above.

This readme was last modified on the 26th of September, 2017.

## How To
In order to use the python scripts, you need to have SPINSpy installed on your python path.
To do, simply add the SPINSpy directory to your PYTHONPATH environment variable. This can be done
by adding the following line to your ~/.bashrc or equivalent file.
  **EXPORT PYTHONPATH=${PYTHONPATH}:"<path/to/spins>/python/SPINSpy"**

## SPINSpy

SPINSpy includes its own documentation, but in brief, it's a set of python scripts that have been
designed to parse SPINS outputs. They were designed with python 2 in mind, and may not work with python 3.
If you run into compatability problems, please email me (bastorer@uwaterloo.ca), I'd be more than happy to help.

## About the scripts
There are two groups of scripts, which can be found in SCRIPTS_QG and SCRIPTS_PE.
The former contains scripts relevant to the Quasi-Geostrophic version of SPINS (SPINS QG), 
while the latter contains scripts relevant to base spins (PE = primitive equations)

The scripts were designed to work with the qg_reader and vortex_reader casefiles respectively,
but they can be modified to work with any casefile.

The following is a brief description of the scripts that are available.
In each case, the corresponding bash script *Postprocessing.sh* automates running all of the post-processing scripts.
The bash script *clear_simulation.sh* removes all simulation outputs from the current directory.

To conveniently run these scripts from your simulation directories, create environment variables pointing to the script directories. 
For example, in your ~/.bashrc, use
  **export SPINS_SCRIPT_PE="</path/to/spins>/python/SCRIPTS_PE"**
  **export SPINS_SCRIPT_QG="</path/to/spins>/python/SCRIPTS_QG"**
Then you can call the scripts via (for example)
  **python $SPINS_SCRIPT_PE/process_diagnostics.py**
or 
  **$SPINS_SCRIPT_PE/Postprocessing.sh**

### QG SCRIPTS
The outputs from all of the below are placed in the directory **Diagnostics**, which is automatically created as a subdirectory of the working directory.
- *process_diagnostics.py*: parses the diagnostics.txt file and produces a collection of line-plots from the data
- *process_spectra.py*: parses the various .bin spectral diagnostics and produces numpy .npz files
- *plot_spectra.py*: produces line and pcolor plots from the .npz spectra files. The plots include power spectra and time derivative of the spectra
- *process_aniso.py*: parses the .bin files for the anisotropy outputs and produces numpy .npz files
- *plot_aniso.py*: produces anisotropy plots from the .npz anisotropy files. The plots include integrated anisotropy and wavenumber-dependent anisotropy
- *plot_mean_fields.py*: in the case of 2D simulations, produces a pcolor of the zonal mean fields as a function of time.

The outputs from all of the below are placed in the directory **Videos**, which is automatically created as a subdirectory of the working directory.
Further, these scripts have been parallelized with mpi. To run in parallel, simply call 'mpirun -n # python scriptname.py', 
where # is the number of processers and scriptname.py is the appropriate script filename.
All video files default as mp4. Framerates and dpi can be specified in the python scripts.
- *make_movies.py*: produces movies for 3D simulations. The produced videos present three 2D slices, which can be specified in the script.
- *make_movies_2d.py*: produces movies for 2D simulations.

### PE SCRIPTS
The outputs from all of the below are placed in the directory **Diagnostics**, which is automatically created as a subdirectory of the working directory.
- *process_diagnostics.py*: parses the diagnostics.txt file and produces a collection of line-plots from the data
- *process_spectra.py*: parses the various .bin spectral diagnostics and produces numpy .npz files
- *plot_spectra.py*: produces line and pcolor plots from the .npz spectra files. The plots include power spectra and time derivative of the spectra
- *process_aniso.py*: parses the .bin files for the anisotropy outputs and produces numpy .npz files
- *plot_aniso.py*: produces anisotropy plots from the .npz anisotropy files. The plots include integrated anisotropy and wavenumber-dependent anisotropy

The outputs from all of the below are placed in the directory **Videos**, which is automatically created as a subdirectory of the working directory.
Further, these scripts have been parallelized with mpi. To run in parallel, simply call 'mpirun -n # python scriptname.py', 
where # is the number of processers and scriptname.py is the appropriate script filename.
All video files default as mp4. Framerates and dpi can be specified in the python scripts.
- *make_movies.py*: produces movies for 3D simulations. The produced videos present three 2D slices, which can be specified in the script.
- *make_movies_2d.py*: produces movies for 2D simulations.

## Tips and Tricks

### matplotlibrc
To improve figure outputs, take some time to set your ~/.config/matplotlib/matplotlibrc file. As an example, I use:

    backend : Agg
    lines.linewidth : 2.0
    font.size : 11
    font.family : serif
    font.sans-serif  : Helvetica, Avant Garde, Computer Modern Sans serif
    text.usetex : True
    axes.linewidth : 2.0
    axes.formatter.limits : -3, 3
    legend.fancybox : True

### latex warnings
Depending on your system, you may get a distressingly large number of warnings about latex outputs (in particular, ComputeCanada systems
seem rather prone to doing this). This can be solved by setting the environment variable KMP_AFFINITY to disabled, via:
export KMP_AFFINITY=disabled
