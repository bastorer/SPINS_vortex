import matplotlib
matplotlib.use('Agg')
import numpy as np
import scipy.interpolate as spi
import spinspy as spy
import matpy as mp
import matplotlib.pyplot as plt
import os
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

smoothing = True # smoothing for the derivatives
params = spy.get_params()
diags = spy.get_diagnostics()

# Get time units
deltaT = diags.time.max() - diags.time.min()
if (deltaT/86400. > 4):
    time_units = ' (day)'
    time = diags.time/86400.
elif (deltaT/3600. > 4):
    time_units = ' (hour)'
    time = diags.time/3600.
elif (deltaT/60. > 4):
    time_units = ' (min)'
    time = diags.time/60.
else:
    time_units = ' (sec)'
    time = diags.time.copy()

TE = diags.KE + diags.PE

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

##
## Plot energetics
##
#with plt.figure() as fig:
try:
    fig = plt.figure()
    ax = fig.add_axes([0.15, 0.2, 0.7, 0.7])
    ax.plot(time[1:], diags.KE[1:], label='KE')
    ax.plot(time[1:], diags.PE[1:], label='PE')
    ax.plot(time[1:], TE[1:]/2., label='$\\frac{1}{2}$TE')
    ax.set_xlabel('time' + time_units)
    ax.set_ylabel('Joules')
    ax.autoscale(tight=True)

    # Legend
    handles, labels = ax.get_legend_handles_labels()
    leg_ax = fig.add_axes([0.15, 0.05, 0.7, 0.1])
    leg_ax.legend(handles, labels, bbox_to_anchor=(0., 0., 1., 1.), ncol=3, mode='expand',
                    frameon = True, borderaxespad=0.)
    leg_ax.set_xticks([])
    leg_ax.set_yticks([])
    for pos in ['left', 'right', 'top', 'bottom']:
        leg_ax.spines[pos].set_visible(False)

    plt.savefig('{0:s}/Dimensional_energetics.pdf'.format(prefix))
    plt.close()
except:
    pass

# Normalized energetics
if TE[1] > 0:
    fig = plt.figure()
    ax = fig.add_axes([0.15, 0.2, 0.7, 0.7])
    ax.plot(time[1:], diags.KE[1:]/(TE[1]), label='KE/TE$_0$')
    ax.plot(time[1:], diags.PE[1:]/(TE[1]), label='PE/TE$_0$')
    ax.plot(time[1:], TE[1:]/(2.*TE[1]), label='$\\frac{1}{2}$TE/TE$_0$')
    ax.set_xlabel('time' + time_units)
    ax.set_ylabel('Proportion of Initial Total Energy')
    ax.axis('tight')
    ax.set_ylim(-0.01,1.01)

    # Legend
    handles, labels = ax.get_legend_handles_labels()
    leg_ax = fig.add_axes([0.15, 0.05, 0.7, 0.1])
    leg_ax.legend(handles, labels, bbox_to_anchor=(0., 0., 1., 1.), ncol=3, mode='expand',
                    frameon = True, borderaxespad=0.)
    leg_ax.set_xticks([])
    leg_ax.set_yticks([])
    for pos in ['left', 'right', 'top', 'bottom']:
        leg_ax.spines[pos].set_visible(False)

    plt.savefig('{0:s}/Nondimensional_energetics.pdf'.format(prefix))
    plt.close()

    plt.figure()
    dTE = TE[1:]/TE[1] - 1.
    plt.plot(time[1:][dTE>0], dTE[dTE>0], '.k')
    plt.plot(time[1:][dTE<0], -dTE[dTE<0], '.r')
    plt.xlabel('time' + time_units)
    plt.ylabel('Relative change in total energy')
    plt.yscale('log')
    plt.axis('tight')
    plt.tight_layout(True)
    plt.savefig('{0:s}/Relative_energy_change.pdf'.format(prefix))
    plt.close()


# Plot time per time-step
plt.figure()
ax1 = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

dt = diags.time[1:] - diags.time[:-1]
step_time = diags.step_time[1:] - diags.step_time[:-1]

iters = diags.iteration[1:] - diags.iteration[:-1]
dt = dt/iters
step_time = step_time/iters

ax1.plot(diags.time[1:]/86400., step_time, '.r', markersize=1)
ax1.set_ylabel('Physical Seconds per time-step')
ax1.set_xlabel('Time (days)')
ax1.set_ylim((0, np.percentile(step_time,99)))

ax2 = ax1.twinx()
ax2.plot(diags.time[1:]/86400., dt/step_time, '.b', markersize=1)
ax2.set_ylabel('Simulated seconds per physical second')
ax2.set_ylim((0, np.percentile(dt/step_time,99)))

ax3 = ax1.twinx()
ax3.plot(diags.time[1:]/86400, dt, '.g', markersize=1)
ax3.set_ylabel('Simulated seconds per time step')
ax3.set_ylim((0, np.percentile(dt,99)))

offset = 60
new_fixed_axis = ax3.get_grid_helper().new_fixed_axis
ax3.axis["right"] = new_fixed_axis(loc="right", axes=ax3, offset=(offset, 0))
ax3.axis["right"].toggle(all=True)

ax1.axis["left"].label.set_color('r')
ax2.axis["right"].label.set_color('b')
ax3.axis["right"].label.set_color('g')

ax1.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
ax2.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
ax3.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))

plt.savefig('Diagnostics/ComputationTime.pdf')

plt.close()
