import numpy as np
import spinspy as spy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import cmocean
import sys, os

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)
dpi = 250

params = spy.get_params()
diags  = spy.get_diagnostics()
x,y    = spy.get_grid()
t      = diags.time/86400.
Ny     = params.Ny

x = x/1e3
y = y/1e3

Y = np.zeros((Ny+1))
dy = y[1] - y[0]
Y[1:] = y + dy/2.
Y[0]  = y[0] - dy/2.

ubar   = np.fromfile('ubar.bin')
qbar   = np.fromfile('qbar.bin')
psibar = np.fromfile('psibar.bin')
Nt = ubar.shape[0]//Ny

T = np.zeros((Nt+1))
T[1:] = t
T[0]  = t[0] - 0.5*(t[1] - t[0]) 

ubar   = ubar.reshape((Nt,Ny))
qbar   = qbar.reshape((Nt,Ny))
psibar = psibar.reshape((Nt,Ny))

# Plot psibar
plt.figure()
ax1 = plt.subplot2grid((1,3), (0, 0), colspan=2)
ax2 = plt.subplot2grid((1,3), (0, 2), colspan=1)
ax1.tick_params(axis='both', which='major')
ax2.tick_params(axis='both', which='major')

# Since only the gradient of psi matters, set it to zero at mid-y
psibar = psibar - np.tile(psibar[:,Ny//2].reshape((Nt,1)), (1,Ny))

ax1.pcolormesh(T, Y, psibar.T, vmin = psibar.min(), vmax = psibar.max(),
        cmap=cmocean.cm.haline, linewidth=0, rasterized=True)
#cax = ax1.contourf(t, y, psibar.T, 128, vmin = psibar.min(), vmax = psibar.max(),
#        cmap=cmocean.cm.haline)
#for c in cax.collections:
#        c.set_edgecolor("face")
ax1.set_xlim((t[0],t[-1]))
ax1.set_ylim((Y[0],Y[-1]))
ax1.set_xlabel('time (days)')
ax1.set_ylabel('Meridional Coordinate (km)')

psimean = np.mean(psibar.ravel())
ax2.plot([psimean, psimean], [y[0], y[-1]], '--k')
ax2.plot(np.mean(psibar, axis=0), y)
ax2.set_xlabel('Streamfuntion $(m^2/s)$')
ax2.set_title('Time-mean psibar')
plt.setp(ax2.get_xticklabels(), rotation='vertical')
ax2.set_ylim(ax1.get_ylim())

plt.tight_layout(True)
plt.savefig('Diagnostics/psibar.pdf', dpi=dpi)
plt.close()

# Plot ubar
plt.figure()
ax1 = plt.subplot2grid((1,3), (0, 0), colspan=2)
ax1.tick_params(axis='both', which='major')

cv = np.max(np.abs(ubar.ravel()))
cax = ax1.pcolormesh(T, Y, ubar.T, vmin = -cv, vmax = cv, cmap=cmocean.cm.balance,
        linewidth=0, rasterized=True)
#cax = ax1.contourf(t, y, ubar.T, 128, vmin = ubar.min(), vmax = ubar.max(),
#        cmap=cmocean.cm.balance)
#for c in cax.collections:
#        c.set_edgecolor("face")
ax1.set_xlim((t[0],t[-1]))
ax1.set_ylim((Y[0],Y[-1]))
ax1.set_xlabel('time (days)')
ax1.set_ylabel('Meridional Coordinate (km)')
cbar = plt.colorbar(cax)
cbar.solids.set_edgecolor("face")

ax2 = plt.subplot2grid((1,3), (0, 2), colspan=1)
ax2.tick_params(axis='both', which='major')
ax2.plot(0*y, y, '--k')
ax2.plot(np.mean(ubar, axis=0), y)
ax2.set_xlabel('Velocity (m/s)')
ax2.set_title('Time-mean ubar')
ax2.set_ylim(ax1.get_ylim())
plt.setp(ax2.get_xticklabels(), rotation='vertical')

plt.tight_layout(True)
plt.savefig('Diagnostics/ubar.pdf', dpi=dpi)
plt.close()

# Plot qbar
plt.figure()
ax1 = plt.subplot2grid((1,3), (0, 0), colspan=2)
ax1.tick_params(axis='both', which='major')

cv = np.max(np.abs(qbar.ravel()))
cax = ax1.pcolormesh(T, Y, qbar.T, vmin = -cv, vmax = cv, cmap=cmocean.cm.balance,
        linewidth=0, rasterized=True)
#cax = ax1.contourf(t, y, qbar.T, 128, vmin = qbar.min(), vmax = qbar.max(),
#        cmap=cmocean.cm.balance)
#for c in cax.collections:
#        c.set_edgecolor("face")
ax1.set_xlim((t[0],t[-1]))
ax1.set_ylim((Y[0],Y[-1]))
ax1.set_xlabel('time (days)')
ax1.set_ylabel('Meridional Coordinate (km)')
cbar = plt.colorbar(cax)
cbar.solids.set_edgecolor("face")

ax2 = plt.subplot2grid((1,3), (0, 2), colspan=1)
ax2.tick_params(axis='both', which='major')
ax2.plot(0*y, y, '--k')
ax2.plot(np.mean(qbar, axis=0), y)
ax2.set_xlabel('PV (1/s)')
ax2.set_title('Time-mean qbar')
ax2.set_ylim(ax1.get_ylim())
plt.setp(ax2.get_xticklabels(), rotation='vertical')

plt.tight_layout(True)
plt.savefig('Diagnostics/qbar.pdf', dpi=dpi)
plt.close()
