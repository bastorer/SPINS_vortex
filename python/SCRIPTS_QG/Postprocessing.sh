COL='\033[1;37m'
NC='\033[0m' # No Color
#echo -e "I ${COL}love${NC} Stack Overflow"

echo -e "${COL}Processing diagnostics.txt${NC}"
python "${SPINS_SCRIPTS_QG}/process_diagnostics.py" #> Postprocessing.log
echo -e "${COL}    done processing diagnostics.txt${NC}"

if [[ -e qbar.bin  ]]; then
    echo -e "${COL}Plotting mean fields${NC}"
    python "${SPINS_SCRIPTS_QG}/plot_mean_fields.py" #> Postprocessing.log
    echo -e "${COL}    done plottig mean fields${NC}"
else
    echo -e "${COL}Skipping plots of mean fields${NC}"
fi


if [[ -e KE_1d_spect.bin  ]]; then
    echo -e "${COL}Processing spectra${NC}"
    python "${SPINS_SCRIPTS_QG}/process_spectra.py" #> Postprocessing.log
    echo -e "${COL}    done processing spectra${NC}"

    echo -e "${COL}Plotting spectra${NC}"
    python "${SPINS_SCRIPTS_QG}/plot_spectra.py" #> Postprocessing.log
    echo -e "${COL}    done plotting spectra${NC}"
else
    echo -e "${COL}Skipping spectra${NC}"
fi


if [[ -e KE_aniso.bin  ]]; then
    echo -e "${COL}Processing anisotropy${NC}"
    python "${SPINS_SCRIPTS_QG}/process_aniso.py" #> Postprocessing.log
    echo -e "${COL}    done processing anisotropy${NC}"

    echo -e "${COL}Plotting anisotropy${NC}"
    python "${SPINS_SCRIPTS_QG}/plot_aniso.py" #> Postprocessing.log
    echo -e "${COL}    done plotting anisotropy${NC}"
else
    echo -e "${COL}Skipping anisotropy${NC}"
fi

# Determine whether to call 2D or 3D movie maker.
Nz_flag=`grep Nz spins.conf`
count=0
for s in $Nz_flag
do
    if [ "$count" -gt "1" ]; then
        Nz=$((s + 0));
    else
        count=$((count + 1));
    fi 
done

# Determine number of processors for movie making
if [ -z "${SLURM_NTASKS+x}" ]; then 
    echo "SLURM_NTASKS is not set. Likely not using salloc. Will only use one processor for movie production."; 
    npc=1;
else 
    npc=${SLURM_NTASKS};
    echo "Will use ${npc} processors for movie production."
fi

if [ $Nz -gt "1" ]; then
    echo -e "${COL}Making movies (3D)${NC}"
    mpirun -n ${npc} python "${SPINS_SCRIPTS_QG}/make_movies.py" #> Postprocessing.log
else
    echo -e "${COL}Making movies (2D)${NC}"
    mpirun -n ${npc} python "${SPINS_SCRIPTS_QG}/make_movies_2d.py" #> Postprocessing.log
fi
echo -e "${COL}    done making movies${NC}"

