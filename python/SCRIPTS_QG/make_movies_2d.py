import matplotlib
matplotlib.use('Agg')
import matpy as mp
import numpy as np
import spinspy as spy
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import sys, os, shutil, tempfile
import subprocess, glob
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cmocean

try: # Try using mpi
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    num_procs = comm.Get_size()
except:
    rank = 0
    num_procs = 1

## USER CHANGE THIS SECTION
out_direct    = os.getcwd() + '/Videos'           # Where to put the movies
the_name      = 'results_'    # What to call the movies
out_suffix    = 'mp4'           # Movie type
mov_fps       = 10              # Framerate for movie
div_cmap      = cmocean.cm.balance      # Which divergent colourmap to use
nondiv_cmap   = cmocean.cm.deep
frames_direct = ''
dpi           = 300
##

# Determine which fields exist
# BAS: KE?
dat = spy.get_params()
vs = glob.glob('*.1')
plt_var = [v[:-2] for v in vs]

plot_perts_l = [[False]]*len(plt_var)
for ii in range(len(plt_var)):
    if hasattr(dat, plt_var[ii]+'b_file'):
        bg_name = getattr(dat, plt_var[ii]+'b_file')
        if os.path.isfile(bg_name):
            plot_perts_l[ii] = [False,True]

if (rank == 0):
    pstr = 'Producing videos for: '
    for ii in range(len(plt_var)):
        if plot_perts_l[ii] == [False,True]:
            pstr += plt_var[ii] + '(full,pert) '
        else:
            pstr += plt_var[ii] + '(full) '
    print(pstr)

# If the the out_directory doesn't exist, create it
if (rank == 0) and not(os.path.exists(out_direct)):
    os.makedirs(out_direct)

# Load some information
xt,yt = spy.get_grid()

aspect_ratio = (xt.max() - xt.min())/(yt.max() - yt.min())
xlabel = 'x'
ylabel = 'y'

# Determine scale
if xt.max() - xt.min() > 5e3:
    xlabel += ' (km)'
    xt = xt*1e-3
elif xt.max() - xt.min() < 1e-2:
    xlabel += ' (mm)'
    xt = xt*1e3
elif xt.max() - xt.min() < 2:
    xlabel += ' (cm)'
    xt = xt*1e2
else:
    xlabel += ' (m)'

if yt.max() - yt.min() > 5e3:
    ylabel += ' (km)'
    yt = yt*1e-3
elif yt.max() - yt.min() < 1e-2:
    ylabel += ' (mm)'
    yt = yt*1e3
elif yt.max() - yt.min() < 2:
    ylabel += ' (cm)'
    yt = yt*1e2
else:
    ylabel += ' (m)'

dx = xt[1] - xt[0]
dy = yt[1] - yt[0]

x = np.zeros(len(xt)+1)
x[1:] = xt + dx/2.
x[0] = xt[0] - dx/2.

y = np.zeros(len(yt)+1)
y[1:] = yt + dy/2.
y[0] = yt[0] - dy/2.

dt = dat.tplot/86400.
method = dat.method

## USER SHOULDN'T NEED TO CHANGE ANYTHING AFTER THIS

# Prepare directories
if rank == 0:
    print('Video files will be saved in {0:s}'.format(out_direct))
    if frames_direct == '':
        tmp_dir = tempfile.mkdtemp(dir=out_direct)
    else:
        tmp_dir = out_direct + '/' + frames_direct
        if not(os.path.exists(tmp_dir)):
            os.makedirs(tmp_dir)
    fig_prefix = tmp_dir + '/' + the_name # path for saving frames
    out_prefix = out_direct + '/' + the_name # path for saving videos
    for proc in range(1,num_procs):
        comm.send(tmp_dir,dest=proc,tag=1)
        comm.send(fig_prefix,dest=proc,tag=2)
        comm.send(out_prefix,dest=proc,tag=3)
else:
    tmp_dir = comm.recv(source=0,tag=1)
    fig_prefix = comm.recv(source=0,tag=2)
    out_prefix = comm.recv(source=0,tag=3)

# Determine which frames are already done
if not(frames_direct == ''):
    list_of_frames_already_done = glob.glob(fig_prefix + '*')
else:
    list_of_frames_already_done = []

if rank == 0:
    print(list_of_frames_already_done)

# Initialize the three meshes
fig_rat = max( 0.25, min(dat.Ly/dat.Lx, 4))
if fig_rat >= 1:
    fig = plt.figure(figsize=(10,10*fig_rat))
else:
    fig = plt.figure(figsize=(10./fig_rat,10))

sp = plt.subplot(1,1,1)

pos1 = sp.get_position()
pos2 = [pos1.x0, pos1.y0 + 0.2*pos1.height, pos1.width, pos1.height*0.8] 
sp.set_position(pos2)

# Initial pcolormesh objects
QM1 = sp.pcolormesh(x,y,np.zeros((len(y)-1,len(x)-1)))

sp.axis('tight')
plt.axes().set_aspect('equal', 'datalim')
sp.locator_params(axis='both', nbins=6)

sp.set_xlabel(xlabel)
sp.set_ylabel(ylabel)

# Initialize colorbars
div1 = make_axes_locatable(sp)
cax1 = div1.append_axes("bottom", size="5%", pad=0.5)
cbar1 = plt.colorbar(QM1, cax=cax1, format="%.2e", orientation='horizontal')

tick_locator = matplotlib.ticker.MaxNLocator(nbins=7)
cbar1.locator = tick_locator

cax1.yaxis.set_visible(False)
cax1.set_xticklabels(cax1.get_xticklabels(),rotation=90,ha='center')

var_figs = ['']*len(plt_var)
var_figs_pert = ['']*len(plt_var)
fig_ttl = fig.suptitle('Title', fontsize=16)
fig.set_tight_layout({'rect':[0,0,1,0.97]})

# Start at var0 and keep going until we run out.
for var_ind in range(len(plt_var)):
    var = plt_var[var_ind]
    plot_perts = plot_perts_l[var_ind]

    ii = rank # parallel, so start where necessary
    cont = True

    # Load background state if needed
    if (np.any(plot_perts) and method == 'nonlinear') or (np.any(map(lambda x: not(x),plot_perts)) and method != 'nonlinear'):
        if (var == 'KE'):
            bg_uname = getattr(dat, 'ub_file')
            bg_vname = getattr(dat, 'vb_file')

            tmp1 = spy.reader(bg_uname,0,indx1,indy1,indz1,ordering='matlab',force_name=True)
            tmp2 = spy.reader(bg_vname,0,indx1,indy1,indz1,ordering='matlab',force_name=True)
            bg_xy = tmp1**2 + tmp2**2
        else:    
            bg_name = getattr(dat, var+'b_file')
            bg_xy = spy.reader(bg_name,0,ordering='matlab',force_name=True)
    else:
        bg_xy = 0.

    while cont:

        if True: 
            try:
                if (var == 'KE'):
                    tmp1 = spy.reader('u',ii,ordering='matlab')
                    tmp2 = spy.reader('v',ii,ordering='matlab')
                    var_xy = tmp1**2 + tmp2**2
                elif (var == 'PE'):
                    tmp1 = spy.reader('psi',ii,ordering='matlab')
                    var_xy = tmp1**2
                elif (var == 'psi'):
                    var_xy = spy.reader('psi',ii,ordering='matlab')
                    tmp1 = np.mean(var_xy)
                    #var_xy = var_xy - tmp1
                else:
                    var_xy = spy.reader(var,ii,ordering='matlab')

                for plot_pert in plot_perts:

                    if (((var == 'psi') and (plot_pert == False)) or (var == 'KE') or (var == 'PE')):
                        cmap = nondiv_cmap
                        centre_cbar = False
                    else:
                        cmap = div_cmap
                        centre_cbar = True

                    if (isinstance(cmap, str)):
                        QM1.cmap = getattr(matplotlib.cm,cmap)
                    else:
                        QM1.cmap = cmap

                    if plot_pert:
                        frame_name = '{0:s}{1:s}-{2:05d}_pert.png'.format(fig_prefix,var,ii)
                    else:
                        frame_name = '{0:s}{1:s}-{2:05d}.png'.format(fig_prefix,var,ii)

                    if ii % 1 == 0:
                        print('Processor {0:d} accessing {1:s}.{2:d}'.format(rank,var,ii) + ' pert'*plot_pert)

                    if method == 'nonlinear':
                        plt_xy = var_xy - plot_pert*bg_xy
                    else:
                        plt_xy = var_xy + (1 - plot_pert)*bg_xy

                    QM1.set_array(np.ravel(plt_xy))

                    if centre_cbar:

                        cv_xy = np.max(np.abs(np.ravel(plt_xy)))

                        if cv_xy == 0:
                            cv_xy = 1
                        QM1.set_clim((-cv_xy,cv_xy))

                    else:
                        mn_xy = np.min(np.ravel(plt_xy))

                        mx_xy = np.max(np.ravel(plt_xy))
                
                        QM1.set_clim((mn_xy,mx_xy))

                    if (var == 'eps_qg') or (var == 'eps_cyclo'):
                        QM1.set_clim((0,0.1))

                    QM1.changed()

                    if plot_pert:
                        fig_ttl.set_text('{0:s} pert : t = {1:.3g} days'.format(var,ii*dt))
                    else:
                        fig_ttl.set_text('{0:s} : t = {1:.3g} days'.format(var,ii*dt))
                    plt.draw()
                    if (dpi == 'auto'):
                        fig.savefig(frame_name, dpi=max(len(xt),len(yt))/2)
                    else:
                        fig.savefig(frame_name, dpi=dpi)

                ii += num_procs # Parallel, so skip a `bunch'
            except:
                cont = False
                # We don't know which one failed, so assume they both did.
                for plot_pert in plot_perts:
                    if plot_pert:
                        var_figs_pert[var_ind] = '{0:s}{1:s}-%05d_pert.png'.format(fig_prefix,var)
                    else:
                        var_figs[var_ind] = '{0:s}{1:s}-%05d.png'.format(fig_prefix,var)

# Have processor 0 wait for the others
if num_procs > 1:
    if rank > 0:
        isdone = True
        comm.send(isdone, dest=0, tag=rank)
        print('Processor {0:d} done.'.format(rank))
    elif rank == 0:
        isdone = False
        for proc in range(1,num_procs):
            isdone = comm.recv(source=proc,tag=proc)

# Now that the individual files have been written, we need to parse them into a movie.
if rank == 0:
    for var_ind in range(len(plt_var)):
        var = plt_var[var_ind]
        plot_perts = plot_perts_l[var_ind]
        for plot_pert in plot_perts:
            if plot_pert:
                in_name  = var_figs_pert[var_ind]
                out_name = '{0:s}{1:s}_pert.{2:s}'.format(out_prefix,var,out_suffix)
            else:
                in_name  = var_figs[var_ind]
                out_name = '{0:s}{1:s}.{2:s}'.format(out_prefix,var,out_suffix)
            cmd = ['ffmpeg', '-framerate', str(mov_fps), '-i', in_name, '-y', '-q', '1', '-pix_fmt', 'yuv420p', out_name]
            print(cmd)
            subprocess.call(cmd)

if rank == 0:
    print('--------')
    if frames_direct == '':
        print('Deleting directory of intermediate frames.')
        shutil.rmtree(tmp_dir)
    print('Video creation complete.')
    print('Processor {0:d} done.'.format(rank))


