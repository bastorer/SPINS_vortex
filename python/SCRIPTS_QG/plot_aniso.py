import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import spinspy as spy
import matpy as mp
import os, cmocean, sys
from scipy.fftpack import fftfreq
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Load stuff
prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

aniso = np.load('Diagnostics/anisotropy_diagnostics.npz')
aniso_KE = aniso['aniso_KE']
aniso_PE = aniso['aniso_PE']
aniso_EN = aniso['aniso_EN']

aniso_KE_flux = aniso['flux_KE']
aniso_PE_flux = aniso['flux_PE']
aniso_EN_flux = aniso['flux_EN']

TT   = aniso['TT']
KK   = aniso['KK']
time = aniso['t']
k    = aniso['k']
kcut = aniso['kcut']

Rhines = aniso['Rhines']

spect_data = np.load('Diagnostics/spectral_diagnostics_1d.npz')

# Lines of wavenumber-dependent anisotropy
plt.figure(figsize=(4,6))
ax1 = plt.subplot(3,1,1)
ax2 = plt.subplot(3,1,2)
ax3 = plt.subplot(3,1,3)

cm_subsection = np.linspace(0., 1., 11.) 
colors = [ mp.darkjet(III) for III in cm_subsection ]
pnum = 0;

tmpk = k
tmpk[0] = (k[1]-k[0])/2.

mins = [np.inf, np.inf, np.inf]
maxs = [0, 0, 0]

for ii in range(0,len(TT)-1,int(np.floor((len(TT)-1)//10))):
        
    if (ii == 0):
        ii = 1

    ax1.plot(tmpk[1:], aniso_KE[ii,1:], color=colors[pnum])
    ax2.plot(tmpk[1:], aniso_PE[ii,1:], color=colors[pnum])
    ax3.plot(tmpk[1:], aniso_EN[ii,1:], color=colors[pnum])

    mins[0] = min(mins[0], np.min(aniso_KE[ii,1:][tmpk[1:] < kcut]))
    mins[1] = min(mins[1], np.min(aniso_PE[ii,1:][tmpk[1:] < kcut]))
    mins[2] = min(mins[2], np.min(aniso_EN[ii,1:][tmpk[1:] < kcut]))

    maxs[0] = max(maxs[0], np.max(aniso_KE[ii,1:][tmpk[1:] < kcut]))
    maxs[1] = max(maxs[1], np.max(aniso_PE[ii,1:][tmpk[1:] < kcut]))
    maxs[2] = max(maxs[2], np.max(aniso_EN[ii,1:][tmpk[1:] < kcut]))

    pnum += 1

for ax in [ax1, ax2, ax3]:
    ax.set_xscale('log')
    #ax.set_yscale('log')
    ax.axis('tight')
    ax.set_xlim(tmpk[0], tmpk[-1])
    ax.plot([kcut, kcut], ax.get_ylim(), 'k')
    ax.grid(True)

ax1.set_ylim(mins[0], maxs[0])
ax2.set_ylim(mins[1], maxs[1])
ax3.set_ylim(mins[2], maxs[2])

for ax in [ax1, ax2]:
    ax.set_xticklabels([])

ax1.set_ylabel('KE anisotropy')
ax2.set_ylabel('PE anisotropy')
ax3.set_ylabel('EN anisotropy')
ax3.set_xlabel('Inverse Wavelength')

plt.tight_layout(True)
plt.savefig('Diagnostics/Aniso_lines.pdf')


##
## Plot anisos
##
for aniso in ['KE', 'PE', 'EN']:
    plt.figure(figsize=(6,10))
    ax1 = plt.subplot(1,1,1)

    # Throw away first wavenumber, since anisotropy of mean is meaningless
    if aniso == 'KE':
        tmp_aniso = aniso_KE[:,1:]
    elif aniso == 'PE':
        tmp_aniso = aniso_PE[:,1:]
    elif aniso == 'EN':
        tmp_aniso = aniso_EN[:,1:]
    tmp_k     = k[1:]

    vmin = np.percentile(tmp_aniso[:,tmp_k < kcut], 1)
    if vmin == 0:
        vmin = vmax/1e2
    vmax = np.percentile(tmp_aniso[:,tmp_k < kcut], 99)

    q = plt.pcolormesh(tmp_k, TT, tmp_aniso, cmap=cmocean.cm.tempo, vmin = vmin, vmax = vmax, 
                linewidth=0, rasterized=True)
    q.set_edgecolor('face')

    cbar = plt.colorbar(q, pad = 0.15)
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor("face")
    ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

    # Add contours of field
    cv = np.log10(np.max(np.abs(spect_data['dat_' + aniso])))
    vmin = 10**(np.ceil(cv - 6) - 0.5)
    vmax = 10**cv
    q2 = plt.contour(spect_data['k'], spect_data['TT'][1:], spect_data['dat_' + aniso],
                np.logspace(np.log10(vmin), np.log10(vmax), 5),
                cmap=cmocean.cm.dense, norm=LogNorm(vmin=vmin, vmax=vmax) )

    #cbar2 = plt.colorbar(q2, pad = 0.15)

    plt.xscale('log')
    plt.axis('tight')
    plt.xlim(KK[0], KK[-1])
    plt.xlabel('Inverse Lengthscale')
    plt.ylabel('Time (days)')
    plt.title(aniso + ' Anisotropy')
    plt.plot([kcut, kcut], ax1.get_ylim(), 'y')
    ax1.plot(1./Rhines[Rhines > 0], TT[1:][Rhines > 0], 'm')

    plt.tight_layout(True)
    plt.savefig('Diagnostics/{0}_anisotropy.pdf'.format(aniso))
    plt.close()

##
## Plot aniso delta
##
for aniso in ['KE', 'PE', 'EN']:
    plt.figure(figsize=(6,10))
    ax1 = plt.subplot(1,1,1)

    # Throw away first wavenumber, since anisotropy of mean is meaningless
    if aniso == 'KE':
        tmp_flux = aniso_KE_flux[:,1:]
    elif aniso == 'PE':
        tmp_flux = aniso_PE_flux[:,1:]
    elif aniso == 'EN':
        tmp_flux = aniso_EN_flux[:,1:]
    tmp_k     = k[1:]

    cv = np.percentile(np.abs(tmp_flux[:,tmp_k < kcut]), 99)
    q = plt.pcolormesh(tmp_k, TT, tmp_flux, cmap=cmocean.cm.balance, vmin = -cv, vmax = cv,
            linewidth=0, rasterized=True)
    q.set_edgecolor('face')


    cbar = plt.colorbar(q, pad = 0.15)
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor("face")
    ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

    plt.xscale('log')
    plt.axis('tight')
    plt.xlim(KK[0], KK[-1])
    plt.xlabel('Inverse Lengthscale')
    plt.ylabel('Time (days)')
    ax1.set_title('$\\frac{d}{dt}$' + aniso + ' Anisotropy (per day)')
    plt.plot([kcut, kcut], ax1.get_ylim(), 'c')
    ax1.plot(1./Rhines[Rhines > 0], TT[1:][Rhines > 0], 'm')

    plt.tight_layout(True)
    plt.savefig('Diagnostics/{0}_anisotropy_delta.pdf'.format(aniso), dpi=350)
    plt.close()

