import matplotlib
matplotlib.use('Agg')
import numpy as np
import scipy.interpolate as spi
import spinspy as spy
import matpy as mp
import matplotlib.pyplot as plt
import os
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

smoothing = True # smoothing for the derivatives
params = spy.get_params()
diags = spy.get_diagnostics()

# Get time units
deltaT = diags.time.max() - diags.time.min()
if (deltaT/86400. > 4):
    time_units = ' (day)'
    time = diags.time/86400.
elif (deltaT/3600. > 4):
    time_units = ' (hour)'
    time = diags.time/3600.
elif (deltaT/60. > 4):
    time_units = ' (min)'
    time = diags.time/60.
else:
    time_units = ' (sec)'
    time = diags.time.copy()

TE = diags.KE + diags.APE

# The diagnostics are (in order):
# time, iteration, step_time, max_q,
# q_pert_norm, q_norm, KE, APE

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

# Plot global aniso metrics
try:
    assert(params.compute_aniso != 'False')
    plt.figure()
    ax1 = plt.subplot(1,1,1)

    ax1.plot(time[1:], diags.KE_aniso[1:], label='KE')
    ax1.plot(time[1:], diags.PE_aniso[1:], label='PE')
    ax1.plot(time[1:], diags.EN_aniso[1:], label='Enst.')

    ax1.set_xlabel('Time' + time_units)

    plt.legend(loc='best')

    ax1.axis('tight')
    ax1.set_ylim(0, 1)

    plt.tight_layout(True)
    plt.savefig('Diagnostics/Global_aniso.pdf')

    plt.close()
except:
    print('Unable to plot anisotropy.')

# Plot enstrophy
try:
    plt.figure()
    if abs(diags.enstrophy[0]) < 1e-20:
        plt.plot(time, diags.enstrophy, '.')
        plt.ylabel('Enstrophy')
    else:
        tmp = diags.enstrophy/(diags.enstrophy[0]) - 1.
        plt.plot(time, tmp, '.')
        plt.ylabel('Relative Enstrophy Deviation')
    plt.axis('tight')
    plt.xlabel('time' + time_units)
    plt.savefig('{0:s}/Enstrophy.pdf'.format(prefix))
    plt.close()
except:
    print('Unable to plot enstrophy.')

# Plot circulation
try:
    plt.figure()
    ax1 = plt.subplot(1,1,1)
    ax1.set_xlabel('time' + time_units)
    ax2 = ax1.twinx()

    ax1.plot(time, diags.circ_N, 'r')
    ax2.plot(time, diags.circ_S, 'b')
    
    ax1.set_ylabel('Circulation (North Wall)', color='r')
    ax2.set_ylabel('Circulation (South Wall)', color='b')

    ax1.tick_params('y', colors='r')
    ax2.tick_params('y', colors='b')

    ax1.yaxis.get_offset_text().set_color('r')
    ax2.yaxis.get_offset_text().set_color('b')

    plt.axis('tight')
    plt.savefig('{0:s}/Circulation.pdf'.format(prefix))
    plt.close()
except:
    print('Unable to plot circulation.')

# Plot transport
try:
    plt.figure()
    if params.Nz == 1:
        tmp = diags.net_transport/params.Lz*3600*24
        ylab = 'Net Zonal Transport (m$^2$/day)'
    else:
        tmp = diags.net_transport*3600*24
        ylab = 'Net Zonal Transport (m$^3$/day)'
    plt.plot(time, tmp, '.')
    plt.axis('tight')
    plt.ylabel(ylab)
    plt.xlabel('time' + time_units)
    plt.savefig('{0:s}/Transport.pdf'.format(prefix))
    plt.close()
except:
    print('Unable to plot net transport.')

# Plot mass
try:
    plt.figure()
    plt.plot(time, diags.mass, '.')
    plt.axis('tight')
    plt.xlabel('time' + time_units)
    plt.savefig('{0:s}/Mass.pdf'.format(prefix))
    plt.close()
except:
    print('Unable to plot mass.')

# Plot energetics
fig = plt.figure()
ax = fig.add_axes([0.15, 0.2, 0.7, 0.7])
ax.plot(time[1:], diags.KE[1:], label='KE')
ax.plot(time[1:], diags.APE[1:], label='APE')
ax.plot(time[1:], TE[1:]/2., label='$\\frac{1}{2}$TE')
ax.set_xlabel('time' + time_units)
ax.set_ylabel('Joules')
ax.autoscale(tight=True)

# Legend
handles, labels = ax.get_legend_handles_labels()
leg_ax = fig.add_axes([0.15, 0.05, 0.7, 0.1])
leg_ax.legend(handles, labels, bbox_to_anchor=(0., 0., 1., 1.), ncol=3, mode='expand',
                frameon = True, borderaxespad=0.)
leg_ax.set_xticks([])
leg_ax.set_yticks([])
for pos in ['left', 'right', 'top', 'bottom']:
    leg_ax.spines[pos].set_visible(False)

plt.savefig('{0:s}/Dimensional_energetics.pdf'.format(prefix))
plt.close()

if TE[1] > 0:
    fig = plt.figure()
    ax = fig.add_axes([0.15, 0.2, 0.7, 0.7])
    ax.plot(time[1:], diags.KE[1:]/(TE[1]), label='KE/TE$_0$')
    ax.plot(time[1:], diags.APE[1:]/(TE[1]), label='APE/TE$_0$')
    ax.plot(time[1:], TE[1:]/(2.*TE[1]), label='$\\frac{1}{2}$TE/TE$_0$')
    ax.set_xlabel('time' + time_units)
    ax.set_ylabel('Proportion of Initial Total Energy')
    ax.axis('tight')
    ax.set_ylim(-0.01,1.01)

    # Legend
    handles, labels = ax.get_legend_handles_labels()
    leg_ax = fig.add_axes([0.15, 0.05, 0.7, 0.1])
    leg_ax.legend(handles, labels, bbox_to_anchor=(0., 0., 1., 1.), ncol=3, mode='expand',
                    frameon = True, borderaxespad=0.)
    leg_ax.set_xticks([])
    leg_ax.set_yticks([])
    for pos in ['left', 'right', 'top', 'bottom']:
        leg_ax.spines[pos].set_visible(False)

    plt.savefig('{0:s}/Nondimensional_energetics.pdf'.format(prefix))
    plt.close()

    plt.figure()
    dTE = TE[1:]/TE[1] - 1.
    plt.plot(time[1:][dTE>0], dTE[dTE>0], '.k')
    plt.plot(time[1:][dTE<0], -dTE[dTE<0], '.r')
    plt.xlabel('time' + time_units)
    plt.ylabel('Relative change in total energy')
    plt.yscale('log')
    plt.axis('tight')
    plt.tight_layout(True)
    plt.savefig('{0:s}/Relative_energy_change.pdf'.format(prefix))
    plt.close()


# Plot time per time-step
plt.figure()
ax1 = host_subplot(111, axes_class=AA.Axes)
plt.subplots_adjust(right=0.75)

dt = diags.time[1:] - diags.time[:-1]
step_time = diags.step_time[1:] - diags.step_time[:-1]

iters = diags.iteration[1:] - diags.iteration[:-1]
dt = dt/iters
step_time = step_time/iters

ax1.plot(diags.time[1:]/86400., step_time, '.r', markersize=1)
ax1.set_ylabel('Physical Seconds per time-step')
ax1.set_xlabel('Time (days)')
ax1.set_ylim((0, np.percentile(step_time,99)))

ax2 = ax1.twinx()
ax2.plot(diags.time[1:]/86400., dt/step_time, '.b', markersize=1)
ax2.set_ylabel('Simulated seconds per physical second')
ax2.set_ylim((0, np.percentile(dt/step_time,99)))

ax3 = ax1.twinx()
ax3.plot(diags.time[1:]/86400, dt, '.g', markersize=1)
ax3.set_ylabel('Simulated seconds per time step')
ax3.set_ylim((0, np.percentile(dt,99)))

offset = 60
new_fixed_axis = ax3.get_grid_helper().new_fixed_axis
ax3.axis["right"] = new_fixed_axis(loc="right", axes=ax3, offset=(offset, 0))
ax3.axis["right"].toggle(all=True)

ax1.axis["left"].label.set_color('r')
ax2.axis["right"].label.set_color('b')
ax3.axis["right"].label.set_color('g')

ax1.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
ax2.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))
ax3.axis.axes.get_yaxis().get_major_formatter().set_powerlimits((0, 5))

plt.savefig('Diagnostics/ComputationTime.pdf')

plt.close()

# Plot Rhines scale
if (hasattr(params, 'beta')) and (params.beta != 0):
    plt.figure()

    Nx = params.Nx
    Lx = params.Lx
    Ny = params.Ny
    Ly = params.Ly
    Nz = params.Nz
    Lz = params.Lz

    vol = Lx*Ly*Lz

    rho0 = 1e3

    Rhines = np.sqrt(np.sqrt(diags.KE/(0.5*vol*rho0))/params.beta)
    plt.plot(diags.time/86400., Rhines/1e3)
    plt.title('Rhines Scale (km)')
    plt.savefig('Diagnostics/RhinesScale.pdf')
    print('Mean Rhines Scale is {0:.4g}km'.format(np.mean(Rhines)/1e3))

    plt.close()

# Plot perturbation norms
if ( hasattr(diags, 'q_pert_norm') ):
    # Plot norms
    time = diags.time/86400.
    time_units = ' (day)'

    if smoothing:
        Nt = time.shape[0]
        newt  = np.linspace(time[0],time[-1],time.shape[0]/2)

        qpi   = spi.interp1d(time,diags.q_pert_norm,kind='slinear')
        q_pert_norm = qpi(newt)

        qi   = spi.interp1d(time,diags.q_norm,kind='slinear')
        q_norm = qi(newt)

        time = newt
        Dt = mp.FiniteDiff(time,4,spb=True,uniform=True,Periodic=False)
        print('After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,time.shape[0]))
    else:
        q_pert_norm = diag.q_pert_norm
        q_norm = diags.q_norm
        Dt = mp.FiniteDiff(time,4,spb=True,uniform=False,Periodic=False)

    gr    = Dt.dot(np.log(q_pert_norm))

    fig = plt.figure()
    ax1 = fig.add_axes([0.15, 0.2, 0.7, 0.7])
    ax2 = ax1.twinx()

    l1, = ax1.plot(time, q_pert_norm, 'r',label='$||q-Q||_2$')
    ax1.set_xlabel('$t$ (days)')
    ax1.set_yscale('log')
    ax1.axis('tight')

    l2, = ax2.plot(time, gr, 'b', label='$\\frac{1}{Ro}\\cdot\\frac{d}{dt}(\\log(||q-Q||_2))$')
    ax1.axis('tight')
    cvp = np.percentile(gr,99)*1.1
    cvm = max(0, np.percentile(gr,1)*0.9)
    tmp = plt.gca().get_ylim()
    ax2.set_ylim(cvm,cvp)

    # Legend
    leg_ax1 = fig.add_axes([0.15, 0.05, 0.35, 0.1])
    leg_ax2 = fig.add_axes([0.5,  0.05, 0.35, 0.1])

    handles, labels = ax1.get_legend_handles_labels()
    leg1 = leg_ax1.legend(handles, labels, loc='lower left', mode='expand', borderaxespad=0., frameon = False,
                    bbox_to_anchor=(0., 0., 1., 1.))

    handles, labels = ax2.get_legend_handles_labels()
    leg2 = leg_ax2.legend(handles, labels, loc='lower left', mode='expand', borderaxespad=0., frameon = False,
                    bbox_to_anchor=(0., 0., 1., 1.))

    for leg_ax in [leg_ax1, leg_ax2]:
        leg_ax.set_xticks([])
        leg_ax.set_yticks([])

    leg_ax1.spines['right'].set_visible(False)
    for side in ['left', 'top', 'bottom']:
        leg_ax1.spines[side].set_linewidth(0.5)
        leg_ax1.spines[side].set_linestyle('--')

    leg_ax2.spines['left'].set_visible(False)
    for side in ['right', 'top', 'bottom']:
        leg_ax2.spines[side].set_linewidth(0.5)
        leg_ax2.spines[side].set_linestyle('--')

    for legend in [leg1, leg2]:
        for t in legend.get_texts():
            t.set_va('center')

    plt.savefig('{0:s}/perturbation_q_norm.pdf'.format(prefix))
    plt.close()

    # Plot normalized perturbation norm
    try:
        qb = spy.reader(params.qb_file,0,force_name=True)
        qb_norm = np.sqrt(np.sum(np.ravel(qb)**2))*(params.Lx*params.Ly*params.Lz)/(params.Nx*params.Ny*params.Nz)

        fig = plt.figure()
        ax1 = fig.add_axes([0.15, 0.2, 0.7, 0.7])
        ax2 = ax1.twinx()

        l1, = ax1.plot(time, q_pert_norm/(q_norm + qb_norm),'r',label='$\\frac{||q - Q||_2}{||q||_2 + ||Q||_2}$')
        ax1.set_xlabel('$t$ (days)')
        ax1.set_yscale('log')
        ax1.axis('tight')
    
        l2, = ax2.plot(newt, gr, 'b', label='$\\frac{1}{Ro}\\cdot\\frac{d}{dt}(\\log(||q-Q||_2))$')
        plt.axis('tight')
        cvp = np.percentile(gr,99)*1.1
        cvm = max(0, np.percentile(gr,1)*0.9)
        ax2.set_ylim(cvm,cvp)
        ax2.grid(True)

        # Legend
        leg_ax1 = fig.add_axes([0.15, 0.05, 0.35, 0.1])
        leg_ax2 = fig.add_axes([0.5,  0.05, 0.35, 0.1])

        handles, labels = ax1.get_legend_handles_labels()
        leg_ax1.legend(handles, labels, loc='lower left', mode='expand', borderaxespad=0., frameon = False,
                        bbox_to_anchor=(0., 0., 1., 1.) )

        handles, labels = ax2.get_legend_handles_labels()
        leg_ax2.legend(handles, labels, loc='lower left', mode='expand', borderaxespad=0., frameon = False,
                        bbox_to_anchor=(0., 0., 1., 1.))

        for leg_ax in [leg_ax1, leg_ax2]:
            leg_ax.set_xticks([])
            leg_ax.set_yticks([])

        leg_ax1.spines['right'].set_visible(False)
        for side in ['left', 'top', 'bottom']:
            leg_ax1.spines[side].set_linewidth(0.5)
            leg_ax1.spines[side].set_linestyle('--')

        leg_ax2.spines['left'].set_visible(False)
        for side in ['right', 'top', 'bottom']:
            leg_ax2.spines[side].set_linewidth(0.5)
            leg_ax2.spines[side].set_linestyle('--')

        plt.savefig('{0:s}/perturbation_q_norm_normalized.pdf'.format(prefix))
        plt.close()
    except:
        print("Unable to plot normalized perturbation norms.")
