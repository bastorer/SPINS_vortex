import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import cm
import sys, os, cmocean
import spinspy as spy
import scipy.stats as sts
import matpy as mp

## Extract nice colour maps from cmocean 'balance'
Np = 256
bal = cmocean.tools.get_dict(cmocean.cm.balance, N=Np)

bal_red   = np.array([x[2] for x in bal['red']  ])
bal_blue  = np.array([x[2] for x in bal['blue'] ])
bal_green = np.array([x[2] for x in bal['green']])

just_red = np.vstack([bal_red[Np//2:], bal_green[Np//2:], bal_blue[Np//2:]]).T
just_blue = np.vstack([bal_red[:Np//2][::-1], bal_green[:Np//2][::-1], bal_blue[:Np//2][::-1]]).T

red_map = cmocean.tools.cmap(just_red, N=Np//2)
blue_map = cmocean.tools.cmap(just_blue, N=Np//2)

red_map.set_under('w')
blue_map.set_under('w')

dense_map = cmocean.cm.dense
dense_map.set_under('w')

def moving_average_1d(x,y,window):
    out = np.zeros(y.shape)
    
    Nt, = y.shape
    for ii in range(Nt):
        out[ii] = np.mean(y[(abs(x - x[ii]) <= window)])

    return out

def moving_average_2d(x,y,window):
    out = np.zeros(y.shape)
    Nt,Nk = y.shape
    for ii in range(Nt):
        out[ii,:] = np.mean(y[abs(x - x[ii]) <= window,:], axis=0)
    return out

def slope_helper(xp, x, y, window):
    wind = window
    std_err = np.nan
    slope = np.nan

    dx = np.abs(x - xp)

    while (np.isnan(std_err) and (wind <= 1.)):
        if np.sum(dx <= wind) > 4:
            try:
                slope, intercept, r_value, p_value, std_err = \
                    sts.linregress(x[dx <= wind], y[dx <= wind])
            except:
                std_err = np.nan
                wind *= 2
        else:
            std_err = np.nan
            wind *= 2
    
    return slope, std_err

def compute_slopes(x,y,window):
    xt = x[~np.isnan(y)]
    yt = y[~np.isnan(y)]

    slope_errs = [ slope_helper(xp, xt, yt, window) for xp in x ]

    slopes  = np.array( [ se[0] for se in slope_errs  ] ).reshape(y.shape)
    stderrs = np.array( [ se[1] for se in slope_errs  ] ).reshape(y.shape)


prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

num_ords = 6 # number of orders to plot for log scales
log_perc = 100 # percentile for log upperbound
lin_perc = 100 # percentile for linear upperbound
smoothing = True

def add_neg(cax):
    labels = map(lambda st: st[:14] + '-' + st[14:],
                 map(lambda tick: tick.get_text(),
                      cax.get_yticklabels()))
    cax.set_yticklabels(labels)

# Load and define some things
diags = spy.get_diagnostics()
params = spy.get_params()

Nx = params.Nx
Lx = params.Lx
Ny = params.Ny
Ly = params.Ly
Nz = params.Nz
Lz = params.Lz

def plot_energy_spectrum(KK, TT, data, cvs, title, filename):
    plt.figure(figsize=(6,10))
    ax1 = plt.subplot(1,1,1)
    cv = max(cvs)
    vmin = 10**(np.ceil(cv-num_ords) - 0.5)
    vmax = 10**cv
    q = plt.pcolormesh(KK,TT,data, cmap=cmocean.cm.dense, norm=LogNorm(vmin=vmin, vmax=vmax),\
            linewidth=0, rasterized=True)
    q.set_edgecolor('face')
    cbar = plt.colorbar(q, pad = 0.15, extend='min')
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor("face")
    ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

    plt.xscale('log')
    plt.axis('tight')
    plt.xlabel('Inverse Lengthscale')
    plt.ylabel('Time (days)')
    plt.title(title)
    plt.plot([kcut, kcut], ax1.get_ylim(), 'k')

    plt.plot(1./Rhines[Rhines > 0], TT[1:][Rhines > 0], 'g')

    plt.tight_layout(True)

    plt.savefig(filename)
    plt.close()

def plot_energy_flux_log_sign(KK, TT, data, cvs, title, filename):
    fig = plt.figure(figsize=(6, 10))

    ax1 = fig.add_axes([0.15,0.1,0.7,0.8])

    vmax = 10**(max(cvs))
    vmin = 10**(np.ceil(max(cvs)-num_ords)-1.0)
    q1 = ax1.pcolormesh(KK,TT,np.abs(data)*(data > 0), cmap=red_map, \
            norm=LogNorm(vmin=vmin, vmax=vmax), linewidth=0, rasterized=True)
    q2 = ax1.pcolormesh(KK,TT,np.abs(data)*(data < 0), cmap=blue_map, \
            norm=LogNorm(vmin=vmin, vmax=vmax), linewidth=0, rasterized=True)
    ax1.yaxis.get_major_formatter().set_powerlimits((0, 4))

    plt.axis('tight')
    ax1.set_xlabel('Inverse Lengthscale')
    ax1.set_ylabel('Time (days)')
    ax1.set_title(title)
    ax1.plot([kcut, kcut], ax1.get_ylim(), 'k')
    ax1.set_xscale('log')

    plt.plot(1./Rhines[Rhines > 0], TT[1:][Rhines > 0], 'g')

    cax1 = fig.add_axes([0.9,0.525,0.025,0.375])
    cax2 = fig.add_axes([0.9,0.1,0.025,0.375])

    cbar1 = plt.colorbar(q1, cax=cax1, extend='min')
    cbar1.solids.set_rasterized(True)
    cbar1.solids.set_edgecolor("face")
    cbar2 = plt.colorbar(q2, cax=cax2, extend='min')
    cbar2.solids.set_rasterized(True)
    cbar2.solids.set_edgecolor("face")
    add_neg(cax2)

    cax2.invert_yaxis()

    plt.savefig(filename, dpi=500)
    plt.close()

def plot_spectral_slopes(KK, TT, data, title, filename):

    plt.figure()
    ax1 = plt.subplot(2,1,1)
    ax2 = plt.subplot(2,1,2)

    cm_subsection = np.linspace(0., 1., 11.) 
    colors = [ mp.darkjet(III) for III in cm_subsection ]
    pnum = 0;

    for ii in range(0,len(TT)-1,int(np.floor((len(TT)-1)//10))):
        
        if (ii == 0):
            ii = 1

        # Skip first point (it's zero)
        tmp_dat = data[ii,1:]
        tmp_k   = k[1:]

        tmp_k   = tmp_k[np.abs(tmp_dat) > 0]
        tmp_dat = tmp_dat[np.abs(tmp_dat) > 0]

        win = 1./20
        ax1.plot(tmp_k, tmp_dat, color=colors[pnum])

        win = 1./10
        slp, err = compute_slopes(np.log10(tmp_k), np.log10(tmp_dat), win)
        ax2.plot(tmp_k[~(np.isnan(err))], slp[~(np.isnan(err))], color=colors[pnum])

        pnum += 1

    ax1.set_ylabel('Power Spectra',fontsize=10)
    ax1.set_yscale('log')
    ax1.set_xscale('log')
    ax1.axis('tight')
    ax1.set_ylim(max(ax1.get_ylim()[1]/1e10, ax1.get_ylim()[0]), min(1.1, ax1.get_ylim()[1]))
    ax1.plot([kcut, kcut], ax1.get_ylim(), 'k')
    ax1.grid(True)

    if np.mean(Rhines) != 0:
        ax1.plot([1./np.mean(Rhines), 1./np.mean(Rhines)], ax1.get_ylim(), 'g')

    ax2.set_xscale('log')
    ax2.axis('tight')
    ax2.set_xlim(ax1.get_xlim())
    ax2.set_ylim(max(-6,ax2.get_ylim()[0]),1)
    ax2.plot([kcut, kcut], ax2.get_ylim(), 'k')
    ax2.grid(True)

    if np.mean(Rhines) != 0:
        ax2.plot([1./np.mean(Rhines), 1./np.mean(Rhines)], ax2.get_ylim(), 'g')

    ax2.set_xlabel('Inverse Wavelength',fontsize=10)
    ax2.set_ylabel('Spectral Slopes',fontsize=10)

    plt.tight_layout(True)
    plt.savefig(filename)

##
## Start with some plots of the spectra and their fluxes
##

for style in ['1d','x','y']:

    print('Starting ' + style)

    # Load the previously computed spectra data, but recompute the
    # net fluxes, since we may have changed how the time divisions were defined.
    spect_data     = np.load('Diagnostics/spectral_diagnostics_' + style + '.npz')

    Rhines = spect_data['Rhines']

    k  = spect_data['k']
    KK = spect_data['KK']
    TT = spect_data['TT']

    kcut = spect_data['kcut']

    dat_TE = spect_data['dat_TE']
    dat_PE = spect_data['dat_PE']
    dat_KE = spect_data['dat_KE']
    
    flux_TE = spect_data['flux_TE']
    flux_PE = spect_data['flux_PE']
    flux_KE = spect_data['flux_KE']

    t = TT[1:]
    ma_win = t[-1]/200.

    cv_PE   = np.nanpercentile(np.log10(np.abs(dat_PE[np.abs(dat_PE) > 0])), 99)
    cv_KE   = np.nanpercentile(np.log10(np.abs(dat_KE[np.abs(dat_KE) > 0])), 99)
    cv_TE   = np.nanpercentile(np.log10(np.abs(dat_TE[np.abs(dat_TE) > 0])), 99)
    cvs_dat = (cv_PE, cv_KE, cv_TE)

    cv_PE = np.nanpercentile(np.log10(np.abs(flux_PE[np.abs(flux_PE) > 0])), 99.5)
    cv_KE = np.nanpercentile(np.log10(np.abs(flux_KE[np.abs(flux_KE) > 0])), 99.5)
    cv_TE = np.nanpercentile(np.log10(np.abs(flux_TE[np.abs(flux_TE) > 0])), 99.5)
    cvs = (cv_PE, cv_KE, cv_TE)

    tmp = moving_average_2d(t,flux_PE,ma_win)
    cv_PE_ma = np.nanmax(np.log10(np.abs(tmp[np.abs(tmp) > 0])))
    tmp = moving_average_2d(t,flux_KE,ma_win)
    cv_KE_ma = np.nanmax(np.log10(np.abs(tmp[np.abs(tmp) > 0])))
    tmp = moving_average_2d(t,flux_TE,ma_win)
    cv_TE_ma = np.nanmax(np.log10(np.abs(tmp[np.abs(tmp) > 0])))
    cvs_ma = (cv_PE_ma, cv_KE_ma, cv_TE_ma)

    plot_energy_spectrum(KK, TT, dat_TE, cvs_dat, 'TE Spectrum', 'Diagnostics/' + style + '_TE.pdf')
    plot_energy_flux_log_sign(KK, TT, flux_TE, cvs, '$\\frac{d}{dt}$TE', 'Diagnostics/' + style + '_TE_flux.pdf')
    #plot_spectral_slopes(KK, TT, dat_TE, 'TE Spectrum', 'Diagnostics/' + style + '_TE_lines.pdf')

    plot_energy_spectrum(KK, TT, dat_KE, cvs_dat, 'KE Spectrum', 'Diagnostics/' + style + '_KE.pdf')
    plot_energy_flux_log_sign(KK, TT, flux_KE, cvs, '$\\frac{d}{dt}$KE', 'Diagnostics/' + style + '_KE_flux.pdf')
    #plot_spectral_slopes(KK, TT, dat_KE, 'KE Spectrum', 'Diagnostics/' + style + '_KE_lines.pdf')

    plot_energy_spectrum(KK, TT, dat_PE, cvs_dat, 'PE Spectrum', 'Diagnostics/' + style + '_PE.pdf')
    plot_energy_flux_log_sign(KK, TT, flux_PE, cvs, '$\\frac{d}{dt}$PE', 'Diagnostics/' + style + '_PE_flux.pdf')
    #plot_spectral_slopes(KK, TT, dat_PE, 'PE Spectrum', 'Diagnostics/' + style + '_PE_lines.pdf')

    dat_EN = spect_data['dat_EN']
    flux_EN = spect_data['flux_EN']

    cv_EN   = np.nanpercentile(np.log10(np.abs(dat_EN[np.abs(dat_EN) > 0])), 99)
    cvs_dat = (cv_EN, cv_EN, cv_EN)
    cv_EN   = np.nanpercentile(np.log10(np.abs(flux_EN[np.abs(flux_EN) > 0])), 99.5)
    cvs     = (cv_EN, cv_EN, cv_EN)

    plot_energy_spectrum(KK, TT, dat_EN, cvs_dat, 'Enstrophy Spectrum', 'Diagnostics/' + style + '_EN.pdf')
    plot_energy_flux_log_sign(KK, TT, flux_EN, cvs, '$\\frac{d}{dt}$Enstrophy', 'Diagnostics/' + style + '_EN_flux.pdf')
    #plot_spectral_slopes(KK, TT, dat_EN, 'Enstrophy Spectrum', 'Diagnostics/' + style + '_EN_lines.pdf')
