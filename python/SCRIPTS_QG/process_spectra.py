import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import spinspy as spy
import matpy as mp
from scipy.fftpack import fftfreq
import scipy.interpolate as spi

def moving_average(x,y,window):
    out = np.zeros(y.shape)
    Nt,Nk = y.shape
    for ii in range(Nt):
        out[ii,:] = np.mean(y[abs(x - x[ii]) <= window,:], axis=0)
    return out

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

smoothing = True

# Load and define some things
diags = spy.get_diagnostics()
params = spy.get_params()

Nx = params.Nx
Lx = params.Lx
Ny = params.Ny
Ly = params.Ly
Nz = params.Nz
Lz = params.Lz

rho0 = 1.e3

vol = Lx*Ly*Lz/(Nx*Ny*Nz)

##
## Start with some plots of the spectra and their fluxes
##

for style in ['1d','x','y']:

    print('Starting ' + style)

    if (style == '1d'):
        N = min(params.Nx,params.Ny)//2
        d = min(params.Lx/params.Nx, params.Ly/params.Ny)
    elif (style == 'x'):
        N = params.Nx//2
        d = params.Lx/params.Nx
    elif (style == 'y'):
        N = params.Ny//2
        d = params.Ly/params.Ny

    kcut = params.f_cutoff/(2.*d)
    k = fftfreq(2*N,d=d)[:N]
    dk = k[1] - k[0]

    KK = np.zeros(N+1)
    KK[:-1] = k
    KK[-1] = k[-1] + dk
    KK[0] = dk/2. # Don't actually go to zero, for log scales

    dat_KE = np.fromfile('KE_' + style + '_spect.bin')
    dat_PE = np.fromfile('PE_' + style + '_spect.bin')
    dat_EN = np.fromfile('EN_' + style + '_spect.bin')

    Nt = dat_KE.shape[0]//N
    t = diags.time[:Nt]/86400.
    ma_win = t[-1]/100.

    # SPINS returns depth-integrated spectra
    dat_KE = dat_KE.reshape((Nt,N))*vol/(Nx*Ny)
    dat_PE = dat_PE.reshape((Nt,N))*vol/(Nx*Ny)
    dat_EN = dat_EN.reshape((Nt,N))*vol/(Nx*Ny)

    # Try a bit of smoothing
    if smoothing:
        try:
            newt = np.linspace(t[0],t[-1],Nt//2)
            NewNt = newt.shape[0]

            Dt = mp.FiniteDiff(newt,4,spb=True,uniform=True)

            proj = spi.interp1d(t, dat_KE, kind='slinear',axis=0)
            dat_KE = proj(newt)

            proj = spi.interp1d(t, dat_PE, kind='slinear',axis=0)
            dat_PE = proj(newt)

            proj = spi.interp1d(t, dat_EN, kind='slinear',axis=0)
            dat_EN = proj(newt)
    
            dat_TE = dat_KE + dat_PE

            KE = diags.KE.ravel()[:Nt]
            PE = diags.APE.ravel()[:Nt]
            EN = diags.enstrophy.ravel()[:Nt]

            if params.beta != 0:
                Rhines = np.sqrt(np.sqrt(KE/(0.5*Lx*Ly*Lz*rho0))/params.beta)
            else:
                Rhines = 0.*KE

            proj = spi.interp1d(t, KE, kind='slinear')
            KE = proj(newt)

            proj = spi.interp1d(t, PE, kind='slinear')
            PE = proj(newt)

            proj = spi.interp1d(t, EN, kind='slinear')
            EN = proj(newt)

            proj = spi.interp1d(t, Rhines, kind='slinear')
            Rhines = proj(newt)

            TE = KE + PE

            t = newt
            print('   After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,t.shape[0]))
            Nt = t.shape[0]
        except MemoryError:
            print('   Memory error: project {0:d} points to {1:d} points. Trying with fewer points'.format(Nt, NewNt))
            t = diags.time[:Nt]/86400.
            newt = np.linspace(t[0],t[-1],40000)

            proj = spi.interp1d(t, dat_KE, kind='slinear',axis=0)
            dat_KE = proj(newt)

            proj = spi.interp1d(t, dat_PE, kind='slinear',axis=0)
            dat_PE = proj(newt)

            proj = spi.interp1d(t, dat_EN, kind='slinear',axis=0)
            dat_EN = proj(newt)
    
            dat_TE = dat_KE + dat_PE

            KE = diags.KE.ravel()[:Nt]
            PE = diags.APE.ravel()[:Nt]

            if params.beta != 0:
                Rhines = np.sqrt(np.sqrt(KE/(0.5*Lx*Ly*Lz*rho0))/params.beta)
            else:
                Rhines = 0.*KE

            proj = spi.interp1d(t, KE, kind='slinear')
            KE = proj(newt)

            proj = spi.interp1d(t, PE, kind='slinear')
            PE = proj(newt)

            proj = spi.interp1d(t, EN, kind='slinear')
            EN = proj(newt)

            proj = spi.interp1d(t, Rhines, kind='slinear')
            Rhines = proj(newt)

            TE = KE + PE

            Dt = mp.FiniteDiff(newt,2,spb=True,uniform=True)
            t = newt

            print('   After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,t.shape[0]))
            Nt = t.shape[0]

    else:
        Dt = mp.FiniteDiff(t,8,spb=True,uniform=False)

        TE = (diags.KE.ravel() + diags.APE.ravel())[:Nt]
        KE = diags.KE.ravel()[:Nt]
        PE = diags.APE.ravel()[:Nt]

    TT = np.zeros(Nt+1)
    TT[1:] = t
    TT[0] = 0.

    flux_KE = Dt.dot(dat_KE)
    flux_PE = Dt.dot(dat_PE)
    flux_TE = Dt.dot(dat_TE)
    flux_EN = Dt.dot(dat_EN)

    # Now save everything that we computed
    np.savez('Diagnostics/spectral_diagnostics_' + style + '.npz', 
            KK=KK, TT=TT, kcut=kcut, k=k,
            flux_TE = flux_TE, flux_PE = flux_PE, flux_KE = flux_KE,
            dat_TE = dat_TE, dat_PE = dat_PE,  dat_KE = dat_KE,
            flux_EN = flux_EN, dat_EN = dat_EN,  Rhines = Rhines)

    ## Now we're going to use spectra/fluxes that are integrated from the small scales
    for ii in range(N):
        dat_KE[:,ii] = np.sum(dat_KE[:,ii:], axis=1)
        dat_PE[:,ii] = np.sum(dat_PE[:,ii:], axis=1)
        dat_TE[:,ii] = np.sum(dat_TE[:,ii:], axis=1)
        dat_EN[:,ii] = np.sum(dat_EN[:,ii:], axis=1)

        flux_KE[:,ii] = np.sum(flux_KE[:,ii:], axis=1)
        flux_PE[:,ii] = np.sum(flux_PE[:,ii:], axis=1)
        flux_TE[:,ii] = np.sum(flux_TE[:,ii:], axis=1)
        flux_EN[:,ii] = np.sum(flux_EN[:,ii:], axis=1)


    # Sanity checks
    MAPE_TE = np.nanmean(np.abs((TE[1:] - dat_TE[1:,0])/TE[1:])*100.)
    MAPE_KE = np.nanmean(np.abs((KE[1:] - dat_KE[1:,0])/KE[1:])*100.)
    MAPE_PE = np.nanmean(np.abs((PE[1:] - dat_PE[1:,0])/PE[1:])*100.)
    MAPE_EN = np.nanmean(np.abs((EN[1:] - dat_EN[1:,0])/EN[1:])*100.)

    print('   MAPE(KE,PE,TE,EN) = ({0:.4g} %%, {1:.4g} %%, {2:.4g} %% {3:.4g} %%)'.format(MAPE_KE, MAPE_PE, MAPE_TE, MAPE_EN))

    plt.figure()
    plt.subplot(1,2,1)
    plt.plot(TT[1:], (KE - dat_KE[:,0])/(KE + 1e-10)*100)
    plt.xlabel('time (days)')
    plt.ylabel('percent (already multiplied by 100)')
    plt.title('Percent error in KE')
    plt.axis('tight')

    plt.subplot(1,2,2)
    plt.plot(TT[1:], (PE - dat_PE[:,0])/(PE + 1e-10)*100)
    plt.xlabel('time (days)')
    plt.title('Percent error in PE')
    plt.axis('tight')

    plt.tight_layout(True)
    plt.savefig('Diagnostics/Energy_error_' + style + '.pdf')

    # Now save everything that we computed
    np.savez('Diagnostics/integrated_spectral_diagnostics_' + style + '.npz', 
            KK=KK, TT=TT, flux_TE = flux_TE, flux_PE = flux_PE, flux_KE = flux_KE,
                           dat_TE = dat_TE,   dat_PE = dat_PE,   dat_KE = dat_KE,
                           dat_EN = dat_EN, flux_EN = flux_EN,
                           Rhines = Rhines)
