import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys, os
import spinspy as spy
import matpy as mp
from scipy.fftpack import fftfreq
import scipy.interpolate as spi

prefix = os.getcwd() + '/Diagnostics'
if not(os.path.exists(prefix)):
    os.makedirs(prefix)

smoothing = True

# Load and define some things
diags = spy.get_diagnostics()
params = spy.get_params()

Nx = params.Nx
Lx = params.Lx
Ny = params.Ny
Ly = params.Ly
Nz = params.Nz
Lz = params.Lz

vol = Lx*Ly*Lz/(Nx*Ny*Nz)

rho0 = 1000.

print('Processing anisotropy')

N = max(params.Nx,params.Ny)//2
d = min(params.Lx/params.Nx, params.Ly/params.Ny)

kcut = params.f_cutoff/(2.*d)
k = fftfreq(2*N,d=d)[:N]
dk = k[1] - k[0]

KK = np.zeros(N+1)
KK[:-1] = k
KK[-1] = k[-1] + dk
KK[0] = dk/2. # Don't actually go to zero, for log scales

aniso_KE = np.fromfile('KE_aniso.bin')
aniso_PE = np.fromfile('PE_aniso.bin')
aniso_EN = np.fromfile('EN_aniso.bin')

Nt = aniso_KE.shape[0]//N
t = diags.time[:Nt]/86400.

orig_time = t.copy()

# SPINS returns depth-integrated spectra
aniso_KE = aniso_KE.reshape((Nt,N))
aniso_PE = aniso_PE.reshape((Nt,N))
aniso_EN = aniso_EN.reshape((Nt,N))

# Compute flux
#Dt = mp.FiniteDiff(t, 8, spb=True, uniform=False)
#flux_KE = Dt.dot(aniso_KE)
#flux_PE = Dt.dot(aniso_PE)
#flux_EN = Dt.dot(aniso_EN)

# Try a bit of smoothing
if smoothing:
    newt = np.linspace(t[0],t[-1],Nt//4)
    NewNt = newt.shape[0]

    Dt = mp.FiniteDiff(newt,8,spb=True,uniform=True)

    proj = spi.interp1d(t, aniso_KE, kind='slinear',axis=0)
    aniso_KE = proj(newt)

    proj = spi.interp1d(t, aniso_PE, kind='slinear',axis=0)
    aniso_PE = proj(newt)

    proj = spi.interp1d(t, aniso_EN, kind='slinear',axis=0)
    aniso_EN = proj(newt)

    if params.beta != 0:
        Rhines = np.sqrt(np.sqrt(diags.KE/(0.5*Lx*Ly*Lz*rho0))/params.beta)
    else:
        Rhines = 0.*diags.KE
    proj = spi.interp1d(t, Rhines, kind='slinear',axis=0)
    Rhines = proj(newt)

    t = newt
    print('   After smoothing, {0:d} t points reduced to {1:d} t points.'.format(Nt,t.shape[0]))
    Nt = t.shape[0]
else:
    Dt = mp.FiniteDiff(t,8,spb=True,uniform=False)
    pass

TT = np.zeros(Nt+1)
TT[1:] = t
TT[0] = 0.

flux_KE = Dt.dot(aniso_KE)
flux_PE = Dt.dot(aniso_PE)
flux_EN = Dt.dot(aniso_EN)

# Now save everything that we computed
np.savez('Diagnostics/anisotropy_diagnostics.npz',
        KK=KK, TT=TT, kcut=kcut, k=k, t=orig_time,
        flux_PE = flux_PE, flux_KE = flux_KE, flux_EN = flux_EN,
        aniso_PE = aniso_PE, aniso_KE = aniso_KE, aniso_EN = aniso_EN,
        Rhines = Rhines)

