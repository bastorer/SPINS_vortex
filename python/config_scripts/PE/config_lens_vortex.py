# config_lens_vortex.py
#
# SUMMARY
#   This config file specifies an interior lens vortex.
#
# INITIAL CONDITIONS
#   The profile has a Gaussian streamfunction 
#     and is in cyclostrophic balance.
#
#   The input problem parameters are:
#     name  : Dimensions    : Description
#     Lv    : m             : vertical scale for the vortex
#     N0    : 1/s           : Buoyancy frequency for linear stratification
#     Bu    : n/a           : Burger number
#     Ro    : n/a           : Rossby number
#     alpha : n/a           : Aspect ratio (width / height) of the vortex
#
#   The velocity and horizontal scale of the vortex, as well as the Coriolis f-plane 
#       parameters are determined from the prescribed dimensionless values.
#
# SETTING
#   rotation : f-plane
#   stratification : linear
#
# BOUNDARY CONDITIONS:
#   x  : periodic
#   y  : periodic
#   z  : periodic


import numpy as np
from scipy.fftpack import fftfreq, ifftn

def main():

    case = 'vortex_'
    total_run_time = 7*24*3600
    np.random.seed(seed=100)

    # Non-dimensional parameters
    Bu    =  5.00               # Burger number: determines f0 (along with alpha)
    Ro    = -1.00               # Rossby number: determines U0
    alpha = 1./100              # Aspect ratio: determines L

    # Dimensional parameters
    Lv = 400.                   # Vertical vortex halfwidth
    N0 = np.sqrt(5)*1e-3        # Buoyancy frequency

    # Derived dimensional parameters
    f0 = N0*alpha/np.sqrt(Bu)   # Coriolis f-plane
    Lh = Lv/alpha               # Horizontal vortex halfwidth
    U0 = Ro*f0*Lh               # Vortex velocity

    L = 20.*Lh                  # Horizontal domain extent
    H = 10.*Lv                  # Vertical domain extent

    # Time parameters
    t0 = 0                      # Initial time
    tf = 600*24*3600/abs(Ro)    # Final time
    tw_3D = tf/50.              # Time for 3D write 
    tw_2D = tf/200.             # Time for 2D write (approximate)
    tw_0D = tw_3D/200.          # Time for diagnostics (approximate)

    # Grid parameters
    Nx, Ny, Nz = 256, 256, 256

    # Diagnostic options
    compute_norms = True
    compute_spectra = True
    compute_eps_cyclo = True
    compute_eps_qg = True

    # Perturbation
    pert = 1e-6  # Relative strength (perturb buoyancy)
    pert_type = 2  # 0 = columnar envelope barotropic, 
                   # 1 = columnar envelope non-barotropic,
                   # 2 = 3D gaussian envelope, 
                   # 3 = no envelope
                   # 4 = 3D gaussian envelop over barotropic perturbation

    g  = 9.81
    nu = 0  
    kappa = 0 
    rho_0 = 1e3 

    # Filter parameters
    f_strength = 20. 
    f_cutoff   = 0.6
    f_order    = 2.0

    ##
    ## DO NOT MODIFY ANYTHING AFTER THIS
    ## UNLESS YOU WISH TO CHANGE THE INITIAL
    ## CONDITIONS
    ##

    # Create the grid
    x = (L/Nx)*np.arange(0.5,Nx) - L/2
    y = (L/Ny)*np.arange(0.5,Ny) - L/2
    z = (H/Nz)*np.arange(0.5,Nz) - H

    x,y,z = np.meshgrid(x, y, z, indexing='ij', sparse=True)

    # Define the initial conditions
    Omeg0 = U0/Lh

    x0,y0,z0 = (0,0,-H/2)
    x = (x - x0)/Lh
    y = (y - y0)/Lh
    z = (z - z0)/Lv

    Omeg = 0.5*Omeg0*np.exp( - x**2 - y**2 - z**2) 

    # u
    var = -Lh*y*Omeg
    var = np.transpose(var,(0,2,1))
    var.tofile('bg_u.0')
    var.tofile(case+'u')

    # v
    var = Lh*x*Omeg
    var = np.transpose(var,(0,2,1))
    var.tofile('bg_v.0')
    var.tofile(case+'v')

    # w
    var *= 0
    var = np.transpose(var,(0,2,1))
    var.tofile('bg_w.0')
    var.tofile(case+'w')

    # epv (ertel potential vorticity)
    alp = Lh/Lv
    var = -4*(z**2)*(Omeg**2)*(2*Omeg + f0)*(x**2 + y**2)*(alp**2) + \
            (f0 + 2*Omeg*(1 - x**2 - y**2))*(N0**2 + (alp**2)*Omeg*(Omeg*(1 - 4*(z**2)) + f0*(1 - 2*(z**2))))
    var = np.transpose(var,(0,2,1))
    var.tofile('bg_epv.0')

    if (var.min() < 0):
        print('Danger Will Robinson! min(Ertel PV) = {0:.4g} < 0, inertial instabilities abound.'.format(var.min()))

    # b
    var =  z*(Lh**2/Lv)*(Omeg+f0)*Omeg
    var = np.transpose(var,(0,2,1))
    var.tofile('bg_b.0')

    ##
    ## Specify perturbation
    ##

    # Get a scale for b
    b0 = np.abs(var).max()

    # Make 3D coloured noise
    rhat = np.random.randn(Nx,Ny,Nz) + 1j*np.random.randn(Nx,Ny,Nz)
    kx = fftfreq(Nx, d=L/Nx)
    ky = fftfreq(Ny, d=L/Ny)
    kz = fftfreq(Nz, d=H/Nz)
    KX, KY, KZ = np.meshgrid(kx, ky, kz, indexing='ij',sparse=True)
    K2 = KX**2 + KY**2 + KZ**2
    K2i = 1./K2
    K2i[K2==0] = 0
    K2i[np.abs(K2) > f_cutoff*K2.max()] = 0

    r_red = ifftn( rhat * (K2i**0.5) ).real
    r_red *= 1./(np.abs(r_red).max())

    # Use a broader Gaussian for the perturbation
    if (pert_type == 0):
        b_pert = pert*b0*np.tile(np.reshape(r_red[:,:,Nz/2]*np.exp( - (x/3)**2 - (y/3)**2 ) , (Nx,Ny,1)), (1,1,Nz))
    elif (pert_type == 1):
        b_pert = pert*b0*r_red*np.tile(np.reshape(np.exp( - (x/3)**2 - (y/3)**2 ) , (Nx,Ny,1)), (1,1,Nz))
    elif (pert_type == 2):
        b_pert = pert*b0*r_red*np.exp( - (x/3)**2 - (y/3)**2 - (z/1.5)**2) 
    elif (pert_type == 3):
        b_pert = pert*b0*r_red
    elif (pert_type == 4):
        b_pert = pert*b0*np.tile(np.reshape(r_red[:,:,Nz/2], (Nx,Ny,1)), (1,1,Nz))*np.exp( - (x/3)**2 - (y/3)**2 - (z/1.5)**2) 

    b_pert = np.transpose(b_pert,(0,2,1))
    var += b_pert
    var.tofile(case+'b')

    ##
    ## Create spins.conf
    ##

    # Open file
    text_file = open("spins.conf", "w")

    # Write variables to file
    text_file.write("## Grid Parameters\n")
    text_file.write("# Number of Gridpoints\n")
    text_file.write("Nx = {0:d} \n".format(Nx))
    text_file.write("Ny = {0:d} \n".format(Ny))
    text_file.write("Nz = {0:d} \n".format(Nz))
    text_file.write("\n")
    
    text_file.write("# Grid Types\n")
    text_file.write("type_x = FOURIER \n")
    text_file.write("type_y = FOURIER \n")
    text_file.write("type_z = FREE_SLIP \n")
    text_file.write("\n")
    
    text_file.write("# Domain Extents\n")
    text_file.write("Lx = {0:.12g} \n".format(L))
    text_file.write("Ly = {0:.12g} \n".format(L))
    text_file.write("Lz = {0:.12g} \n".format(H))
    text_file.write("\n")
    
    text_file.write("# Domain Minima\n")
    text_file.write("min_x = {0:.12g} \n".format(0))
    text_file.write("min_y = {0:.12g} \n".format(0))
    text_file.write("min_z = {0:.12g} \n".format(-H))
    text_file.write("mapped_grid = false\n")
    text_file.write("\n")

    text_file.write("# Grid Filename\n")
    text_file.write("xgrid = {0:s}\n".format(case+'x'))
    text_file.write("ygrid = {0:s}\n".format(case+'y'))
    text_file.write("zgrid = {0:s}\n".format(case+'z'))
    text_file.write("\n")
    
    text_file.write("# Grid Filetype\n")
    text_file.write('file_type = FULL\n')

    text_file.write("\n")
    text_file.write("## Filenames for Input Fields and ICs\n")
    text_file.write("u_file = {0:s}\n".format(case+'u'))
    text_file.write("v_file = {0:s}\n".format(case+'v'))
    text_file.write("w_file = {0:s}\n".format(case+'w'))
    text_file.write("rho_file = {0:s}\n".format(case+'b'))
    text_file.write("bg_b_filename = {0:s}\n".format('bg_b.0'))
    text_file.write("\n")
   
    text_file.write("# Tracer Flag\n")
    text_file.write("enable_tracer = false \n")
    text_file.write("\n")
     
    text_file.write("### Parameters\n")
    text_file.write("\n")

    text_file.write("## Simulation Parameters\n")
    text_file.write("# Specified Dimensionless \n")
    text_file.write("Bu = {0:.12g} \n".format(Bu))
    text_file.write("Ro = {0:.12g} \n".format(Ro))
    text_file.write("alpha = {0:.12g} \n".format(alpha))
    text_file.write("\n")

    text_file.write("# Specified Dimensional \n")
    text_file.write("Lv = {0:.12g} \n".format(Lv))
    text_file.write("g = {0:.12g} \n".format(g))   
    text_file.write("rot_f = {0:.12g} \n".format(f0))
    text_file.write("N0 = {0:.12g} \n".format(N0))
    text_file.write("visc = {0:.12g} \n".format(nu))
    text_file.write("kappa = {0:.12g} \n".format(kappa))
    text_file.write("\n")
    
    text_file.write("# Derived Dimensional \n")
    text_file.write("Lh = {0:.12g} \n".format(Lh))
    text_file.write("U0 = {0:.12g} \n".format(U0))
    text_file.write("L = {0:.12g} \n".format(L))
    text_file.write("H = {0:.12g} \n".format(H))
    text_file.write("\n")
    
    text_file.write("## Temporal Parameters\n")
    text_file.write("init_time = {0:.12g} \n".format(t0))
    text_file.write("final_time = {0:.12g} \n".format(tf))
    text_file.write("plot_interval = {0:.12g} \n".format(tw_3D))
    text_file.write("plot_interval_2D = {0:.12g} \n".format(tw_2D))
    text_file.write("diag_time = {0:.12g} \n".format(tw_0D))
    text_file.write("\n")

    text_file.write("## Filter Parameters\n")
    text_file.write("f_strength = {0:.12g} \n".format(f_strength))
    text_file.write("f_cutoff = {0:.12g} \n".format(f_cutoff))
    text_file.write("f_order = {0:.12g} \n".format(f_order))
    text_file.write("\n")

    text_file.write("## Diagnostic Parameters\n")
    text_file.write("compute_norms = {0} \n".format(compute_norms))
    text_file.write("compute_spectra = {0} \n".format(compute_spectra))
    text_file.write("compute_eps_cyclo = {0} \n".format(compute_eps_cyclo))
    text_file.write("compute_eps_qg = {0} \n".format(compute_eps_qg))
    text_file.write("compute_time  = {0:d} \n".format(total_run_time))
    text_file.write("\n")

    text_file.write("## Restart Parameters\n")
    text_file.write("restart = false \n")
    text_file.write("restart_from_dump = false \n")
    text_file.write("restart_time = 0 \n")
    text_file.write("restart_sequence = 0 \n")
    text_file.write("\n")

    #Close file
    text_file.close()

if __name__ == "__main__":
    main()
