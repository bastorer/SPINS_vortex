#include "../qg_functions.hpp"

void qg_functions::solnclass::stitch_chain_diagnostics() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::stitch_chain_diagnostics\n"); }

    if (Nz > 1) {
        for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
            offset_from = JJ*Nz;
            offset_to   = (JJ + prev_chain_write_count)*Nz;

            // level_mass
            MPI_File_seek(level_mass_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(level_mass_temp_file, level_mass, Nz, MPI_DOUBLE, &status);
            MPI_File_seek(level_mass_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(level_mass_final_file, level_mass, Nz, MPI_DOUBLE, &status);
        }

        MPI_Barrier(MPI_COMM_WORLD);

        MPI_File_close(&level_mass_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "level_mass_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &level_mass_temp_file);
    }

    if ((Nz == 1) and (use_zonal_decomp)) {
        for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
            offset_from = JJ*Ny;
            offset_to   = (JJ + prev_chain_write_count)*Ny;

            // ubar
            MPI_File_seek(ubar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(ubar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
            MPI_File_seek(ubar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(ubar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);

            // qbar
            MPI_File_seek(qbar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(qbar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
            MPI_File_seek(qbar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(qbar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);

            // psibar
            MPI_File_seek(psibar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(psibar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
            MPI_File_seek(psibar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(psibar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);
        }

        MPI_Barrier(MPI_COMM_WORLD);

        MPI_File_close(&ubar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "ubar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &ubar_temp_file);

        MPI_File_close(&qbar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "qbar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &qbar_temp_file);

        MPI_File_close(&psibar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "psibar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &psibar_temp_file);

    }
}

