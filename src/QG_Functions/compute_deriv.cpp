#include "../qg_functions.hpp"

// Compute arbitrary derivative
void qg_functions::solnclass::compute_deriv(string const var, string const der ) { 

    if (debug and master()) { fprintf(stdout, "entering solnclass::compute_deriv\n"); }

    if (var == "u") {
        if (der == "y") {
            // u_hat
            // 1st derivative
            if (type_y == FOURIER)   { deriv_fft(*(u[0]), *uY_xform, *(u_derivs[Y_IND])); }
            else if (type_y == SINE) { deriv_dct(*(u[0]), *uY_xform, *(u_derivs[Y_IND])); }
            *(u_derivs[Y_IND]) *= norm_y;

            // 2nd derivative
            if (type_y == FOURIER)   { deriv_fft(*(u_derivs[Y_IND]), *uY_xform, *(u_derivs2[Y_IND])); }
            else if (type_y == SINE) { deriv_dst(*(u_derivs[Y_IND]), *uY_xform, *(u_derivs2[Y_IND])); }
            *(u_derivs2[Y_IND]) *= norm_y;

            if (use_zonal_decomp) {
                // u_bar
                // 1st derivative
                if (type_y_bar == FOURIER)     { deriv_fft(*(ubar[0]), *uY_xform_bar, *(u_derivs_bar[Y_IND])); }
                else if (type_y_bar == COSINE) { deriv_dst(*(ubar[0]), *uY_xform_bar, *(u_derivs_bar[Y_IND])); }
                *(u_derivs_bar[Y_IND]) *= norm_y;

                // 2nd derivative
                if (type_y_bar == FOURIER)     { deriv_fft(*(u_derivs_bar[Y_IND]), *uY_xform_bar, *(u_derivs2_bar[Y_IND])); }
                else if (type_y_bar == COSINE) { deriv_dct(*(u_derivs_bar[Y_IND]), *uY_xform_bar, *(u_derivs2_bar[Y_IND])); }
                *(u_derivs2_bar[Y_IND]) *= norm_y;

                // u_laut
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        double yt = ygrid[KK] + Ly/2.;
                        // 1st derivative
                        (*u_derivs_laut[Y_IND] )(rank, JJ, KK) = 3*alaut[JJ]*pow(yt, 2) + 2*blaut[JJ]*yt + claut[JJ];

                        // 2nd derivative
                        (*u_derivs2_laut[Y_IND])(rank, JJ, KK) = 6*alaut[JJ]*yt         + 2*blaut[JJ];
                    }
                }
            }
        }
    }
}
