#include "../qg_functions.hpp"

// Compute requested derivatives
void qg_functions::solnclass::compute_requested_derivatives() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::compute_requested_derivatives\n"); }

    //if (compute_u_derivs[X_IND]) { compute_deriv("u", "x", u_derivs[X_IND], u_derivs_bar[X_IND], u_derivs_laut[X_IND]); }
    if (compute_u_derivs[Y_IND]) { compute_deriv("u", "y"); } 
    //if (compute_u_derivs[Z_IND]) { compute_deriv("u", "z", u_derivs[Z_IND], u_derivs_bar[Z_IND], u_derivs_laut[Z_IND]); }

    //if (compute_v_derivs[X_IND]) { compute_deriv("v", "x", v_derivs[X_IND], v_derivs_bar[X_IND], v_derivs_laut[X_IND]); }
    //if (compute_v_derivs[Y_IND]) { compute_deriv("v", "y", v_derivs[Y_IND], v_derivs_bar[Y_IND], v_derivs_laut[Y_IND]); }
    //if (compute_v_derivs[Z_IND]) { compute_deriv("v", "z", v_derivs[Z_IND], v_derivs_bar[Z_IND], v_derivs_laut[Z_IND]); }

    //if (compute_q_derivs[X_IND]) { compute_deriv("q", "x", q_derivs[X_IND], q_derivs_bar[X_IND], q_derivs_laut[X_IND]); }
    //if (compute_q_derivs[Y_IND]) { compute_deriv("q", "y", q_derivs[Y_IND], q_derivs_bar[Y_IND], q_derivs_laut[Y_IND]); }
    //if (compute_q_derivs[Z_IND]) { compute_deriv("q", "z", q_derivs[Z_IND], q_derivs_bar[Z_IND], q_derivs_laut[Z_IND]); }

}

