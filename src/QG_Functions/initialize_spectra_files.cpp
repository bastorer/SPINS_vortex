#include "../qg_functions.hpp"

void qg_functions::solnclass::initialize_spectra_files() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_spectra_files\n"); }

    N_spect  = min(Nx,Ny)/2;
    Nx_spect = Nx/2;
    Ny_spect = Ny/2;

    //
    //// KE
    //

    // Initialize KE anisotrophy array
    KE_aniso_array = new double[N_spect];
    if (compute_aniso) {
        MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &KE_aniso_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "KE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &KE_aniso_final_file);
    }

    // Initialize 2D KE spectrum (depth integrated)
    KE_2d_spect = new double*[Nx_spect];
    for (int II = 0; II < Nx_spect; II++) { KE_2d_spect[II] = new double[Ny_spect]; }
    MPI_File_open(MPI_COMM_WORLD, "KE_2d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_2d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_2d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &KE_2d_spect_final_file);

    // Initialize azimuthally-integrated KE spectrum (depth integrated)
    KE_1d_spect = new double[N_spect];
    MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_1d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &KE_1d_spect_final_file);

    // Initialize x KE spectrum (yz integrated)
    KE_x_spect = new double[Nx_spect];
    MPI_File_open(MPI_COMM_WORLD, "KE_x_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_x_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_x_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &KE_x_spect_final_file);

    // Initialize y KE spectrum (xz integrated)
    KE_y_spect = new double[Ny_spect];
    MPI_File_open(MPI_COMM_WORLD, "KE_y_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_y_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_y_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &KE_y_spect_final_file);

    //
    //// Now initialize PE
    //

    // Initialize PE anisotrophy array
    PE_aniso_array = new double[N_spect];
    if (compute_aniso) {
        MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &PE_aniso_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "PE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &PE_aniso_final_file);
    }

    // Initialize 2D PE spectrum (depth integrated)
    PE_2d_spect = new double*[Nx_spect];
    for (int II = 0; II < Nx_spect; II++) { PE_2d_spect[II] = new double[Ny_spect]; }
    MPI_File_open(MPI_COMM_WORLD, "PE_2d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_2d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_2d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &PE_2d_spect_final_file);

    // Initialize azimuthally-integrated PE spectrum (depth integrated)
    PE_1d_spect = new double[N_spect];
    MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_1d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &PE_1d_spect_final_file);

    // Initialize x PE spectrum (yz integrated)
    PE_x_spect = new double[Nx_spect];
    MPI_File_open(MPI_COMM_WORLD, "PE_x_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_x_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_x_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &PE_x_spect_final_file);

    // Initialize y PE spectrum (xz integrated)
    PE_y_spect = new double[Ny_spect];
    MPI_File_open(MPI_COMM_WORLD, "PE_y_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_y_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_y_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &PE_y_spect_final_file);


    //
    //// Now initialize Enstrophy (EN)
    //

    // Initialize EN anisotrophy array
    EN_aniso_array = new double[N_spect];
    if (compute_aniso) {
        MPI_File_open(MPI_COMM_WORLD, "EN_aniso_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &EN_aniso_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "EN_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &EN_aniso_final_file);
    }

    // Initialize 2D EN spectrum (depth integrated)
    EN_2d_spect = new double*[Nx_spect];
    for (int II = 0; II < Nx_spect; II++) { EN_2d_spect[II] = new double[Ny_spect]; }
    MPI_File_open(MPI_COMM_WORLD, "EN_2d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &EN_2d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "EN_2d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &EN_2d_spect_final_file);

    // Initialize azimuthally-integrated EN spectrum (depth integrated)
    EN_1d_spect = new double[N_spect];
    MPI_File_open(MPI_COMM_WORLD, "EN_1d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &EN_1d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "EN_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &EN_1d_spect_final_file);

    // Initialize x EN spectrum (yz integrated)
    EN_x_spect = new double[Nx_spect];
    MPI_File_open(MPI_COMM_WORLD, "EN_x_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &EN_x_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "EN_x_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &EN_x_spect_final_file);

    // Initialize y EN spectrum (xz integrated)
    EN_y_spect = new double[Ny_spect];
    MPI_File_open(MPI_COMM_WORLD, "EN_y_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &EN_y_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "EN_y_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
            MPI_INFO_NULL, &EN_y_spect_final_file);

    //
    //// HANDLE THE BIN COUNTS (USEFUL FOR TESTING WITH PARCEVAL AFTER THE FACT)
    //

    // Compute the bin counts for the 1D spectra
    horiz_spectrum_bin_counts(*u[0], KE_1d_spect, KE_x_spect, KE_y_spect, Lx, Ly, FOURIER, type_y_u);

    // azimuthally integrated
    MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts.bin", 
            MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
    if (master()) {
        MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
        MPI_File_write(spect_cnt_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);
    }
    MPI_File_close(&spect_cnt_file);

    // x
    MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts_x.bin", 
            MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
    if (master()) {
        MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
        MPI_File_write(spect_cnt_file, KE_x_spect, N_spect, MPI_DOUBLE, &status);
    }
    MPI_File_close(&spect_cnt_file);

    // y
    MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts_y.bin", 
            MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
    if (master()) {
        MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
        MPI_File_write(spect_cnt_file, KE_y_spect, N_spect, MPI_DOUBLE, &status);
    }
    MPI_File_close(&spect_cnt_file);

}
