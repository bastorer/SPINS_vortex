#include "../qg_functions.hpp"

// Load background profile for computing norms
void qg_functions::solnclass::initialize_background_q(string const &qb_fn) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_background_q\n"); }

    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    qb.resize(1);
    qb[0] = new DTArray(local_lbound,local_extent,local_storage);

    if (master()) 
        fprintf(stdout,"Reading qb from %s\n", qb_fn.c_str());
    read_array_qg(*qb[0],qb_fn.c_str(),Nx,Ny,Nz);
}
