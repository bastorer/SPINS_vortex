#include "../qg_functions.hpp"

// Write initial diagnostics
void qg_functions::solnclass::write_initial_diagnostics(){

    if (debug and master()) { fprintf(stdout, "entering solnclass::wite_initial_diagnostics\n"); }

    if (use_zonal_decomp) { compute_laut_fields(); }
    update_energy_diagnostics();

    // Compute norms
    if (method == "nonlinear") { 
        if (use_zonal_decomp) {
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        //(*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK);
                        (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qlaut[0])(rank,JJ,KK);
                    }
                }
            }
        } else {
            (*work[0]) = (*q[0]);
        }
        if (compute_norms) {
            *work[1] = *(work[0]) - *(qb[0]);
            q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
            q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
        }
    } else {
        if (use_zonal_decomp) {
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qb[0])(II,JJ,KK);
                    }
                }
            }
        } else {
            (*work[0]) = *q[0];
        }
        if (compute_norms) {
            *work[1] = *(work[0]) - *(qb[0]);
            q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]))), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
            q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
        }
    }

    max_q = pvmax(*work[0]);
    min_q = pvmin(*work[0]);
    enstrophy = 0.5*pssum(sum( (*work[0])*(*work[0]) ));

    // Write the diagnostics
    if (master()) {
        fprintf(stdout,"t = %.4g days (%.3g%%, 0 outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                t/86400.,100.*t/tf,KE,PE,dt0);
        fprintf(stdout,"\n");
        diagnostics = fopen("diagnostics_tmp.txt", "a");
        assert(diagnostics);
        fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
        fprintf(diagnostics,", %.16g, %.16g, %.16g", enstrophy, max_q, min_q);
        if (compute_norms) { fprintf(diagnostics,", %.16g, %.16g", q_pert_norm, q_norm); }
        fprintf(diagnostics,", %.16g, %.16g, %.16g", KE_aniso, PE_aniso, EN_aniso);
        fprintf(diagnostics,", %.16g, %.16g, %.16g", Bu, Bu_x, Bu_y);
        fprintf(diagnostics,", %.16g, %.16g, %.16g, %.16g", curr_circ_N, curr_circ_S, init_mass, net_transport);
        fprintf(diagnostics,"\n");
        fclose(diagnostics);
    }

}
