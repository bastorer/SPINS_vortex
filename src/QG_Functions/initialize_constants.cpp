#include "../qg_functions.hpp"

// Initialize various constants
void qg_functions::solnclass::initialize_constants() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_constants\n"); }

    // Compute initial mass
    // Will be used to determine the constant for psi
    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                if (method != "nonlinear") {
                    (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psib[0])(II,JJ,KK);
                } else {
                    (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK);
                }
            }
        }
    }

    if (Nz > 1) {
        dpsidz(*work[0], work[1], Lx, Ly, Lz, Nx, Ny, Nz);
        compute_mass_per_level(work[1], level_mass);
        init_mass = 0;
        for (int II = 0; II < Nz; II++) init_mass += level_mass[II];

    } else {
        init_mass = pssum(sum(*work[0]))*dx*dy;
    }

    // Deformation radius
    Lr = pow(g*H0,0.5)/f0;
    if ((Nz == 1) and master()) { fprintf(stdout, "Deformation radius is: %.6g\n", Lr); } 

    if (use_zonal_decomp) {
        // Now extract the zonal mean
        // This loop block computes the zonal mean
        for (int JJ = q[0]->lbound(secondDim); JJ <= q[0]->ubound(secondDim); JJ++) {
            for (int KK = q[0]->lbound(thirdDim); KK <= q[0]->ubound(thirdDim); KK++) {
                yz_slice[JJ][KK] = 0.;
                for (int II = q[0]->lbound(firstDim); II <= q[0]->ubound(firstDim); II++) {
                    yz_slice[JJ][KK] += (*q[0])(II,JJ,KK)/Nx;
                }
            }
        }
        if (debug and master()) { fprintf(stdout, "Proc %d: initialize_constants_3\n", my_rank); }

        // Now communicate the mean
        sum_reduce_yz_slice();
        MPI_Barrier(MPI_COMM_WORLD);
        if (debug and master()) { fprintf(stdout, "Proc %d: initialize_constants_4\n", my_rank); }

        // Now subtract the mean and record it
        for (int JJ = q[0]->lbound(secondDim); JJ <= q[0]->ubound(secondDim); JJ++) {
            for (int KK = q[0]->lbound(thirdDim); KK <= q[0]->ubound(thirdDim); KK++) {
                for (int II = q[0]->lbound(firstDim); II <= q[0]->ubound(firstDim); II++) {
                    (*q[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) - yz_slice_red[JJ][KK];
                }
                (*qbar[0])(rank,JJ,KK) = yz_slice_red[JJ][KK];
            }
        }
    }
    if (debug and master()) { fprintf(stdout, "Proc %d: initialize_constants_5\n", my_rank); }

    if (method == "nonlinear") { include_bg_in_spectra = 0.; }

    //
    //// HANDLE RESTARTING
    //

    // If we're restarting, determine how what offset to use for the spectra.
    // and other chain diagnostics
    if (restarting) {
        if (Nz > 1) {
            MPI_File_get_size(level_mass_final_file, &old_chain_len);
            prev_chain_write_count = old_chain_len/sizeof(double)/Nz;
        }
        if (Nz == 1) {
            MPI_File_get_size(ubar_final_file, &old_chain_len);
            prev_chain_write_count = old_chain_len/sizeof(double)/Ny;
        }
    } else {
        prev_chain_write_count = 0;
    }
    if (debug and master()) { fprintf(stdout, "Proc %d: initialize_constants_6\n", my_rank); }

    // Initialize necessary derivative fields
    for (int II = 0; II < 3; II++) {
        if (compute_u_derivs[II]) {
            // First derivatives
            u_derivs[II]       = new DTArray(local_lbound,local_extent,local_storage);
            u_derivs_bar[II]   = new DTArray(mlbound, mextent, mstorage);
            u_derivs_laut[II]  = new DTArray(mlbound, mextent, mstorage);
            // Second derivatives
            u_derivs2[II]      = new DTArray(local_lbound,local_extent,local_storage);
            u_derivs2_bar[II]  = new DTArray(mlbound, mextent, mstorage);
            u_derivs2_laut[II] = new DTArray(mlbound, mextent, mstorage);
        }
        if (compute_v_derivs[II]) {
            v_derivs[II]      = new DTArray(local_lbound,local_extent,local_storage);
            v_derivs_bar[II]  = new DTArray(mlbound, mextent, mstorage);
            v_derivs_laut[II] = new DTArray(mlbound, mextent, mstorage);
        }
        if (compute_q_derivs[II]) {
            q_derivs[II]      = new DTArray(local_lbound,local_extent,local_storage);
            q_derivs_bar[II]  = new DTArray(mlbound, mextent, mstorage);
            q_derivs_laut[II] = new DTArray(mlbound, mextent, mstorage);
        }
    }

}

// Function to swap transform types
S_EXP swap_trig( S_EXP the_exp ) {
    if ( the_exp == SINE ) {
        return COSINE; }
    else if ( the_exp == COSINE ) {
        return SINE; }
    else if ( the_exp == FOURIER ) {
        return FOURIER; }
    else if ( the_exp == CHEBY ) {
        return CHEBY; }
    else {
        MPI_Finalize(); exit(1); // stop
    }
}
