#include "../qg_functions.hpp"

void qg_functions::solnclass::finalize(int plot_count, double final_time) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::finalize\n"); }

    // Update the dump file with with a final time too large so
    // that restarting doesn't occur.
    if (master()){
        FILE * dump_file;
        dump_file = fopen("dump_time.txt","w");
        assert(dump_file);
        fprintf(dump_file,"The dump 'time' was:\n%.12g\n", 2*final_time);
        fprintf(dump_file,"The dump index was:\n%d\n", plot_count);
        fclose(dump_file);
    }
    for (int II = 0; II < Nz; II++) {
        delete[] yz_slice[II];
        delete[] yz_slice_red[II];
    }
    delete[] yz_slice;
    delete[] yz_slice_red;
}


