#include "../qg_functions.hpp"

// Load background profiles
void qg_functions::solnclass::initialize_background_states(string const &ub_fn, string const &vb_fn, string const &qxb_fn, 
        string const &qyb_fn, string const &psib_fn) {
    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_background_states\n"); }

    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    ub[0] = new DTArray(local_lbound,local_extent,local_storage);
    vb[0] = new DTArray(local_lbound,local_extent,local_storage);
    qxb[0] = new DTArray(local_lbound,local_extent,local_storage);
    qyb[0] = new DTArray(local_lbound,local_extent,local_storage);
    psib[0] = new DTArray(local_lbound,local_extent,local_storage);

    if (master()) 
        fprintf(stdout,"Reading ub from %s\n", ub_fn.c_str());
    read_array_qg(*ub[0],ub_fn.c_str(),Nx,Ny,Nz);
    if (master()) 
        fprintf(stdout,"Reading vb from %s\n", vb_fn.c_str());
    read_array_qg(*vb[0],vb_fn.c_str(),Nx,Ny,Nz);
    if (master()) 
        fprintf(stdout,"Reading qxb from %s\n", qxb_fn.c_str());
    read_array_qg(*qxb[0],qxb_fn.c_str(),Nx,Ny,Nz);
    if (master()) 
        fprintf(stdout,"Reading qyb from %s\n", qyb_fn.c_str());
    read_array_qg(*qyb[0],qyb_fn.c_str(),Nx,Ny,Nz);
    if (master()) 
        fprintf(stdout,"Reading psib from %s\n", psib_fn.c_str());
    read_array_qg(*psib[0],psib_fn.c_str(),Nx,Ny,Nz);

    // Now determine upper bound of background velocities
    double tmp1, tmp2;
    tmp1 =  pvmax(*ub[0]);
    tmp2 = -pvmin(*ub[0]);
    if (tmp1 > tmp2) U0 = tmp1;
    else U0 = tmp2;

    tmp1 =  pvmax(*vb[0]);
    tmp2 = -pvmin(*vb[0]);
    if (tmp1 > tmp2) V0 = tmp1;
}
