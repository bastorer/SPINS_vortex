#include "../qg_functions.hpp"

void qg_functions::solnclass::dump_if_needed(int plot_count) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::dump_if_needed\n"); }

    write_array_qg(*q[0],"q.dump",-1);

    // Write the dump time to a text file for restart purposes
    if (master()){
        FILE * dump_file;
        dump_file = fopen("dump_time.txt","w");
        assert(dump_file);
        fprintf(dump_file,"The dump time was:\n%.12g\n", t);
        fprintf(dump_file,"The dump index was:\n%d\n", plot_count);
        fclose(dump_file);
    }
    if (master()) fprintf(stdout,"Too close to end of alloted computation time, dump!\n");  

}
