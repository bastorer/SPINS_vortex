#include "../qg_functions.hpp"

void qg_functions::solnclass::initialize_chain_diagnostics_files() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_chain_diagnostics_files\n"); }

    if (Nz > 1) {
        // Initialize level_mass
        level_mass = new double[Nz];
        MPI_File_open(MPI_COMM_WORLD, "level_mass_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &level_mass_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "level_mass.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &level_mass_final_file);
    }

    if ((Nz == 1) and (use_zonal_decomp)) {
        // Initialize ubar, qbar, and psibar files
        temp_bar = new double[Ny];
        MPI_File_open(MPI_COMM_WORLD, "ubar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &ubar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "ubar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &ubar_final_file);

        MPI_File_open(MPI_COMM_WORLD, "qbar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &qbar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "qbar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &qbar_final_file);

        MPI_File_open(MPI_COMM_WORLD, "psibar_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &psibar_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "psibar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                MPI_INFO_NULL, &psibar_final_file);
    }

}

