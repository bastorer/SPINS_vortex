#include "../qg_functions.hpp"

// Initialize the diagnostics file
void qg_functions::solnclass::initialize_diagnostics_files(){

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_diagnostics_files\n"); }

    if (master()) {

        // Only over-write diagnostics.txt if not restarting
        if (!restarting) {
            diagnostics = fopen("diagnostics.txt", "w");
            assert(diagnostics);
            fprintf(diagnostics, "iteration, time, step_time, KE, APE" );
            fprintf(diagnostics, ", enstrophy, max_q, min_q" );
            if (compute_norms) { fprintf(diagnostics, ", q_pert_norm, q_norm" ); } 
            fprintf(diagnostics, ", KE_aniso, PE_aniso, EN_aniso" );
            fprintf(diagnostics, ", Bu, Bu_x, Bu_y" );
            fprintf(diagnostics, ", circ_N, circ_S, mass, net_transport\n" );
        }
        else {
            diagnostics = fopen("diagnostics.txt", "a");
            assert(diagnostics);
        }
        fclose(diagnostics);

        // Always over-write diagnostics_tmp.txt
        diagnostics = fopen("diagnostics_tmp.txt", "w");
        assert(diagnostics);
        fclose(diagnostics);
    }
}
