#include "../qg_functions.hpp"

void qg_functions::solnclass::compute_laut_fields() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::compute_laut_fields\n"); }

    if (use_zonal_decomp) {
        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            if (debug and master()) {
                fprintf(stdout, "compute_laut_fields: alaut = %.6g, blaut = %.6g, claut = %.6g, uS = %.6g, uN = %.6g, t = %.6g\n",
                        alaut[JJ], blaut[JJ], claut[JJ], uS[JJ], uN[JJ], t);
            }
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                double yt = ygrid[KK] + Ly/2.;
                (*ulaut[0])(rank,   JJ, KK) = alaut[JJ]*pow(yt,3) + blaut[JJ]*pow(yt,2) + claut[JJ]*yt + uS[JJ];
                (*psilaut[0])(rank, JJ, KK) = -(alaut[JJ]*pow(yt,4)/4 + blaut[JJ]*pow(yt,3)/3 + claut[JJ]*pow(yt,2)/2 + uS[JJ]*yt);
                if (Nz == 1) {
                    (*qlaut[0])(rank, JJ, KK) = -(3*alaut[JJ]*pow(yt,2) + 2*blaut[JJ]*yt + claut[JJ] + pow(Lr,-2)*(*psilaut[0])(rank,JJ,KK));
                    (*qlauty[0])(rank, JJ, KK) = -(6*alaut[JJ]*yt + 2*blaut[JJ] - pow(Lr,-2)*(*ulaut[0])(rank,JJ,KK));
                }
            }
        }
    }
}
