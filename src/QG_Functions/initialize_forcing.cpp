#include "../qg_functions.hpp"

// Load forcing function if needed
void qg_functions::solnclass::initialize_forcing() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_forcing\n"); }

    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    mlbound = alloc_lbound(num_procs,Nz,Ny);
    mextent = alloc_extent(num_procs,Nz,Ny);
    mstorage = alloc_storage(num_procs,Nz,Ny);

    forcing.resize(1); 
    forcing_bar.resize(1);
    forcing[0] = new DTArray(local_lbound,local_extent,local_storage);
    forcing_bar[0] = new DTArray(mlbound, mextent, mstorage);
}
