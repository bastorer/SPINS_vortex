#include "../qg_functions.hpp"

void qg_functions::solnclass::load_forcing(string const &qforce) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::load_forcing\n"); }

    if (master()) { fprintf(stdout,"Reading q forcing from %s\n", qforce.c_str()); }
    read_array_qg(*forcing[0], qforce.c_str(), Nx, Ny, Nz);

}
