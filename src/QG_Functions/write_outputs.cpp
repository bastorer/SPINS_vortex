#include "../qg_functions.hpp"

// Save state
void qg_functions::solnclass::write_outputs(int iter, bool write_psi, bool write_vels) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::write_outputs\n"); }

    if (use_zonal_decomp) { compute_laut_fields(); }

    if (use_zonal_decomp) {
        // Temporary fix until I figure out how to add 2D to 3D with Blitz
        for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
            for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                    (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qlaut[0])(rank,JJ,KK);
                    (*work[1])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK) + (*psilaut[0])(rank,JJ,KK);
                    (*work[2])(II,JJ,KK) = (*u[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK) + (*ulaut[0])(rank,JJ,KK);
                }
            }
        }
        write_array_qg(*work[0],"q",iter);
        if (!(write_psi == 0)) { write_array_qg(*work[1],"psi",iter); }
        if (!(write_vels == 0)) {
            write_array_qg(*work[2],"u",iter);
            write_array_qg(*v[0],"v",iter);
        }
    } else {
        write_array_qg(*q[0],"q",iter);
        if (!(write_psi == 0)) { write_array_qg(*psi[0],"psi",iter); }
        if (!(write_vels == 0)) {
            write_array_qg(*u[0],"u",iter);
            write_array_qg(*v[0],"v",iter);
        }
    }

}
