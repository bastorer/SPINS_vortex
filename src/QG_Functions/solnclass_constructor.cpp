#include "../qg_functions.hpp"

// Constructor method
qg_functions::solnclass::solnclass(int sx, int sy, int sz, double lx, double ly, double lz,
        string const &flux_method, string const &ygrid_type, string const &zgrid_type,
        bool zonal_decomp) {

    debug = false;

    if (debug and master()) { fprintf(stdout, "entering solnclass::solnclass (constructor)\n"); }

    start_clock_time = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&num_procs);

    all = blitz::Range::all();

    X_IND = 0;
    Y_IND = 2;
    Z_IND = 1;

    Nx = sx;
    Ny = sy;
    Nz = sz;

    Lx = lx;
    Ly = ly;
    Lz = lz;

    xgrid = new double[Nx];
    ygrid = new double[Ny];
    zgrid = new double[Nz];
    for (int II = 0; II < Nx; II++) { xgrid[II] = (II+0.5)/Nx*Lx - Lx/2.; }
    for (int JJ = 0; JJ < Ny; JJ++) { ygrid[JJ] = (JJ+0.5)/Ny*Ly - Ly/2.; }
    for (int KK = 0; KK < Nz; KK++) { zgrid[KK] = (KK+0.5)/Nz*Lz - Lz; }

    w0 = 0.;
    w1 = 0.;
    w2 = 0.;

    do_forcing = false; // Default to no forcing.
    use_zonal_decomp = zonal_decomp;

    yz_slice = new double*[Nz];
    yz_slice_red = new double*[Nz];
    for (int II = 0; II < Nz; II++) {
        yz_slice[II] = new double[Ny];
        yz_slice_red[II] = new double[Ny];
        for (int JJ = 0; JJ < Ny; JJ++) {
            yz_slice[II][JJ] = 0.;
            yz_slice_red[II][JJ] = 0.;
        }
    }

    uS = new double[Nz];
    uN = new double[Nz];
    alaut = new double[Nz];
    blaut = new double[Nz];
    claut = new double[Nz];
    for (int II = 0; II < Nz; II++) {
        uS[II] = 0.;
        uN[II] = 0.;
        alaut[II] = 0.;
        blaut[II] = 0.;
        claut[II] = 0.;
    }

    dx = Lx/Nx;
    dy = Ly/Ny;
    dz = Lz/Nz;

    q_pert_norm = 0.0;
    q_norm = 0.0;

    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    mlbound = alloc_lbound(num_procs,Nz,Ny);
    mextent = alloc_extent(num_procs,Nz,Ny);
    mstorage = alloc_storage(num_procs,Nz,Ny);

    q.resize(1);
    psi.resize(1);
    u.resize(1);
    v.resize(1);
    qx.resize(1);
    qy.resize(1);

    // These are for the background state
    // and are needed for the nonlinear_pert
    // and linear flux functions. They are
    // initialized here 
    ub.resize(1);
    vb.resize(1);
    qxb.resize(1);
    qyb.resize(1);
    psib.resize(1);

    q[0]   = new DTArray(local_lbound,local_extent,local_storage);
    psi[0] = new DTArray(local_lbound,local_extent,local_storage);
    u[0]   = new DTArray(local_lbound,local_extent,local_storage);
    v[0]   = new DTArray(local_lbound,local_extent,local_storage);
    qx[0]  = new DTArray(local_lbound,local_extent,local_storage);
    qy[0]  = new DTArray(local_lbound,local_extent,local_storage);

    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                (*q[0])(II,JJ,KK) = 0.;
                (*psi[0])(II,JJ,KK) = 0.;
                (*u[0])(II,JJ,KK) = 0.;
                (*v[0])(II,JJ,KK) = 0.;
                (*qx[0])(II,JJ,KK) = 0.;
                (*qy[0])(II,JJ,KK) = 0.;
            }
        }
    }

    qbar.resize(1);
    qbar_flux.resize(3);
    qbary.resize(1);
    ubar.resize(1);
    psibar.resize(1);
    meanvq.resize(1);

    qlaut.resize(1);
    qlauty.resize(1);
    ulaut.resize(1);
    psilaut.resize(1);

    if (use_zonal_decomp) {
        qbar[0] = new DTArray(mlbound, mextent, mstorage);
        qbar_flux[0] = new DTArray(mlbound, mextent, mstorage);
        qbar_flux[1] = new DTArray(mlbound, mextent, mstorage);
        qbar_flux[2] = new DTArray(mlbound, mextent, mstorage);
        qbary[0] = new DTArray(mlbound, mextent, mstorage);
        ubar[0] = new DTArray(mlbound, mextent, mstorage);
        psibar[0] = new DTArray(mlbound, mextent, mstorage);
        meanvq[0] = new DTArray(mlbound, mextent, mstorage);

        qlaut[0] = new DTArray(mlbound, mextent, mstorage);
        qlauty[0] = new DTArray(mlbound, mextent, mstorage);
        ulaut[0] = new DTArray(mlbound, mextent, mstorage);
        psilaut[0] = new DTArray(mlbound, mextent, mstorage);

        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                (*qbar[0])(rank,JJ,KK) = 0.;
                (*qbar_flux[0])(rank,JJ,KK) = 0.;
                (*qbar_flux[1])(rank,JJ,KK) = 0.;
                (*qbar_flux[2])(rank,JJ,KK) = 0.;
                (*qbary[0])(rank,JJ,KK) = 0.;
                (*ubar[0])(rank,JJ,KK) = 0.;
                (*psibar[0])(rank,JJ,KK) = 0.;
                (*meanvq[0])(rank,JJ,KK) = 0.;

                (*qlaut[0])(rank,JJ,KK) = 0.;
                (*qlauty[0])(rank,JJ,KK) = 0.;
                (*ulaut[0])(rank,JJ,KK) = 0.;
                (*psilaut[0])(rank,JJ,KK) = 0.;
            }
        }
    }

    work.resize(3);
    work[0] = new DTArray(local_lbound,local_extent,local_storage);
    work[1] = new DTArray(local_lbound,local_extent,local_storage);
    work[2] = new DTArray(local_lbound,local_extent,local_storage);

    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                (*work[0])(II,JJ,KK) = 0.;
                (*work[1])(II,JJ,KK) = 0.;
                (*work[2])(II,JJ,KK) = 0.;
            }
        }
    }

    work_bar.resize(1);
    if (use_zonal_decomp) {
        work_bar[0] = new DTArray(mlbound, mextent, mstorage);
    }

    dt0 = 1.0;
    dt1 = 1.0;
    dt2 = 1.0;
    do_plot = false;

    U0 = 0.0;
    V0 = 0.0;
    kappa = 0.0;
    g = 9.81;

    iterct = 0;

    next_diag_time = 0.0;
    chain_write_count = 0;
    rho0 = 1000.;
    vol  = Lx*Ly*Lz/(Nx*Ny*Nz);
    KE = 0.;
    PE = 0.;
    TE = 0.;
    enstrophy = 0.;
    KE_aniso = 0.;
    PE_aniso = 0.;
    EN_aniso = 0.;
    Bu = 0.;

    include_bg_in_spectra = 0.;

    method = flux_method;

    if (ygrid_type == "PERIODIC") {
        type_y = FOURIER;
        type_y_bar = FOURIER;
        type_y_u = FOURIER;
        //if (use_zonal_decomp) {
        //    if (master()) fprintf(stdout, "Doubly-periodic geometry, disabling zonal decomposition.\n");
        //    use_zonal_decomp = false;
        //}
    } else if (ygrid_type == "FREE_SLIP") {
        type_y = SINE;
        type_y_bar = COSINE;
        type_y_u = COSINE;
    } else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    if (zgrid_type == "FOURIER") {
        type_z = FOURIER;
    } else if (zgrid_type == "REAL") { 
        type_z = COSINE;
    } else if (zgrid_type == "CHEB") {
        type_z = NONE;
    } else if (zgrid_type == "NONE") {
        type_z = NONE;
    } else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
        MPI_Finalize(); exit(1);
    }


    // Derivative flags
    compute_u_derivs.resize(3);
    compute_v_derivs.resize(3);
    compute_q_derivs.resize(3);

    for (int II = 0; II < 3; II++) {
        compute_u_derivs[II] = false;
        compute_v_derivs[II] = false;
        compute_q_derivs[II] = false;
    }

    u_derivs.resize(3);
    u_derivs_bar.resize(3);
    u_derivs_laut.resize(3);
    u_derivs2.resize(3);
    u_derivs2_bar.resize(3);
    u_derivs2_laut.resize(3);
    v_derivs.resize(3);
    v_derivs_bar.resize(3);
    v_derivs_laut.resize(3);
    q_derivs.resize(3);
    q_derivs_bar.resize(3);
    q_derivs_laut.resize(3);

}
