#include "../qg_functions.hpp"

// Sum-reduce yz_slice
void qg_functions::solnclass::sum_reduce_yz_slice() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::sum_reduce_yz_slice\n"); }

    for (int II = 0; II < Nz; II++) {
        MPI_Allreduce(yz_slice[II], yz_slice_red[II], Ny, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    }

    if (debug and master()) { fprintf(stdout, "leaving solnclass::sum_reduce_yz_slice\n"); }
}
