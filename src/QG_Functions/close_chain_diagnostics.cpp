#include "../qg_functions.hpp"

void qg_functions::solnclass::close_chain_diagnostics() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::close_chain_diagnostics\n"); }

    if (Nz > 1) {
        MPI_File_close(&level_mass_temp_file);
        MPI_File_close(&level_mass_final_file);
    }

    if ((Nz == 1) and (use_zonal_decomp)) {
        MPI_File_close(&ubar_temp_file);
        MPI_File_close(&ubar_final_file);

        MPI_File_close(&qbar_temp_file);
        MPI_File_close(&qbar_final_file);

        MPI_File_close(&psibar_temp_file);
        MPI_File_close(&psibar_final_file);
    }
}

