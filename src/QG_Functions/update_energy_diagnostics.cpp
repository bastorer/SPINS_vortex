#include "../qg_functions.hpp"

void qg_functions::solnclass::update_energy_diagnostics() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::update_energy_diagnostics\n"); }

    // If Nz == 1, write ubar, qbar, and psibar
    if ((Nz == 1) and (master()) and (use_zonal_decomp)) {
        for (int JJ = 0; JJ < Ny; JJ++) {
            temp_bar[JJ] = (*ubar[0])(0,0,JJ) + (*ulaut[0])(0,0,JJ);
        }
        MPI_File_seek(ubar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(ubar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);

        for (int JJ = 0; JJ < Ny; JJ++) {
            temp_bar[JJ] = (*qbar[0])(0,0,JJ) + (*qlaut[0])(0,0,JJ);
        }
        MPI_File_seek(qbar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(qbar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);

        for (int JJ = 0; JJ < Ny; JJ++) {
            temp_bar[JJ] = (*psibar[0])(0,0,JJ) + (*psilaut[0])(0,0,JJ);
        }
        MPI_File_seek(psibar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(psibar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
    }

    // KE
    if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> start KE\n"); }
    if (method != "nonlinear") {
        *work[0] = *(u[0]) + *(ub[0]);
        *work[1] = *(v[0]) + *(vb[0]);
    } else {
        *work[0] = *(u[0]);
        *work[1] = *(v[0]);
    }
    if (use_zonal_decomp) {
        for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
            for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                    (*work[0])(II,JJ,KK) = (*work[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK) + (*ulaut[0])(rank,JJ,KK);
                }
            }
        }
    }
    net_transport = pssum(sum(*work[0]))*dx*dy*dz;

    KE   = 0.5*rho0*vol*pssum(sum(((*work[0])*(*work[0]) + (*work[1])*(*work[1]))));
    KE_x = 0.5*rho0*vol*pssum(sum(((*work[0])*(*work[0]))));
    KE_y = 0.5*rho0*vol*pssum(sum(((*work[1])*(*work[1]))));

    // Compute circulation at North and South walls if appropriate
    if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> circulation\n"); }
    if (type_y_u == COSINE) {
        curr_circ_N = pssum(sum((*work[0])(all,all,Ny-1))); 
        curr_circ_S = pssum(sum((*work[0])(all,all,0))); 
        if (Nz > 1) {
            curr_circ_N *= dx*dz;
            curr_circ_S *= dx*dz;
        }
        else{
            curr_circ_N *= dx;
            curr_circ_S *= dx;
        }
    }
    else {
        curr_circ_N = 0.;
        curr_circ_S = 0.;
    }

    if (compute_spectra) {
        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> KE spectra\n"); }

        // 1. Compute the spectra
        KE_spect(&KE_aniso, KE_aniso_array, u[0], v[0], ub[0], 
                vb[0], ubar[0], work[0], work_bar[0], use_zonal_decomp,
                work[1], KE_1d_spect, KE_x_spect, KE_y_spect, 
                include_bg_in_spectra, compute_aniso, Lx, Ly, type_y,
                fcutoff);

        // 2. Scale the spectra
        for (int II = 0; II < N_spect; II++) {
            KE_1d_spect[II] *= 0.5*rho0;
        }
        for (int II = 0; II < Nx_spect; II++) {
            KE_x_spect[II]  *= 0.5*rho0;
            for (int JJ = 0; JJ < Ny_spect; JJ++) {
                KE_2d_spect[II][JJ] *= 0.5*rho0;
            }
        }
        for (int II = 0; II < Ny_spect; II++) {
            KE_y_spect[II]  *= 0.5*rho0;
        }

        // 3. Write spectra to a file
        if (master()) {
            if (compute_aniso) {
                MPI_File_seek(KE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_aniso_temp_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);
            }

            MPI_File_seek(KE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(KE_1d_spect_temp_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);

            MPI_File_seek(KE_x_spect_temp_file, Nx_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(KE_x_spect_temp_file, KE_x_spect, Nx_spect, MPI_DOUBLE, &status);

            MPI_File_seek(KE_y_spect_temp_file, Ny_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(KE_y_spect_temp_file, KE_y_spect, Ny_spect, MPI_DOUBLE, &status);
        }
    }

    // PE
    if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> start PE\n"); }
    if (use_zonal_decomp) {
        for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
            for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                    if ( (method != "nonlinear") and (include_bg_in_spectra > 0.1) ) {
                        (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK) + (*psib[0])(II,JJ,KK);
                    } else {
                        (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK) + (*psilaut[0])(rank,JJ,KK);
                    }
                }
            }
        }
    } else {
        if ( (method != "nonlinear") and (include_bg_in_spectra > 0.1) ) {
            (*work[0]) = (*psi[0]) + (*psib[0]);
        } else {
            (*work[0]) = (*psi[0]);
        }
    }

    if (Nz > 1) {
        dpsidz(*work[0], work[1], Lx, Ly, Lz, Nx, Ny, Nz);
        PE = 0.5*rho0*f0*f0/(N0*N0)*vol*pssum(sum((*work[1])*(*work[1]))); // 0.5*rho0*(dpsidz*f0/N0)^2

        compute_mass_per_level(work[1], level_mass); // For the sake of curiosity

        if (master()) {
            MPI_File_seek(level_mass_temp_file, Nz*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(level_mass_temp_file, level_mass, Nz, MPI_DOUBLE, &status);
        }

        curr_mass = 0;
        for (int II = 0; II < Nz; II++) curr_mass += level_mass[II];

    } else {
        PE = 0.5*rho0*vol*pssum(sum((*work[0])*(*work[0])))/(Lr*Lr);
        curr_mass = pssum(sum(*work[0]))*dx*dy;
    }

    if (compute_spectra) {

        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> PE spect\n"); }
        // 1. Compute the spectra
        PE_spect(&PE_aniso, PE_aniso_array, psi[0], psib[0], psibar[0], 
                work[0], work[1], work_bar[0], use_zonal_decomp, 
                work[2], PE_1d_spect, PE_x_spect, PE_y_spect,
                include_bg_in_spectra, compute_aniso, Lx, Ly, Lz, f0, N0, type_y,
                fcutoff);

        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> scale PE spect\n"); }
        // 2. Scale the spectra
        PE_scale = (Nz > 1) ? 0.5*rho0*f0*f0/(N0*N0) : 0.5*rho0/(Lr*Lr);
        for (int II = 0; II < N_spect; II++) {
            PE_1d_spect[II] *= PE_scale;
        }
        for (int II = 0; II < Nx_spect; II++) {
            PE_x_spect[II]  *= PE_scale;
            for (int JJ = 0; JJ < Ny_spect; JJ++) {
                PE_2d_spect[II][JJ] *= PE_scale;
            }
        }
        for (int II = 0; II < Ny_spect; II++) {
            PE_y_spect[II]  *= PE_scale;
        }

        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> write PE spect\n"); }
        // 3. Write spectra to a file
        if (master()) {
            if (compute_aniso) {
                MPI_File_seek(PE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_aniso_temp_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
            }

            MPI_File_seek(PE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(PE_1d_spect_temp_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);

            MPI_File_seek(PE_x_spect_temp_file, Nx_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(PE_x_spect_temp_file, PE_x_spect, Nx_spect, MPI_DOUBLE, &status);

            MPI_File_seek(PE_y_spect_temp_file, Ny_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(PE_y_spect_temp_file, PE_y_spect, Ny_spect, MPI_DOUBLE, &status);
        }
    }

    // Compute Burger number(s)
    Bu = 0.5*KE/PE;
    Bu_x = KE_x/PE;
    Bu_y = KE_y/PE;

    // Enstrophy
    if (compute_spectra) {

        // 1. Compute the spectra
        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> Enstrophy_spect\n"); }
        // qb[0] isn't defined for unclude_bg_in_spectra = 0,
        //   so in that case we need to pass something else as filler
        if (include_bg_in_spectra > 0) {
            Enstrophy_spect(&EN_aniso, EN_aniso_array, q[0], qb[0], qbar[0], 
                    work[0], work_bar[0], use_zonal_decomp, 
                    work[2], EN_1d_spect, EN_x_spect, EN_y_spect,
                    include_bg_in_spectra, compute_aniso, Lx, Ly, type_y,
                    fcutoff);
        }
        else {
            Enstrophy_spect(&EN_aniso, EN_aniso_array, q[0], q[0], qbar[0], 
                    work[0], work_bar[0], use_zonal_decomp, 
                    work[2], EN_1d_spect, EN_x_spect, EN_y_spect,
                    include_bg_in_spectra, compute_aniso, Lx, Ly, type_y,
                    fcutoff);
        }
            

        // 2. Scale spectra
        for (int II = 0; II < N_spect; II++) {
            EN_1d_spect[II] *= 0.5;
        }
        for (int II = 0; II < Nx_spect; II++) {
            EN_x_spect[II]  *= 0.5;
            for (int JJ = 0; JJ < Ny_spect; JJ++) {
                EN_2d_spect[II][JJ] *= 0.5;
            }
        }
        for (int II = 0; II < Ny_spect; II++) {
            EN_y_spect[II]  *= 0.5;
        }
        
        // 3. Write spectra to a file
        if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> write enstrophy_spect to file\n"); }
        if (master()) {
            if (compute_aniso) {
                MPI_File_seek(EN_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(EN_aniso_temp_file, EN_aniso_array, N_spect, MPI_DOUBLE, &status);
            }

            MPI_File_seek(EN_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(EN_1d_spect_temp_file, EN_1d_spect, N_spect, MPI_DOUBLE, &status);

            MPI_File_seek(EN_x_spect_temp_file, Nx_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(EN_x_spect_temp_file, EN_x_spect, Nx_spect, MPI_DOUBLE, &status);

            MPI_File_seek(EN_y_spect_temp_file, Ny_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(EN_y_spect_temp_file, EN_y_spect, Ny_spect, MPI_DOUBLE, &status);
        }
    }

    chain_write_count += 1;
}

