#include "../qg_functions.hpp"

// Update after step
void qg_functions::solnclass::update_after_step(int plot_count){

    if (debug and master()) { fprintf(stdout, "entering solnclass::update_after_step\n"); }

    iterct++;
    t = t + dt0;
    curr_clock_time = MPI_Wtime();
    step_time = curr_clock_time - start_step_time;

    if (use_zonal_decomp) { compute_laut_fields(); }

    // Correct mean(psi)
    // Since only the gradient of psi occurs in
    // the evolution equation, we restrict the
    // mean of psi to conserve mass on each level
    // This is to ensure that each processor has the same
    // psibar_bar
    if (method != "nonlinear") {
        (*work[0]) = (*psi[0]) + (*psib[0]);
    } else {
        (*work[0]) = (*psi[0]);
    }

    if (Nz > 1) {
        dpsidz(*work[0], work[1], Lx, Ly, Lz, Nx, Ny, Nz);

        compute_mass_per_level(work[1], level_mass);
        curr_mass = 0;
        for (int II = 0; II < Nz; II++) curr_mass += level_mass[II];

    } else {
        curr_mass = pssum(sum(*work[0]))*dx*dy;
    }

    if (use_zonal_decomp) {
        // Update psibar to have the appropriate mean
        double delta_mass_per_bar_point = ((init_mass - curr_mass)/(dy*dx))/(Ny*Nx);
        for (int JJ = psibar[0]->lbound(secondDim); JJ <= psibar[0]->ubound(secondDim); JJ++) {
            psibar_bar = 0;
            for (int KK = psibar[0]->lbound(thirdDim); KK <= psibar[0]->ubound(thirdDim); KK++) {
                psibar_bar += (*psibar[0])(rank,JJ,KK);
            }
            for (int KK = psibar[0]->lbound(thirdDim); KK <= psibar[0]->ubound(thirdDim); KK++) {
                (*psibar[0])(rank,JJ,KK) = (*psibar[0])(rank,JJ,KK) - psibar_bar/Ny + delta_mass_per_bar_point;
            }

            // Now, have master transmit to the other processors
            // This shouldn't be necessary, but it seems that it is.
            if (master()) {
                for (int KK = 0; KK < Ny; KK++) {
                    yz_slice[0][KK] = (*psibar[0])(rank,JJ,KK);
                }
            }
            MPI_Bcast(yz_slice[0], Ny, MPI_DOUBLE, 0, MPI_COMM_WORLD);
            for (int KK = 0; KK < Ny; KK++) {
                (*psibar[0])(rank,JJ,KK) = yz_slice[0][KK];
            }
        }
    }


    // If appropriate, do diagnostics
    // Note that tdiag = 0 (the default) will always satisfy this
    if (next_diag_time - t <= 0.0001*tdiag) {

        if (use_zonal_decomp) { compute_laut_fields(); }
        update_energy_diagnostics();

        // Compute norms
        if (method == "nonlinear") { 
            if (use_zonal_decomp) {
                for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                    for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                        for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                            (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qlaut[0])(rank,JJ,KK);
                        }
                    }
                }
            } else {
                *work[0] = *q[0];
            }

            if (compute_norms) { *work[1] = *(work[0]) - *(qb[0]); }

        } else {
            if (use_zonal_decomp) {
                for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                    for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                        for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                            (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qb[0])(II,JJ,KK);
                        }
                    }
                }
            } else {
                *work[0] = *q[0] + *qb[0]; 
            }

            if (compute_norms) { *work[1] = *(work[0]) - *(qb[0]); }

        }

        if (compute_norms) {
            q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]))), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
            q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
        }

        max_q = pvmax(*work[0]);
        min_q = pvmin(*work[0]);
        /*
        for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
            for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                    (*work[0])(II,JJ,KK) = (*work[0])(II,JJ,KK) + beta*ygrid[KK];
                }
            }
        }
        */
        enstrophy = 0.5*vol*pssum(sum( (*work[0])*(*work[0]) ));

        // Write the diagnostics
        if (master()) {
            fprintf(stdout,"t = %.4g days (%.3g%%, %d outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                    t/86400.,100.*t/tf,plot_count-1,KE,PE,dt0);
            fprintf(stdout,"\n");
            diagnostics = fopen("diagnostics_tmp.txt", "a");
            assert(diagnostics);
            fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
            fprintf(diagnostics,", %.16g, %.16g, %.16g", enstrophy, max_q, min_q);
            if (compute_norms) { fprintf(diagnostics,", %.16g, %.16g", q_pert_norm, q_norm); }
            fprintf(diagnostics,", %.16g, %.16g, %.16g", KE_aniso, PE_aniso, EN_aniso);
            fprintf(diagnostics,", %.16g, %.16g, %.16g", Bu, Bu_x, Bu_y);
            fprintf(diagnostics,", %.16g, %.16g, %.16g, %.16g", curr_circ_N, curr_circ_S, curr_mass, net_transport);
            fprintf(diagnostics,"\n");
            fclose(diagnostics);
        }

        // Update next diag time
        next_diag_time += tdiag;
    }
}
