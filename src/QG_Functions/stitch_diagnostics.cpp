#include "../qg_functions.hpp"

void qg_functions::solnclass::stitch_diagnostics() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::stitch_diagnostics\n"); }
    if (master()) {
        diagnostics_tmp.open ("diagnostics_tmp.txt");
        diagnostics = fopen("diagnostics.txt","a");
        assert(diagnostics);

        // Copy everything from diagnostics_tmp.txt to diagnostics.txt
        while (getline (diagnostics_tmp,diag_tmp) ) {
            fprintf(diagnostics, diag_tmp.c_str()); 
            fprintf(diagnostics, "\n"); 
        }

        fclose(diagnostics);
        diagnostics_tmp.close();

        // Now wipe the diagnostics_tmp.txt file
        diagnostics = fopen("diagnostics_tmp.txt","w");
        assert(diagnostics);
        fclose(diagnostics);
    }
}
