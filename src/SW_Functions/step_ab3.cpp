#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;

// Compute the weights for the AB3 method
void compute_weights3(solnclass & soln) {
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_weights3\n"); }

    double a,b,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn = soln.t;

    a  = soln.t - soln.dt1;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w0 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t - soln.dt1;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w1 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1;
    ts = soln.t - soln.dt1 - soln.dt2;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w2 = w;


    return;

}


// Apply the AB3 step
void step_ab3(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_ab3\n"); }

    // Make nice references for arrays
    DTArray &eta     = *soln.eta[0], &u     = *soln.u[0], &v     = *soln.v[0]; 
    DTArray &fluxeta = *flux.eta[2], &fluxu = *flux.u[2], &fluxv = *flux.v[2];

    // Compute new timestep, full dt for ab3
    compute_dt(soln);
    compute_weights3(soln);

    // Compute flux
    method.flux(soln, fluxu, fluxv, fluxeta, Sz);

    // Advance solution
    u   = u   - soln.w0*fluxu   - soln.w1*(*flux.u[1]  ) - soln.w2*(*flux.u[0]);
    v   = v   - soln.w0*fluxv   - soln.w1*(*flux.v[1]  ) - soln.w2*(*flux.v[0]);
    eta = eta - soln.w0*fluxeta - soln.w1*(*flux.eta[1]) - soln.w2*(*flux.eta[0]);

    // Exponential Filter
    apply_filter(soln, Sz);

    // Record fluxes for future
    // flux[0] has the oldest, flux[2] has the newest
    *flux.u[0] = *flux.u[1];
    *flux.u[1] = fluxu; 

    *flux.v[0] = *flux.v[1];
    *flux.v[1] = fluxv; 

    *flux.eta[0] = *flux.eta[1];
    *flux.eta[1] = fluxeta; 
}

