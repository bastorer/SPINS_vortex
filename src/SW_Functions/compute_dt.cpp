#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;

// Apply the CFL condition to compute dt
void compute_dt(solnclass & soln) {  
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_dt\n"); }

    // Compute restrictions
    double tmp1, tmp2;
    double max_u, max_v;
    double dt;

    double c0 = sqrt(soln.g * soln.H0);

    tmp1 =  pvmax(*soln.u[0]);
    tmp2 = -pvmin(*soln.u[0]);
    max_u = max(tmp1,tmp2) + soln.U0 + 2*c0;

    tmp1 =  pvmax(*soln.v[0]);
    tmp2 = -pvmin(*soln.v[0]);
    max_v = max(tmp1, tmp2) + soln.V0 + 2*c0;

    dt = min(soln.dx/max_u, soln.dy/max_v)/5.;

    // For minimum, use 1% of gravity wave timestep
    double dt_min = (min(soln.dx, soln.dy) / c0) / 200.;

    if (dt < dt_min) {
        if (master()) {
            fprintf(stderr, "Time step is %g seconds, too small to continue. Force exit!\n",dt);
            fprintf(stderr, "max_u = %.12g, max_v = %.12g\n", max_u, max_v);
        }
        MPI_Finalize(); exit(1);
    }
    else if(dt > 15000.0) { // BAS: Why 15000?
        dt = 15000.0;
    }

    if (dt + soln.t >= soln.next_plot_time) {
        dt  = soln.next_plot_time - soln.t;
        soln.do_plot = true;
    }

    soln.dt2 = soln.dt1;
    soln.dt1 = soln.dt0;
    soln.dt0 = dt;

    return;
}
