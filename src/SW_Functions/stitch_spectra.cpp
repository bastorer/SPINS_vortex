#include "../sw_functions.hpp"

void sw_functions::solnclass::stitch_spectra() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::stitch_spectra\n"); }

    for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
        
        //
        //// KE
        //

        offset_from = JJ*N_spect;
        offset_to   = (JJ + prev_chain_write_count)*N_spect;

        // KE anisotropy
        if (compute_aniso) {
            MPI_File_seek(KE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(KE_aniso_temp_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);
            MPI_File_seek(KE_aniso_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(KE_aniso_final_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);
        }

        // azimuthally average KE spectrum
        MPI_File_seek(KE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(KE_1d_spect_temp_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);
        MPI_File_seek(KE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(KE_1d_spect_final_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);

        offset_from = JJ*Nx_spect;
        offset_to   = (JJ + prev_chain_write_count)*Nx_spect;

        // x KE spectrum
        MPI_File_seek(KE_x_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(KE_x_spect_temp_file, KE_x_spect, Nx_spect, MPI_DOUBLE, &status);
        MPI_File_seek(KE_x_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(KE_x_spect_final_file, KE_x_spect, Nx_spect, MPI_DOUBLE, &status);

        offset_from = JJ*Ny_spect;
        offset_to   = (JJ + prev_chain_write_count)*Ny_spect;

        // y KE spectrum
        MPI_File_seek(KE_y_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(KE_y_spect_temp_file, KE_y_spect, Ny_spect, MPI_DOUBLE, &status);
        MPI_File_seek(KE_y_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(KE_y_spect_final_file, KE_y_spect, Ny_spect, MPI_DOUBLE, &status);

        offset_from = JJ*N_spect;
        offset_to   = (JJ + prev_chain_write_count)*N_spect;

        //
        //// PE
        //

        // PE anisotropy
        if (compute_aniso) {
            MPI_File_seek(PE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
            MPI_File_read(PE_aniso_temp_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
            MPI_File_seek(PE_aniso_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
            MPI_File_write(PE_aniso_final_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
        }

        // azimuthally averaged PE spectrum
        MPI_File_seek(PE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(PE_1d_spect_temp_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);
        MPI_File_seek(PE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(PE_1d_spect_final_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);

        offset_from = JJ*Nx_spect;
        offset_to   = (JJ + prev_chain_write_count)*Nx_spect;

        // x PE spectrum
        MPI_File_seek(PE_x_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(PE_x_spect_temp_file, PE_x_spect, Nx_spect, MPI_DOUBLE, &status);
        MPI_File_seek(PE_x_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(PE_x_spect_final_file, PE_x_spect, Nx_spect, MPI_DOUBLE, &status);

        offset_from = JJ*Ny_spect;
        offset_to   = (JJ + prev_chain_write_count)*Ny_spect;

        // y PE spectrum
        MPI_File_seek(PE_y_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
        MPI_File_read(PE_y_spect_temp_file, PE_y_spect, Ny_spect, MPI_DOUBLE, &status);
        MPI_File_seek(PE_y_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
        MPI_File_write(PE_y_spect_final_file, PE_y_spect, Ny_spect, MPI_DOUBLE, &status);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    // KE
    if (compute_aniso) {
        MPI_File_close(&KE_aniso_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &KE_aniso_temp_file);
    }

    MPI_File_close(&KE_1d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_1d_spect_temp_file);

    MPI_File_close(&KE_x_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_x_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_x_spect_temp_file);

    MPI_File_close(&KE_y_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "KE_y_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &KE_y_spect_temp_file);

    // PE
    if (compute_aniso) {
        MPI_File_close(&PE_aniso_temp_file);
        MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", 
                MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                MPI_INFO_NULL, &PE_aniso_temp_file);
    }

    MPI_File_close(&PE_1d_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_1d_spect_temp_file);

    MPI_File_close(&PE_x_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_x_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_x_spect_temp_file);

    MPI_File_close(&PE_y_spect_temp_file);
    MPI_File_open(MPI_COMM_WORLD, "PE_y_spect_temp.bin", 
            MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
            MPI_INFO_NULL, &PE_y_spect_temp_file);

}
