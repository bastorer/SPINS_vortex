#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;

void apply_filter(solnclass & soln, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering apply_filter\n"); }

    DTArray &u   = *soln.u[0];
    DTArray &v   = *soln.v[0];
    DTArray &eta = *soln.eta[0];

    TransWrapper &XYZ_xform = *Sz.XYZ_xform;

    double &fcut = soln.fcutoff,
           &ford = soln.forder,
           &fstr = soln.fstrength;

    filter3(u,   XYZ_xform, Sz.type_x, NONE, Sz.type_y, fcut, ford, fstr); 
    filter3(v,   XYZ_xform, Sz.type_x, NONE, Sz.type_y, fcut, ford, fstr); 
    filter3(eta, XYZ_xform, Sz.type_x, NONE, Sz.type_y, fcut, ford, fstr); 

}

