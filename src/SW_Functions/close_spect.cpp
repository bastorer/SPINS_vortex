#include "../sw_functions.hpp"

void sw_functions::solnclass::close_spect() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::close_spect\n"); }

    //
    //// Spectra
    //

    // KE
    MPI_File_close(&KE_1d_spect_temp_file);
    MPI_File_close(&KE_1d_spect_final_file);
    MPI_File_close(&KE_x_spect_temp_file);
    MPI_File_close(&KE_x_spect_final_file);
    MPI_File_close(&KE_y_spect_temp_file);
    MPI_File_close(&KE_y_spect_final_file);

    // PE
    MPI_File_close(&PE_1d_spect_temp_file);
    MPI_File_close(&PE_1d_spect_final_file);
    MPI_File_close(&PE_x_spect_temp_file);
    MPI_File_close(&PE_x_spect_final_file);
    MPI_File_close(&PE_y_spect_temp_file);
    MPI_File_close(&PE_y_spect_final_file);

    //
    //// Anisotropy
    //
    if (compute_aniso) {
        // KE
        MPI_File_close(&KE_aniso_temp_file);
        MPI_File_close(&KE_aniso_final_file);
        
        // PE
        MPI_File_close(&PE_aniso_temp_file);
        MPI_File_close(&PE_aniso_final_file);
    }
}

