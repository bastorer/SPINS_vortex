#include "../sw_functions.hpp"

// Save state
void sw_functions::solnclass::write_outputs(int iter) {

    if (debug and master()) { fprintf(stdout, "entering solnclass::write_outputs\n"); }

    write_array_sw(*eta[0], "eta", iter);
    write_array_sw(*u[0],   "u", iter);
    write_array_sw(*v[0],   "v", iter);

}
