#include "../sw_functions.hpp"

void sw_functions::solnclass::update_energy_diagnostics() {

    double area = (Lx * Ly) / (Nx * Ny);

    if (debug and master()) { fprintf(stdout, "entering solnclass::update_energy_diagnostics\n"); }

    // KE
    if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> start KE\n"); }

    KE = 0.5 * rho0 * area * pssum(sum( (H0 + *eta[0]) *  ((*u[0])*(*u[0]) + (*v[0])*(*v[0])))); // 0.5 * h * u^2

    // PE
    if (debug and master()) { fprintf(stdout, "   update_energy_diagnostics -> start PE\n"); }
    //PE = 0.5 * rho0 * g * ( area * pssum(sum( (H0 + *eta[0]) * (H0 + *eta[0]) )) - Lx*Ly*pow(H0,2.) )  ; // 0.5 * g * (h^2 - H0^2)
    PE = 0.5 * rho0 * g * area * pssum(sum( (*eta[0]) * (*eta[0]) ))  ;
}

