#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;

// Write the grids to the corresponding files. Assumes uniform grids.
void write_grids(DTArray & tmp, double Lx, double Ly, double Lz,
        int Nx, int Ny, int Nz) {

    bool debug = false;
    if (debug and master()) { fprintf(stdout, "Entering write_grids\n"); }

    // Blitz index placeholders
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    if (Nx > 1) {
        tmp = (ii + 0.5)/Nx*Lx + 0*jj + 0*kk - Lx/2;
        write_array_sw(tmp,"xgrid");
    }

    if (Ny > 1) {
        tmp = 0*ii + (kk + 0.5)/Ny*Ly + 0*jj - Ly/2;
        write_array_sw(tmp,"ygrid");
    }

}
