#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;


// Compute the weights for the AB2 method
void compute_weights2(solnclass & soln) {
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_weights2\n"); }

    double a,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn  = soln.t;

    a  = soln.t - soln.dt1;
    ts = soln.t;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w0 = w;

    a  = soln.t;
    ts = soln.t - soln.dt1;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w1 = w;

    soln.w2 = 0;

    return;

}

// Apply the AB2 step
void step_ab2(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_ab2\n"); }

    // Make nice references for arrays
    DTArray &eta     = *soln.eta[0], &u     = *soln.u[0], &v     = *soln.v[0]; 
    DTArray &fluxeta = *flux.eta[1], &fluxu = *flux.u[1], &fluxv = *flux.v[1];

    // Compute new timestep, decrease for ab2
    compute_dt(soln);

    soln.dt0 *= 0.1;
    compute_weights2(soln);

    // Compute flux
    method.flux(soln, fluxu, fluxv, fluxeta, Sz);

    // Advance solution
    u   = u   - soln.w0 * fluxu   - soln.w1 * (*flux.u[0]);
    v   = v   - soln.w0 * fluxv   - soln.w1 * (*flux.v[0]);
    eta = eta - soln.w0 * fluxeta - soln.w1 * (*flux.eta[0]);

    // Exponential Filter 
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.u[0] = *flux.u[1];
    *flux.u[1] = fluxu; 

    *flux.v[0] = *flux.v[1];
    *flux.v[1] = fluxv; 

    *flux.eta[0] = *flux.eta[1];
    *flux.eta[1] = fluxeta; 
}

