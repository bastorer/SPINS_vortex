#include "../sw_functions.hpp"

void sw_functions::solnclass::do_stitching() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::do_stitching\n"); }

    if (compute_spectra) { stitch_spectra(); }
    stitch_chain_diagnostics();
    stitch_diagnostics();

    prev_chain_write_count += chain_write_count;
    chain_write_count = 0;
}
