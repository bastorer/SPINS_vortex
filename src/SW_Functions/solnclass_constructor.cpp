#include "../sw_functions.hpp"

// Constructor method
sw_functions::solnclass::solnclass(int sx, int sy, int sz, double lx, double ly, double lz,
        string const &flux_method, string const &xgrid_type, string const &ygrid_type) {

    debug = false;

    if (debug and master()) { fprintf(stdout, "entering solnclass::solnclass (constructor)\n"); }

    start_clock_time = MPI_Wtime();
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&num_procs);

    all = blitz::Range::all();

    X_IND = 0;
    Y_IND = 2;
    Z_IND = 1;

    Nx = sx;
    Ny = sy;
    Nz = sz;

    Lx = lx;
    Ly = ly;
    Lz = lz;

    xgrid = new double[Nx];
    ygrid = new double[Ny];
    zgrid = new double[Nz];
    for (int II = 0; II < Nx; II++) { xgrid[II] = (II+0.5)/Nx*Lx - Lx/2.; }
    for (int JJ = 0; JJ < Ny; JJ++) { ygrid[JJ] = (JJ+0.5)/Ny*Ly - Ly/2.; }
    for (int KK = 0; KK < Nz; KK++) { zgrid[KK] = (KK+0.5)/Nz*Lz - Lz; }

    w0 = 0.;
    w1 = 0.;
    w2 = 0.;

    dx = Lx/Nx;
    dy = Ly/Ny;
    dz = Lz/Nz;

    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    u.resize(1);
    u[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *u[0]   = 0*ii + 0*jj + 0*kk;

    ux.resize(1);
    ux[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *ux[0]   = 0*ii + 0*jj + 0*kk;

    uy.resize(1);
    uy[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *uy[0]   = 0*ii + 0*jj + 0*kk;

    v.resize(1);
    v[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *v[0]   = 0*ii + 0*jj + 0*kk;

    vx.resize(1);
    vx[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *vx[0]   = 0*ii + 0*jj + 0*kk;

    vy.resize(1);
    vy[0]   = new DTArray(local_lbound,local_extent,local_storage);
    *vy[0]   = 0*ii + 0*jj + 0*kk;

    eta.resize(1);
    eta[0] = new DTArray(local_lbound,local_extent,local_storage);
    *eta[0] = 0*ii + 0*jj + 0*kk;

    etax.resize(1);
    etax[0] = new DTArray(local_lbound,local_extent,local_storage);
    *etax[0] = 0*ii + 0*jj + 0*kk;

    etay.resize(1);
    etay[0] = new DTArray(local_lbound,local_extent,local_storage);
    *etay[0] = 0*ii + 0*jj + 0*kk;

    work.resize(3);
    work[0] = new DTArray(local_lbound,local_extent,local_storage);
    work[1] = new DTArray(local_lbound,local_extent,local_storage);
    work[2] = new DTArray(local_lbound,local_extent,local_storage);

    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                (*work[0])(II,JJ,KK) = 0.;
                (*work[1])(II,JJ,KK) = 0.;
                (*work[2])(II,JJ,KK) = 0.;
            }
        }
    }

    dt0 = 1.0;
    dt1 = 1.0;
    dt2 = 1.0;
    do_plot = false;

    U0 = 0.0;
    V0 = 0.0;
    kappa = 0.0;
    g = 9.81;

    iterct = 0;

    next_diag_time = 0.0;
    chain_write_count = 0;
    rho0 = 1000.;
    vol  = Lx*Ly*Lz/(Nx*Ny*Nz);
    KE = 0.;
    PE = 0.;
    TE = 0.;
    KE_aniso = 0.;
    PE_aniso = 0.;

    include_bg_in_spectra = 0.;

    method = flux_method;

    if (xgrid_type == "PERIODIC") { type_x = FOURIER; }
    else if (xgrid_type == "FREE_SLIP") { type_x = SINE; }
    else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    if (ygrid_type == "PERIODIC") { type_y = FOURIER; }
    else if (ygrid_type == "FREE_SLIP") { type_y = SINE; }
    else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

}
