#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;

// Apply the Euler step
void step_euler(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_euler\n"); }

    // Make nice references for arrays
    DTArray &eta     = *soln.eta[0], &u     = *soln.u[0], &v     = *soln.v[0]; 
    DTArray &fluxeta = *flux.eta[0], &fluxu = *flux.u[0], &fluxv = *flux.v[0];

    // Compute new timestep, decrease for euler
    compute_dt(soln);

    soln.dt0 *= 0.1;
    soln.w0   = soln.dt0;
    soln.w1   = 0;
    soln.w2   = 0;

    // Compute flux
    method.flux(soln, fluxu, fluxv, fluxeta, Sz);

    // Advance solution
    u   = u   - soln.dt0 * fluxu;
    v   = v   - soln.dt0 * fluxv;
    eta = eta - soln.dt0 * fluxeta;

    // Apply exponential filter
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.u[1]   = fluxu;
    *flux.v[1]   = fluxv;
    *flux.eta[1] = fluxeta;

}
