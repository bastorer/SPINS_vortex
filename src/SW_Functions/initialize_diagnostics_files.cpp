#include "../sw_functions.hpp"

// Initialize the diagnostics file
void sw_functions::solnclass::initialize_diagnostics_files(){

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_diagnostics_files\n"); }

    if (master()) {

        // Only over-write diagnostics.txt if not restarting
        if (!restarting) {
            diagnostics = fopen("diagnostics.txt", "w");
            assert(diagnostics);
            fprintf(diagnostics, "iteration, time, step_time, KE, PE");
            fprintf(diagnostics, "\n");
        }
        else {
            diagnostics = fopen("diagnostics.txt", "a");
            assert(diagnostics);
        }
        fclose(diagnostics);

        // Always over-write diagnostics_tmp.txt
        diagnostics = fopen("diagnostics_tmp.txt", "w");
        assert(diagnostics);
        fclose(diagnostics);
    }
}
