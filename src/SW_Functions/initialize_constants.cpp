#include "../sw_functions.hpp"

// Initialize various constants
void sw_functions::solnclass::initialize_constants() {

    if (debug and master()) { fprintf(stdout, "entering solnclass::initialize_constants\n"); }

    //
    //// HANDLE RESTARTING
    //

    // If we're restarting, determine how what offset to use for the spectra.
    // and other chain diagnostics
    if (restarting) {
        MPI_File_get_size(KE_1d_spect_final_file, &old_chain_len);
        prev_chain_write_count = old_chain_len/sizeof(double)/Ny;
    } else {
        prev_chain_write_count = 0;
    }

}

// Function to swap transform types
S_EXP swap_trig( S_EXP the_exp ) {
    if ( the_exp == SINE ) {
        return COSINE; }
    else if ( the_exp == COSINE ) {
        return SINE; }
    else if ( the_exp == FOURIER ) {
        return FOURIER; }
    else if ( the_exp == CHEBY ) {
        return CHEBY; }
    else {
        MPI_Finalize(); exit(1); // stop
    }
}
