#include "../sw_functions.hpp"

// Write initial diagnostics
void sw_functions::solnclass::write_initial_diagnostics(){

    if (debug and master()) { fprintf(stdout, "entering solnclass::wite_initial_diagnostics\n"); }

    update_energy_diagnostics();

    // Write the diagnostics
    if (master()) {
        fprintf(stdout,"t = %.4g days (%.3g%%, 0 outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                t/86400.,100.*t/tf,KE,PE,dt0);
        fprintf(stdout,"\n");
        diagnostics = fopen("diagnostics_tmp.txt", "a");
        assert(diagnostics);
        fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
        fprintf(diagnostics,"\n");
        fclose(diagnostics);
    }

}
