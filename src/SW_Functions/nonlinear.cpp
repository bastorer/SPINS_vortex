#include "../sw_functions.hpp"
#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Splits.hpp"
#include "../gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace sw_functions;


// Nonlinear Flux Method
void nonlinear(solnclass & soln, DTArray & flux_u, DTArray & flux_v, DTArray & flux_eta, Transgeom & Sz)
{
    if (soln.debug and master()) { fprintf(stdout, "Entering nonlinear\n"); }

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // Make nice references for arrays used
    DTArray &u   = *soln.u[0],   &ux   = *soln.ux[0],    &uy   = *soln.uy[0];
    DTArray &v   = *soln.v[0],   &vx   = *soln.vx[0],    &vy   = *soln.vy[0];
    DTArray &eta = *soln.eta[0], &etax = *soln.etax[0],  &etay = *soln.etay[0];

    double  &norm_x  = Sz.norm_x,
            &norm_y  = Sz.norm_y;
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;

    // x derivatives
    if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> x derivatives\n"); }
    if (Sz.type_x == FOURIER)   {
        deriv_fft(u,   X_xform, ux  );
        deriv_fft(v,   X_xform, vx  );
        deriv_fft(eta, X_xform, etax);
    }
    else if (Sz.type_x == SINE) {
        deriv_dst(u,   X_xform, ux  );
        deriv_dct(v,   X_xform, vx  );
        deriv_dct(eta, X_xform, etax);
    }
    ux   *= norm_x;
    vx   *= norm_x;
    etax *= norm_x;

    // y derivatives
    if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> y derivatives\n"); }
    if (Sz.type_y == FOURIER)   {
        deriv_fft(u,   Y_xform, uy  );
        deriv_fft(v,   Y_xform, vy  );
        deriv_fft(eta, Y_xform, etay);
    }
    else if (Sz.type_y == SINE) {
        deriv_dct(u,   Y_xform, uy  );
        deriv_dst(v,   Y_xform, vy  );
        deriv_dct(eta, Y_xform, etay);
    }
    uy   *= norm_y;
    vy   *= norm_y;
    etay *= norm_y;


    // Compute flux
    if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> computing flux\n"); }
    flux_u   =  u * ux   +   v * uy   +   soln.g * etax   - soln.f0 * v;
    flux_v   =  u * vx   +   v * vy   +   soln.g * etay   + soln.f0 * u;
    flux_eta =  (ux + vy) * (soln.H0 + eta)   +   u * etax   +    v * etay;
};
