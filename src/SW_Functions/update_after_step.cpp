#include "../sw_functions.hpp"

// Update after step
void sw_functions::solnclass::update_after_step(int plot_count){

    if (debug and master()) { fprintf(stdout, "entering solnclass::update_after_step\n"); }

    iterct++;
    t = t + dt0;
    curr_clock_time = MPI_Wtime();
    step_time = curr_clock_time - start_step_time;

    // If appropriate, do diagnostics
    // Note that tdiag = 0 (the default) will always satisfy this
    if (next_diag_time - t <= 0.0001*tdiag) {

        update_energy_diagnostics();

        // Write the diagnostics
        if (master()) {
            fprintf(stdout,"t = %.4g days (%.3g%%, %d outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                    t/86400.,100.*t/tf,plot_count-1,KE,PE,dt0);
            fprintf(stdout,"\n");
            diagnostics = fopen("diagnostics_tmp.txt", "a");
            assert(diagnostics);
            fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
            fprintf(diagnostics,"\n");
            fclose(diagnostics);
        }

        // Update next diag time
        next_diag_time += tdiag;
    }
}
