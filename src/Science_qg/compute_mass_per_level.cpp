#include "../Science_qg.hpp"

namespace TA = TArrayn;

void compute_mass_per_level(DTArray * rho, double * level_mass, MPI_Comm c) {

    int Nz = rho->extent(TA::secondDim);
    double * local_cnt = new double[Nz];
    for (int KK = rho->lbound(TA::secondDim); KK <= rho->ubound(TA::secondDim); KK++) {
        local_cnt[KK] = 0.;
    }

    for (int II = rho->lbound(TA::firstDim); II <= rho->ubound(TA::firstDim); II++) {
        for (int KK = rho->lbound(TA::secondDim); KK <= rho->ubound(TA::secondDim); KK++) {
            for (int JJ = rho->lbound(TA::thirdDim); JJ <= rho->ubound(TA::thirdDim); JJ++) {
                local_cnt[KK] += (*rho)(II,KK,JJ);
            }
        }
    }

    double * full_cnt = new double[Nz];
    MPI_Reduce(local_cnt, full_cnt, Nz, MPI_DOUBLE, MPI_SUM, 0, c);

    if (master()) {
        for (int KK = 0; KK < Nz; KK++) {
            level_mass[KK] = full_cnt[KK];
        }
    }

}

