#include "../Science_qg.hpp"

namespace Tf = Transformer;
namespace NSI = NSIntegrator;

void dpsidz(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int Nx, int Ny, int Nz) {

    //BAS: At the moment, hardcoded for free slip
    static NSI::DIMTYPE DIM_Z = NSI::FREE_SLIP;
    static Tf::Trans1D trans_z (Nx, Nz, Ny, Tf::secondDim, (DIM_Z == NSI::PERIODIC ? Tf::FOURIER : Tf::REAL));

    if (DIM_Z == NSI::FREE_SLIP) {
        deriv_dct(psi,trans_z,*out);
        *out *= M_PI/Lz;
    }
    return;
}
