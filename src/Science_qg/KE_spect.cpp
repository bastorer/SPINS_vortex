#include "../Science_qg.hpp"

using blitz::Array;
using blitz::cos;
using namespace TArrayn;
using namespace NSIntegrator;
using namespace Transformer;

//namespace TA = TArrayn;
//namespace Tf = Transformer;

void KE_spect(double * aniso, double * aniso_array, DTArray * u, DTArray * v, DTArray * ub, DTArray * vb,
        DTArray * ubar, DTArray * work, DTArray * work_bar, bool use_zonal_decomp,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, 
        double include_bg_in_spectra, bool compute_aniso,
        double Lx, double Ly, S_EXP type_y, double fcutoff, MPI_Comm c) {
    
    bool debug = false;

    if (debug and master()) { fprintf(stdout, "KE_Spect: Starting\n"); }
    if (debug and master()) { 
        if (use_zonal_decomp) {
            fprintf(stdout, "use_zonal_decomp is true\n");
        } else {
            fprintf(stdout, "use_zonal_decomp is false\n");
        }
    }
    
    // Start by extracting problem information
    int Nx, Ny, Nz, N, numproc, my_rank;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(v->extent(firstDim),c);
    Ny = v->extent(thirdDim);
    Nz = v->extent(secondDim);

    double Nxd = Nx*1.0;

    // Declarations for internal variables
    S_EXP x_form, y_form, y_form_bar;

    CTArray *var_hat_c, *var_hat_c_bar, *temp_2d_hat_c;
    DTArray *var_hat_r, *var_hat_r_bar, *temp_2d_hat_r;
    bool is_complex, is_complex_bar;
    int Ilb, Iub, Jlb, Jub, Klb, Kub;
    int Ilb_bar, Iub_bar, Jlb_bar, Jub_bar, Klb_bar, Kub_bar;
    double norm_x, norm_y;
    double kxmax, kymax, kmax;
    double kx, ky, ky_bar, dk, dkx, dky;
    bool chopped_x, chopped_y;
    double mult_x, mult_y, curr_cnt;
    double test_val;

    double kh;
    int tmpII, tmpJJ;

    ////
    //// Arrays for storing the many and several spectra
    ////
    if (debug and master()) { fprintf(stdout, "KE_Spect: Declaring arrays\n"); }
    N = min(Nx,Ny);
    int N_spect  = static_cast<int>(N/2);
    int Nx_spect = static_cast<int>(Nx/2);
    int Ny_spect = static_cast<int>(Ny/2);

    double x_nyq = M_PI / (Lx/Nx);
    double y_nyq = M_PI / (Ly/Ny);
    dkx = x_nyq / Nx_spect;
    dky = y_nyq / Ny_spect;
    dk  = max(x_nyq, y_nyq) / N_spect;

    // Azimuthally integrated spectrum
    double ** ans     = new double*[Nz];
    double * cnt_u    = new double[N_spect];
    double * cnt_v    = new double[N_spect];

    // x spectrum
    double ** ans_x     = new double*[Nz];
    double * cnt_x_u    = new double[Nx_spect];
    double * cnt_x_v    = new double[Nx_spect];

    // y spectrum
    double ** ans_y     = new double*[Nz];
    double * cnt_y_u    = new double[Ny_spect];
    double * cnt_y_v    = new double[Ny_spect];

    for (int II = 0; II < Nz; II++) {
        ans[II]   = new double[N_spect];
        ans_x[II] = new double[Nx_spect];
        ans_y[II] = new double[Ny_spect];
    }

    //
    ////
    ////// First, we deal with u, and incorporate it's portion
    /////  into the 2D spectrum.
    ////
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: Starting with 'u'\n"); }

    if (include_bg_in_spectra > 0.1) {
        *work = *u + *ub;
    } else { 
        *work = *u;
    }
    if (use_zonal_decomp) {
        for (int JJ = u->lbound(secondDim); JJ <= u->ubound(secondDim); JJ++) {
            for (int KK = u->lbound(thirdDim); KK <= u->ubound(thirdDim); KK++) {
                (*work_bar)(my_rank,JJ,KK) = (*ubar)(my_rank,JJ,KK);
            }
        }
    }

    x_form     = FOURIER; //(type_x == "PERIODIC") ? FOURIER : SINE;
    y_form     = (type_y == FOURIER) ? FOURIER : COSINE;
    y_form_bar = (type_y == FOURIER) ? FOURIER : SINE;

    is_complex     = ( (x_form == FOURIER) or (y_form == FOURIER) ); 
                // This should always be true because x can only be FOURIER
    is_complex_bar = (y_form == FOURIER);
                // This can be true or false

    // Transform the temp_2d array to store 2D information
    static TransWrapper XY_xform_temp_2d  = TransWrapper(Nx, Nz, Ny, x_form, NONE, y_form);
    XY_xform_temp_2d.forward_transform(temp_2d, x_form, NONE, y_form);
    if ( is_complex )    { temp_2d_hat_c  = XY_xform_temp_2d.get_complex_temp(); } 
    else                 { temp_2d_hat_r  = XY_xform_temp_2d.get_real_temp();    }

    // Create the forward transformation wrapper and compute the wave numbers
    static TransWrapper XY_xform_u     = TransWrapper(Nx,      Nz, Ny, x_form, NONE, y_form);
    static TransWrapper XY_xform_u_bar = TransWrapper(numproc, Nz, Ny, NONE,   NONE, y_form_bar);

    norm_x = (x_form == FOURIER) ? 2*M_PI/Lx : M_PI/Lx;
    norm_y = (y_form == FOURIER) ? 2*M_PI/Ly : M_PI/Ly;
    if (debug and master()) { fprintf(stdout, "norm_x = %.6g, norm_y = %.6g\n", norm_x, norm_y); }

    kxmax     = XY_xform_u.max_wavenum(firstDim)*norm_x;
    kymax     = XY_xform_u.max_wavenum(thirdDim)*norm_y;
    kmax      = max(kxmax,kymax);

    // Some awkwardness because we need to handle real and complex arrays independently
    if (debug and master()) { fprintf(stdout, "KE_Spect: Performing transforms\n"); }
    XY_xform_u.forward_transform(work, x_form, NONE, y_form);
    if (use_zonal_decomp) XY_xform_u_bar.forward_transform(work_bar, NONE,   NONE, y_form_bar);

    if ( is_complex )    { var_hat_c = XY_xform_u.get_complex_temp(); } 
    else                 { var_hat_r = XY_xform_u.get_real_temp();    }
    if (use_zonal_decomp) {
        if ( is_complex_bar) { var_hat_c_bar = XY_xform_u_bar.get_complex_temp(); } 
        else                 { var_hat_r_bar = XY_xform_u_bar.get_real_temp();    }
    }

    Ilb     = is_complex     ? (*var_hat_c).lbound(firstDim)      : (*var_hat_r).lbound(firstDim);
    Iub     = is_complex     ? (*var_hat_c).ubound(firstDim)      : (*var_hat_r).ubound(firstDim);
    Jlb     = is_complex     ? (*var_hat_c).lbound(thirdDim)      : (*var_hat_r).lbound(thirdDim);
    Jub     = is_complex     ? (*var_hat_c).ubound(thirdDim)      : (*var_hat_r).ubound(thirdDim);
    Klb     = is_complex     ? (*var_hat_c).lbound(secondDim)     : (*var_hat_r).lbound(secondDim);
    Kub     = is_complex     ? (*var_hat_c).ubound(secondDim)     : (*var_hat_r).ubound(secondDim);
    if (use_zonal_decomp) {
        Ilb_bar = is_complex_bar ? (*var_hat_c_bar).lbound(firstDim)  : (*var_hat_r_bar).lbound(firstDim);
        Iub_bar = is_complex_bar ? (*var_hat_c_bar).ubound(firstDim)  : (*var_hat_r_bar).ubound(firstDim);
        Jlb_bar = is_complex_bar ? (*var_hat_c_bar).lbound(thirdDim)  : (*var_hat_r_bar).lbound(thirdDim);
        Jub_bar = is_complex_bar ? (*var_hat_c_bar).ubound(thirdDim)  : (*var_hat_r_bar).ubound(thirdDim);
        Klb_bar = is_complex_bar ? (*var_hat_c_bar).lbound(secondDim) : (*var_hat_r_bar).lbound(secondDim);
        Kub_bar = is_complex_bar ? (*var_hat_c_bar).ubound(secondDim) : (*var_hat_r_bar).ubound(secondDim);
    }

    if (debug and master()) { fprintf(stdout, "KE_Spect: Computing wavenumbers\n"); }
    Array<double,1> kvec = XY_xform_u.wavenums(firstDim);
    Array<double,1> lvec = XY_xform_u.wavenums(thirdDim);
    Array<double,1> lvec_bar = XY_xform_u_bar.wavenums(thirdDim);
    //Array<double,1> lvec_bar;
    //if (use_zonal_decomp) lvec_bar = XY_xform_u_bar.wavenums(thirdDim);

    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    chopped_y = (y_form == FOURIER);
    chopped_x = (y_form != FOURIER) and (x_form == FOURIER);

    // Currently, we have u and ubar transforms seperately. However, for Parceval's
    // equality to hold, we need to combine them into one spectrum before we square.
    // We'll do that now, by merging them into var_hat_(r,c)
    // Remember that ubar has no x-dependence, so it is entirely in the mean field.
    //
    // This will be somewhat unpleasant, but the idea is to loop through all of the wavenumbers and combine the
    // spectral components whenever the wavenumbers match.
    if (debug and master()) { fprintf(stdout, "KE_Spect: melding u and ubar transforms\n"); }
    if (use_zonal_decomp) {
        if (debug and master()) { fprintf(stdout, "KE_Spect: melding step 0\n"); }
        double merge_count = 0.;
        for (int I = Ilb; I <= Iub; I++) {
            kx = norm_x*kvec(I);

            if (kx == 0.) {
                // Since ubar is entirely in the mean field, if kx != 0 then we don't care

                for (int J = Jlb; J <= Jub; J++) {
                    ky = norm_y*lvec(J);

                    // This makes DSTs and DCTs play well together
                    if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} 

                    for (int Jbar = Jlb_bar; Jbar <= Jub_bar; Jbar++) {
                        ky_bar = norm_y*lvec_bar(Jbar);

                        // This makes DSTs and DCTs play well together
                        if ((y_form_bar == SINE) and (lvec_bar(Jbar) == Ny)) { ky_bar = 0. ;} 

                        if (ky == ky_bar) { // If both have the same wavelength, then add them together

                            for (int K = Klb; K <= Kub; K++) {
                                merge_count++;
                                // If chopped in y, then count twice.
                                if ((chopped_y) and not((ky == 0) or (ky == kymax))) { merge_count++; }
                                if (is_complex) {
                                    if (is_complex_bar) { (*var_hat_c)(I,K,J) += Nxd*((*var_hat_c_bar)(Ilb_bar,K,Jbar)); }
                                    else                { (*var_hat_c)(I,K,J) += Nxd*((*var_hat_r_bar)(Ilb_bar,K,Jbar)); }
                                } else {
                                    // Really, this case should never be happening in general.
                                    // But if it does, then both should be real transforms.
                                    assert(!is_complex_bar);
                                    (*var_hat_r)(I,K,J) += Nxd*((*var_hat_r_bar)(Ilb_bar,K,Jbar)); 
                                }
                            }
                        }
                    }
                }
            }
        }
        if (debug and master()) { fprintf(stdout, "KE_Spect: melding complete, checking counts\n"); }
        double full_merge_count = 0.;
        MPI_Allreduce(&merge_count, &full_merge_count, 1, MPI_DOUBLE, MPI_SUM, c);
        if (full_merge_count != Ny*Nz) { fprintf(stdout, "Rank %d has count %g, not %g. Local count was %g.\n", my_rank, full_merge_count, Ny*Nz, merge_count); }
        assert(full_merge_count == Ny*Nz);
    }


    // To make life interesting, the DST and DCT have extra multiplicative factors
    // that need to be accounted for depending on the wavenumber. mult_x and mult_y
    // are going to do just that.
    if (debug and master()) { fprintf(stdout, "KE_Spect: preparing to azimuthally integrate 'u' component\n"); }

    if (debug and master()) { fprintf(stdout, "KE_Spect: zeroing out ans\n"); }
    // Zero everything out before we start
    for (int KK = 0; KK < Nz; KK++) {
        for (int II = 0; II < N_spect; II++) {
            ans[KK][II] = 0.;
        }
        for (int II = 0; II < Nx_spect; II++) {
            ans_x[KK][II] = 0.;
        }
        for (int II = 0; II < Ny_spect; II++) {
            ans_y[KK][II] = 0.;
        }
    }

    if (debug and master()) { fprintf(stdout, "KE_Spect: zeroing out temp_2d\n"); }
    for (int I = Ilb; I <= Iub; I++) {
        for (int J = Jlb; J <= Jub; J++) {
            for (int K = Klb; K<= Kub; K++) {
                if (is_complex) { (*temp_2d_hat_c)(I,K,J) = 0.; }
                else            { (*temp_2d_hat_r)(I,K,J) = 0.; }
            }
        }
    }

    if (debug and master()) { fprintf(stdout, "KE_Spect: zeroing out cnt\n"); }
    for (int II = 0; II < N_spect; II++) {
        cnt_u[II] = 0.;
    }
    for (int II = 0; II < Nx_spect; II++) {
        cnt_x_u[II] = 0.;
    }
    for (int II = 0; II < Ny_spect; II++) {
        cnt_y_u[II] = 0.;
    }

    if (debug and master()) { fprintf(stdout, "KE_Spect: performing integrations\n"); }
    // First loop: through the first index of the transformed field
    for (int I = Ilb; I <= Iub; I++) {

        if (debug and master()) { fprintf(stdout, "KE_Spect: I = %d\n", I); }
        kx = norm_x*kvec(I);
        if (debug and master()) { fprintf(stdout, "KE_Spect:                Determining mult_x and curr_cnt\n"); }
        mult_x = 1.;
        if ((x_form == SINE) or (x_form == COSINE)) {
            mult_x = 0.5;
            if ((I == 0) and (x_form == COSINE)) {mult_x *= 0.5;}
        } else if (chopped_x) {
            if ((kvec(I) == 0) or (kvec(I)*norm_x == kxmax)) {
                mult_x *= 1.; curr_cnt = 1.;}
            else {  mult_x *= 2.; curr_cnt = 2.;}
        }

        // Second loop: through the second index of the transformed field
        for (int J = Jlb; J <= Jub; J++) {
            if (debug and master()) { fprintf(stdout, "KE_Spect:          J = %d\n", J); }
            ky = norm_y*lvec(J);
            if (debug and master()) { fprintf(stdout, "KE_Spect:                Determining mult_y and curr_cnt\n"); }
            if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            mult_y = 1.;
            if ((y_form == SINE) or (y_form == COSINE)) {
                mult_y = 0.5;
                if ((J == 0) and (y_form == COSINE)) {mult_y *= 0.5;}
            } else if (chopped_y) {
                if ((ky == 0) or (ky == kymax)) {
                    mult_y *= 1.; curr_cnt = 1.;}
                else {  mult_y *= 2.; curr_cnt = 2.;}
            }

            // Since we went ahead and merged the two spectra earlier, now we can use Pareval's equality without
            // having to be worried about the u / ubar decomposition.

            if (debug and master()) { fprintf(stdout, "KE_Spect: computing 2d spectrum\n"); }
            // 2D spectrum
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { (*temp_2d_hat_c)(I,K,J) += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else {            (*temp_2d_hat_r)(I,K,J) += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
            }

            if (debug and master()) { fprintf(stdout, "KE_Spect: computing integrated spectrum\n"); }
            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans[K][tmpII]            += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_u[tmpII] += curr_cnt;
            }

            if (debug and master()) { fprintf(stdout, "KE_Spect: computing x spectrum\n"); }
            // x spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_x[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans_x[K][tmpII]            += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_x_u[tmpII] += curr_cnt;
            }

            if (debug and master()) { fprintf(stdout, "KE_Spect: computing y spectrum\n"); }
            // y spectrum
            tmpII = floor(abs(ky)/dky + 0.5);
            if (tmpII >= Ny_spect) { tmpII = Ny_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_y[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans_y[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_y_u[tmpII] += curr_cnt;
            }
        }
    }

    //
    ////
    ////// Next, we deal with v, and incorporate it's portion
    /////  into the 2D spectrum.
    ////
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: starting 'v'\n"); }

    // v_bar is identically zero, so we can ignore it

    if (include_bg_in_spectra > 0.1) {
        *work = *v + *vb;
    } else {
        *work = *v;
    }

    x_form = FOURIER; //(type_x == "PERIODIC") ? FOURIER : COSINE;
    y_form = (type_y == FOURIER) ? FOURIER : SINE;

    // Create the forward transformation wrapper and compute the wave numbers
    static TransWrapper XY_xform_v = TransWrapper(Nx, Nz, Ny, x_form, NONE, y_form);

    // Some awkwardness because we need to handle real and complex arrays independently
    if (debug and master()) { fprintf(stdout, "KE_Spect: performing transforms\n"); }
    is_complex = ( (x_form == FOURIER) or (y_form == FOURIER) );
    XY_xform_v.forward_transform(work, x_form, NONE, y_form);
    if ( is_complex ) { var_hat_c = XY_xform_v.get_complex_temp(); } 
    else              { var_hat_r = XY_xform_v.get_real_temp(); }

    Ilb = is_complex ? (*var_hat_c).lbound(firstDim)  : (*var_hat_r).lbound(firstDim);
    Iub = is_complex ? (*var_hat_c).ubound(firstDim)  : (*var_hat_r).ubound(firstDim);
    Jlb = is_complex ? (*var_hat_c).lbound(thirdDim)  : (*var_hat_r).lbound(thirdDim);
    Jub = is_complex ? (*var_hat_c).ubound(thirdDim)  : (*var_hat_r).ubound(thirdDim);
    Klb = is_complex ? (*var_hat_c).lbound(secondDim) : (*var_hat_r).lbound(secondDim);
    Kub = is_complex ? (*var_hat_c).ubound(secondDim) : (*var_hat_r).ubound(secondDim);

    if (debug and master()) { fprintf(stdout, "KE_Spect: computing wavenumbers\n"); }
    kvec = XY_xform_v.wavenums(firstDim);
    lvec = XY_xform_v.wavenums(thirdDim);

    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    chopped_y = (y_form == FOURIER);
    chopped_x = (y_form != FOURIER) and (x_form == FOURIER);

    // To make life interesting, the DST and DCT have extra multiplicative factors
    // that need to be accounted for depending on the wavenumber. mult_x and mult_y
    // are going to do just that.

    // Since v does not have a vbar component (it is identically zero),
    // we don't need to worry about merging spectra (yay!).
    if (debug and master()) { fprintf(stdout, "KE_Spect: performing azimuthal integration\n"); }

    // Zero everything out before we begin
    for (int II = 0; II < N_spect; II++) {
        cnt_v[II] = 0.;
    }
    for (int II = 0; II < Nx_spect; II++) {
        cnt_x_v[II] = 0.;
    }
    for (int II = 0; II < Ny_spect; II++) {
        cnt_y_v[II] = 0.;
    }

    // First loop: through the first index of the transformed field
    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        mult_x = 1.;
        if ((x_form == SINE) or (x_form == COSINE)) {
            mult_x = 0.5;
            if ((I == 0) and (x_form == COSINE)) {mult_x *= 0.5;}
        } else if (chopped_x) {
            if ((kvec(I) == 0) or (kvec(I)*norm_x == kxmax)) {
                mult_x *= 1.; curr_cnt = 1.;}
            else {  mult_x *= 2.; curr_cnt = 2.;}
        }

        // Second loop: through the second index of the transformed field
        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            mult_y = 1.;
            if ((y_form == SINE) or (y_form == COSINE)) {
                mult_y = 0.5;
                if ((J == 0) and (y_form == COSINE)) {mult_y *= 0.5;}
            } else if (chopped_y) {
                if ((ky == 0) or (ky == kymax)) {
                    mult_y *= 1.; curr_cnt = 1.;}
                else {  mult_y *= 2.; curr_cnt = 2.;}
            }

            // 2D spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }

            tmpJJ = floor(abs(ky)/dky + 0.5);
            if (tmpJJ >= Ny_spect) { tmpJJ = Ny_spect - 1; }

            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { (*temp_2d_hat_c)(I,K,J) += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else            { (*temp_2d_hat_r)(I,K,J) += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
            }

            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_v[tmpII] += curr_cnt;
            }

            // x spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_x[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans_x[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_x_v[tmpII] += curr_cnt;
            }

            // y spectrum
            tmpII = floor(abs(ky)/dky + 0.5);
            if (tmpII >= Ny_spect) { tmpII = Ny_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_y[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_c)(I,K,J)), 2.0); }
                else { ans_y[K][tmpII] += mult_x*mult_y*pow(abs((*var_hat_r)(I,K,J)), 2.0); }
                cnt_y_v[tmpII] += curr_cnt;
            }
        }
    }

    //
    ////
    ////// Now that the transforms are done
    /////  we communicate to collect.
    ////
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: collecting and communicating\n"); }
    
    // Azimuthally integrated spectrum
    double ** full_ans = new double*[Nz];
    double * full_cnt_u = new double[N_spect];
    double * full_cnt_v = new double[N_spect];
    for (int II = 0; II < Nz; II++) {
        full_ans[II] = new double[N_spect];
        MPI_Allreduce(ans[II], full_ans[II], N_spect, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_u, full_cnt_u, N_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_v, full_cnt_v, N_spect, MPI_DOUBLE, MPI_SUM, c);

    // x spectrum
    double ** full_ans_x = new double*[Nz];
    double * full_cnt_x_u = new double[Nx_spect];
    double * full_cnt_x_v = new double[Nx_spect];
    for (int II = 0; II < Nz; II++) {
        full_ans_x[II] = new double[Nx_spect];
        MPI_Allreduce(ans_x[II], full_ans_x[II], Nx_spect, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_x_u, full_cnt_x_u, Nx_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_x_v, full_cnt_x_v, Nx_spect, MPI_DOUBLE, MPI_SUM, c);

    // y_spectrum
    double ** full_ans_y = new double*[Nz];
    double * full_cnt_y_u = new double[Ny_spect];
    double * full_cnt_y_v = new double[Ny_spect];
    for (int II = 0; II < Nz; II++) {
        full_ans_y[II] = new double[Ny_spect];
        MPI_Allreduce(ans_y[II], full_ans_y[II], Ny_spect, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_y_u, full_cnt_y_u, Ny_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_y_v, full_cnt_y_v, Ny_spect, MPI_DOUBLE, MPI_SUM, c);

    // Confirm that the total number of binned wavenumbers equals
    // the total number of wavenumbers.
    if (debug and master()) { fprintf(stdout, "KE_Spect: checking counts\n"); }
    if (master()) {
        double check_u = 0.; 
        double check_v = 0.; 
        double check_x_u = 0.;
        double check_x_v = 0.;
        double check_y_u = 0.;
        double check_y_v = 0.;
        for (int II = 0; II < N_spect; II++) { 
            check_u   += full_cnt_u[II];
            check_v   += full_cnt_v[II];
        }
        for (int II = 0; II < Nx_spect; II++) { 
            check_x_u += full_cnt_x_u[II];
            check_x_v += full_cnt_x_v[II];
        }
        for (int II = 0; II < Ny_spect; II++) { 
            check_y_u += full_cnt_y_u[II];
            check_y_v += full_cnt_y_v[II];
        }

        if ( (abs(check_u   - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_u is wrong! %.13g instead of %d\n", check_u, Nx*Ny*Nz); }
        if ( (abs(check_v   - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_v is wrong! %.13g instead of %d\n", check_v, Nx*Ny*Nz); }

        if ((abs(check_x_u - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_u is wrong! %.13g instead of %d\n", check_x_u, Nx*Ny*Nz); }
        if ((abs(check_x_v - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_v is wrong! %.13g instead of %d\n", check_x_v, Nx*Ny*Nz); }

        if ((abs(check_y_u - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_u is wrong! %.13g instead of %d\n", check_y_u, Nx*Ny*Nz); }
        if ((abs(check_y_v - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_v is wrong! %.13g instead of %d\n", check_y_v, Nx*Ny*Nz); }
    }

    //
    //// Pass back the depth-averaged answers
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: returning results\n"); }
    for (int II = 0; II < N_spect; II++) {
        spect[II]   = 0.;
    }
    for (int II = 0; II < Nx_spect; II++) {
        spect_x[II] = 0.;
    }
    for (int II = 0; II < Ny_spect; II++) {
        spect_y[II] = 0.;
    }

    for (int KK = 0; KK < Nz; KK++) {
        for (int II = 0; II < N_spect; II++) {
            spect[II] += full_ans[KK][II];
        }

        for (int II = 0; II < Nx_spect; II++) {
            spect_x[II] += full_ans_x[KK][II];
        }

        for (int II = 0; II < Ny_spect; II++) {
            spect_y[II] += full_ans_y[KK][II];
        }
    }

    //
    //// Compute the anisotropy
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: computing anisotropy\n"); }

    double the_sum = 0.;
    double the_ref_L = 0.;
    double the_ref_R = 0.;
    double *aniso_tmp = new double[N_spect];
    double *aniso_ref_L = new double[N_spect];
    double *aniso_ref_R = new double[N_spect];

    double the_sum_full = 0.;
    double the_ref_full_L = 0.;
    double the_ref_full_R = 0.;
    double *aniso_tmp_full = new double[N_spect];
    double *aniso_ref_full_L = new double[N_spect];
    double *aniso_ref_full_R = new double[N_spect];
    
    double mean_val, spect_val;

    double spect_cnt;
    if (chopped_x) {
        spect_cnt = (Nx/2 + 1)*Ny*Nz;
    } else if (chopped_y) {
        spect_cnt = Nx*(Ny/2 + 1)*Nz;
    } else {
        spect_cnt = Nx*Ny*Nz;
    }
    
    if (compute_aniso) {

        // Zero everything out
        for (int II = 0; II < N_spect; II++) {
            aniso_tmp[II] = 0.;
            aniso_ref_L[II] = 0.;
            aniso_ref_R[II] = 0.;
        }

        for (int I = Ilb; I <= Iub; I++) {
            for (int J = Jlb; J <= Jub; J++) {

                kx = norm_x*kvec(I);
                ky = norm_y*lvec(J);

                kh = pow(kx*kx + ky*ky, 0.5);
                tmpII = floor(kh/dk + 0.5);
                if ( tmpII >= N_spect) { tmpII = N_spect - 1; }

                if (tmpII > 0) {
                    // Ignore the `mean`

                    for (int K = Klb; K <= Kub; K++) {
                        mean_val = full_ans[K][tmpII] / (full_cnt_u[tmpII] / Nz);
                        if (is_complex) { spect_val = abs((*temp_2d_hat_c)(I,K,J)); }
                        else            { spect_val = abs((*temp_2d_hat_r)(I,K,J)); }

                        // Divide by kh to account for the increased number of 'bins' for 
                        //   larger wavenumbers.
                        // For consistency, use tmpII * dk to approximate kh

                        spect_val *= 1./( tmpII * dk );
                        mean_val  *= 1./( tmpII * dk );

                        if ( ( abs(kx) < fcutoff * x_nyq ) and  ( abs(ky) < fcutoff * y_nyq ) ) {
                            the_sum   += pow(spect_val - mean_val, 2.);
                            the_ref_L += pow(spect_val, 2.);
                            the_ref_R += pow(mean_val,  2.);
                        }

                        aniso_tmp[tmpII]   += pow(spect_val -  mean_val, 2.);
                        aniso_ref_L[tmpII] += pow(spect_val, 2.);
                        aniso_ref_R[tmpII] += pow(mean_val, 2.);
                    }
                }
            }
        }

        MPI_Allreduce(aniso_tmp, aniso_tmp_full, N_spect, MPI_DOUBLE, MPI_SUM, c);
        MPI_Allreduce(aniso_ref_L, aniso_ref_full_L, N_spect, MPI_DOUBLE, MPI_SUM, c);
        MPI_Allreduce(aniso_ref_R, aniso_ref_full_R, N_spect, MPI_DOUBLE, MPI_SUM, c);
        MPI_Allreduce(&the_sum,  &the_sum_full, 1, MPI_DOUBLE, MPI_SUM, c);
        MPI_Allreduce(&the_ref_L,  &the_ref_full_L, 1, MPI_DOUBLE, MPI_SUM, c);
        MPI_Allreduce(&the_ref_R,  &the_ref_full_R, 1, MPI_DOUBLE, MPI_SUM, c);

        //if ((abs(the_cnt_full - spect_cnt) > 0.1) ) 
        //{ fprintf(stderr, "aniso count is wrong! %.13g instead of %d\n", the_cnt_full, Nx*Ny*Nz); }

        for (int II = 0; II < N_spect; II++) {
            if ( (aniso_ref_full_L[II] != 0.) or (aniso_ref_full_R[II] != 0.) ) {
                double upper_bnd =      pow(aniso_ref_full_L[II], 0.5) + pow(aniso_ref_full_R[II], 0.5)  ;
                double lower_bnd = abs( pow(aniso_ref_full_L[II], 0.5) - pow(aniso_ref_full_R[II], 0.5) );
                double norm_of_diff = pow( aniso_tmp_full[II], 0.5);
                aniso_array[II] = norm_of_diff / upper_bnd;
            }
        }

        double upper_bnd =      pow(the_ref_full_L, 0.5) + pow(the_ref_full_R, 0.5)  ;
        double lower_bnd = abs( pow(the_ref_full_L, 0.5) - pow(the_ref_full_R, 0.5) );
        double norm_of_diff = pow( the_sum_full, 0.5);
        if (upper_bnd != 0.) { 
            *aniso = norm_of_diff / upper_bnd;
        }

    }

    //
    //// Now delete all of the arrays that we have created
    //
    if (debug and master()) { fprintf(stdout, "KE_Spect: deleting everything\n"); }

    delete[] aniso_tmp;
    delete[] aniso_ref_L;
    delete[] aniso_ref_R;
    delete[] aniso_tmp_full;
    delete[] aniso_ref_full_L;
    delete[] aniso_ref_full_R;

    for (int II = 0; II < Nz; II++) {

        delete[] ans[II];
        delete[] ans_x[II];
        delete[] ans_y[II];
        delete[] full_ans[II];
        delete[] full_ans_x[II];
        delete[] full_ans_y[II];
    }

    delete[] full_cnt_u;
    delete[] full_cnt_v;
    delete[] cnt_u;
    delete[] cnt_v;

    delete[] full_cnt_x_u;
    delete[] full_cnt_x_v;
    delete[] cnt_x_u;
    delete[] cnt_x_v;

    delete[] full_cnt_y_u;
    delete[] full_cnt_y_v;
    delete[] cnt_y_u;
    delete[] cnt_y_v;

    delete[] ans;
    delete[] ans_x;
    delete[] ans_y;
    delete[] full_ans;
    delete[] full_ans_x;
    delete[] full_ans_y;
}
