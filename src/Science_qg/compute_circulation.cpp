#include "../Science_qg.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

// Compute circulation about the North and South walls
void compute_circulation(double * circ_N, double * circ_S,
        DTArray & u, Tf::S_EXP type_y, MPI_Comm c) {

    int Nx, Ny, Nz, N, numproc, ub, lb, my_rank;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(u.extent(TA::firstDim),c);
    Ny = u.extent(TA::thirdDim);
    Nz = u.extent(TA::secondDim);

    DTArray * u_hat;
    
    static Tf::TransWrapper XY_xform = Tf::TransWrapper(Nx,Nz,Ny,Tf::NONE,Tf::NONE,type_y);

    XY_xform.forward_transform(&u, Tf::NONE, Tf::NONE, type_y);
    u_hat = XY_xform.get_real_temp();

    double tmp_N = 0, tmp_S = 0;

    for( int II = u_hat->lbound(TA::firstDim);  II <= u_hat->ubound(TA::firstDim);  II++) {
        for( int JJ = u_hat->lbound(TA::secondDim); JJ <= u_hat->ubound(TA::secondDim); JJ++) {
            for( int KK = u_hat->lbound(TA::thirdDim);  KK <= u_hat->ubound(TA::thirdDim);  KK++) {
                tmp_S +=            (*u_hat)(II,JJ,KK);
                tmp_N += pow(-1,KK)*(*u_hat)(II,JJ,KK);
            }
        }
    }

    double ans_N = 0, ans_S = 0;
    MPI_Reduce(&tmp_N, &ans_N, 1, MPI_DOUBLE, MPI_SUM, 0, c);
    MPI_Reduce(&tmp_S, &ans_S, 1, MPI_DOUBLE, MPI_SUM, 0, c);

    *circ_N = ans_N;
    *circ_S = ans_S;

}
