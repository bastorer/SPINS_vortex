
#ifndef SW_FUNCTIONS_HPP // Prevent double inclusion
#define SW_FUNCTIONS_HPP 1

#include "NSIntegrator.hpp"
#include "Science_sw.hpp"
#include "TArray.hpp"
#include "T_util.hpp"
#include "Par_util.hpp"
#include "Splits.hpp"
#include <iostream>
#include <fstream>
#include <blitz/array.h>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

namespace sw_functions {

    // Class to store solution variables
    struct solnclass {

        private:
            TinyVector<int,3> local_lbound, local_extent;
            GeneralArrayStorage<3> local_storage;

            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            bool debug;

            blitz::Range all;

            // Grids (for internal use, not saved, vectors only)
            double *xgrid, *ygrid, *zgrid;

            // Some handy indices
            int X_IND, Y_IND, Z_IND;

            // Gradient object
            Grad * gradient_op;
            Trans1D *X_xform, *Y_xform;
            double norm_3d;
            double norm_x, norm_y, norm_z;

            // Nonlinear Fields
            vector<DTArray *> u, ux, uy;
            vector<DTArray *> v, vx, vy;
            vector<DTArray *> eta, etax, etay;

            // Some work arrays
            vector<DTArray *> work;

            // Parameters for timestep
            double dx, dy, dz, t, next_plot_time;
            double w0,w1,w2;
            double dt0;
            double dt1;
            double dt2;
            bool do_plot;

            double U0;
            double V0;

            // Some problem parameters
            double g, H0, beta, f0, tf, kappa;
            int Nx, Ny, Nz;
            double Lx, Ly, Lz;
            bool restarting;
            TArrayn::S_EXP type_x, type_y;

            // Filter parameters
            double fstrength, fcutoff, forder; 

            // Diagnostics variables
            FILE * diagnostics;
            ifstream diagnostics_tmp;
            string diag_tmp;
            int iterct;
            double start_clock_time, 
                   curr_clock_time, 
                   start_step_time,
                   step_time;
            bool compute_norms, compute_spectra, compute_aniso;
            double next_diag_time, tdiag;
            double KE, PE, vol, rho0, PE_scale,
                   KE_aniso, PE_aniso,
                   *KE_aniso_array, *PE_aniso_array;
            double TE;
            double net_transport;
            double include_bg_in_spectra;

            // Spectra and other vector diagnostics
            int offset_from, offset_to, N_spect, Nx_spect, Ny_spect;
            int prev_chain_write_count, chain_write_count;
            MPI_Status status;
            MPI_File spect_cnt_file;

            // KE spect variables
            double **KE_2d_spect, *KE_1d_spect, *KE_x_spect, *KE_y_spect; 
            MPI_File KE_1d_spect_temp_file, KE_x_spect_temp_file,
                     KE_y_spect_temp_file, KE_2d_spect_temp_file,
                     KE_aniso_temp_file;
            MPI_File KE_1d_spect_final_file, KE_x_spect_final_file,
                     KE_y_spect_final_file, KE_2d_spect_final_file,
                     KE_aniso_final_file;

            // PE spect variables
            double **PE_2d_spect, *PE_1d_spect, *PE_x_spect, *PE_y_spect; 
            MPI_File PE_1d_spect_temp_file, PE_x_spect_temp_file,
                     PE_y_spect_temp_file, PE_2d_spect_temp_file,
                     PE_aniso_temp_file;
            MPI_File PE_1d_spect_final_file, PE_x_spect_final_file,
                     PE_y_spect_final_file, PE_2d_spect_final_file,
                     PE_aniso_final_file;

            MPI_Offset old_chain_len;

            string method;
            int my_rank, rank, num_procs;

            // Constructor method
            solnclass(int sx, int sy, int sz, double lx, double ly, double lz,
                    string const &flux_method, string const &xgrid_type, string const &ygrid_type);

            // Initialize various constants
            void initialize_constants();

            // Load background profiles
            void initialize_background_states(string const &ub_fn, string const &vb_fn, string const &qxb_fn, 
                    string const &qyb_fn, string const &psib_fn);

            // Load background profile for computing norms
            void initialize_background_q(string const &qb_fn);

            // Save state
            void write_outputs(int iter);

            // Initialize the diagnostics file
            void initialize_diagnostics_files();

            void stitch_diagnostics();

            void initialize_chain_diagnostics_files();

            void initialize_spectra_files();

            void update_energy_diagnostics();

            void stitch_chain_diagnostics();

            void stitch_spectra();

            void do_stitching();

            void close_chain_diagnostics();

            void close_spect();

            // Write initial diagnostics
            void write_initial_diagnostics();

            // Update after step
            void update_after_step(int plot_count);

            void dump_if_needed(int plot_count);

            void finalize(int plot_count, double final_time);

    };


    struct fluxclass {

        private:
            TinyVector<int,3> local_lbound, local_extent;
            GeneralArrayStorage<3> local_storage;

            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            vector<DTArray *> u;
            vector<DTArray *> v;
            vector<DTArray *> eta;

            // Constructor method
            fluxclass(int Nx, int Ny, int Nz) {

                local_lbound = alloc_lbound(Nx,Nz,Ny);
                local_extent = alloc_extent(Nx,Nz,Ny);
                local_storage = alloc_storage(Nx,Nz,Ny);

                u.resize(3);
                v.resize(3);
                eta.resize(3);

                for (int ind = 0; ind < 3; ind++) {
                    u[ind]   = new DTArray(local_lbound,local_extent,local_storage);
                    v[ind]   = new DTArray(local_lbound,local_extent,local_storage);
                    eta[ind] = new DTArray(local_lbound,local_extent,local_storage);

                    *u[ind]   = 0*ii + 0*jj + 0*kk;
                    *v[ind]   = 0*ii + 0*jj + 0*kk;
                    *eta[ind] = 0*ii + 0*jj + 0*kk;
                }

            }
    };

    // Transform information
    struct Transgeom {

        private:   
            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            TArrayn::S_EXP type_x, type_y;

            double norm_3d;
            double norm_x, norm_y, norm_z;

            double Lx, Ly;

            Trans1D *X_xform,
                    *Y_xform;
            TransWrapper *XYZ_xform;

            void initialize(solnclass &soln, 
                    string const &xgrid_type,
                    string const &ygrid_type) {

                Lx = soln.Lx;
                Ly = soln.Ly;

                if (xgrid_type == "PERIODIC") {
                    type_x = FOURIER;
                    norm_x = 2.*M_PI/Lx;
                } else if (xgrid_type == "FREE_SLIP") {
                    type_x = SINE; //FREE_SLIP;
                    norm_x = M_PI/Lx;
                } else {
                    if (master()) fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
                    MPI_Finalize(); exit(1);
                }

                if (ygrid_type == "PERIODIC") {
                    type_y = FOURIER;
                    norm_y = 2.*M_PI/Ly;
                } else if (ygrid_type == "FREE_SLIP") {
                    type_y = SINE; //FREE_SLIP;
                    norm_y = M_PI/Ly;
                } else {
                    if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
                    MPI_Finalize(); exit(1);
                }

            }
    };

    // Class to define flux function
    struct methodclass {

        void (*flux)(solnclass & soln, DTArray & flux_u, DTArray & flux_v,
                DTArray & flux_eta, Transgeom & Sz); 

    };
};

using namespace sw_functions;

// Declare some functions
void compute_dt(sw_functions::solnclass & soln);

void compute_weights2(sw_functions::solnclass & soln);

void compute_weights3(sw_functions::solnclass & soln);

void write_grids(DTArray & tmp, double Lx, double Ly, double Lz, int Nx, int Ny, int Nz);

void nonlinear(sw_functions::solnclass & soln, 
        DTArray & flux_u, DTArray & flux_v, DTArray & flux_eta, 
        sw_functions::Transgeom & Sz);; 

void apply_filter(sw_functions::solnclass & soln, sw_functions::Transgeom & Sz);

void step_euler(sw_functions::solnclass & soln, sw_functions::fluxclass & flux, sw_functions::methodclass & method, sw_functions::Transgeom & Sz);

void step_ab2(sw_functions::solnclass & soln, sw_functions::fluxclass & flux, sw_functions::methodclass & method, sw_functions::Transgeom & Sz);

void step_ab3(sw_functions::solnclass & soln, sw_functions::fluxclass & flux, sw_functions::methodclass & method, sw_functions::Transgeom & Sz);

void initialize_diagnostics_files();

#endif
