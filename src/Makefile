# Makefile to build SPINS, generalized for multiple systemd

# The "system.mk" file in the current directory will contain
# system-specific make variables, most notably the
# C/C++ compiler/linker, 
include system.mk


# Compile with debugging flags
DEBUG?=false
# Compile with optimizations
OPTIM?=true
# Compile with extra optimizations that may require significantly
# longer for compilation
SLOW_OPTIM?=false
NETCDF?=false

# If MPICXX isn't separately defined, then set it to be the same
# as CXX
ifeq ($(strip $(MPICXX)),)
   MPICXX:=$(CXX)
endif

# Assemble the CFLAGS
CFLAGS:=$(SYSTEM_CFLAGS) $(MPI_CFLAGS) $(SYSTEM_CXXFLAGS)
LDFLAGS:=$(SYSTEM_LDFLAGS)
ifeq ($(DEBUG),true)
   CFLAGS:=$(CFLAGS) $(DEBUG_CFLAGS)
   LDFLAGS:=$(LDFLAGS) $(DEBUG_LDFLAGS)
endif
ifeq ($(OPTIM),true)
   CFLAGS:=$(CFLAGS) $(OPTIM_CFLAGS)
   LDFLAGS:=$(LDFLAGS) $(OPTIM_LDFLAGS)
   ifeq ($(SLOW_OPTIM),true)
      CFLAGS:=$(CFLAGS) $(EXTRA_OPTIM_CFLAGS)
      LDFLAGS:=$(LDFLAGS) $(EXTRA_OPTIM_LDFLAGS)
   endif
endif

ifeq ($(NETCDF),true)
    CFLAGS:=$(CFLAGS) -DUSE_NETCDF
endif

INCLUDE_DIRS := -I../include $(MPI_INCDIR) $(LAPACK_INCDIR) $(BLITZ_INCDIR) $(FFTW_INCDIR) $(UMF_INCDIR)
CFLAGS := $(CFLAGS) $(INCLUDE_DIRS)

LIB_DIRS := -L../lib $(MPI_LIBDIR) $(LAPACK_LIBDIR) $(BLITZ_LIBDIR) $(FFTW_LIBDIR) $(UMF_LIBDIR)
LDLIBS := -lfftw3 $(MPI_LIB) -lumfpack -lamd -lboost_program_options $(LAPACK_LIB) -lblitz -lm -lstdc++
LDFLAGS := $(LDFLAGS) $(LIB_DIRS) 

ifeq ($(NETCDF), true)
    INCLUDE_DIRS := $(INCLUDE_DIRS) -I$(ZLIB_INCDIR) -I$(HDF5_INCDIR) -I$(NETCDF_INCDIR) 
    LIB_DIRS := $(LIB_DIRS) -L$(ZLIB_LIBDIR) -L$(HDF5_LIBDIR) -L$(NETCDF_LIBDIR)
    LDLIBS := $(LDLIBS) $(NETCDF_LIB) $(HDF5_LIB) $(ZLIB_LIB) -lcurl
endif

.PHONY: all 
all: tests/test_deriv_x tests/test_write_x tests/test_esolve_x tests/test_heat_x tests/test_ode_x tests/test_ns_x

.PHONY: clean
clean:
	rm -f *.o tests/*.o cases/*.o cases/*.src.? tests/*.src.? QG_Forcing_Library/*.o Science_qg/*.o QG_Functions/*.o Science/*.o BaseCase/*.o Science_sw/*.o SW_Functions/*.o

objfiles: $(shell ls *.cpp | sed -e 's/cpp/o/g')

# SPINS files
SPINS_BASE := TArray.o T_util.o Parformer.o ESolver.o Timestep.o NSIntegrator.o Splits.o Par_util.o Split_reader.o gmres.o gmres_1d_solver.o gmres_2d_solver.o grad.o multigrid.o Options.o BaseCase.o

BASECASE_CPPS := $(wildcard BaseCase/*.cpp)
BASECASE_OBJS := $(addprefix BaseCase/,$(notdir $(BASECASE_CPPS:.cpp=.o)))

SCIENCE_CPPS := $(wildcard Science/*.cpp)
SCIENCE_OBJS := $(addprefix Science/,$(notdir $(SCIENCE_CPPS:.cpp=.o)))

# SW SPINS Files
SW_SPINS_BASE := TArray.o T_util.o Parformer.o ESolver.o Timestep.o NSIntegrator.o Splits.o Par_util.o Split_reader.o gmres.o gmres_1d_solver.o gmres_2d_solver.o grad.o multigrid.o Options.o BaseCase.o

SW_FUNCTNS_CPPS := $(wildcard SW_Functions/*.cpp)
SW_FUNCTNS_OBJS := $(addprefix SW_Functions/,$(notdir $(SW_FUNCTNS_CPPS:.cpp=.o)))

SW_SCIENCE_CPPS := $(wildcard Science_sw/*.cpp)
SW_SCIENCE_OBJS := $(addprefix Science_sw/,$(notdir $(SW_SCIENCE_CPPS:.cpp=.o)))

# QG SPINS Files
QG_SPINS_BASE := TArray.o T_util.o Parformer.o ESolver.o Timestep.o NSIntegrator.o Splits.o Par_util.o Split_reader.o gmres.o gmres_1d_solver.o gmres_2d_solver.o grad.o multigrid.o Options.o BaseCase.o

QG_FORCING_CPPS := $(wildcard QG_Forcing_Library/*.cpp)
QG_FORCING_OBJS := $(addprefix QG_Forcing_Library/,$(notdir $(QG_FORCING_CPPS:.cpp=.o)))

QG_FUNCTNS_CPPS := $(wildcard QG_Functions/*.cpp)
QG_FUNCTNS_OBJS := $(addprefix QG_Functions/,$(notdir $(QG_FUNCTNS_CPPS:.cpp=.o)))

QG_SCIENCE_CPPS := $(wildcard Science_qg/*.cpp)
QG_SCIENCE_OBJS := $(addprefix Science_qg/,$(notdir $(QG_SCIENCE_CPPS:.cpp=.o)))

NSIntegrator.o: NSIntegrator.cpp NSIntegrator_impl.cc

tests/test%.o: tests/test%.cpp
	$(MPICXX) $(CFLAGS) -o $@ -c $<

tests/test%_x: tests/test%.o tests/test%.src.o TArray.o Parformer.o T_util.o  ESolver.o Timestep.o NSIntegrator.o BaseCase.o Science.o Splits.o Par_util.o Split_reader.o gmres.o gmres_1d_solver.o gmres_2d_solver.o grad.o multigrid.o Options.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS) 

cases/%.o: cases/%.cpp NSIntegrator_impl.cc NSIntegrator.hpp
	$(MPICXX) $(CFLAGS) -o $@ -c  $<

cases/%.x: cases/%.o cases/%.src.o $(SCIENCE_OBJS) $(BASECASE_OBJS) $(SPINS_BASE) 
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

cases/%_x: cases/%.o cases/%.src.o $(SCIENCE_OBJS) $(BASECASE_OBJS) $(SPINS_BASE) 
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

cases/sw_reader.x: cases/sw_reader.o $(SW_SPINS_BASE) $(BASECASE_OBJS) $(SW_SCIENCE_OBJS) $(SW_FUNCTNS_OBJS) 
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

cases/qg_reader.x: cases/qg_reader.o $(QG_SPINS_BASE) $(BASECASE_OBJS) $(QG_SCIENCE_OBJS) $(QG_FUNCTNS_OBJS) qg_functions.o $(QG_FORCING_OBJS) qg_forcing_library.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

cases/qg_reader_netcdf.x: cases/qg_reader_netcdf.o TArray.o T_util.o Parformer.o ESolver.o Timestep.o NSIntegrator.o BaseCase.o Science_qg.o Splits.o Par_util.o Split_reader.o gmres.o gmres_1d_solver.o gmres_2d_solver.o grad.o multigrid.o Options.o qg_functions_netcdf.o
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

tests/test%.src.c: tests/test%.cpp CaseFileSource.c
	echo "const char casefilesource[] = {`xxd -i < $<`, 0x00};" > $@
	echo "const char casefilename[] = \"$<\";" >> $@
	cat CaseFileSource.c >> $@

cases/%.src.c: cases/%.cpp CaseFileSource.c
	echo "const char casefilesource[] = {`xxd -i < $<`, 0x00};" > $@
	echo "const char casefilename[] = \"$<\";" >> $@
	cat CaseFileSource.c >> $@

%.o: %.cpp	*.hpp
	$(MPICXX) $(CFLAGS) -o $@ -c $< 

.SECONDARY:
