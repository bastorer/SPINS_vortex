/* WARNING: Science Content!

   Collection of various analysis routines that are general enough to be useful
   over more than one project */

#ifndef SCIENCE_SW_HPP
#define SCIENCE_SW_HPP 1

#include "T_util.hpp"
#include "TArray.hpp"
#include <blitz/array.h>
#include <blitz/tinyvec-et.h>
#include "math.h"
#include "Par_util.hpp"
#include "stdio.h"
#include "Split_reader.hpp"
#include <stdlib.h>
#include <complex>
#include "NSIntegrator.hpp"
#include <string>
#include "Parformer.hpp"

// Depth-averaged and azimuthally-integrated KE spectrum
void horiz_spectrum_bin_counts(DTArray & var, 
        double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, Transformer::S_EXP type_x, 
        Transformer:: S_EXP type_y, MPI_Comm c = MPI_COMM_WORLD);

void KE_spect(double * aniso, double * aniso_array, DTArray * u, DTArray * v,
        DTArray * work, DTArray * work2,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, bool compute_aniso,
        double Lx, double Ly, TArrayn::S_EXP type_y, double fcutoff, MPI_Comm c = MPI_COMM_WORLD);

void PE_spect(double * aniso, double * aniso_array, DTArray * psi,
        DTArray * work, DTArray * work2,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, bool compute_aniso,
        double Lx, double Ly, TArrayn::S_EXP type_y, double fcutoff, MPI_Comm c = MPI_COMM_WORLD);

#endif
