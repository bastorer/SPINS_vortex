/* vortex_reader.cpp -- 
 *
 * general case for looking at the evolution of large-scale
 * (f-plane) 3D systems. It was specifically designed to study
 * 3D vortices.
 *
 * Input data and configuration provided at runtime
 * via a configuration file (spins.conf). See config_vortex.py 
 *
 */

// Include the necessary packakges
#include "../Science.hpp"
#include "../TArray.hpp"
#include "../Par_util.hpp"
#include "../NSIntegrator.hpp"
#include "../BaseCase.hpp"
#include "../Options.hpp"
#include "../Parformer.hpp"
#include <stdio.h>
#include <mpi.h>
#include <vector>
#include <random/uniform.h>
#include <random/normal.h>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <boost/program_options.hpp> 
#include <fftw3.h> 
#include <omp.h>

namespace po = boost::program_options;

using std::string;


/* Domain Parameters  */
int      Nx,   Ny,   Nz;   // Number of points in x, y, z
double   Lx,   Ly,   Lz,   // Grid lengths of x, y, z 
         MinX, MinY, MinZ; // Minimum x/y/z points
bool mapped;               // Is the domain mapped?


/* Pertinent (typically input) filenames */
string xgrid_filename,
       ygrid_filename,
       zgrid_filename,
       u_filename,
       v_filename,
       w_filename,
       rho_filename,
       tracer_filename,
       bg_b_filename;

/* Physical parameters */
double g, rot_f, N0, vel_mu, dens_kappa, tracer_kappa, tracer_g,
       alpha, beta, H; 
double BL_const, BL_depth;

/* Numerical Parameters */
double max_dt  = 10000.0; // Initialize as something, is computed later
double perturb = 0;  // Amount of perturbation to add
bool have_tracer;         // Is there a passive tracer?

/* Writeout parameters */
double final_time, plot_interval;
int plot_write_ratio;
double initial_time;
bool compute_norms;
int slice_write = 0;

// Grid types
DIMTYPE intype_x, intype_y, intype_z;

static enum {
   MATLAB,
   CTYPE,
   FULL3D
} input_data_types;

using std::vector;

blitz::firstIndex ii;
blitz::secondIndex jj;
blitz::thirdIndex kk;

using ranlib::Normal;

int myrank = -1;

bool restarting = false;
double restart_time = 0;
int restart_sequence = -1;

// Dump parameters
double t_step;
double real_start_time;
double compute_time;
bool restart_from_dump = false;
double total_run_time;
double avg_write_time;

////
//// Declarations for chains and slices
////

int X_ind = 0;
int Y_ind = 1;
int Z_ind = 2;

// Initialize slice files 
int num_slices[3];
double * slice_coords[3];
int slice_write_count, prev_slice_write_count;
MPI_File * u_slice_files[3];        // Create a pair of files for each variable
MPI_File * u_slice_final_files[3];  //   that you want to write to a slice.
MPI_File * v_slice_files[3];        //   Each variable will be written to every slice.
MPI_File * v_slice_final_files[3];
MPI_File * w_slice_files[3];
MPI_File * w_slice_final_files[3];
MPI_File * b_slice_files[3];
MPI_File * b_slice_final_files[3];
MPI_File * e_slice_files[3];
MPI_File * e_slice_final_files[3];

// Initialize chain files
int num_chains[3];
double ** chain_coords[3];
int chain_write_count, prev_chain_write_count;
double * x_chain_data_buffer;       // To avoid repeatedly allocating these,
double * y_chain_data_buffer;       //   declare them now.
double * z_chain_data_buffer;
MPI_File * w_chain_files[3];        // Create a pair of files for each variable
MPI_File * w_chain_final_files[3];  //   that you want to write to a chain.
MPI_File * b_chain_files[3];        //   Each variable will be written to every chain.
MPI_File * b_chain_final_files[3];
MPI_File * epv_chain_files[3];        //   Each variable will be written to every chain.
MPI_File * epv_chain_final_files[3];

// Diagnostics

double diag_time, next_diag_time;
bool computed_epv;
double min_epv;

bool compute_bu, compute_Ro;
double global_bu, global_Ro;

bool compute_eps_qg, computed_eps_qg;
bool compute_eps_cyclo, computed_eps_cyclo;
double rms_div_adv, rms_zeta_z, rms_lap_p;
double ener_weighted_cyclo_balance, ener_weighted_geo_balance;

int N_spect;
bool compute_spectra;
MPI_File spect_cnt_file;
double *KE_1d_spect, *KE_x_spect, *KE_y_spect; 
double *PE_1d_spect, *PE_x_spect, *PE_y_spect; 
double *KE_aniso_array, *PE_aniso_array;
double **KE_2d_spect, **PE_2d_spect;
double KE_aniso, PE_aniso;
MPI_File KE_1d_spect_temp_file,
         KE_1d_spect_final_file,
         PE_1d_spect_temp_file,
         PE_1d_spect_final_file,
         KE_aniso_file,
         KE_aniso_temp_file,
         PE_aniso_file,
         PE_aniso_temp_file;
MPI_Status status;
int offset_from, offset_to;
Transformer::S_EXP u_x_type, u_y_type, v_x_type, v_y_type, w_x_type,
    w_y_type, b_x_type, b_y_type;

/* ------------------------------- */
/* Begin definition of userControl */
/* ------------------------------- */
class userControl : public BaseCase {
    public:
        
        // Arrays for 1D grids defined here
        Array<double,1> xx, yy,zz;

        /* Variables for tracking progress  */
        int plotnum, itercount, lastplot, last_writeout;
        bool plot_now;
        double nextplot;

        /* Variables for timing the simulation */
        double start_rho_spread, step_dur;
        double clock_time, start_time;

        /* Functions that return domain information */
        int     size_x()       const { return Nx; }
        int     size_y()       const { return Ny; }
        int     size_z()       const { return Nz; }
        double  length_x()     const { return Lx; }
        double  length_y()     const { return Ly; }
        double  length_z()     const { return Lz; }
        DIMTYPE type_x()       const { return intype_x; }
        DIMTYPE type_y()       const { return intype_y; }
        DIMTYPE type_default() const { return intype_z; }
        bool    is_mapped()    const { return mapped;   }

        // Function to return restart information
        int  get_restart_sequence()  const { return restart_sequence; }

        // Grids
        Array<double,1> xgrid, ygrid, zgrid;

        /* Variables for Diagnostics */ 
        DTArray *e_pv, *temp_a, *temp_b, *bg_b;   // Ertel PV, temp arrays, and background buoyancy
        DTArray *eps_qg, *temp_c, *eps_cyclo;
        double KE, PE, rho0, b_pert_norm, // Some diagnostics
               del_b, norm_u, norm_v, norm_w, norm_b, BPE, APE,
               curr_val, old_var, tmp_double;
        double * chain_data_buffer;
        double * slice_data_buffer;
        int II, JJ, KK, Ix, Iy, Iz;
        double Vol() const  { return (Lx/Nx)*(Ly/Ny)*(Lz/Nz);}
        double Vol_PE() const  { return (Lx/Nx)*(Ly/Ny);}
        FILE * diagnostics;  // File for writing diagnostics
        ifstream diagnostics_tmp;  // File for writing diagnostics
        string diag_tmp;
        FILE * plottimes;    // File for tracking output times

        int my_rank, num_procs, pointflag;

        /* Function to access viscosity */
        double get_visco() const {
            return vel_mu;
        }

        /* Function to access diffusivity */
        double get_diffusivity(int t) const {
            if (t == 0) return dens_kappa; 
            if (t == 1) return tracer_kappa;
            else assert(0 && "Invalid tracer number!");
        }

        /* Function to return the start time */
        double init_time() const { 
            return initial_time;
        }

        /* Function to create the grids */
        void do_mapping(DTArray & xg, DTArray & yg, DTArray & zg) {
            if (input_data_types == MATLAB) {
                if (master())
                    fprintf(stdout,"Reading MATLAB-format xgrid (%d x %d) from %s\n",
                            Nx,Nz,xgrid_filename.c_str());
                read_2d_slice(xg,xgrid_filename.c_str(),Nx,Nz);
                if (master())
                    fprintf(stdout,"Reading MATLAB-format zgrid (%d x %d) from %s\n",
                            Nx,Nz,zgrid_filename.c_str());
                read_2d_slice(zg,zgrid_filename.c_str(),Nx,Nz);
            } else if (input_data_types == CTYPE ||
                    input_data_types == FULL3D) {
                if (master())
                    fprintf(stdout,"Reading CTYPE-format xgrid (%d x %d) from %s\n",
                            Nx,Nz,xgrid_filename.c_str());
                read_2d_restart(xg,xgrid_filename.c_str(),Nx,Nz);
                if (master())
                    fprintf(stdout,"Reading CTYPE-format zgrid (%d x %d) from %s\n",
                            Nx,Nz,zgrid_filename.c_str());
                read_2d_restart(zg,zgrid_filename.c_str(),Nx,Nz);
            }
            // Automatically generate y-grid
            yg = 0*ii + MinY + Ly*(0.5+jj)/Ny + 0*kk;

            // Write out the grids 
            write_array(xg,"xgrid");
            write_array(yg,"ygrid");
            write_array(zg,"zgrid");
        }

        /* We have an active tracer, namely density */
        int numActive() const { return 1; }

        /* We're given a passive tracer to advect */
        int numPassive() const {
            if (have_tracer) return 1;
            else return 0;
        }

        /* Timestep-check function.  This (along with "write everything" outputs) should
           really be bumped into the BaseCase */
        double check_timestep(double intime, double now) {
            if (intime < 1e-9) {
                /* Timestep's too small, somehow stuff is blowing up */
                if (master()) fprintf(stderr,"Tiny timestep (%e), aborting\n",intime);
                return -1;
                //FJP: intime sets time step
            } else if (intime > max_dt) {
                /* Cap the maximum timestep size */
                intime = max_dt;
            }
            /* Calculate how long we have until the next plottime, and then adjust
               the timestep such that we take a whole number of steps to ge there */
            double until_plot = nextplot - now;
            double steps = ceil(until_plot / intime);
            double real_until_plot = steps*intime;

            if (fabs(until_plot - real_until_plot) < 1e-5*plot_interval) {
                /* Close enough for scientific work */
                return intime;
            } else {
                /* Adjust the timestep */
                return (until_plot / steps);
            }
        }

        void initialize_diagnostics_files() {
            // Create empty files for storage and add header when necessary
            if (master()) {
                fprintf(stdout,"Initializing diagnostics files. \n");

                // File to track plot times
                if (!restarting) plottimes = fopen("plot_times.txt","w");
                else             plottimes = fopen("plot_times.txt","a");
                assert(plottimes);
                fclose(plottimes);

                // File to track diagnostics
                if (!restarting) {
                    diagnostics = fopen("diagnostics.txt","w");
                    assert(diagnostics);
                    fprintf(diagnostics,"time, iteration");
                    fprintf(diagnostics,", norm_u, norm_v, norm_w, norm_b");
                    fprintf(diagnostics,", del_b, KE, APE, BPE");
                    fprintf(diagnostics,", step_time");
                    fprintf(diagnostics,", KE_aniso, PE_aniso");
                    fprintf(diagnostics,", min_epv");
                    if (compute_norms)  fprintf(diagnostics,", b_pert_norm");
                    if (compute_eps_cyclo) fprintf(diagnostics, ", rms_div_adv, rms_zeta_z, rms_lap_p, ener_weighted_cyclo");
                    if (compute_eps_qg) fprintf(diagnostics, ", ener_weighted_geo");
                    if (compute_bu) fprintf(diagnostics, ", bu");
                    if (compute_Ro) fprintf(diagnostics, ", Ro");
                    fprintf(diagnostics,"\n");
                }
                else {
                    diagnostics = fopen("diagnostics.txt","a");
                    assert(diagnostics);
                }
                fclose(diagnostics);

                diagnostics = fopen("diagnostics_tmp.txt","w");
                assert(diagnostics);
                fclose(diagnostics);
            }
        }

        void print_progress(double sim_time) {
            if (master()) {
                fprintf(stdout,"t = %.4g (days) [ii = %d, %.4g %%]: \n",
                        sim_time/86400.,itercount,100*sim_time/final_time);
            }
        }

        void write_diagnostics(double sim_time) {
            if (master()) {

                // Update diagnostics file
                diagnostics = fopen("diagnostics_tmp.txt","a");
                assert(diagnostics);
                fprintf(diagnostics,"%f, %d", sim_time, itercount);
                fprintf(diagnostics,", %.12g, %.12g, %.12g, %.12g", norm_u, norm_v, norm_w, norm_b);
                fprintf(diagnostics,", %.12g, %.12g, %.12g, %.12g", del_b, KE, APE, BPE);
                fprintf(diagnostics,", %.12g", step_dur);
                fprintf(diagnostics,", %.12g, %.12g", KE_aniso, PE_aniso);
                fprintf(diagnostics,", %.12g", min_epv);
                if (compute_norms) fprintf(diagnostics, ", %.12g", b_pert_norm);
                if (compute_eps_cyclo) fprintf(diagnostics, ", %.12g, %.12g, %.12g, %.12g", rms_div_adv, rms_zeta_z, rms_lap_p, ener_weighted_cyclo_balance);
                if (compute_eps_qg) fprintf(diagnostics, ", %.12g", ener_weighted_geo_balance);
                if (compute_bu) fprintf(diagnostics, ", %.12g", global_bu);
                if (compute_Ro) fprintf(diagnostics, ", %.12g", global_Ro);
                fprintf(diagnostics, "\n");
                fclose(diagnostics);

                // Write step information to stdout
                //fprintf(stdout,"norm(u,v,w) = (%.2g, %.2g, %.2g) : delta(b) = %.2g : (KE,APE,BPE,PE) = (%.3g,%.3g,%.2g,%.2g)",
                //        norm_u,norm_v,norm_w,del_b,KE,APE,BPE,PE);
                //if (compute_norms) fprintf(stdout, " : epv_pert = %.2g",b_pert_norm);
                //if (compute_eps_cyclo) fprintf(stdout, ": ener_weighted_cyclo  = %.3g", ener_weighted_cyclo_balance);
                //if (compute_eps_qg) fprintf(stdout, ": ener_weighted_geo  = %.3g", ener_weighted_geo_balance);
                //fprintf(stdout, "\n");
            }
        }

        void stitch_diagnostics() {
            if (master()) {
                diagnostics_tmp.open ("diagnostics_tmp.txt");
                diagnostics = fopen("diagnostics.txt","a");
                assert(diagnostics);

                // Copy everything from diagnostics_tmp.txt to diagnostics.txt
                while (getline (diagnostics_tmp,diag_tmp) ) {
                    //fprintf(diagnostics, diag_tmp.c_str()); // Technically unsafe?
                    fputs(diag_tmp.c_str(), diagnostics); 
                    fprintf(diagnostics, "\n"); 
                }

                fclose(diagnostics);
                diagnostics_tmp.close();

                // Now wipe the diagnostics_tmp.txt file
                diagnostics = fopen("diagnostics_tmp.txt","w");
                assert(diagnostics);
                fclose(diagnostics);
            }
        }

        // Initialize the chain files
        void initialize_chains() {
            // This function initializes all of the files required for writing chains.
            // To add more variable outputs, simple replicate these samples.

            // Initialize the w chain files
            initialize_chain_tmps("w", w_chain_files, chain_coords, num_chains);
            initialize_chain_finals("w", w_chain_final_files, chain_coords, num_chains);

            // Initialize the b chain files
            initialize_chain_tmps("b", b_chain_files, chain_coords, num_chains);
            initialize_chain_finals("b", b_chain_final_files, chain_coords, num_chains);

            // Initialize the epv chain files
            initialize_chain_tmps("epv", epv_chain_files, chain_coords, num_chains);
            initialize_chain_finals("epv", epv_chain_final_files, chain_coords, num_chains);
        }

        // User-specified "chain" (1D) outputs
        void write_chains(DTArray & w, vector<DTArray *> & tracer) {
            // This function writes to the chain files.
            //
            write_chains_2(w, w_chain_files, chain_coords, num_chains, Nx, Ny, Nz, chain_write_count,
                    x_chain_data_buffer, y_chain_data_buffer, z_chain_data_buffer);
            write_chains_2(*tracer[0], b_chain_files, chain_coords, num_chains, Nx, Ny, Nz, chain_write_count,
                    x_chain_data_buffer, y_chain_data_buffer, z_chain_data_buffer);
            write_chains_2(*e_pv, epv_chain_files, chain_coords, num_chains, Nx, Ny, Nz, chain_write_count,
                    x_chain_data_buffer, y_chain_data_buffer, z_chain_data_buffer);

        }

        // Re-open chain files after stitching
        void reopen_chains() {
            // After the chains have been 'stitched' (for restart-proofing)
            // re-open the temporary files.

            initialize_chain_tmps("w", w_chain_files, chain_coords, num_chains); // Reopen w chain files
            initialize_chain_tmps("b", b_chain_files, chain_coords, num_chains); // Reopen b chain files
            initialize_chain_tmps("epv", epv_chain_files, chain_coords, num_chains); // Reopen b chain files
        }

        void close_chains() {
            // When finished, close the chains.
            // Temp files will automatically be deleted.
            for (int II = 0; II < 3; II++) {
                for (int JJ = 0; JJ < num_chains[II]; JJ++) {
                    MPI_File_close(&w_chain_files[II][JJ]);
                    MPI_File_close(&w_chain_final_files[II][JJ]);

                    MPI_File_close(&b_chain_files[II][JJ]);
                    MPI_File_close(&b_chain_final_files[II][JJ]);

                    MPI_File_close(&epv_chain_files[II][JJ]);
                    MPI_File_close(&epv_chain_final_files[II][JJ]);
                }
            }
        }

        // Stitch together the chain data into the final output files
        void stitch_chains(DTArray & w) {
            // To make the code more restart-proof, the chains are written to temp files
            // until a major (3D) output is reached, at which point the temp file is appended
            // to the full chain output.

            stitch_chains_2("w", w_chain_files, w_chain_final_files, num_chains,
                    Nx, Ny, Nz, chain_coords, chain_write_count, prev_chain_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_chains_2("b", b_chain_files, b_chain_final_files, num_chains,
                    Nx, Ny, Nz, chain_coords, chain_write_count, prev_chain_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_chains_2("epv", epv_chain_files, epv_chain_final_files, num_chains,
                    Nx, Ny, Nz, chain_coords, chain_write_count, prev_chain_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));
        }

        void initialize_slices() {

            // Initialize u slice files
            initialize_slice_tmps("u", u_slice_files, slice_coords, num_slices);
            initialize_slice_finals("u", u_slice_final_files, slice_coords, num_slices);

            // Initialize v slice files
            initialize_slice_tmps("v", v_slice_files, slice_coords, num_slices);
            initialize_slice_finals("v", v_slice_final_files, slice_coords, num_slices);

            // Initialize w slice files
            initialize_slice_tmps("w", w_slice_files, slice_coords, num_slices);
            initialize_slice_finals("w", w_slice_final_files, slice_coords, num_slices);

            // Initialize b slice files
            initialize_slice_tmps("b", b_slice_files, slice_coords, num_slices);
            initialize_slice_finals("b", b_slice_final_files, slice_coords, num_slices);

            // Initialize epv slice files
            initialize_slice_tmps("epv", e_slice_files, slice_coords, num_slices);
            initialize_slice_finals("epv", e_slice_final_files, slice_coords, num_slices);
        }

        // User-specified "slice" (2D) outputs
        void write_slices(DTArray &u, DTArray &v, DTArray &w, vector<DTArray *> & tracer, DTArray *epv) {

            write_slices_2(u, u_slice_files, slice_coords, num_slices, Nx, Ny, Nz, slice_write_count);
            write_slices_2(v, v_slice_files, slice_coords, num_slices, Nx, Ny, Nz, slice_write_count);
            write_slices_2(w, w_slice_files, slice_coords, num_slices, Nx, Ny, Nz, slice_write_count);
            write_slices_2(*tracer[0], b_slice_files, slice_coords, num_slices, Nx, Ny, Nz, slice_write_count);
            write_slices_2(*epv, e_slice_files, slice_coords, num_slices, Nx, Ny, Nz, slice_write_count);

        }

        // Process the slices stitch together the slice and chain information when necessary
        // Afterwards, re-open the appropriate files, but wipe them clean.
        void stitch_slices(DTArray & w) {

            stitch_slices_2("u", u_slice_files, u_slice_final_files, num_slices,
                    Nx, Ny, Nz, slice_coords, slice_write_count, prev_slice_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_slices_2("v", v_slice_files, v_slice_final_files, num_slices,
                    Nx, Ny, Nz, slice_coords, slice_write_count, prev_slice_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_slices_2("w", w_slice_files, w_slice_final_files, num_slices,
                    Nx, Ny, Nz, slice_coords, slice_write_count, prev_slice_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_slices_2("b", b_slice_files, b_slice_final_files, num_slices,
                    Nx, Ny, Nz, slice_coords, slice_write_count, prev_slice_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));

            stitch_slices_2("epv", e_slice_files, e_slice_final_files, num_slices,
                    Nx, Ny, Nz, slice_coords, slice_write_count, prev_slice_write_count,
                    w.lbound(firstDim), w.ubound(firstDim));
        }

        void reopen_slices() {

            initialize_slice_tmps("u", u_slice_files, slice_coords, num_slices);
            initialize_slice_tmps("v", v_slice_files, slice_coords, num_slices);
            initialize_slice_tmps("w", w_slice_files, slice_coords, num_slices);
            initialize_slice_tmps("b", b_slice_files, slice_coords, num_slices);
            initialize_slice_tmps("epv", e_slice_files, slice_coords, num_slices);

        }

        void close_slices() {
            for (int II = 0; II < 3; II++) {
                for (int JJ = 0; JJ < num_slices[II]; JJ++) {
                    MPI_File_close(&u_slice_files[II][JJ]);
                    MPI_File_close(&u_slice_final_files[II][JJ]);

                    MPI_File_close(&v_slice_files[II][JJ]);
                    MPI_File_close(&v_slice_final_files[II][JJ]);

                    MPI_File_close(&w_slice_files[II][JJ]);
                    MPI_File_close(&w_slice_final_files[II][JJ]);

                    MPI_File_close(&b_slice_files[II][JJ]);
                    MPI_File_close(&b_slice_final_files[II][JJ]);

                    MPI_File_close(&e_slice_files[II][JJ]);
                    MPI_File_close(&e_slice_final_files[II][JJ]);
                }
            }
        }

        void initialize_spectra() {

        }

        void stitch_KE_spect() {

            for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
                offset_from = JJ*N_spect;
                offset_to   = (JJ + prev_chain_write_count)*N_spect;

                MPI_File_seek(KE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_1d_spect_temp_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);
                MPI_File_seek(KE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_1d_spect_final_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);

                MPI_File_seek(KE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_aniso_temp_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);
                MPI_File_seek(KE_aniso_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_aniso_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);
            }

            MPI_Barrier(MPI_COMM_WORLD);
            MPI_File_close(&KE_1d_spect_temp_file);
            MPI_File_close(&KE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_aniso_temp_file);
        }

        void stitch_PE_spect() {

            for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
                offset_from = JJ*N_spect;
                offset_to   = (JJ + prev_chain_write_count)*N_spect;

                MPI_File_seek(PE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_1d_spect_temp_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);
                MPI_File_seek(PE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_1d_spect_final_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);

                MPI_File_seek(PE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_aniso_temp_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
                MPI_File_seek(PE_aniso_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_aniso_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
            }

            MPI_Barrier(MPI_COMM_WORLD);
            MPI_File_close(&PE_1d_spect_temp_file);
            MPI_File_close(&PE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_aniso_temp_file);
        }

        void specify_chains_and_slices() {

            num_chains[X_ind] = 0; // Number of x-chains
            num_chains[Y_ind] = 1; // Number of y-chains
            num_chains[Z_ind] = 1; // Number of z-chains

            x_chain_data_buffer = new double[Nx/num_procs]; // This assumes equal distribution...
            y_chain_data_buffer = new double[Ny];
            z_chain_data_buffer = new double[Nz];

            chain_coords[X_ind] = new double* [3];
            chain_coords[Y_ind] = new double* [3];
            chain_coords[Z_ind] = new double* [3];
            chain_coords[X_ind][0] = new double[num_chains[X_ind]]; // y coord for x-chains
            chain_coords[X_ind][1] = new double[num_chains[X_ind]]; // z coord for x-chains
            chain_coords[Y_ind][0] = new double[num_chains[Y_ind]]; // x coord for y-chains
            chain_coords[Y_ind][1] = new double[num_chains[Y_ind]]; // z coord for y-chains
            chain_coords[Z_ind][0] = new double[num_chains[Z_ind]]; // x coord for z-chains
            chain_coords[Z_ind][1] = new double[num_chains[Z_ind]]; // y coord for z-chains

            chain_coords[Y_ind][0][0] = 0.5;
            chain_coords[Y_ind][1][0] = 0.5;

            chain_coords[Z_ind][0][0] = 0.5;
            chain_coords[Z_ind][1][0] = 0.5;

            num_slices[X_ind] = 1; // Number of yz-slices
            num_slices[Y_ind] = 0; // Number of xz-slices
            num_slices[Z_ind] = 2; // Number of xy-slices

            slice_coords[X_ind] = new double[num_slices[X_ind]]; // x coord for yz-slices
            slice_coords[Y_ind] = new double[num_slices[Y_ind]]; // y coord for yz-slices
            slice_coords[Z_ind] = new double[num_slices[Z_ind]]; // z coord for xy-slices

            slice_coords[X_ind][0] = 0.5; 
            slice_coords[Z_ind][0] = 0.4;
            slice_coords[Z_ind][1] = 0.5;

        }

        /* What analysis should be done after each timestep */
        void analysis(double sim_time, DTArray & u, DTArray & v, DTArray & w,
                vector<DTArray *> tracer, DTArray & pressure) {

            // Increase the itercount
            itercount = itercount + 1;
            rho0   = 1.; // not entirely sure why, but need this for eps_qg to make sense
            computed_epv = false;
            computed_eps_qg = false;
            computed_eps_cyclo = false;

            // Do all of this stuff on the first iteration only
            if (itercount == 1) {
                
                // Set the next diagnostic time to zero so that it happens now
                next_diag_time = 0.;

                // Determine rank information
                MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
                MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

                // Some of our calculations are fairly intensive and require the use of temp arrays
                // To avoid either  a) having each function maintain a static array, or
                //                  b) constantly creating and destroying the arrays,
                // we will create some here and just pass them by reference.
                // Given my (rather limited) knowledge of c++, this seems like the best approach.
                temp_a = alloc_array(Nx,Ny,Nz);
                temp_b = alloc_array(Nx,Ny,Nz);
                temp_c = alloc_array(Nx,Ny,Nz);
                if (compute_eps_qg) { eps_qg = alloc_array(Nx,Ny,Nz); }
                if (compute_eps_cyclo) { eps_cyclo = alloc_array(Nx,Ny,Nz); }

                // For the moment, hard-code the chain and slice
                // information until we can sort out how to include
                // then in the spins.conf.
                specify_chains_and_slices();

                // Create the files for writing slices and chains
                initialize_chains();
                initialize_slices();
                chain_write_count = 0;
                prev_chain_write_count = 0;
                slice_write_count = 0;
                prev_chain_write_count = 0;

                // If compute norms, then intialize the field
                if (compute_norms) {
                    bg_b = alloc_array(Nx,Ny,Nz);
                    read_array(*bg_b,bg_b_filename.c_str(),Nx,Ny,Nz);
                }

                // Begin tracking the time
                if (master()) start_time = MPI_Wtime();

                // Initial files for diagnostic outputs
                if (!restarting) initialize_diagnostics_files();

                // Since analysis doesn't run before the first
                // step, we'll have to output after the first step
                // Since the first (Euler) step is short, 
                // there should't be much of a change.
                e_pv = alloc_array(Nx,Ny,Nz);
                if (!restarting) {
                    if (master()) fprintf(stdout,"Writing epv.0\n");
                    ertel_pv(u, v, w, *tracer[0], e_pv, *temp_a, *temp_b,
                            rot_f, N0, Lx, Ly, Lz, type_x(), type_y(), type_z());
                    min_epv = pvmin(*e_pv);
                    write_array(*e_pv,"epv",0);
                    computed_epv = true;
                }

                if (compute_eps_qg) {
                    if (!computed_eps_qg) {
                        ener_weighted_geo_balance = measure_geostrophic_balance(eps_qg, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, rho0, rot_f, N0, Lx, Ly);
                        computed_eps_qg = true;
                    }
                    write_array(*eps_qg,"eps_qg",plotnum);
                }

                if (compute_eps_cyclo) {
                    if (!computed_eps_cyclo) {
                        ener_weighted_cyclo_balance = measure_cyclostrophic_balance(eps_cyclo, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, 
                                *temp_c, rho0, rot_f, N0, Lx, Ly, rms_div_adv, rms_zeta_z, rms_lap_p);
                        computed_eps_cyclo = true;
                    }
                    write_array(*eps_cyclo,"eps_cyclo",plotnum);
                }

                N_spect = max(Nx,Ny)/2;
                if (compute_spectra) {
                    // Initialize spectra arrays

                    KE_1d_spect = new double[N_spect]; 
                    KE_x_spect = new double[N_spect]; 
                    KE_y_spect = new double[N_spect]; 
                    KE_aniso_array = new double[N_spect];

                    PE_1d_spect = new double[N_spect]; 
                    PE_x_spect = new double[N_spect]; 
                    PE_y_spect = new double[N_spect]; 
                    PE_aniso_array = new double[N_spect]; 
                    
                    KE_2d_spect = new double*[N_spect];
                    PE_2d_spect = new double*[N_spect];
                    for (int II = 0; II < N_spect; II++) {
                        KE_2d_spect[II] = new double[N_spect];
                        PE_2d_spect[II] = new double[N_spect];
                    }

                    // Initial spectra files
                    MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                            MPI_INFO_NULL, &KE_1d_spect_temp_file);
                    MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                            MPI_INFO_NULL, &KE_1d_spect_final_file);

                    MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                            MPI_INFO_NULL, &KE_aniso_temp_file);
                    MPI_File_open(MPI_COMM_WORLD, "KE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                            MPI_INFO_NULL, &KE_aniso_file);

                    MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                            MPI_INFO_NULL, &PE_1d_spect_temp_file);
                    MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                            MPI_INFO_NULL, &PE_1d_spect_final_file);

                    MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                            MPI_INFO_NULL, &PE_aniso_temp_file);
                    MPI_File_open(MPI_COMM_WORLD, "PE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                            MPI_INFO_NULL, &PE_aniso_file);

                    // Determine what kind of transforms to use for the spectra
                    u_x_type = type_x() == PERIODIC ? FOURIER : SINE;
                    v_x_type = type_x() == PERIODIC ? FOURIER : COSINE;
                    w_x_type = type_x() == PERIODIC ? FOURIER : COSINE;
                    b_x_type = type_x() == PERIODIC ? FOURIER : COSINE;

                    u_y_type = type_y() == PERIODIC ? FOURIER : COSINE;
                    v_y_type = type_y() == PERIODIC ? FOURIER : SINE;
                    w_y_type = type_y() == PERIODIC ? FOURIER : COSINE;
                    b_y_type = type_y() == PERIODIC ? FOURIER : COSINE;

                    // Compute the bin counts for the 1D spectra
                    horiz_spectrum_bin_counts(u, KE_1d_spect, KE_x_spect, KE_y_spect, Lx, Ly, u_x_type, u_y_type);
                    MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
                    if (master()) {
                        MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
                        MPI_File_write(spect_cnt_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);
                    }
                    MPI_File_close(&spect_cnt_file);
                }

                // If we're restarting, determine how what offset to use for the spectra/chains
                if (restarting) {
                    MPI_Offset old_chain_len;
                    MPI_File_get_size(KE_1d_spect_final_file, &old_chain_len);
                    prev_chain_write_count = old_chain_len/sizeof(double)/(N_spect);
                }

                // If we're restarting, determine how what offset to use for the slices
                if (restarting) {
                    MPI_Offset old_slice_len;
                    MPI_File_get_size(u_slice_final_files[2][0], &old_slice_len);
                    prev_slice_write_count = old_slice_len/sizeof(double)/(Nx*Ny);
                }

                // Determine last plot if restarting from the dump case
                if (restart_from_dump){
                    lastplot = restart_sequence*plot_interval;    
                }
            }

            // Compute how long the last step took
            if (master()) {
                clock_time = MPI_Wtime();
                step_dur = clock_time - start_time;
            }

            // If appropriate, do diagnostics
            // Note that diag_time = 0 will always satisfy this
            if (next_diag_time - sim_time <= 0.0001*diag_time) {

                if (compute_spectra) {

                    // Compute KE and PE spectra, as well as anisotropies
                    KE_spect(&KE_aniso, KE_aniso_array, u, v, w, KE_2d_spect, KE_1d_spect,
                            KE_x_spect, KE_y_spect, Lx, Ly, 
                            type_x() == PERIODIC ? "PERIODIC" : "FREE SLIP", type_y() == PERIODIC ? "PERIODIC" : "FREE SLIP");
                    PE_spect(&PE_aniso, PE_aniso_array, *tracer[0], PE_2d_spect, PE_1d_spect,
                            PE_x_spect, PE_y_spect, Lx, Ly, 
                            type_x() == PERIODIC ? "PERIODIC" : "FREE SLIP", type_y() == PERIODIC ? "PERIODIC" : "FREE SLIP");
                    for (int II = 0; II < N_spect; II++) {
                        KE_1d_spect[II] *= 0.5*rho0;
                        PE_1d_spect[II] *= 0.5*rho0/N0/N0;
                    }

                    // Now write them
                    if (master()) {
                        MPI_File_seek(KE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                        MPI_File_write(KE_1d_spect_temp_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);

                        MPI_File_seek(KE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                        MPI_File_write(KE_aniso_temp_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);

                        MPI_File_seek(PE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                        MPI_File_write(PE_1d_spect_temp_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);

                        MPI_File_seek(PE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                        MPI_File_write(PE_aniso_temp_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);
                    }
                }

                // Estimate the Burger Number
                if (compute_bu) {
                    estimate_bu(&global_bu, *tracer[0], u, v, w, *temp_a, *temp_b, *temp_c, rho0, Lx, Ly, Lz, N0, rot_f,
                            type_x() == PERIODIC ? "PERIODIC" : "FREE SLIP", type_y() == PERIODIC ? "PERIODIC" : "FREE SLIP");
                }

                // Estimate the Rossby Number
                if (compute_Ro) {
                    estimate_Ro(&global_Ro, *tracer[0], u, v, w, *temp_a, *temp_b, *temp_c, rho0, Lx, Ly, N0, rot_f,
                            type_x() == PERIODIC ? "PERIODIC" : "FREE SLIP", type_y() == PERIODIC ? "PERIODIC" : "FREE SLIP");
                }

                // Compute energy diagnostics
                KE = pssum(sum(0.5*rho0*(u*u + v*v + w*w)))*Vol();
                PE = pssum(sum(0.5*rho0*((*tracer[0])*(*tracer[0]))))/N0/N0*Vol();
                BPE = 0.0;
                del_b   = pvmax(*tracer[0]) - pvmin(*tracer[0]);
                APE     = PE - BPE;

                // Compute epv if we're going to need it
                if (!computed_epv) {
                    ertel_pv(u,  v,  w,  *tracer[0], e_pv, *temp_a, *temp_b, 
                            rot_f, N0, Lx, Ly, Lz, type_x(),   type_y(),   type_z());
                    min_epv = pvmin(*e_pv);
                    computed_epv = true;
                }

                if (compute_eps_qg and (!computed_eps_qg)) {
                    ener_weighted_geo_balance = measure_geostrophic_balance(eps_qg, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, rho0, rot_f, N0, Lx, Ly);
                    computed_eps_qg = true;
                }

                if (compute_eps_cyclo and (!computed_eps_cyclo)) {
                    ener_weighted_cyclo_balance = measure_cyclostrophic_balance(eps_cyclo, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, 
                            *temp_c, rho0, rot_f, N0, Lx, Ly, rms_div_adv, rms_zeta_z, rms_lap_p);
                    computed_eps_cyclo = true;
                }

                // Write chain outputs
                write_chains(w,tracer);
                chain_write_count++;

                // Compute norms
                if (compute_norms) {
                    b_pert_norm = pow(pssum(sum((*tracer[0] - *bg_b)*(*tracer[0] - *bg_b)))/(Nx*Ny*Nz),0.5); 
                }
                norm_u = pow(pssum(sum(u*u)),0.5);
                norm_v = pow(pssum(sum(v*v)),0.5);
                norm_w = pow(pssum(sum(w*w)),0.5);
                norm_b = pow(pssum(sum((*tracer[0])*(*tracer[0]))),0.5);

                // Write a summary of the step 
                write_diagnostics(sim_time);

                // Update next diag time
                next_diag_time += diag_time;
            }

            // Determine if this is a plot interval
            if ((sim_time - nextplot) > -1e-5*plot_interval) {
                plot_now = true;
            }
            else { plot_now = false; }

            // If it's time to output, then output
            if (plot_now) {

                // Compute epv if we're going to need it
                if (!computed_epv) {
                    ertel_pv(u,  v,  w,  *tracer[0], e_pv, *temp_a, *temp_b,
                            rot_f, N0, Lx, Ly, Lz, type_x(),   type_y(),   type_z());
                    min_epv = pvmin(*e_pv);
                    computed_epv = true;
                }

                // If appropriate, write 2D slices
                if (plot_write_ratio > 1) {
                    write_slices(u,v,w,tracer,e_pv);
                    slice_write++;
                    slice_write_count++;
                }

                // If appropraite, write 3D slices and associated information
                if (slice_write % plot_write_ratio == 0) {
                    plotnum = plotnum + 1;
                    t_step = MPI_Wtime(); // time just before write (for dump)
                    write_array(u,"u",plotnum);
                    write_array(v,"v",plotnum);
                    write_array(w,"w",plotnum);
                    write_array(*tracer[0],"b",plotnum);
                    if (have_tracer) { write_array(*tracer[1],"dye1",plotnum); }
                    write_array(*e_pv,"epv",plotnum);

                    if (compute_eps_qg) {
                        if (!computed_eps_qg) {
                            ener_weighted_geo_balance = measure_geostrophic_balance(eps_qg, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, rho0, rot_f, N0, Lx, Ly);
                        }
                        write_array(*eps_qg,"eps_qg",plotnum);
                        computed_eps_qg = true;
                    }

                    if (compute_eps_cyclo) {
                        if (!computed_eps_cyclo) {
                            ener_weighted_cyclo_balance = measure_cyclostrophic_balance(eps_cyclo, pressure, u, v, w, *tracer[0], *temp_a, *temp_b, 
                                    *temp_c, rho0, rot_f, N0, Lx, Ly, rms_div_adv, rms_zeta_z, rms_lap_p);
                        }
                        write_array(*eps_cyclo,"eps_cyclo",plotnum);
                        computed_eps_cyclo = true;
                    }

                    // In order to be able to gracefully (and readily) restart,
                    // stitch the chain and slice files together now
                    stitch_chains(w);
                    stitch_slices(w);
                    if (compute_spectra) {
                        stitch_KE_spect();
                        stitch_PE_spect();
                    }
                    stitch_diagnostics();
                    prev_chain_write_count += chain_write_count;
                    prev_slice_write_count += slice_write_count;
                    chain_write_count = 0;
                    slice_write_count = 0;
                    reopen_chains();
                    reopen_slices();

                    clock_time = MPI_Wtime(); // time just afer write (for dump)
                    avg_write_time = (avg_write_time*(plotnum-restart_sequence-1) + (clock_time - t_step))/
                        (plotnum-restart_sequence);

                    lastplot = itercount;
                    if (master()) {
                        plottimes = fopen("plot_times.txt","a");
                        assert(plottimes);
                        fprintf(plottimes,"%.10g\n",sim_time);
                        fclose(plottimes);
                        fprintf(stdout,"*");
                    }
                }
                nextplot = nextplot + plot_interval;

                if (sim_time - final_time > 1e-9) {
                    close_chains();
                    close_slices();
                    if (compute_spectra) {
                        MPI_File_close(&KE_1d_spect_temp_file);
                        MPI_File_close(&KE_1d_spect_final_file);
                        MPI_File_close(&KE_aniso_temp_file);
                        MPI_File_close(&KE_aniso_file);
                        MPI_File_close(&PE_1d_spect_temp_file);
                        MPI_File_close(&PE_1d_spect_final_file);
                        MPI_File_close(&PE_aniso_temp_file);
                        MPI_File_close(&PE_aniso_file);
                    }
                }

            } 

            print_progress(sim_time);
            last_writeout = itercount;

            // See if close to end of compute time and dump if necessary
            check_and_dump(clock_time, real_start_time, compute_time, sim_time, avg_write_time,
                    plotnum, u, v, w, tracer, itercount);

            // Change dump log file if successfully reached final time
            // the dump time will be twice final time so that a restart won't actually start
            successful_dump(plotnum, final_time, plot_interval);
        }

        // User-specified variables to dump
        void write_variables(DTArray & u,DTArray & v, DTArray & w,
                vector<DTArray *> & tracer) {
            write_array(u,"u.dump");
            write_array(v,"v.dump");
            write_array(w,"w.dump");
            write_array(*tracer[0],"b.dump");
            if (have_tracer) {  write_array(*tracer[1], "dye1.dump"); }
            stitch_chains(w);
            stitch_slices(w);
            stitch_diagnostics();
            if (compute_spectra) {
                stitch_KE_spect();
                stitch_PE_spect();
                MPI_File_close(&KE_1d_spect_temp_file);
                MPI_File_close(&KE_1d_spect_final_file);
                MPI_File_close(&KE_aniso_temp_file);
                MPI_File_close(&KE_aniso_file);
                MPI_File_close(&PE_1d_spect_temp_file);
                MPI_File_close(&PE_1d_spect_final_file);
                MPI_File_close(&PE_aniso_temp_file);
                MPI_File_close(&PE_aniso_file);
            }
            close_chains();
            close_slices();
        }


        void init_vels(DTArray & u, DTArray & v, DTArray & w) {
            // Initialize the velocities from read-in data
            if (master()) fprintf(stdout,"Initializing velocities\n");
            if (restarting and (!restart_from_dump)) {
                init_vels_restart(u, v, w);
            }
            else if (restarting and restart_from_dump) {
                init_vels_dump(u, v, w);
            }
            else {
                // Read in the appropriate data types
                switch(input_data_types) {
                    case MATLAB: // MATLAB data
                        if (master())
                            fprintf(stdout,"reading matlab-type u (%d x %d) from %s\n",
                                    Nx,Nz,u_filename.c_str());
                        read_2d_slice(u,u_filename.c_str(),Nx,Nz);
                        if (v_filename != "" && (Ny >> 1 || rot_f != 0)) {
                            if (master())
                                fprintf(stdout,"reading matlab-type v (%d x %d) from %s\n",
                                        Nx,Nz,v_filename.c_str());
                            read_2d_slice(v,v_filename.c_str(),Nx,Nz);
                        } else {
                            v = 0;
                        }
                        if (master())
                            fprintf(stdout,"reading matlab-type w (%d x %d) from %s\n",
                                    Nx,Nz,w_filename.c_str());
                        read_2d_slice(w,w_filename.c_str(),Nx,Nz);
                        break;
                    case CTYPE: // Column-major 2D data
                        if (master())
                            fprintf(stdout,"reading ctype u (%d x %d) from %s\n",
                                    Nx,Nz,u_filename.c_str());
                        read_2d_restart(u,u_filename.c_str(),Nx,Nz);
                        if (v_filename != "" && (Ny >> 1 || rot_f != 0)) {
                            if (master())
                                fprintf(stdout,"reading ctype v (%d x %d) from %s\n",
                                        Nx,Nz,v_filename.c_str());
                            read_2d_restart(v,v_filename.c_str(),Nx,Nz);
                        } else {
                            v = 0;
                        }
                        if (master())
                            fprintf(stdout,"reading ctype w (%d x %d) from %s\n",
                                    Nx,Nz,w_filename.c_str());
                        read_2d_restart(w,w_filename.c_str(),Nx,Nz);
                        break;
                    case FULL3D:

                        if (master()) 
                            fprintf(stdout,"Reading u from %s\n",
                                    u_filename.c_str());
                        read_array(u,u_filename.c_str(),Nx,Ny,Nz);
                        if (master()) 
                            fprintf(stdout,"Reading v from %s\n",
                                    v_filename.c_str());
                        read_array(v,v_filename.c_str(),Nx,Ny,Nz);
                        if (master()) 
                            fprintf(stdout,"Reading w from %s\n",
                                w_filename.c_str());
                        read_array(w,w_filename.c_str(),Nx,Ny,Nz);

                        break;
                }
            }


            // Add a random perturbation to trigger any 3D instabilities
            if (perturb > 0) {
                int myrank;
                MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
                Normal<double> rnd(0,1);
                for (int i = u.lbound(firstDim); i <= u.ubound(firstDim); i++) {
                    rnd.seed(i);
                    for (int j = u.lbound(secondDim); j <= u.ubound(secondDim); j++) {
                        for (int k = u.lbound(thirdDim); k <= u.ubound(thirdDim); k++) {
                            u(i,j,k) *= 1+perturb*rnd.random();
                            v(i,j,k) *= 1+perturb*rnd.random();
                            w(i,j,k) *= 1+perturb*rnd.random();
                        }
                    }
                }
            }

            /* Write out initial values */
            write_array(u,"u",plotnum);
            write_array(v,"v",plotnum);
            write_array(w,"w",plotnum);

        }

        void init_tracer(int t_num, DTArray & the_tracer) {
            if (master()) fprintf(stdout,"Initializing tracer %d\n",t_num);
            /* Initialize the density and take the opportunity to write out the grid */
            if (t_num == 0) {
                if (restarting and (!restart_from_dump)) {
                    init_tracer_restart("b",the_tracer);
                }
                else if (restarting and restart_from_dump) {
                    init_tracer_dump("b",the_tracer);
                }
                else {
                    switch (input_data_types) {
                        case MATLAB:
                            if (master())
                                fprintf(stdout,"reading matlab-type rho (%d x %d) from %s\n",
                                        Nx,Nz,rho_filename.c_str());
                            read_2d_slice(the_tracer,rho_filename.c_str(),Nx,Nz);
                            break;
                        case CTYPE:
                            if (master())
                                fprintf(stdout,"reading ctype rho (%d x %d) from %s\n",
                                        Nx,Nz,rho_filename.c_str());
                            read_2d_restart(the_tracer,rho_filename.c_str(),Nx,Nz);
                            break;
                        case FULL3D:
                            if (master())
                                fprintf(stdout,"reading rho (%d x %d x %d) from %s\n",
                                        Nx,Ny,Nz,rho_filename.c_str());
                            read_array(the_tracer,rho_filename.c_str(),Nx,Ny,Nz);
                            break;
                    }
                }
                write_array(the_tracer,"b",plotnum);
            } else if (t_num == 1) {
                if (restarting and (!restart_from_dump)) {
                    init_tracer_restart("dye1",the_tracer);
                }
                else if (restarting and restart_from_dump) {
                    init_tracer_dump("dye1",the_tracer);
                }
                else {
                    switch (input_data_types) {
                        case MATLAB:
                            if (master())
                                fprintf(stdout,"reading matlab-type tracer (%d x %d) from %s\n",
                                        Nx,Nz,tracer_filename.c_str());
                            read_2d_slice(the_tracer,tracer_filename.c_str(),Nx,Nz);
                            break;
                        case CTYPE:
                            if (master())
                                fprintf(stdout,"reading ctype tracer (%d x %d) from %s\n",
                                        Nx,Nz,tracer_filename.c_str());
                            read_2d_restart(the_tracer,tracer_filename.c_str(),Nx,Nz);
                            break;
                        case FULL3D:
                            if (master())
                                fprintf(stdout,"reading tracer (%d x %d x %d) from %s\n",
                                        Nx,Ny,Nz,tracer_filename.c_str());
                            read_array(the_tracer,tracer_filename.c_str(),Nx,Ny,Nz);
                            break;
                    }
                write_array(the_tracer,"dye1",plotnum);
                }
            }
        }

        void forcing(double t, DTArray & u, DTArray & u_f,
                DTArray & v, DTArray & v_f, DTArray & w, DTArray & w_f,
                vector<DTArray *> & tracers, vector<DTArray *> & tracers_f) {
            /* Velocity forcing */
            u_f = rot_f * v; 
            v_f = -rot_f * u;
            w_f = *(tracers[0]);
            // This is now b = bbar + b'. Vert forcing might change ...
            // BAS: tracer[0] is just b', so the vertical forcing should be correct
            //      not sure who wrote previous note...
            // BAS: Includes Beer-Lambert law for surface ''heating/cooling''
            //      It just impacts buoyancy directly in this case (neglects equation of state)
            //      for the sake of ease
            *(tracers_f[0]) = -N0*N0*w + BL_const*exp(-pow(zz(kk)/BL_depth, 2));
            //*(tracers_f[0]) = -N0*N0*w;
            // BAS: Re-introduced the passive-tracer option
            if (have_tracer) {
                *(tracers_f[1]) = 0;
            }
        }

        userControl() :
            xx(split_range(Nx)), yy(Ny), zz(Nz),
            plotnum(restart_sequence), 
            nextplot(initial_time + plot_interval), itercount(0), lastplot(0),
            last_writeout(0) 
    {
        compute_quadweights(size_x(),size_y(),size_z(),
                length_x(),length_y(),length_z(),
                type_x(),type_y(),type_z());

        // If this is an unmapped grid, generate/write the
        // 3D grid files
        if (!is_mapped()) {
            Array<double,1> xgrid(split_range(Nx)), ygrid(Ny), zgrid(Nz);
            automatic_grid(MinX, MinY, MinZ, &xgrid, &ygrid, &zgrid);
        }
        if (type_x() == NO_SLIP) {
            xx = MinX + Lx*(0.5+0.5*cos(M_PI*ii/(Nx-1)));
        } else {
            xx = MinX + Lx*(ii+0.5)/Nx;
        }
        yy = MinY + Ly*(ii+0.5)/Ny;
        if (type_z() == NO_SLIP) {
            zz = MinZ + Lz*(0.5+0.5*cos(M_PI*ii/(Nz-1)));
        } else {
            zz = MinZ + Lz*(0.5+ii)/Nz;
        }
        //automatic_grid(MinX,MinY,MinZ);
    }
};

int main(int argc, char ** argv) {
    MPI_Init(&argc,&argv);
    int my_rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);

    fprintf(stdout, "Hello from processor %d!\n", my_rank);

    // BAS: Use a wisdom file to store the cumulative wisdom
    //      At the beginning this may be expensive, but that should
    //      disappear quickly in favour of optimal transforms.
    //      I have also pre-computed some wisdom using
    //      fftw-wisdom -v -x -n -c -o wisdom
    //fftw_import_wisdom_from_filename("/work/bastorer/SPINS/spins/src/wisdom");
    // BAS: The above line should store the wisdom information, but it does not
    // appear to do so correctly

    real_start_time = MPI_Wtime();     // for dump
    // To properly handle the variety of options, set up the boost
    // program_options library using the abbreviated interface in
    // ../Options.hpp

    options_init(); // Initialize options

    /* Grid options */
    option_category("Grid Options");

    add_option("Nx",&Nx,"Number of points in X");
    add_option("Ny",&Ny,1,"Number of points in Y");
    add_option("Nz",&Nz,"Number of points in Z");

    string xgrid_type, ygrid_type, zgrid_type;
    add_option("type_x",&xgrid_type,
            "Grid type in X.  Valid values are:\n"
            "   FOURIER: Periodic\n"
            "   FREE_SLIP: Cosine expansion\n"
            "   NO_SLIP: Chebyhsev expansion");
    add_option("type_y",&ygrid_type,"FOURIER","Grid type in Y");
    add_option("type_z",&zgrid_type,"Grid type in Z");

    add_option("Lx",&Lx,"X-length");
    add_option("Ly",&Ly,1.0,"Y-length");
    add_option("Lz",&Lz,"Z-length");

    add_option("min_x",&MinX,0.0,"Unmapped grids: Minimum X-value");
    add_option("min_y",&MinY,0.0,"Minimum Y-value");
    add_option("min_z",&MinZ,0.0,"Minimum Z-value");

    /* Options for Grid Mapping */
    option_category("Grid mapping options");
    add_option("mapped_grid",&mapped,false,"Use a mapped (2D) grid");
    add_option("xgrid",&xgrid_filename,"x-grid filename");
    add_option("ygrid",&ygrid_filename,"","y-grid filename");
    add_option("zgrid",&zgrid_filename,"z-grid filename");

    /* Options for Input Data */
    option_category("Input data");
    string datatype;
    add_option("file_type",&datatype,
            "Format of input data files, including that for the mapped grid."
            "Valid options are:\n"
            "   MATLAB: \tRow-major 2D arrays of size Nx x Nz\n"
            "   CTYPE:  \tColumn-major 2D arrays (including that output by 2D SPINS runs)\n"
            "   FULL:   \tColumn-major 3D arrays; implies CTYPE for grid mapping if enabled"); 

    add_option("u_file",&u_filename,"U-velocity filename");
    add_option("v_file",&v_filename,"","V-velocity filename");
    add_option("w_file",&w_filename,"W-velocity filename");
    add_option("rho_file",&rho_filename,"Rho (density) filename");

    /* Options for Second Tracer */
    option_category("Second tracer");
    add_option("enable_tracer",&have_tracer,"Enable evolution of a second (currently passive) tracer");
    add_option("tracer_file",&tracer_filename,"Tracer filename");
    add_option("tracer_kappa",&tracer_kappa,0.0,"Diffusivity of tracer");
    add_option("tracer_gravity",&tracer_g,0.0,"Gravity for the second tracer");

    /* Physcial Parameters */
    option_category("Physical parameters");
    add_option("g",&g,9.81,"Gravitational acceleration");
    add_option("rot_f",&rot_f,0.0,"Coriolis force term");
    add_option("N0",&N0,0.0,"Buoyancy frequency term");
    add_option("visc",&vel_mu,0.0,"Kinematic viscosity");
    add_option("kappa",&dens_kappa,0.0,"Thermal diffusivity");
    add_option("perturbation",&perturb,0.0,"Velocity perturbation (multiplicative white noise) applied to read-in data.");

    // Running Options
    option_category("Running options");
    add_option("init_time",&initial_time,0.0,"Initial time");
    add_option("final_time",&final_time,"Final time");
    add_option("plot_interval",&plot_interval,"Interval between output times");
    add_option("plot_write_ratio",&plot_write_ratio,1,"Ratio between plotting and writing");

    // Restart Options 
    option_category("Restart options");
    add_switch("restart",&restarting,"Restart from prior output time. OVERRIDES many other values.");
    add_option("restart_time",&restart_time,0.0,"Time to restart from");
    add_option("restart_sequence",&restart_sequence,
            "Sequence number to restart from (if plot_interval has changed)");

    // Filtering Options 
    option_category("Filtering options");
    add_option("f_strength",&f_strength,20.0,"filter strength");
    add_option("f_cutoff",&f_cutoff,0.6,"filter cutoff");
    add_option("f_order",&f_order,2.0,"fiter order");

    // Forcing Options
    option_category("Forcing options");
    add_option("BL_const",&BL_const,0.0,"Beer-Lambert forcing coefficient");
    add_option("BL_depth",&BL_depth,1.0,"Beer-Lambert forcing depth");

    // Diagnostics Options
    option_category("Diagnostics options");
    add_option("compute_norms",&compute_norms,false,"Compute epv perturbation norms?");
    add_option("bg_b_filename",&bg_b_filename,"","Where to find epv basic state if needed.");
    add_option("diag_time",&diag_time, 0.,"(Approximate) Frequency for diagnostic outputs.");
    add_option("compute_eps_qg",&compute_eps_qg,false,"Measure level of QG imbalance?");
    add_option("compute_eps_cyclo",&compute_eps_cyclo,false,"Measure level of cyclostrophic imbalance?");
    add_option("compute_spectra",&compute_spectra,false,"Output 1D spectra?");
    add_option("compute_bu",&compute_bu,false,"Estimate global Burger number?");
    add_option("compute_Ro",&compute_Ro,false,"Estimate global Rossby number?");

    // Chain Options
    
//        std::vector<int> intvec;
//        add_option("Name",&intvec,"Description);
//
//        When run, options of the form --Name 1 2 3 4 (at the command
//            line) or Name = 1 2 3 4 (in the config file) will be added
//            to the vector 'intvec' as separate entries.
    // Slice Options

    // Dumping options
    option_category("Dumping options");
    add_option("compute_time",&compute_time,-1.0,"Time permitted for computation");
    add_option("restart_from_dump",&restart_from_dump,false,"If restart from dump");

    // Parse the options from the command line and config file
    options_parse(argc,argv);
    max_dt = (2*M_PI/N0)/20.0; // BAS: Yes, 20 is necessary

    // Now, make sense of the options received.  Many of these values
    // can be directly used, but the ones of string-type need further
    // procesing.

    // Read dump_time.txt and check if past final time
    if (restart_from_dump){
        restarting = true;
        string dump_str;
        ifstream dump_file;
        dump_file.open ("dump_time.txt");

        getline (dump_file,dump_str); // ingnore 1st line

        getline (dump_file,dump_str);
        restart_time = atof(dump_str.c_str());

        getline (dump_file,dump_str); // ingore 3rd line

        getline (dump_file,dump_str);
        restart_sequence = atoi(dump_str.c_str());
        dump_file.close();

        if (restart_time > final_time){
            // Die, ungracefully
            if (master()){
                fprintf(stderr,"Restart dump time (%.4g) is past final time (%.4g). Quitting now.\n",restart_time,final_time);
            }
            MPI_Finalize(); exit(1);
        }
    }
    if (compute_time > 0){
        avg_write_time = max(100.0*Nx*Ny*Nz/pow(512.0,3), 20.0);
    }

    // Grid types:

    if (xgrid_type == "FOURIER") {
        intype_x = PERIODIC;
    } else if (xgrid_type == "FREE_SLIP") {
        intype_x = FREE_SLIP;
    } else if (xgrid_type == "NO_SLIP") {
        intype_x = NO_SLIP;
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
        MPI_Finalize(); exit(1);
    }
    if (ygrid_type == "FOURIER") {
        intype_y = PERIODIC;
    } else if (ygrid_type == "FREE_SLIP") {
        intype_y = FREE_SLIP;
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }
    if (zgrid_type == "FOURIER") {
        intype_z = PERIODIC;
    } else if (zgrid_type == "FREE_SLIP") {
        intype_z = FREE_SLIP;
    } else if (zgrid_type == "NO_SLIP") {
        intype_z = NO_SLIP;
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    // Input filetypes

    if (datatype=="MATLAB") {
        input_data_types = MATLAB;
    } else if (datatype == "CTYPE") {
        input_data_types = CTYPE;
    } else if (datatype == "FULL") {
        input_data_types = FULL3D;
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for file_type\n",datatype.c_str());
        MPI_Finalize(); exit(1);
    }

    if (restarting) {
        if (restart_sequence <= 0) {
            restart_sequence = int(restart_time/plot_interval);
        }
        if (master()) {
            fprintf(stdout,"Restart flags detected\n");
            fprintf(stdout,"Restarting from time %g, at sequence number %d\n",
                    restart_time,restart_sequence);
        }
        initial_time = restart_time;
    } else {
        // Not restarting, so set the initial sequence number
        // to the initial time / plot_interval
        restart_sequence = int(initial_time/plot_interval);
        if (fmod(initial_time,plot_interval) != 0.0) {
            if (master()) {
                fprintf(stderr,"Warning: the initial time (%g) does not appear to be an even multiple of the plot interval (%g)\n",
                        initial_time,plot_interval);
            }
        }
    }
    userControl mycode;
    FluidEvolve<userControl> kevin_kh(&mycode);
    kevin_kh.initialize();
    kevin_kh.do_run(final_time);

    //fftw_export_wisdom_to_filename("/work/bastorer/SPINS/spins/src/wisdom2");

    MPI_Finalize();
}
