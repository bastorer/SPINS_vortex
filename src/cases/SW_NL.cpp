#include "../TArray.hpp"
#include <blitz/array.h>
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Options.hpp"
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include "../Splits.hpp"
#include <random/normal.h>

using std::string;

//FJP: insert beta, nu, kappa, tau, rho0, wind-forcing, dissipation, perturb amp for h
//FJP: fitler parameters
//FJP: any need to write xgrid,ygrid??
//FJP: write ICs
//FJP: adaptive time stepping

// Input file names

string xgrid_filename,
  ygrid_filename,
  zgrid_filename,
  rho_filename,
  u_filename,
  v_filename,
  h_filename;

int          Nx, Ny, Nz;               // Number of points in x, y, z
double       Lx, Ly;                   // Grid lengths of x, y 
double       N0;                       // Buoyancy frequency
double       g, f0, beta, H;           // gravity and Coriolis
double       nu, kap, tau0;            // viscosity, diffusivity and wind forcing amplitude
double       INITIAL_T, dt, FINAL_T;   // temporal parameters
int          itskip, nct;              // number of plots to skip and total number
double       Ujet, Ljet;               // amplitude and y scale of the jet
int          NL;                       // Nonlinear parameter
double       Norm_x, Norm_y;           // Normalization factors

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using ranlib::Normal;

using namespace std;

// Structure to specify how to compute derivatives
struct Transgeom {
  
  TArrayn::S_EXP type;
  TArrayn::S_EXP u;
  TArrayn::S_EXP v;
  TArrayn::S_EXP h;

};

// Use this to store the solution or the flux arrays
struct solnclass {

  vector<DTArray *> u;
  vector<DTArray *> v;
  vector<DTArray *> h;
  double * rho;
      
};

// Use this to store the differentiation operators
struct deriv_class {

    void (*u_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*u_y)(DTArray &, Trans1D &, Array<double,3> &);
    void (*v_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*v_y)(DTArray &, Trans1D &, Array<double,3> &);
    void (*h_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*h_y)(DTArray &, Trans1D &, Array<double,3> &);

};

/* Initialize Transform types */
Transgeom Sx, Sy;

/* Initialize derivatives  */
deriv_class deriv;
   
// Blitz index placeholders

blitz::firstIndex ii;
blitz::secondIndex jj;
blitz::thirdIndex kk;

void flux_sw(solnclass & soln,                        // solution with u,v,h
             vector<DTArray *> & flux,                // flux for one time step
             DTArray & temp1, DTArray & temp2, DTArray & temp3,       // temporary arrays
             Trans1D & X_xform, Trans1D & Y_xform,    // 1D transformers
             TransWrapper & XY_xform,                 // 2D transformers
             DTArray & ygrid);          

void step_euler(solnclass & soln,                     // solution with u,v,h
                solnclass & flux,                     // flux for three time steps
                DTArray & temp1, DTArray & temp2, DTArray & temp3,    // temporary arrays
                Trans1D & X_xform, Trans1D & Y_xform, // 1D transformers
                TransWrapper & XY_xform,              // 2D transformers
        DTArray & xgrid, DTArray & ygrid,      
                double dt);

void step_ab2(solnclass & soln,                       // solution with u,v,h
              solnclass & flux,                       // flux for three time steps
              DTArray & temp1, DTArray & temp2, DTArray & temp3,      // temporary arrays
              Trans1D & X_xform, Trans1D & Y_xform,   // 1D transformers
              TransWrapper & XY_xform,                // 2D transformers
              DTArray & xgrid, DTArray & ygrid,
              double dt);

void step_ab3(solnclass & soln,                       // solution stores u,v,h
              solnclass & flux,                       // flux for three time steps
              DTArray & temp1, DTArray & temp2, DTArray & temp3,      // temporary arrays
              Trans1D & X_xform, Trans1D & Y_xform,   // 1D transformers
              TransWrapper & XY_xform,                // 2D transformers
              DTArray & xgrid, DTArray & ygrid, // grid
              double dt);

int main(int argc, char ** argv) {

   // Initialize MPI
   MPI_Init(&argc, &argv);

   // To properly handle the variety of options, set up the boost
   // program_options library using the abbreviated interface in
   // ../Options.hpp

   //FJP: clean up options
   options_init(); // Initialize options

   option_category("Grid Options");

   // Grid size
   add_option("Nx",&Nx,"Number of points in X");
   add_option("Ny",&Ny,"Number of points in Y");
   add_option("Nz",&Nz,"Number of layers in Z");

   // Geometry
   string xgrid_type, ygrid_type;
   add_option("type_x",&xgrid_type,
         "Grid type in X.  Valid values are:\n"
         "   FOURIER: Periodic\n"
         "   REAL: Sine/Cosine expansion\n"
         "   CHEB: Chebyshev expansion");
   add_option("type_y",&ygrid_type,
         "Grid type in Y.  Valid values are:\n"
         "   FOURIER: Periodic\n"
         "   REAL: Sine/Cosine expansion\n"
         "   CHEB: Chebyshev expansion");
   
   // Grid lengths
   add_option("Lx",&Lx,"X-length");
   add_option("Ly",&Ly,"Y-length"); 

   // Defines for physical parameters
   add_option("g", &g, "g-gravity");
   add_option("f0",&f0,"f0-Coriolis");
   add_option("H", &H, "H-Mean depth");
   add_option("beta", &beta, "beta-gradient of Corioilis");
   add_option("nu", &nu, "nu-Viscosity");
   add_option("kap", &kap, "kap-Diffusivity");
   add_option("tau0", &tau0, "tau0-Amplitude of wind fording");

   // Timestep parameters
   add_option("t0", &INITIAL_T,"t0-initial time");
   add_option("dt", &dt,"dt-time step");
   add_option("tf", &FINAL_T,"tf-final time");
   add_option("npt",&itskip,"npt-number of iterations per plot");
   add_option("nct",&nct,"nct-total number of iterations");

   // Parameters for Initial Conditions
   add_option("Ujet",&Ujet,"maximum speed of jet");
   add_option("Ljet",&Ljet,"width of the jet");

   add_option("NL",&NL,"Nonlinear or Linear");

   option_category("Grid mapping options");
   add_option("xgrid",&xgrid_filename,"x-grid filename");
   add_option("ygrid",&ygrid_filename,"","y-grid filename");
   add_option("zgrid",&zgrid_filename,"z-grid filename");

   option_category("Input data");
   string datatype;

   //FJP: why is there an extra "" in the v file?
   add_option("rho_file",&rho_filename,"Density filename");
   add_option("u_file",&u_filename,"U-velocity filename");
   add_option("v_file",&v_filename,"","V-velocity filename");
   add_option("h_file",&h_filename,"h-height filename");
   
   // Parse the options from the command line and config file
   options_parse(argc,argv);

   if (master()) printf("Parameters used in sw_channelfft.cpp:\n");
   if (master()) printf("---------------------------\n");
   if (master()) printf("Nx   = %d and Ny = %d (Nz = %d) \n",Nx,Ny,Nz);
   if (master()) printf("Lx   = %g and Ly = %g\n",Lx,Ly);
   if (master()) printf("g    = %g\n",g);
   if (master()) printf("f0   = %g\n",f0);
   if (master()) printf("H    = %g\n",H);
   if (master()) printf("t0   = %g\n",INITIAL_T);
   if (master()) printf("dt   = %g\n",dt);
   if (master()) printf("tf   = %g\n",FINAL_T);
   if (master()) printf("npt  = %d\n",itskip);
   if (master()) printf("nct  = %d\n",nct);
   if (master()) printf("Ujet = %g\n",Ujet);
   if (master()) printf("Ljet = %g\n",Ljet);
   if (master()) printf("NL   = %d\n",NL);
   if (master()) printf("---------------------------\n");
   if (master()) printf(" \n");

   // Define Norm Factors and derivative functions
   if (xgrid_type == "FOURIER") {
     Sx.type = FOURIER;
     Sx.u = FOURIER;
     Sx.v = FOURIER;
     Sx.h = FOURIER;
     deriv.u_x = &deriv_fft;
     deriv.v_x = &deriv_fft;
     deriv.h_x = &deriv_fft;
     Norm_x = (2.*M_PI/Lx);
   } else if (xgrid_type == "REAL") {
     Sx.type = REAL;
     Sx.u = SINE;
     Sx.v = COSINE;
     Sx.h = COSINE;
     deriv.u_x = &deriv_dst;
     deriv.v_x = &deriv_dct;
     deriv.h_x = &deriv_dct;
     Norm_x = (1.*M_PI/Lx);
   } else if (xgrid_type == "CHEB") {
     Sx.type = CHEBY;
     Sx.u = CHEBY;
     Sx.v = CHEBY;
     Sx.h = CHEBY;
     deriv.u_x = &deriv_cheb;
     deriv.v_x = &deriv_cheb;
     deriv.h_x = &deriv_cheb;
     Norm_x = (2./Lx);
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
      MPI_Finalize(); exit(1);
   }

   if (ygrid_type == "FOURIER") {
     Sy.type = FOURIER;
     Sy.u = FOURIER;
     Sy.v = FOURIER;
     Sy.h = FOURIER;
     deriv.u_y = &deriv_fft;
     deriv.v_y = &deriv_fft;
     deriv.h_y = &deriv_fft;
     Norm_y = (2.*M_PI/Ly);
   } else if (ygrid_type == "REAL") {
     Sy.type = REAL;
     Sy.u = COSINE;
     Sy.v = SINE;
     Sy.h = COSINE;
     deriv.u_y = &deriv_dct;
     deriv.v_y = &deriv_dst;
     deriv.h_y = &deriv_dct;
     Norm_y = (1.*M_PI/Ly);
   } else if (ygrid_type == "CHEB") {
     Sy.type = CHEBY;
     Sy.u = CHEBY;
     Sy.v = CHEBY;
     Sy.h = CHEBY;
     deriv.u_y = &deriv_cheb;
     deriv.v_y = &deriv_cheb;
     deriv.h_y = &deriv_cheb;
     Norm_y = (2./Ly);
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
      MPI_Finalize(); exit(1);
   }

   TinyVector<int,3> local_lbound, local_extent;
   GeneralArrayStorage<3> local_storage;

   // Get parameters for local array storage
   local_lbound = alloc_lbound(Nx,Nz,Ny);
   local_extent = alloc_extent(Nx,Nz,Ny);
   local_storage = alloc_storage(Nx,Nz,Ny);

   /* Initialize solution and fluxes */
   solnclass soln, flux; 

   soln.u.resize(1);
   soln.v.resize(1);
   soln.h.resize(1);
   flux.u.resize(3);
   flux.v.resize(3);
   flux.h.resize(3);
     
   // Allocate the arrays used in the above
   DTArray xgrid(local_lbound,local_extent,local_storage),
           ygrid(local_lbound,local_extent,local_storage),
           zgrid(local_lbound,local_extent,local_storage);
   soln.u[0] = new DTArray(local_lbound,local_extent,local_storage);
   soln.v[0] = new DTArray(local_lbound,local_extent,local_storage);
   soln.h[0] = new DTArray(local_lbound,local_extent,local_storage);
   flux.u[0] = new DTArray(local_lbound,local_extent,local_storage);
   flux.u[1] = new DTArray(local_lbound,local_extent,local_storage);
   flux.u[2] = new DTArray(local_lbound,local_extent,local_storage);
   flux.v[0] = new DTArray(local_lbound,local_extent,local_storage);
   flux.v[1] = new DTArray(local_lbound,local_extent,local_storage);
   flux.v[2] = new DTArray(local_lbound,local_extent,local_storage);
   flux.h[0] = new DTArray(local_lbound,local_extent,local_storage);
   flux.h[1] = new DTArray(local_lbound,local_extent,local_storage);
   flux.h[2] = new DTArray(local_lbound,local_extent,local_storage);

   double mb;
   
   // Read Layer Densities
   DTArray rho_tmp(alloc_lbound(1,Nz,1),alloc_extent(1,Nz,1),alloc_storage(1,Nz,1));
   double my_rho[Nz];
   if (master()) 
     fprintf(stdout,"Reading rho from %s\n",
        rho_filename.c_str());
   read_array(rho_tmp,rho_filename.c_str(),1,Nz,1);
   for (int j = 0; j < Nz; j++) {
     my_rho[j] = rho_tmp(0,j,0);
   }
   soln.rho = my_rho;
   
   // Read  Grid
   if (master()) 
     fprintf(stdout,"Reading x from %s\n",
             xgrid_filename.c_str());
   read_array(xgrid,xgrid_filename.c_str(),Nx,Nz,Ny);
   
   if (master()) 
     fprintf(stdout,"Reading y from %s\n",
             ygrid_filename.c_str());
   read_array(ygrid,ygrid_filename.c_str(),Nx,Nz,Ny);
   
   if (master()) 
     fprintf(stdout,"Reading z from %s\n",
             zgrid_filename.c_str());
   read_array(zgrid,zgrid_filename.c_str(),Nx,Nz,Ny);
   
   // Read Initial Conditions
   if (master()) 
     fprintf(stdout,"Reading u from %s\n",
             u_filename.c_str());
   read_array(*soln.u[0],u_filename.c_str(),Nx,Nz,Ny);
   
   mb = pvmax(*soln.u[0]);
   if (master()) printf("Max u in IC = %g\n",mb);
   
   if (master()) 
     fprintf(stdout,"Reading v from %s\n",
             v_filename.c_str());
   read_array(*soln.v[0],v_filename.c_str(),Nx,Nz,Ny);
   
   mb = pvmax(*soln.v[0]);
   if (master()) printf("Max v in IC = %g\n",mb);
   
   if (master()) 
     fprintf(stdout,"Reading h from %s\n",
            h_filename.c_str());
   read_array(*soln.h[0],h_filename.c_str(),Nx,Nz,Ny);

   mb = pvmax(*soln.h[0]);
   if (master()) printf("Max h in IC = %g\n",mb);
   

   // Necessary FFT Based Transformers
   Trans1D X_xform(Nx,Nz,Ny,firstDim,Sx.type), Y_xform(Nx,Nz,Ny,thirdDim,Sy.type);
   
   TransWrapper XY_xform(Nx,Nz,Ny,Sx.type,NONE,Sy.type);
   
   // Normalization factor for the 2D transform
   double norm_2d = XY_xform.norm_factor();

   // Allocating some physical-space temporaries
   DTArray temp1(local_lbound,local_extent,local_storage),
           temp2(local_lbound,local_extent,local_storage),
           temp3(local_lbound,local_extent,local_storage);

   // BAS: Read grid from config files.
   /*
   // Build Grid in x, y
   Array<double,1> xgrid(split_range(Nx)), ygrid(Ny);

   //FJP: store in function
   if (Sx.type == SINE || Sx.type == COSINE || Sx.type == FOURIER || Sx.type==REAL) {
     xgrid = (ii+0.5)/Nx*Lx - Lx/2;
   }
   if (Sx.type == CHEBY) {
     xgrid = -cos(M_PI*ii/(Nx-1));
     xgrid = Lx/2.*xgrid(ii);
   }
   if (Sy.type == SINE || Sy.type == COSINE || Sy.type == FOURIER || Sy.type==REAL) {
     ygrid = (ii+0.5)/Ny*Ly - Ly/2;
   }
   if (Sy.type == CHEBY) {
     ygrid = -cos(M_PI*ii/(Ny-1));
     ygrid = Ly/2.*ygrid(ii);
   }
   */

   // iteration count
   int iterct = 0;
   double t = 0;

   /* This is now done in the config file

   // Output Grid and Initial Conditions
   temp1 = xgrid(ii)+0*kk;
   write_array(temp1,"xgrid");
   write_reader(temp1,"xgrid");
   temp1 = ygrid(kk);
   write_array(temp1,"ygrid");
   write_reader(temp1,"ygrid");

   // Initial Conditions: Bickley jet with f0*u = - g*dy*h
   *soln.u[0] = Ujet*pow(cosh((ygrid(kk))/Ljet),-2) + 0*xgrid(ii);
   *soln.v[0] = 0*ygrid(kk)+0*xgrid(ii);
   *soln.h[0] =-Ujet*f0*Ljet/g*tanh((ygrid(kk))/Ljet) + 0*xgrid(ii);

   // Initial Conditions: perturbations
   int i, j;
   Normal<double> rnd(0,1);
   for (int i = soln.h[0]->lbound(firstDim); i <= soln.h[0]->ubound(firstDim); i++) {
      rnd.seed(i);
      for (int k = soln.h[0]->lbound(thirdDim); k <= soln.h[0]->ubound(thirdDim); k++) {
        (*soln.h[0])(i,0,k) += 1e-5*rnd.random();
      }
   }
   */
   
   // Write to a file
   write_reader(*soln.u[0],"u",true);
   write_array(*soln.u[0],"u",0);
   write_reader(*soln.v[0],"v",true);
   write_array(*soln.v[0],"v",0);
   write_reader(*soln.h[0],"h",true);
   write_array(*soln.h[0],"h",0);

   pvmax(*soln.h[0]);
   if (master()) printf("Wrote time %g (iter %d) with Max h = %g\n",t,iterct,mb);

   // Advance Euler Step 
   step_euler(soln, flux, temp1, temp2, temp3, X_xform, Y_xform, XY_xform, xgrid, ygrid, dt);

   // Impose no-normal flow BCS explicitly on a cheb grid
   if (xgrid_type == "CHEB") {
     (*soln.u[0])(0,    blitz::Range::all(), blitz::Range::all()) = 0;
     (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
   }
   if (ygrid_type == "CHEB") {
     (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
     (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
   }
   
   iterct++;
   t = t + dt;
   
   //Advance AB2 Step
   step_ab2(soln, flux, temp1, temp2, temp3, X_xform, Y_xform, XY_xform, xgrid, ygrid, dt);

   // Impose no-normal flow BCS explicitly on a cheb grid
   if (xgrid_type == "CHEB") {
     (*soln.u[0])(0, blitz::Range::all(), blitz::Range::all()) = 0;
     (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
   }
   if (ygrid_type == "CHEB") {
     (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
     (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
   }
   
   iterct++;
   t = t + dt;

   while (t < FINAL_T) {

     //Advance Solution: AB3
     step_ab3(soln, flux,temp1, temp2, temp3, X_xform, Y_xform, XY_xform, xgrid, ygrid, dt);

     // Impose no-normal flow BCS explicitly on a cheb grid
     if (xgrid_type == "CHEB") {
       (*soln.u[0])(0, blitz::Range::all(), blitz::Range::all()) = 0;
       (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
     }
     if (ygrid_type == "CHEB") {
       (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
       (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
     }
   
     iterct++;
     t = t + dt;
     
     if (!(iterct % itskip)) {

       // Write data to a file and maximum values
       mb = pvmax(*soln.h[0]);
       if (master()) printf("Wrote time %g (iter %d) with Max h = %g\n",t,iterct,mb);
       write_array(*soln.u[0],"u",iterct/itskip);
       write_array(*soln.v[0],"v",iterct/itskip);
       write_array(*soln.h[0],"h",iterct/itskip);

     }
   }
   
   if (master()) printf("Finished at time %g!\n",t);  

   MPI_Finalize(); 
   return 0;
}

void flux_sw(solnclass & soln,
             DTArray & fluxu, DTArray & fluxv, DTArray & fluxh, 
             DTArray & temp1, DTArray & temp2, DTArray & temp3,
             Trans1D & X_xform, Trans1D & Y_xform,
             TransWrapper & XY_xform,                 
             DTArray & ygrid)                 
{
  // Make nice references for arrays used
  DTArray &temp = temp1, &rhs = temp2;

  /* 
   * NOTE: Indexing has 0 as top layer and Nz-1 being bottom layer.
   * ALSO NOTE: I've made another temporary array.
   * ALSO ALSO NOTE: The multi-loop structure probably isn't the most efficient,
   *                 but I'm not sure how blitz works.
   */


  DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
  // Compute the intra-layer terms

    // fluxu = u*u_x + v*u_y - f*v + g h_x
    deriv.u_x(u,X_xform,temp);       // calc u_x
    rhs = u * temp*Norm_x;
    deriv.u_y(u,Y_xform,temp);       // calc u_y
    rhs = rhs + v*temp*Norm_y;
    deriv.h_x(h,X_xform,temp);       // calc h_x
    //fluxu = rhs - v*(f0 + beta*ygrid(1,1,kk)) + g*temp*Norm_x ;
    fluxu = rhs - v*f0 + g*temp*Norm_x ;

    // fluxv = u*v_x + v*v_y + f*u + g h_y
    deriv.v_x(v,X_xform,temp);       // calc v_x
    rhs = u * temp * Norm_x;
    deriv.v_y(v,Y_xform,temp);       // calc v_y
    rhs = rhs + v*temp*Norm_y;
    deriv.h_y(h,Y_xform,temp);       // calc h_y
    //fluxv = rhs + u*(f0 + beta*ygrid(1,1,kk)) + g*temp*Norm_y ;
    fluxv = rhs + u*f0 + g*temp*Norm_y ;

    // fluxh = (h*u)_x + (h*v)_y
    temp = u*h;
    deriv.u_x(temp,X_xform,temp);    // calc [h*u]_x 
    rhs = temp * Norm_x;
    temp = v*h;                      // calc [h*v]_y 
    deriv.v_y(temp,Y_xform,temp);
    fluxh = rhs + temp*Norm_y;

  deriv.h_x(h,X_xform,temp1);
  deriv.h_y(h,Y_xform,temp2);
  temp1 *= Norm_x;
  temp2 *= Norm_y;
  for (int i = 0; i < Nx; i++) {
    for (int k = 0; k < Ny; k++) {
      // Pressure from above layers
      for (int j = 0; j < Nz; j++) { // j is layer index, starting from top
        for (int j2 = 0; j2 < j; j2++) { // j2 is index for above layers
          fluxu(i,j,k) += -g/soln.rho[j]*soln.rho[j2]*temp1(i,j2,k);
          fluxv(i,j,k) += -g/soln.rho[j]*soln.rho[j2]*temp2(i,j2,k);
        }
      }
      // Impact of `topography' of lower layer
      for (int j = Nz-1; j >= 0; j--) { // j is layer index, starting from bottom
        for (int j2 = Nz-1; j2 > j; j2--) { // j2 is index for layers below
          fluxu(i,j,k) += -g*temp1(i,j2,k);
          fluxv(i,j,k) += -g*temp2(i,j2,k);
        }
      }
    }
  }

}

void step_euler(solnclass & soln, solnclass & flux,
                DTArray & temp1, DTArray & temp2, DTArray & temp3, 
                        Trans1D & X_xform, Trans1D & Y_xform,
                        TransWrapper & XY_xform, 
                DTArray & xgrid, DTArray & ygrid, 
                double dt) {

  // Make nice references for arrays used
  DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
  DTArray &fluxu = *flux.u[0], &fluxv = *flux.v[0], &fluxh = *flux.h[0];

  // Compute flux
  flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, temp3, X_xform, Y_xform, XY_xform, ygrid);

  // Advance Solution
  u = u - fluxu*dt;
  v = v - fluxv*dt;
  h = h - fluxh*dt;
      
  // Filter, with sensible defaults: cutoff, order, strength
  filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.6, 2.0, 20.0);
  filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.6, 2.0, 20.0);
  filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.6, 2.0, 20.0);

}

void step_ab2(solnclass & soln, solnclass & flux,
              DTArray & temp1, DTArray & temp2, DTArray & temp3,
              Trans1D & X_xform, Trans1D & Y_xform,
              TransWrapper & XY_xform, 
              DTArray & xgrid, DTArray & ygrid, 
              double dt) {
  
  // Make nice references for arrays used
  DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
  DTArray &fluxu = *flux.u[1], &fluxv = *flux.v[1], &fluxh = *flux.h[1];
  
  // Compute flux
  flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, temp3, X_xform, Y_xform, XY_xform, ygrid);
  
  // Advance solution 
  u = u - 0.5*dt*(3*fluxu - *flux.u[0]);
  v = v - 0.5*dt*(3*fluxv - *flux.v[0]);
  h = h - 0.5*dt*(3*fluxh - *flux.h[0]);
      
  // Filter, with sensible defaults: cutoff, order, strength
  filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.6, 2.0, 20.0);
  filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.6, 2.0, 20.0);
  filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.6, 2.0, 20.0);

}

void step_ab3(solnclass & soln, solnclass & flux,
              DTArray & temp1, DTArray & temp2, DTArray & temp3,
              Trans1D & X_xform, Trans1D & Y_xform,
              TransWrapper & XY_xform, 
              DTArray & xgrid, DTArray & ygrid, 
              double dt) {

  // Make nice references for arrays used
  DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
  DTArray &fluxu = *flux.u[2], &fluxv = *flux.v[2], &fluxh = *flux.h[2];
    
  // Compute flux
  flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, temp3, X_xform, Y_xform, XY_xform, ygrid);
  
  // Advance solution
  u = u - dt/12*(23*fluxu - *flux.u[1]*16 + *flux.u[0]*5);
  v = v - dt/12*(23*fluxv - *flux.v[1]*16 + *flux.v[0]*5);
  h = h - dt/12*(23*fluxh - *flux.h[1]*16 + *flux.h[0]*5);

  // Filter, with sensible defaults: cutoff, order, strength
  filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.6, 2.0, 20.0);
  filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.6, 2.0, 20.0);
  filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.6, 2.0, 20.0);

  *flux.u[0] = *flux.u[1];
  *flux.v[0] = *flux.v[1];
  *flux.h[0] = *flux.h[1];
  *flux.u[1] = fluxu;
  *flux.v[1] = fluxv;
  *flux.h[1] = fluxh;

}
