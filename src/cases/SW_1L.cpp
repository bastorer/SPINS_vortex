#include "../TArray.hpp"
#include <blitz/array.h>
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Options.hpp"
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include "../Splits.hpp"
#include <random/normal.h>

using std::string;

// Input file names
string xgrid_filename,
       ygrid_filename,
       zgrid_filename,
       u_filename,
       v_filename,
       h_filename;

int          Nx, Ny, Nz;               // Number of points in x, y, z
double       Lx, Ly;                   // Grid lengths of x, y 
double       N0;                       // Buoyancy frequency
double       g, f0, beta, H;           // gravity and Coriolis
double       t0, tf, tplot;            // temporal parameters
double       next_plot_time;
int          itskip, nct;              // number of plots to skip and total number
int          NL;                       // Nonlinear parameter
double       Norm_x, Norm_y;           // Normalization factors
double       sponge_width_east, sponge_width_west, sponge_width_north, sponge_width_south; 
double       sponge_stren_east, sponge_stren_west, sponge_stren_north, sponge_stren_south; 
double maxh, maxu, maxv;
int out_cnt, iterct;

bool restarting = false;
int restart_sequence;
double restart_time;

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using ranlib::Normal;

using namespace std;

vector<DTArray *> sponge;

// Structure to specify how to compute derivatives
struct Transgeom {

    TArrayn::S_EXP type;
    TArrayn::S_EXP u;
    TArrayn::S_EXP v;
    TArrayn::S_EXP h;

};

// Use this to store the solution or the flux arrays
struct solnclass {

    vector<DTArray *> u;
    vector<DTArray *> v;
    vector<DTArray *> h;

    double t, dt0, dt1, dt2;
    double w0, w1, w2;
    double dx, dy;
    double c0;

    bool do_plot;
    double next_plot_time;

};

// Use this to store the differentiation operators
struct deriv_class {

    void (*u_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*u_y)(DTArray &, Trans1D &, Array<double,3> &);
    void (*v_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*v_y)(DTArray &, Trans1D &, Array<double,3> &);
    void (*h_x)(DTArray &, Trans1D &, Array<double,3> &);
    void (*h_y)(DTArray &, Trans1D &, Array<double,3> &);

};

/* Initialize Transform types */
Transgeom Sx, Sy;

/* Initialize derivatives  */
deriv_class deriv;

// Blitz index placeholders

blitz::firstIndex ii;
blitz::secondIndex jj;
blitz::thirdIndex kk;

void flux_sw(solnclass & soln,                        // solution with u,v,h
        vector<DTArray *> & flux,                // flux for one time step
        DTArray & temp1, DTArray & temp2,        // temporary arrays
        Trans1D & X_xform, Trans1D & Y_xform,    // 1D transformers
        TransWrapper & XY_xform,                 // 2D transformers
        Array<double,1> & ygrid,
        DTArray & sponge);

void compute_dt(solnclass & soln);

void step_euler(solnclass & soln,                     // solution with u,v,h
        solnclass & flux,                     // flux for three time steps
        DTArray & temp1, DTArray & temp2,     // temporary arrays
        Trans1D & X_xform, Trans1D & Y_xform, // 1D transformers
        TransWrapper & XY_xform,              // 2D transformers
        Array<double,1> & xgrid,      
        Array<double,1> & ygrid,      
        DTArray & sponge);

void step_ab2(solnclass & soln,                       // solution with u,v,h
        solnclass & flux,                       // flux for three time steps
        DTArray & temp1, DTArray & temp2,       // temporary arrays
        Trans1D & X_xform, Trans1D & Y_xform,   // 1D transformers
        TransWrapper & XY_xform,                // 2D transformers
        Array<double,1> & xgrid, Array<double,1> & ygrid,
        DTArray & sponge);

void step_ab3(solnclass & soln,                       // solution stores u,v,h
        solnclass & flux,                       // flux for three time steps
        DTArray & temp1, DTArray & temp2,       // temporary arrays
        Trans1D & X_xform, Trans1D & Y_xform,   // 1D transformers
        TransWrapper & XY_xform,                // 2D transformers
        Array<double,1> & xgrid, Array<double,1> & ygrid, // grid
        DTArray & sponge);

int main(int argc, char ** argv) {

    // Initialize MPI
    MPI_Init(&argc, &argv);

    // To properly handle the variety of options, set up the boost
    // program_options library using the abbreviated interface in
    // ../Options.hpp

    options_init(); // Initialize options

    option_category("Grid Options");

    // Grid size
    add_option("Nx",&Nx,"Number of points in X");
    add_option("Ny",&Ny,"Number of points in Y");
    add_option("Nz",&Nz,"Number of layers in Z");

    // Geometry
    string xgrid_type, ygrid_type;
    add_option("type_x",&xgrid_type,
            "Grid type in X.  Valid values are:\n"
            "   FOURIER: Periodic\n"
            "   REAL: Sine/Cosine expansion\n"
            "   CHEB: Chebyshev expansion");
    add_option("type_y",&ygrid_type,
            "Grid type in Y.  Valid values are:\n"
            "   FOURIER: Periodic\n"
            "   REAL: Sine/Cosine expansion\n"
            "   CHEB: Chebyshev expansion");

    // Grid lengths
    add_option("Lx",&Lx,"X-length");
    add_option("Ly",&Ly,"Y-length"); 

    // Defines for physical parameters
    add_option("g", &g, "g-gravity");
    add_option("f0",&f0,"f0-Coriolis");
    add_option("H", &H, "H-Mean depth");
    add_option("beta", &beta, "beta-gradient of Corioilis");

    // Restarting
    option_category("Restart options"); 
    add_option("restart",&restarting,"Restart flag");
    add_option("restart_sequence",&restart_sequence,"Restart Sequence");

    // Timestep parameters
    add_option("t0", &t0, "t0-initial time");
    add_option("tf", &tf, "tf-final time");
    add_option("tplot", &tplot, "tplot - interval between outputs (in seconds)");

    // Sponge parameters
    add_option("sponge_width_east",&sponge_width_east,1.,"width of eastern sponge");
    add_option("sponge_width_west",&sponge_width_west,1.,"width of western sponge");
    add_option("sponge_width_north",&sponge_width_north,1.,"width of northern sponge");
    add_option("sponge_width_south",&sponge_width_south,1.,"width of southern sponge");
    add_option("sponge_stren_east",&sponge_stren_east,0.,"strength of eastern sponge");
    add_option("sponge_stren_west",&sponge_stren_west,0.,"strength of western sponge");
    add_option("sponge_stren_north",&sponge_stren_north,0.,"strength of northern sponge");
    add_option("sponge_stren_south",&sponge_stren_south,0.,"strength of southern sponge");

    // Parameters for Initial Conditions
    add_option("NL",&NL,"Nonlinear or Linear");

    option_category("Grid mapping options");
    add_option("xgrid",&xgrid_filename,"x-grid filename");
    add_option("ygrid",&ygrid_filename,"","y-grid filename");
    //add_option("zgrid",&zgrid_filename,"z-grid filename");

    option_category("Input data");
    string datatype;

    //FJP: why is there an extra "" in the v file?
    add_option("u_file",&u_filename,"U-velocity filename");
    add_option("v_file",&v_filename,"","V-velocity filename");
    add_option("h_file",&h_filename,"h-height filename");

    // Parse the options from the command line and config file
    options_parse(argc,argv);

    if (restarting) restart_time = tplot*restart_sequence;

    if (master()) printf("Parameters used in sw_channelfft.cpp:\n");
    if (master()) printf("---------------------------\n");
    if (master()) printf("(Nx,Ny) = (%d, %d) (Nz = %d) \n",Nx,Ny,Nz);
    if (master()) printf("(Lx,Ly) = (%g, %g)\n",Lx,Ly);
    if (master()) printf("g       = %g\n",g);
    if (master()) printf("f0      = %g\n",f0);
    if (master()) printf("H       = %g\n",H);
    if (master()) printf("t0      = %g\n",t0);
    if (master()) printf("tf      = %g\n",tf);
    if (master()) printf("tplot   = %g\n",tplot);
    if (master()) printf("NL      = %d\n",NL);
    if (restarting) {
        if (master()) fprintf(stdout,"restarting at t = %g\n",restart_time);
        if (master()) fprintf(stdout,"restarting at index = %d\n",restart_sequence);
    }
    if (master()) printf("---------------------------\n");
    if (master()) printf(" \n");


    // Define Norm Factors and derivative functions
    if (xgrid_type == "FOURIER") {
        Sx.type = FOURIER;
        Sx.u = FOURIER;
        Sx.v = FOURIER;
        Sx.h = FOURIER;
        deriv.u_x = &deriv_fft;
        deriv.v_x = &deriv_fft;
        deriv.h_x = &deriv_fft;
        Norm_x = (2.*M_PI/Lx);
    } else if (xgrid_type == "REAL") {
        Sx.type = REAL;
        Sx.u = SINE;
        Sx.v = COSINE;
        Sx.h = COSINE;
        deriv.u_x = &deriv_dst;
        deriv.v_x = &deriv_dct;
        deriv.h_x = &deriv_dct;
        Norm_x = (1.*M_PI/Lx);
    } else if (xgrid_type == "CHEB") {
        Sx.type = CHEBY;
        Sx.u = CHEBY;
        Sx.v = CHEBY;
        Sx.h = CHEBY;
        deriv.u_x = &deriv_cheb;
        deriv.v_x = &deriv_cheb;
        deriv.h_x = &deriv_cheb;
        Norm_x = (2./Lx);
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    if (ygrid_type == "FOURIER") {
        Sy.type = FOURIER;
        Sy.u = FOURIER;
        Sy.v = FOURIER;
        Sy.h = FOURIER;
        deriv.u_y = &deriv_fft;
        deriv.v_y = &deriv_fft;
        deriv.h_y = &deriv_fft;
        Norm_y = (2.*M_PI/Ly);
    } else if (ygrid_type == "REAL") {
        Sy.type = REAL;
        Sy.u = COSINE;
        Sy.v = SINE;
        Sy.h = COSINE;
        deriv.u_y = &deriv_dct;
        deriv.v_y = &deriv_dst;
        deriv.h_y = &deriv_dct;
        Norm_y = (1.*M_PI/Ly);
    } else if (ygrid_type == "CHEB") {
        Sy.type = CHEBY;
        Sy.u = CHEBY;
        Sy.v = CHEBY;
        Sy.h = CHEBY;
        deriv.u_y = &deriv_cheb;
        deriv.v_y = &deriv_cheb;
        deriv.h_y = &deriv_cheb;
        Norm_y = (2./Ly);
    } else {
        if (master())
            fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    TinyVector<int,3> local_lbound, local_extent;
    GeneralArrayStorage<3> local_storage;

    // Get parameters for local array storage
    local_lbound = alloc_lbound(Nx,Nz,Ny);
    local_extent = alloc_extent(Nx,Nz,Ny);
    local_storage = alloc_storage(Nx,Nz,Ny);

    /* Initialize solution and fluxes */
    solnclass soln, flux; 
    soln.dx = Lx/Nx;
    soln.dy = Ly/Ny;
    soln.c0 = pow(g*H,0.5);

    soln.u.resize(1);
    soln.v.resize(1);
    soln.h.resize(1);
    flux.u.resize(3);
    flux.v.resize(3);
    flux.h.resize(3);
    sponge.resize(1);

    // Allocate the arrays used in the above
    sponge[0] = new DTArray(local_lbound,local_extent,local_storage);
    soln.u[0] = new DTArray(local_lbound,local_extent,local_storage);
    soln.v[0] = new DTArray(local_lbound,local_extent,local_storage);
    soln.h[0] = new DTArray(local_lbound,local_extent,local_storage);
    for (int II = 0; II < 3; II++) {
        flux.u[II] = new DTArray(local_lbound,local_extent,local_storage);
        flux.v[II] = new DTArray(local_lbound,local_extent,local_storage);
        flux.h[II] = new DTArray(local_lbound,local_extent,local_storage);
    }

    // Read Initial Conditions
    if (restarting) {
        out_cnt = 1 + restart_sequence;
        soln.t = restart_time;

        char filename[100];

        snprintf(filename,100,"u.%d",restart_sequence);
        if (master()) fprintf(stdout,"Reading u from u.%d\n", restart_sequence);
        read_array(*soln.u[0], filename, Nx, Nz, Ny);

        snprintf(filename,100,"v.%d",restart_sequence);
        if (master()) fprintf(stdout,"Reading v from v.%d\n", restart_sequence);
        read_array(*soln.v[0], filename, Nx, Nz, Ny);

        snprintf(filename,100,"h.%d",restart_sequence);
        if (master()) fprintf(stdout,"Reading h from h.%d\n", restart_sequence);
        read_array(*soln.h[0], filename, Nx, Nz, Ny);
    }
    else { 

        out_cnt = 0;
        soln.t = 0.;

        if (master()) fprintf(stdout,"Reading u from %s\n", u_filename.c_str());
        read_array(*soln.u[0],u_filename.c_str(),Nx,Nz,Ny);

        if (master()) fprintf(stdout,"Reading v from %s\n", v_filename.c_str());
        read_array(*soln.v[0],v_filename.c_str(),Nx,Nz,Ny);

        if (master()) fprintf(stdout,"Reading h from %s\n", h_filename.c_str());
        read_array(*soln.h[0],h_filename.c_str(),Nx,Nz,Ny);
    }
    maxu = pvmax(*soln.u[0]);
    maxv = pvmax(*soln.v[0]);
    maxh = pvmax(*soln.h[0]);
    if (master()) printf("Max (h,u,v) in ICs = (%g,%g,%g)\n",maxh,maxu,maxv);

    // Necessary FFT Based Transformers
    Trans1D X_xform(Nx,1,Ny,firstDim,Sx.type), Y_xform(Nx,1,Ny,thirdDim,Sy.type);

    TransWrapper XY_xform(Nx,1,Ny,Sx.type,NONE,Sy.type);

    // Normalization factor for the 2D transform
    double norm_2d = XY_xform.norm_factor();

    // Allocating some physical-space temporaries
    DTArray temp1(local_lbound,local_extent,local_storage),
            temp2(local_lbound,local_extent,local_storage);

    // Build Grid in x, y
    Array<double,1> xgrid(split_range(Nx)), ygrid(Ny);

    if (Sx.type == SINE || Sx.type == COSINE || Sx.type == FOURIER || Sx.type==REAL) {
        xgrid = (ii+0.5)/Nx*Lx - Lx/2;
    }
    if (Sx.type == CHEBY) {
        xgrid = -cos(M_PI*ii/(Nx-1));
        xgrid = Lx/2.*xgrid(ii);
    }
    if (Sy.type == SINE || Sy.type == COSINE || Sy.type == FOURIER || Sy.type==REAL) {
        ygrid = (ii+0.5)/Ny*Ly - Ly/2;
    }
    if (Sy.type == CHEBY) {
        ygrid = -cos(M_PI*ii/(Ny-1));
        ygrid = Ly/2.*ygrid(ii);
    }

    // BAS: Compute the sponge coefficients
    // sponge = a*arctan((x-x0)/L)
    // a = sponge_stren_*
    // x0 = sponge_width_*
    // L = sponge_sharp_*
    *sponge[0] = (  sponge_stren_east*(1-tanh((Lx/2+xgrid(ii))/sponge_width_east))
            + sponge_stren_west*(1-tanh((Lx/2-xgrid(ii))/sponge_width_west))
            + sponge_stren_north*(1-tanh((Ly/2-ygrid(kk))/sponge_width_north))
            + sponge_stren_south*(1-tanh((Ly/2+ygrid(kk))/sponge_width_south)));

    // iteration count
    iterct = 0;
    soln.dt0 = 1.;
    soln.dt1 = 1.;
    soln.dt2 = 1.;
    soln.next_plot_time = tplot;
    soln.do_plot = false;

    // Write to a file
    write_array(*soln.u[0],"u",0);
    write_array(*soln.v[0],"v",0);
    write_array(*soln.h[0],"h",0);

    maxh = pvmax(*soln.h[0]);
    maxu = pvmax(*soln.u[0]);
    maxv = pvmax(*soln.v[0]);
    if (master()) { 
        printf("Wrote at %.4g days (iter %d, %.4g%%) with max(h,u,v) = (%.4g, %.4g, %.4g)\n", 
            soln.t/86400., iterct, 100.*soln.t/tf, maxh, maxu, maxv);
    }

    // Advance Euler Step 
    step_euler(soln, flux, temp1, temp2, X_xform, Y_xform, XY_xform, xgrid, ygrid, *sponge[0]);

    // Impose no-normal flow BCS explicitly on a cheb grid
    if (xgrid_type == "CHEB") {
        (*soln.u[0])(0,    blitz::Range::all(), blitz::Range::all()) = 0;
        (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
    }
    if (ygrid_type == "CHEB") {
        (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
        (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
    }

    iterct++;
    soln.t += soln.dt0;

    //Advance AB2 Step
    step_ab2(soln, flux, temp1, temp2, X_xform, Y_xform, XY_xform, xgrid, ygrid, *sponge[0]);

    // Impose no-normal flow BCS explicitly on a cheb grid
    if (xgrid_type == "CHEB") {
        (*soln.u[0])(0, blitz::Range::all(), blitz::Range::all()) = 0;
        (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
    }
    if (ygrid_type == "CHEB") {
        (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
        (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
    }

    iterct++;
    soln.t += soln.dt0;

    while (soln.t < tf) {

        //Advance Solution: AB3
        step_ab3(soln, flux, temp1, temp2, X_xform, Y_xform, XY_xform, xgrid, ygrid, *sponge[0]);

        // Impose no-normal flow BCS explicitly on a cheb grid
        if (xgrid_type == "CHEB") {
            (*soln.u[0])(0, blitz::Range::all(), blitz::Range::all()) = 0;
            (*soln.u[0])(Nx-1, blitz::Range::all(), blitz::Range::all()) = 0;
        }
        if (ygrid_type == "CHEB") {
            (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), 0) = 0;
            (*soln.v[0])(blitz::Range::all(), blitz::Range::all(), Ny-1) = 0;
        }

        iterct++;
        soln.t += soln.dt0;

        if (soln.do_plot) {
            out_cnt++;

            // Write data to a file and maximum values
            maxh = pvmax(*soln.h[0]);
            maxu = pvmax(*soln.u[0]);
            maxv = pvmax(*soln.v[0]);
            if (master()) {
                printf("Wrote at %.4g days (iter %d, %.4g%%) with max(h,u,v) = (%.4g, %.4g, %.4g)\n", 
                    soln.t/86400., iterct, 100.*soln.t/tf, maxh, maxu, maxv);
            }
            write_array(*soln.u[0],"u",out_cnt);
            write_array(*soln.v[0],"v",out_cnt);
            write_array(*soln.h[0],"h",out_cnt);

            soln.do_plot = false;
        }
    }

    if (master()) printf("Finished at time %g!\n", soln.t);  

    MPI_Finalize(); 
    return 0;
}

void compute_dt(solnclass & soln) {

    // Compute restrictions
    double tmp1, tmp2;
    double max_u, max_v;
    double dt = 0.;

    tmp1 = pvmax(*soln.u[0]);
    tmp2 = -pvmin(*soln.u[0]);
    max_u = tmp1>tmp2 ? tmp1 : tmp2;
    max_u += 2*soln.c0;

    tmp1 = pvmax(*soln.v[0]);
    tmp2 = -pvmin(*soln.v[0]);
    max_v = tmp1>tmp2 ? tmp1 : tmp2;
    max_v += 2*soln.c0;

    double cfl = 0.1;

    dt = soln.dx/max_u < soln.dy/max_v ? cfl*soln.dx/max_u : cfl*soln.dy/max_v;

    if (dt + soln.t >= soln.next_plot_time) {
        dt = soln.next_plot_time - soln.t;
        soln.do_plot = true;
        soln.next_plot_time += tplot;
    }

    soln.dt2 = soln.dt1;
    soln.dt1 = soln.dt0;
    soln.dt0 = dt;

}


void flux_sw(solnclass & soln,
        DTArray & fluxu, DTArray & fluxv, DTArray & fluxh, 
        DTArray & temp1, DTArray & temp2, 
        Trans1D & X_xform, Trans1D & Y_xform,
        TransWrapper & XY_xform,                 
        Array<double,1> & ygrid,
        DTArray & sponge)
{
    // Make nice references for arrays used
    DTArray &temp = temp1, &rhs = temp2;
    DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 


    // fluxu = u*u_x + v*u_y - f*v + g h_x
    deriv.u_x(u,X_xform,temp);       // calc u_x
    rhs = u * temp*Norm_x;
    deriv.u_y(u,Y_xform,temp);       // calc u_y
    rhs = rhs + v*temp*Norm_y;
    deriv.h_x(h,X_xform,temp);       // calc h_x
    fluxu = rhs - v*(f0 + beta*ygrid(kk))
        + g*temp*Norm_x;// - sponge*u;

    // fluxv = u*v_x + v*v_y + f*u + g h_y
    deriv.v_x(v,X_xform,temp);       // calc v_x
    rhs = u * temp * Norm_x;
    deriv.v_y(v,Y_xform,temp);       // calc v_y
    rhs = rhs + v*temp*Norm_y;
    deriv.h_y(h,Y_xform,temp);       // calc h_y
    fluxv = rhs + u*(f0+beta*ygrid(kk)) 
        + g*temp*Norm_y;// - sponge*v;

    // fluxh = ((H + h)*u)_x + ((H + h)*v)_y
    temp = u*(H + h);
    deriv.u_x(temp,X_xform,temp);          // calc [(H + h)*u]_x 
    rhs = temp * Norm_x;
    temp = v*(H + h);                      // calc [(H + h)*v]_y 
    deriv.v_y(temp,Y_xform,temp);
    fluxh = rhs + temp*Norm_y;// - sponge*h;

}

void step_euler(solnclass & soln, solnclass & flux,
        DTArray & temp1, DTArray & temp2, 
        Trans1D & X_xform, Trans1D & Y_xform,
        TransWrapper & XY_xform, 
        Array<double,1> & xgrid, Array<double,1> & ygrid, 
        DTArray & sponge) {

    // Make nice references for arrays used
    DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
    DTArray &fluxu = *flux.u[0], &fluxv = *flux.v[0], &fluxh = *flux.h[0];

    // Compute flux
    flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, X_xform, Y_xform, XY_xform, ygrid, sponge);

    // Compute new timestep, decrease for euler
    compute_dt(soln);
    soln.dt0 *= 0.1;

    // Advance Solution
    u = u - fluxu*soln.dt0;
    v = v - fluxv*soln.dt0;
    h = h - fluxh*soln.dt0;

    // Filter, with sensible defaults: cutoff, order, strength
    filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.55, 2.0, 20.0);
    filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.55, 2.0, 20.0);
    filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.55, 2.0, 20.0);

    // Record fluxes for future
    *flux.u[1] = fluxu;
    *flux.v[1] = fluxv;
    *flux.h[1] = fluxh;

}

// Compute the weights for the AB2 method
void compute_weights2(solnclass & soln) {

    double a,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn  = soln.t;

    a  = soln.t - soln.dt1;
    ts = soln.t;
    w  = (  0.5*(tnp*tnp - tn*tn)
            - a*(tnp-tn) )
        /(ts-a);
    soln.w0 = w;

    a  = soln.t;
    ts = soln.t - soln.dt1;
    w  = (  0.5*(tnp*tnp - tn*tn)
            - a*(tnp-tn) )
        /(ts-a);
    soln.w1 = w;

    return;

}

void step_ab2(solnclass & soln, solnclass & flux,
        DTArray & temp1, DTArray & temp2, 
        Trans1D & X_xform, Trans1D & Y_xform,
        TransWrapper & XY_xform, 
        Array<double,1> & xgrid, Array<double,1> & ygrid, 
        DTArray & sponge) {

    // Make nice references for arrays used
    DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
    DTArray &fluxu = *flux.u[1], &fluxv = *flux.v[1], &fluxh = *flux.h[1];

    // Compute flux
    flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, X_xform, Y_xform, XY_xform, ygrid, sponge);

    // Compute new timestep, decrease for ab2
    compute_dt(soln);
    soln.dt0 *= 0.1;

    // Advance solution 
    compute_weights2(soln);
    u = u - soln.w0*fluxu - soln.w1*(*flux.u[1]);
    v = v - soln.w0*fluxv - soln.w1*(*flux.v[1]);
    h = h - soln.w0*fluxh - soln.w1*(*flux.h[1]);
    //u = u - 0.5*dt*(3*fluxu - *flux.u[0]);
    //v = v - 0.5*dt*(3*fluxv - *flux.v[0]);
    //h = h - 0.5*dt*(3*fluxh - *flux.h[0]);

    // Filter, with sensible defaults: cutoff, order, strength
    filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.55, 2.0, 20.0);
    filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.55, 2.0, 20.0);
    filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.55, 2.0, 20.0);

    // Record fluxes for future
    *flux.u[0] = *flux.u[1];
    *flux.v[0] = *flux.v[1];
    *flux.h[0] = *flux.h[1];
    *flux.u[1] = fluxu;
    *flux.v[1] = fluxv;
    *flux.h[1] = fluxh;

}

// Compute the weights for the AB3 method
void compute_weights3(solnclass & soln) {

    double a,b,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn = soln.t;

    a  = soln.t - soln.dt1;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn)
            -    0.5*(a+b)*(tnp*tnp - tn*tn)
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w0 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t - soln.dt1;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn)
            -    0.5*(a+b)*(tnp*tnp - tn*tn)
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w1 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1;
    ts = soln.t - soln.dt1 - soln.dt2;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn)
            -    0.5*(a+b)*(tnp*tnp - tn*tn)
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w2 = w;


    return;

}

void step_ab3(solnclass & soln, solnclass & flux,
        DTArray & temp1, DTArray & temp2, 
        Trans1D & X_xform, Trans1D & Y_xform,
        TransWrapper & XY_xform, 
        Array<double,1> & xgrid, Array<double,1> & ygrid, 
        DTArray & sponge) {

    // Make nice references for arrays used
    DTArray &u = *soln.u[0], &v = *soln.v[0], &h = *soln.h[0]; 
    DTArray &fluxu = *flux.u[2], &fluxv = *flux.v[2], &fluxh = *flux.h[2];

    // Compute flux
    flux_sw(soln, fluxu, fluxv, fluxh, temp1, temp2, X_xform, Y_xform, XY_xform, ygrid, sponge);

    // Compute new timestep, full dt for ab3
    compute_dt(soln);

    // Advance solution
    compute_weights3(soln);
    u = u - soln.w0*fluxu - soln.w1*(*flux.u[1]) - soln.w2*(*flux.u[0]);
    v = v - soln.w0*fluxv - soln.w1*(*flux.v[1]) - soln.w2*(*flux.v[0]);
    h = h - soln.w0*fluxh - soln.w1*(*flux.h[1]) - soln.w2*(*flux.h[0]);
    //u = u - dt/12*(23*fluxu - *flux.u[1]*16 + *flux.u[0]*5);
    //v = v - dt/12*(23*fluxv - *flux.v[1]*16 + *flux.v[0]*5);
    //h = h - dt/12*(23*fluxh - *flux.h[1]*16 + *flux.h[0]*5);

    // Filter, with sensible defaults: cutoff, order, strength
    filter3(u,XY_xform,Sx.u,NONE,Sy.u,0.55, 2.0, 20.0);
    filter3(v,XY_xform,Sx.v,NONE,Sy.v,0.55, 2.0, 20.0);
    filter3(h,XY_xform,Sx.h,NONE,Sy.h,0.55, 2.0, 20.0);

    // Record fluxes for future
    *flux.u[0] = *flux.u[1];
    *flux.v[0] = *flux.v[1];
    *flux.h[0] = *flux.h[1];
    *flux.u[1] = fluxu;
    *flux.v[1] = fluxv;
    *flux.h[1] = fluxh;

}
