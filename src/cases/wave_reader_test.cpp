/* wave_reader_vortex.cpp -- general case for looking at the evolution of
   waves, with input data and configuration provided at runtime
   via a configuration file. This differs from wave_reader_sqg.cpp
   in that this is deisgned to study a 3D vortex in the interior.*/

#include "../Science.hpp"
#include "../TArray.hpp"
#include "../Par_util.hpp"
#include "../NSIntegrator.hpp"
#include "../BaseCase.hpp"
#include "../Options.hpp"
#include <stdio.h>
#include <mpi.h>
#include <vector>
#include <random/uniform.h>
#include <random/normal.h>
#include <string>
#include <time.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

using std::string;


int          Nx, Ny, Nz; // Number of points in x, y, z
double       Lx, Ly, Lz; // Grid lengths of x, y, z 
double   MinX, MinY, MinZ; // Minimum x/y/z points


// Input file names

string xgrid_filename,
       ygrid_filename,
       zgrid_filename,
       u_filename,
       v_filename,
       w_filename,
       rho_filename,
       tracer_filename,
       bg_epv_filename;

// Physical parameters
double g, rot_f, N0, vel_mu, dens_kappa, tracer_kappa, tracer_g;
double max_dt=100.0; // Initialize as something, is computed later
double alpha, beta, H; 

// Writeout parameters
double final_time, plot_interval;
double initial_time;
bool compute_norms;

// Mapped grid?
bool mapped;

// Passive tracer?
bool tracer;

// Grid types
DIMTYPE intype_x, intype_y, intype_z;

static enum {
   MATLAB,
   CTYPE,
   FULL3D
} input_data_types;

using std::vector;

blitz::firstIndex ii;
blitz::secondIndex jj;
blitz::thirdIndex kk;

using ranlib::Normal;

int myrank = -1;

bool restarting = false;
double restart_time = 0;
int restart_sequence = -1;

double perturb = 0;

// Dump parameters
bool has_dumped = false;
time_t real_start_time;
double compute_time;
bool restart_from_dump = false;
string u_dump = "u.dump";
string v_dump = "v.dump";
string w_dump = "w.dump";
string b_dump = "b.dump";
string tracer_dump = "tracer.dump";

// Do stuff
class userControl : public BaseCase {
   public:
      int plotnum, itercount, lastplot, last_writeout;
      bool plot_now;
      double nextplot;
      //double max_dt;
      double start_rho_spread, step_dur, total_run_time;
      time_t clock_time, step_start_time;//, real_start_time;

      int size_x() const { return Nx; }
      int size_y() const { return Ny; }
      int size_z() const { return Nz; }

      // Array for Ertel PV computation
      DTArray* e_pv;
      DTArray* bg_epv;

      // Initialize some variables
      double KE, PE, Vol, rho0, epv_pert_norm;
      FILE * diagnostics; 
      FILE * plottimes; 
      FILE * step_times; 

      DIMTYPE type_x() const { return intype_x; }
      DIMTYPE type_y() const { return intype_y; }
      DIMTYPE type_default() const { return intype_z; }

      double length_x() const { return Lx; }
      double length_y() const { return Ly; }
      double length_z() const { return Lz; }

      double get_visco() const {
         return vel_mu;
      }
      double get_diffusivity(int t) const {
         if (t == 0) return dens_kappa; 
         if (t == 1) return tracer_kappa;
         else assert(0 && "Invalid tracer number!");
      }

      double init_time() const { 
         return initial_time;
      }

      bool is_mapped() const {return mapped;}
      void do_mapping(DTArray & xg, DTArray & yg, DTArray & zg) {
         if (input_data_types == MATLAB) {
            if (master())
               fprintf(stderr,"Reading MATLAB-format xgrid (%d x %d) from %s\n",
                     Nx,Nz,xgrid_filename.c_str());
            read_2d_slice(xg,xgrid_filename.c_str(),Nx,Nz);
            if (master())
               fprintf(stderr,"Reading MATLAB-format zgrid (%d x %d) from %s\n",
                     Nx,Nz,zgrid_filename.c_str());
            read_2d_slice(zg,zgrid_filename.c_str(),Nx,Nz);
         } else if (input_data_types == CTYPE ||
                    input_data_types == FULL3D) {
            if (master())
               fprintf(stderr,"Reading CTYPE-format xgrid (%d x %d) from %s\n",
                     Nx,Nz,xgrid_filename.c_str());
            read_2d_restart(xg,xgrid_filename.c_str(),Nx,Nz);
            if (master())
               fprintf(stderr,"Reading CTYPE-format zgrid (%d x %d) from %s\n",
                     Nx,Nz,zgrid_filename.c_str());
            read_2d_restart(zg,zgrid_filename.c_str(),Nx,Nz);
         }
         // Automatically generate y-grid
         yg = 0*ii + MinY + Ly*(0.5+jj)/Ny + 0*kk;

         // Write out the grids and matlab readers
         write_array(xg,"xgrid");write_reader(xg,"xgrid",false);
         if (Ny > 1)
            write_array(yg,"ygrid"); write_reader(yg,"ygrid",false);
         write_array(zg,"zgrid"); write_reader(zg,"zgrid",false);
      }

      /* We have an active tracer, namely density */
      int numActive() const { return 1; }

      /* We're given a passive tracer to advect */
      int numPassive() const {
         if (tracer) return 1;
         else return 0;
      }

      /* Timestep-check function.  This (along with "write everything" outputs) should
         really be bumped into the BaseCase */
      double check_timestep(double intime, double now) {
         if (intime < 1e-9) {
            /* Timestep's too small, somehow stuff is blowing up */
            if (master()) fprintf(stderr,"Tiny timestep (%e), aborting\n",intime);
            return -1;
            //FJP: intime sets time step
         } else if (intime > max_dt) {
            /* Cap the maximum timestep size */
            if (master()) fprintf(stdout,"Step too large (%g), reducing to %g.\n",intime,max_dt);
             intime = max_dt;
         }
         /* Calculate how long we have until the next plottime, and then adjust
            the timestep such that we take a whole number of steps to ge there */
         double until_plot = nextplot - now;
         double steps = ceil(until_plot / intime);
         double real_until_plot = steps*intime;

         if (fabs(until_plot - real_until_plot) < 1e-5*plot_interval) {
            /* Close enough for scientific work */
            return intime;
         } else {
            /* Adjust the timestep */
            return (until_plot / steps);
         }
      }

      void analysis(double sim_time, DTArray & u, DTArray & v, DTArray & w,
            vector<DTArray *> tracer, DTArray & pressure) {
         itercount = itercount + 1;
         double mddiff, max_v, mu, mw;

         // Cell volume
         double Vol = (Lx/Nx)*(Ly/Ny)*(Lz/Nz);

         // Do all of this stuff on the first iteration
         if (itercount == 1) {

            // Alright, this is starting to bug me, please work this time.
            // The hope is that calling ertel_pv will initialize bg_epv
            // so that I can write directly to it.
            if (compute_norms) {
                    ertel_pv(u, v, w, *tracer[0], bg_epv, rot_f, N0,
                                    length_x(), length_y(), length_z(),
                                    size_x(),   size_y(),   size_z(),
                                    type_x(),   type_y(),   type_z());
                    fprintf(stdout,"Trying to read bg_epv \n");
                    read_array(*bg_epv,bg_epv_filename.c_str(),Nx,Ny,Nz);
                    fprintf(stdout,"Successfully read bg_epv \n");
            }

            time(&step_start_time);
            // Create empty files for storage and add header when necessary
            if (master()) {
                fprintf(stdout,"Initializing diagnostics files. \n");
                if (!restarting) plottimes = fopen("plot_times.txt","w");
                else             plottimes = fopen("plot_times.txt","a");
                assert(plottimes);
                fclose(plottimes);
                if (!restarting) {
                    diagnostics = fopen("diagnostics.txt","w");
                    assert(diagnostics);
                        if (compute_norms)  fprintf(diagnostics,"time, iteration, max_u, max_v, max_w, max_del_n, KE, step_time, epv_pert_norm\n");
                        else                fprintf(diagnostics,"time, iteration, max_u, max_v, max_w, max_del_n, KE, step_time\n");
                }
                else {
                    diagnostics = fopen("diagnostics.txt","a");
                    assert(diagnostics);
                }
                fclose(diagnostics);
                if (!restarting) step_times = fopen("step_times.txt","w");
                else             step_times = fopen("step_times.txt","a");
                assert(step_times);
                fprintf(step_times, "iter, step_time\n");
                fclose(step_times);
            }

            // Since analysis doesn't run before the first
            // step, we'll have to output after the first step
            // Since the first (Euler) step is short, 
            // there should't be much of a change.
            if (!restarting) {
              ertel_pv(u, v, w, *tracer[0], e_pv, rot_f, N0,
                       length_x(), length_y(), length_z(),
                       size_x(),   size_y(),   size_z(),
                       type_x(),   type_y(),   type_z());
            
              write_array(*e_pv,"epv",0);
            }
         }

         // Compute how long the last step took
         if (master()) {
            time(&clock_time);
            step_dur = difftime(clock_time,step_start_time);
            step_times = fopen("step_times.txt","a");
            assert(step_times);
            fprintf(step_times, "%d,%.12g\n",itercount,step_dur);
            fclose(step_times);
         }

         // Determine how long we've been running
         // and handle the dump case
         if (master()) {
            
            total_run_time = difftime(clock_time,real_start_time);
            
            if (itercount == 1){
                fprintf(stdout, "Start-up time: %g\n", total_run_time);
            }

            // If we restarted between outputs, we need to correct
            // the next plot time. This probably isn't the best place
            // for this, but it was easy.
            if (restart_from_dump and (itercount == 1)){
                nextplot = (restart_sequence+1)*plot_interval;    
            }
         }

         if ((compute_time - total_run_time < max(300.0*Nx*Ny*Nz/pow(512.0,3),60.)) and (!has_dumped)) {
            fprintf(stdout,"Too close to final time, dumping!\n");
            write_array(u,"u.dump",-1);
            write_array(v,"v.dump",-1);
            write_array(w,"w.dump",-1);
            write_array(*tracer[0],"b.dump",-1);
            has_dumped = true;

            // Write the dump time to a text file for restart purposes
            FILE * dump_file; 
            dump_file = fopen("dump_time.txt","w");
            assert(dump_file);
            fprintf(dump_file,"The dump time was:\n%.12g\n", sim_time);
            fprintf(dump_file,"The dump index was:\n%d\n", plotnum);
            fclose(dump_file);

            // Die, not entirely gracefully
            exit(1);
         }

         // Compute some scalars
         mddiff = pvmax(*tracer[0]) - pvmin(*tracer[0]);
         max_v = psmax(max(abs(v)));
         rho0 = 1000;
         KE = 0.5*rho0*pssum(sum( u*u+v*v+w*w ))*Vol;
         mu = psmax(max(abs(u))),
         mw = psmax(max(abs(w)));

         // Determine if this is a plot interval
         if ((sim_time - nextplot) > -1e-5*plot_interval) {
                 plot_now = true;
                 plotnum = plotnum + 1;
         }
         else {
                 plot_now = false;
         }

         // Compute epv if we're going to need it
         if ((compute_norms) || (plot_now)) {
            ertel_pv(u, v, w, *tracer[0], e_pv, rot_f, N0,
                     length_x(), length_y(), length_z(),
                     size_x(),   size_y(),   size_z(),
                     type_x(),   type_y(),   type_z());
         }

         // If it's time to output, then output
         if (plot_now) {

            write_array(u,"u",plotnum);
            write_array(v,"v",plotnum);
            write_array(w,"w",plotnum);
            write_array(*tracer[0],"b",plotnum);
            write_array(*e_pv,"epv",plotnum);
    
            lastplot = itercount;
            if (master()) {
               plottimes = fopen("plot_times.txt","a");
               assert(plottimes);
               fprintf(plottimes,"%.10g\n",sim_time);
               fclose(plottimes);
               fprintf(stdout,"*");
            }
            nextplot = nextplot + plot_interval;
         } 

         // Now that we've written epv (if we needed to),
         // overwrite it with the perturbation field and
         // compute the norm (if we need to).
         if (compute_norms) {
             //*e_pv += -*bg_epv;
             epv_pert_norm = pow(pssum(sum((*e_pv)*(*e_pv)))/(size_x()*size_y()*size_z()),0.5); 
         }

         // Write a summary of the step 
         if (((itercount - lastplot) % 1 == 0) || plot_now) {
            if (master()) {
                diagnostics = fopen("diagnostics.txt","a");
                assert(diagnostics);
                if (compute_norms) {
                    fprintf(diagnostics,"%f, %d, %.12g, %.12g, %.12g, %.12g, %.12g, %.12g, %.12g\n",
                         sim_time,itercount,mu,max_v,mw,mddiff,KE,step_dur,epv_pert_norm);
                }
                else {
                    fprintf(diagnostics,"%f, %d, %.12g, %.12g, %.12g, %.12g, %.12g, %.12g\n",
                         sim_time,itercount,mu,max_v,mw,mddiff,KE,step_dur);
                }
                fclose(diagnostics);

                if (compute_norms) {
                    fprintf(stdout,"t = %f [ii = %d]: max(u,v,w) = (%.2g, %.2g, %.2g) : delta_b =  %.3g : epv_pert = %.3g\n",
                         sim_time,itercount,mu,max_v,mw,mddiff,epv_pert_norm);
                }
                else{
                    fprintf(stdout,"t = %f [ii = %d]: max(u,v,w) = (%.2g, %.2g, %.2g) : delta_b =  %.3g\n",
                         sim_time,itercount,mu,max_v,mw,mddiff);
                }
            }
            last_writeout = itercount;
         }
      }

      void init_vels(DTArray & u, DTArray & v, DTArray & w) {
         // Initialize the velocities from read-in data
         if (master()) fprintf(stderr,"Initializing velocities\n");
         if (restarting and (!restart_from_dump)) {
            /* Restarting, so build the proper filenames and load
               the data into u, v, w */
            char filename[100];
            /* u */
            snprintf(filename,100,"u.%d",restart_sequence);
            if (master()) fprintf(stdout,"Reading u from %s\n",filename);
            read_array(u,filename,Nx,Ny,Nz);

            /* v, only necessary if this is an actual 3D run or if
               rotation is noNzero */
            if (Ny > 1 || rot_f != 0) {
               snprintf(filename,100,"v.%d",restart_sequence);
               if (master()) fprintf(stdout,"Reading v from %s\n",filename);
               read_array(v,filename,Nx,Ny,Nz);
            }

            /* w */
            snprintf(filename,100,"w.%d",restart_sequence);
            if (master()) fprintf(stdout,"Reading w from %s\n",filename);
            read_array(w,filename,Nx,Ny,Nz);

            return;
         }

         else if (restarting and restart_from_dump) {
            /* Restarting, so build the proper filenames and load
               the data into u, v, w */
            /* u */
            if (master()) fprintf(stdout,"Reading u from u.dump\n");
            read_array(u,u_dump.c_str(),Nx,Ny,Nz);

            /* v, only necessary if this is an actual 3D run or if
               rotation is noNzero */
            if (Ny > 1 || rot_f != 0) {
               if (master()) fprintf(stdout,"Reading v from v.dump\n");
               read_array(v,v_dump.c_str(),Nx,Ny,Nz);
            }

            /* w */
            if (master()) fprintf(stdout,"Reading w from w.dump\n");
            read_array(w,w_dump.c_str(),Nx,Ny,Nz);

            return;
         }

         // Read in the appropriate data types
         switch(input_data_types) {
            case MATLAB: // MATLAB data
               if (master())
                  fprintf(stderr,"reading matlab-type u (%d x %d) from %s\n",
                        Nx,Nz,u_filename.c_str());
               read_2d_slice(u,u_filename.c_str(),Nx,Nz);
               if (v_filename != "" && (Ny >> 1 || rot_f != 0)) {
                  if (master())
                     fprintf(stderr,"reading matlab-type v (%d x %d) from %s\n",
                           Nx,Nz,v_filename.c_str());
                  read_2d_slice(v,v_filename.c_str(),Nx,Nz);
               } else {
                  v = 0;
               }
               if (master())
                  fprintf(stderr,"reading matlab-type w (%d x %d) from %s\n",
                        Nx,Nz,w_filename.c_str());
               read_2d_slice(w,w_filename.c_str(),Nx,Nz);
               break;
            case CTYPE: // Column-major 2D data
               if (master())
                  fprintf(stderr,"reading ctype u (%d x %d) from %s\n",
                        Nx,Nz,u_filename.c_str());
               read_2d_restart(u,u_filename.c_str(),Nx,Nz);
               if (v_filename != "" && (Ny >> 1 || rot_f != 0)) {
                  if (master())
                     fprintf(stderr,"reading ctype v (%d x %d) from %s\n",
                           Nx,Nz,v_filename.c_str());
                  read_2d_restart(v,v_filename.c_str(),Nx,Nz);
               } else {
                  v = 0;
               }
               if (master())
                  fprintf(stderr,"reading ctype w (%d x %d) from %s\n",
                        Nx,Nz,w_filename.c_str());
               read_2d_restart(w,w_filename.c_str(),Nx,Nz);
               break;
            case FULL3D:
               
               if (master()) 
                  fprintf(stdout,"Reading u from %s\n",
                     u_filename.c_str());
               read_array(u,u_filename.c_str(),Nx,Ny,Nz);
               if (master()) 
                  fprintf(stdout,"Reading u from %s\n",
                     v_filename.c_str());
               read_array(v,v_filename.c_str(),Nx,Ny,Nz);
               if (master()) 
                  fprintf(stdout,"Reading w from %s\n",
                     w_filename.c_str());
               read_array(w,w_filename.c_str(),Nx,Ny,Nz);

               break;
         }


         // Add a random perturbation to trigger any 3D instabilities
         if (perturb > 0) {
            int myrank;
            MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
            Normal<double> rnd(0,1);
            for (int i = u.lbound(firstDim); i <= u.ubound(firstDim); i++) {
               rnd.seed(i);
               for (int j = u.lbound(secondDim); j <= u.ubound(secondDim); j++) {
                  for (int k = u.lbound(thirdDim); k <= u.ubound(thirdDim); k++) {
                     u(i,j,k) *= 1+perturb*rnd.random();
                     v(i,j,k) *= 1+perturb*rnd.random();
                     w(i,j,k) *= 1+perturb*rnd.random();
                  }
               }
            }
         }
         /* Write out initial values */
         write_array(u,"u",plotnum);
         //write_reader(u,"u",true);
         if (Ny > 1 || rot_f != 0) {
            write_array(v,"v",plotnum);
            //write_reader(v,"v",true);
         }
         write_array(w,"w",plotnum);
         //write_reader(w,"w",true);

      }
      void init_tracer(int t_num, DTArray & rho) {
         if (master()) fprintf(stderr,"Initializing tracer %d\n",t_num);
         /* Initialize the density and take the opportunity to write out the grid */
         if (t_num == 0) {
            if (restarting and (!restart_from_dump)) {
               /* Restarting, so build the proper filenames and load
                  the data into u, v, w */
               char filename[100];
               /* rho */
               snprintf(filename,100,"b.%d",restart_sequence);
               if (master()) fprintf(stdout,"Reading rho from %s\n",filename);
               read_array(rho,filename,Nx,Ny,Nz);
               return;
            }
            else if (restarting and restart_from_dump) {
               /* Restarting, so build the proper filenames and load
                  the data into u, v, w */
               /* rho */
               if (master()) fprintf(stdout,"Reading b from b.dump\n");
               read_array(rho,b_dump.c_str(),Nx,Ny,Nz);
               return;
                
            }
            switch (input_data_types) {
               case MATLAB:
                  if (master())
                     fprintf(stderr,"reading matlab-type rho (%d x %d) from %s\n",
                           Nx,Nz,rho_filename.c_str());
                  read_2d_slice(rho,rho_filename.c_str(),Nx,Nz);
                  break;
               case CTYPE:
                  if (master())
                     fprintf(stderr,"reading ctype rho (%d x %d) from %s\n",
                           Nx,Nz,rho_filename.c_str());
                  read_2d_restart(rho,rho_filename.c_str(),Nx,Nz);
                  break;
               case FULL3D:
                  if (master())
                     fprintf(stderr,"reading rho (%d x %d x %d) from %s\n",
                           Nx,Ny,Nz,rho_filename.c_str());
                  read_array(rho,rho_filename.c_str(),Nx,Ny,Nz);
                  break;
            }
            write_array(rho,"b",plotnum);
            //write_reader(rho,"p",true);
         } else if (t_num == 1) {
            if (restarting and (!restart_from_dump)) {
               char filename[100];
               /* rho */
               snprintf(filename,100,"tracer.%d",restart_sequence);
               if (master()) fprintf(stdout,"Reading tracer from %s\n",filename);
               read_array(rho,filename,Nx,Ny,Nz);
               return;
            }
            else if (restarting and restart_from_dump) {
               /* tracer */
               if (master()) fprintf(stdout,"Reading tracer from tracer.dump\n");
               read_array(rho,tracer_dump.c_str(),Nx,Ny,Nz);
               return;
            }
            switch (input_data_types) {
               case MATLAB:
                  if (master())
                     fprintf(stderr,"reading matlab-type tracer (%d x %d) from %s\n",
                           Nx,Nz,tracer_filename.c_str());
                  read_2d_slice(rho,tracer_filename.c_str(),Nx,Nz);
                  break;
               case CTYPE:
                  if (master())
                     fprintf(stderr,"reading ctype tracer (%d x %d) from %s\n",
                           Nx,Nz,tracer_filename.c_str());
                  read_2d_restart(rho,tracer_filename.c_str(),Nx,Nz);
                  break;
               case FULL3D:
                  if (master())
                     fprintf(stderr,"reading tracer (%d x %d x %d) from %s\n",
                           Nx,Ny,Nz,tracer_filename.c_str());
                  //read_array(rho,tracer_filename.c_str(),Nx,Ny,Nz);
                  break;
            }
         }
         //write_array(rho,"tracer",plotnum);
         //write_reader(rho,"tracer",true);
      }

      void forcing(double t, DTArray & u, DTArray & u_f,
          DTArray & v, DTArray & v_f, DTArray & w, DTArray & w_f,
          vector<DTArray *> & tracers, vector<DTArray *> & tracers_f) {
         /* Velocity forcing */
         u_f = rot_f * v; 
         v_f = -rot_f * u;
         w_f = *(tracers[0]);
         // This is now b = bbar + b'. Vert forcing might change ...
         *(tracers_f[0]) = -N0*N0*w;
         /* if (tracer) {
          *  *(tracers_f[1]) = 0;
          *  w_f = w_f - tracer_g*((*tracers[1]));
         }*/
      }

      userControl() :
         plotnum(restart_sequence), 
         nextplot(initial_time + plot_interval), itercount(0), lastplot(0),
         last_writeout(0) 
         {
            compute_quadweights(size_x(),size_y(),size_z(),
                  length_x(),length_y(),length_z(),
                  type_x(),type_y(),type_z());
            // If this is an unmapped grid, generate/write the
            // 3D grid files
            if (!is_mapped()) {
               automatic_grid(MinX,MinY,MinZ);
            }
         }
};

int main(int argc, char ** argv) {
   MPI_Init(&argc,&argv);

   time(&real_start_time);
   // To properly handle the variety of options, set up the boost
   // program_options library using the abbreviated interface in
   // ../Options.hpp

   options_init(); // Initialize options

   option_category("Grid Options");

   add_option("Nx",&Nx,"Number of points in X");
   add_option("Ny",&Ny,1,"Number of points in Y");
   add_option("Nz",&Nz,"Number of points in Z");

   string xgrid_type, ygrid_type, zgrid_type;
   add_option("type_x",&xgrid_type,
         "Grid type in X.  Valid values are:\n"
         "   FOURIER: Periodic\n"
         "   FREE_SLIP: Cosine expansion\n"
         "   NO_SLIP: Chebyhsev expansion");
   add_option("type_y",&ygrid_type,"FOURIER","Grid type in Y");
   add_option("type_z",&zgrid_type,"Grid type in Z");

   add_option("Lx",&Lx,"X-length");
   add_option("Ly",&Ly,1.0,"Y-length");
   add_option("Lz",&Lz,"Z-length");

   add_option("min_x",&MinX,0.0,"Unmapped grids: Minimum X-value");
   add_option("min_y",&MinY,0.0,"Minimum Y-value");
   add_option("min_z",&MinZ,0.0,"Minimum Z-value");

   option_category("Grid mapping options");
   add_option("mapped_grid",&mapped,false,"Use a mapped (2D) grid");
   add_option("xgrid",&xgrid_filename,"x-grid filename");
   add_option("ygrid",&ygrid_filename,"","y-grid filename");
   add_option("zgrid",&zgrid_filename,"z-grid filename");

   option_category("Input data");
   string datatype;
   add_option("file_type",&datatype,
         "Format of input data files, including that for the mapped grid."
         "Valid options are:\n"
         "   MATLAB: \tRow-major 2D arrays of size Nx x Nz\n"
         "   CTYPE:  \tColumn-major 2D arrays (including that output by 2D SPINS runs)\n"
         "   FULL:   \tColumn-major 3D arrays; implies CTYPE for grid mapping if enabled"); 

   add_option("u_file",&u_filename,"U-velocity filename");
   add_option("v_file",&v_filename,"","V-velocity filename");
   add_option("w_file",&w_filename,"W-velocity filename");
   add_option("rho_file",&rho_filename,"Rho (density) filename");

   option_category("Second tracer");
   add_switch("enable_tracer",&tracer,"Enable evolution of a second tracer");
   add_option("tracer_file",&tracer_filename,"Tracer filename");
   add_option("tracer_kappa",&tracer_kappa,"Diffusivity of tracer");
   add_option("tracer_gravity",&tracer_g,0.0,"Gravity for the second tracer");

   option_category("Physical parameters");
   add_option("g",&g,9.81,"Gravitational acceleration");
   add_option("rot_f",&rot_f,0.0,"Coriolis force term");
   add_option("N0",&N0,0.0,"Buoyancy frequency term");
   add_option("visc",&vel_mu,0.0,"Kinematic viscosity");
   add_option("kappa",&dens_kappa,0.0,"Thermal diffusivity");
   add_option("perturbation",&perturb,0.0,"Veloc\tity perturbation (multiplicative white noise) applied to read-in data.");

   option_category("Running options");
   add_option("init_time",&initial_time,0.0,"Initial time");
   add_option("final_time",&final_time,"Final time");
   add_option("plot_interval",&plot_interval,"Interval between output times");

   option_category("Restart options");
   add_switch("restart",&restarting,"Restart from prior output time. OVERRIDES many other values.");
   add_option("restart_time",&restart_time,0.0,"Time to restart from");
   add_option("restart_sequence",&restart_sequence,
         "Sequence number to restart from (if plot_interval has changed)");

   option_category("Filtering options");
   add_option("f_strength",&f_strength,20.0,"filter strength");
   add_option("f_cutoff",&f_cutoff,0.6,"filter cutoff");
   add_option("f_order",&f_order,2.0,"fiter order");

   option_category("Diagnostics options");
   add_option("compute_norms",&compute_norms,false,"Compute epv perturbation norms?");
   add_option("bg_epv_filename",&bg_epv_filename,"","Where to find epv basic state if needed.");

   option_category("Dumping options");
   add_option("compute_time",&compute_time,"Time permitted for computation");
   add_option("restart_from_dump",&restart_from_dump,false,"If restart from dump");

   // Parse the options from the command line and config file
   options_parse(argc,argv);
   max_dt = 2*M_PI/N0/10.0;

   // Now, make sense of the options received.  Many of these values
   // can be directly used, but the ones of string-type need further
   // procesing.

   // Read dump_time.txt
   if (restart_from_dump){
           restarting = true;
           string dump_str;
           ifstream dump_file;
           dump_file.open ("dump_time.txt");

           getline (dump_file,dump_str); // ingnore 1st line

           getline (dump_file,dump_str);
           restart_time = atof(dump_str.c_str());

           getline (dump_file,dump_str); // ingore 3rd line

           getline (dump_file,dump_str);
           restart_sequence = atoi(dump_str.c_str());

           fprintf(stdout,"Restart time is %.5g\n",restart_time);
           fprintf(stdout,"Restart sequence is %.5g\n",restart_sequence);
   }

   // Grid types:

   if (xgrid_type == "FOURIER") {
      intype_x = PERIODIC;
   } else if (xgrid_type == "FREE_SLIP") {
      intype_x = FREE_SLIP;
   } else if (xgrid_type == "NO_SLIP") {
      intype_x = NO_SLIP;
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for type_x\n",xgrid_type.c_str());
      MPI_Finalize(); exit(1);
   }
   if (ygrid_type == "FOURIER") {
      intype_y = PERIODIC;
   } else if (ygrid_type == "FREE_SLIP") {
      intype_y = FREE_SLIP;
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
      MPI_Finalize(); exit(1);
   }
   if (zgrid_type == "FOURIER") {
      intype_z = PERIODIC;
   } else if (zgrid_type == "FREE_SLIP") {
      intype_z = FREE_SLIP;
   } else if (zgrid_type == "NO_SLIP") {
      intype_z = NO_SLIP;
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
      MPI_Finalize(); exit(1);
   }

   // Input filetypes

   if (datatype=="MATLAB") {
      input_data_types = MATLAB;
   } else if (datatype == "CTYPE") {
      input_data_types = CTYPE;
   } else if (datatype == "FULL") {
      input_data_types = FULL3D;
   } else {
      if (master())
         fprintf(stderr,"Invalid option %s received for file_type\n",datatype.c_str());
      MPI_Finalize(); exit(1);
   }

   if (restarting) {
      if (restart_sequence <= 0) {
         restart_sequence = int(restart_time/plot_interval);
      }
      if (master()) {
         fprintf(stderr,"Restart flags detected\n");
         fprintf(stderr,"Restarting from time %g, at sequence number %d\n",
               restart_time,restart_sequence);
      }
      initial_time = restart_time;
   } else {
      // Not restarting, so set the initial sequence number
      // to the initial time / plot_interval
      restart_sequence = int(initial_time/plot_interval);
      if (fmod(initial_time,plot_interval) != 0.0) {
         if (master()) {
            fprintf(stderr,"Warning: the initial time (%g) does not appear to be an even multiple of the plot interval (%g)\n",
                  initial_time,plot_interval);
         }
      }
   }
   userControl mycode;
   FluidEvolve<userControl> kevin_kh(&mycode);
   kevin_kh.initialize();
   kevin_kh.do_run(final_time);
   MPI_Finalize();
}
