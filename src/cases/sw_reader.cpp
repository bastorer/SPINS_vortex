#include "../TArray.hpp"
#include "../T_util.hpp"
#include "../Par_util.hpp"
#include "../Options.hpp"
#include "../Splits.hpp"
#include "../Science_sw.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <random/normal.h>
#include <random/uniform.h>
#include "../sw_functions.hpp"
#include <iostream>
#include <fstream>
#include <fenv.h>

using std::string;

string xgrid_filename, ygrid_filename, zgrid_filename, method_name;
string u_filename, v_filename, eta_filename;

// Constant declarations
int          Nx, Ny, Nz;                       // Number of points in x, y, z
double       Lx, Ly, Lz;                       // Grid lengths of x, y, z
double       g, H0;                            // Gravity, mean depth (2D only) 
double       f0, beta, kappa;                  // Coriolis parameters and viscosity (not implemented)
double       t0, tf, tplot, tdiag;             // temporal parameters
double       fstrength, fcutoff, forder;       // filter parameters
int          plot_count;                       // track output number
int          kx, ky;                           // wavenumbers in x and y directions
double       norm_x, norm_y, norm_z;           // Normalization factors
double       next_plot_time;                   // Time for next output
double       next_diag_time;                   // Time for next diagnostic output
int          write_vels, write_psi;            // 0 if no
double       compute_time, avg_step_and_write_time;
double       avg_write_time, time_remaining;
bool         restart_from_dump, compute_norms, compute_spectra, compute_aniso;
int          do_dump;
string       xgrid_type, ygrid_type;
Transformer::S_EXP  type_x, type_y;
double       tmp1, tmp2, tmp3;
double       scale;

// Doubles to track times
double   start_clock_time,
         curr_clock_time,
         step_dur,
         true_start_time,
         start_write,
         stop_write;

using namespace TArrayn;
using namespace Transformer;
using namespace sw_functions;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using ranlib::Normal;
using ranlib::Uniform;

using namespace std;

// Normalization factors
#define Norm_x (2*M_PI/Lx)
#define Norm_y (2*M_PI/Ly)

// Blitz index placeholders
blitz::firstIndex ii;
blitz::secondIndex jj;
blitz::thirdIndex kk;

bool restarting = false;
double restart_time = 0;
int restart_sequence = -1;

int my_rank, num_procs;

// Diagnostics
double KE, PE;
TArrayn::S_EXP type_FOU = FOURIER;
TArrayn::S_EXP type_COS = COSINE;
int offset_from, offset_to;
int prev_chain_write_count, chain_write_count;
MPI_Status status;

int main(int argc, char ** argv) {

    // Initialize MPI
    MPI_Init(&argc, &argv);

    //feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);  // Enable all floating point exceptions but FE_INEXACT

    MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
    MPI_Comm_size(MPI_COMM_WORLD,&num_procs);

    true_start_time = MPI_Wtime();

    // To properly handle the variety of options, set up the boost
    // program_options library using the abbreviated interface in
    // ../Options.hpp

    options_init(); // Initialize options

    option_category("Grid Options");

    // Grid size
    add_option("Nx",&Nx,"Number of points in X");
    add_option("Ny",&Ny,"Number of points in Y");
    add_option("Nz",&Nz,"Number of points in Z");

    // Grid lengths
    add_option("Lx",&Lx,"X-length"); 
    add_option("Ly",&Ly,"Y-length");
    add_option("Lz",&Lz,"Z-length");

    // Defines for physical parameters
    option_category("Physical parameters");
    add_option("f0",    &f0,    0.,   "Coriolis parameter");
    add_option("beta",  &beta,  0.,   "beta parameter");
    add_option("g",     &g,     9.81, "Gravity");
    add_option("H0",    &H0,    1.,   "Mean depth"); 
    add_option("kappa", &kappa,       "Horizontal viscosity (not implemented)");

    // Timestep parameters
    add_option("t0",    &t0,        "t0-initial time");
    add_option("tf",    &tf,        "tf-final time");
    add_option("tplot", &tplot,     "tplot-frequency of output");
    add_option("tdiag", &tdiag, 0., "tdiag-frequency of diagnostic output");

    // y geometry
    add_option("type_x",&xgrid_type,
            "Grid type in X.  Valid values are:\n"
            "   PERIODIC: Periodic\n"
            "   FREE_SLIP: Free-slip walls");
    add_option("type_y",&ygrid_type,
            "Grid type in Y.  Valid values are:\n"
            "   PERIODIC: Periodic\n"
            "   FREE_SLIP: Free-slip walls");

    // Flux Method to use 
    add_option("method",&method_name,"Flux Method to use");

    // Filenames for Initial Conditions
    add_option("u_file",   &u_filename,   "u I.C. filename");
    add_option("v_file",   &v_filename,   "v I.C. filename");
    add_option("eta_file", &eta_filename, "eta I.C. filename");

    // Restarting
    option_category("Restart options"); 
    add_option("restart",          &restarting,       "Restart flag");
    add_option("restart_sequence", &restart_sequence, "Restart Sequence");

    // Dump options
    option_category("Dumping options");
    add_option("compute_time",      &compute_time,      -1.0,  "Time allotted for computation.");
    add_option("restart_from_dump", &restart_from_dump, false, "If restarting from dump.");

    // Filtering
    option_category("Filtering options");
    add_option("f_strength", &fstrength, "filter strength");
    add_option("f_cutoff",   &fcutoff,   "filter cutoff");
    add_option("f_order",    &forder,    "fiter order");

    // Diagnostics
    option_category("Diagnostics options");
    add_option("compute_norms",   &compute_norms,   false, "Compute perturbation norms at each timestep?");
    add_option("compute_spectra", &compute_spectra, false, "Compute horizontal spectra?");
    add_option("compute_aniso",   &compute_aniso,   false, "Compute horizontal anisotropy?");

    // Parse the options from the command line and config file
    options_parse(argc,argv);

    // Read dump_time.txt and check if past final time
    if (restart_from_dump){
        restarting = true;
        string dump_str;
        ifstream dump_file;
        dump_file.open ("dump_time.txt");

        getline (dump_file,dump_str); // ingnore 1st line

        getline (dump_file,dump_str); // Second line has dump time
        restart_time = atof(dump_str.c_str());

        getline (dump_file,dump_str); // ingore 3rd line

        getline (dump_file,dump_str); // Fourth line has index of most recent write
        restart_sequence = atoi(dump_str.c_str());

        if (restart_time > tf){
            // Die, ungracefully
            if (master()){
                fprintf(stderr,"Restart dump time (%.16g) is past final time (%.16g). Quitting now.\n",restart_time,tf);
            }
            MPI_Finalize(); exit(1);
        }
    }
    if (compute_time > 0){
        // Start with a guess a write time
        avg_step_and_write_time = max(100.0*Nx*Ny*Nz/pow(512.0,3), 20.0);
    }

    // Compute the restart time
    if (restarting) restart_time = tplot*restart_sequence;

    if (master()) fprintf(stdout,"Parameters used in sw_reader.cpp:\n");
    if (master()) fprintf(stdout,"---------------------------\n");
    if (master()) fprintf(stdout,"(Nx,Ny,Nz) = (%d, %d, %d)\n",Nx,Ny,Nz);
    if (master()) fprintf(stdout,"(Lx,Ly,Lz) = (%g, %g, %g)\n",Lx,Ly,Lz);
    if (master()) fprintf(stdout,"g          = %6.2e\n",g);
    if (master()) fprintf(stdout,"H0         = %6.2e\n",H0);
    if (master()) fprintf(stdout,"f0         = %6.2e\n",f0);
    if (master()) fprintf(stdout,"beta       = %6.2e\n",beta);
    if (master()) fprintf(stdout,"t0         = %g\n",t0);
    if (master()) fprintf(stdout,"tf         = %g\n",tf);
    if (master()) fprintf(stdout,"tplot      = %g\n",tplot);
    if (master()) fprintf(stdout,"method     = %s\n",method_name.c_str());
    if (master()) fprintf(stdout,"type_x     = %s\n",xgrid_type.c_str());
    if (master()) fprintf(stdout,"type_y     = %s\n",ygrid_type.c_str());
    if (restarting) {
        if (master()) fprintf(stdout,"restarting at t = %g\n",restart_time);
        if (master()) fprintf(stdout,"restarting at index = %d\n",restart_sequence);
    }
    if (master()) fprintf(stdout,"---------------------------\n");
    if (master()) fprintf(stdout," \n");

    if ((!compute_spectra) and (compute_aniso)) {
        if (master()) fprintf(stdout, "\n");
        if (master()) fprintf(stdout, " compute_aniso = True is inconsistent with compute_spectra=false-\n");
        if (master()) fprintf(stdout, " compute_spectra has been changed to true\n");
        if (master()) fprintf(stdout, "\n");
        compute_spectra = true;
    }

    // Specify which flux to use
    methodclass method;
    if (method_name == "nonlinear") {
        method.flux = &nonlinear;
    }

    /* Initialize solution and fluxes */
    solnclass soln(Nx, Ny, Nz, Lx, Ly, Lz, method_name, xgrid_type, ygrid_type);
    fluxclass flux(Nx, Ny, Nz);

    soln.fstrength = fstrength;
    soln.fcutoff = fcutoff;
    soln.forder = forder;
    soln.restarting = restarting;
    soln.f0 = f0;
    soln.beta = beta;
    soln.g = g;
    soln.H0 = H0;
    //soln.kappa = kappa;
    soln.tf = tf;
    soln.tdiag = tdiag;
    soln.compute_norms = compute_norms;
    soln.compute_spectra = compute_spectra;
    soln.compute_aniso = compute_aniso;

    Transgeom Sz;

    write_grids(*soln.u[0], Lx, Ly, Lz, Nx, Ny, Nz);

    // Files for Diagnostics
    if (master()) soln.initialize_diagnostics_files();
    if (compute_spectra) soln.initialize_spectra_files();
    soln.initialize_chain_diagnostics_files();

    // Read Initial Conditions and handle restarting
    if (restarting) {
        plot_count = 1 + restart_sequence;
        soln.t = restart_time;

        if (!restart_from_dump) {
            char filename[100];

            snprintf(filename,100,"u.%d",restart_sequence);
            if (master()) fprintf(stdout,"Reading u from %s\n", filename);
            read_array_sw(*soln.u[0],filename,Nx,Ny,Nz); 

            snprintf(filename,100,"v.%d",restart_sequence);
            if (master()) fprintf(stdout,"Reading v from %s\n", filename);
            read_array_sw(*soln.v[0],filename,Nx,Ny,Nz); 

            snprintf(filename,100,"eta.%d",restart_sequence);
            if (master()) fprintf(stdout,"Reading eta from %s\n", filename);
            read_array_sw(*soln.eta[0],filename,Nx,Ny,Nz); 
        }
        else {
            if (master()) fprintf(stdout,"Reading u from u.dump\n");
            read_array_sw(*soln.u[0],"u.dump",Nx,Ny,Nz); 

            if (master()) fprintf(stdout,"Reading v from v.dump\n");
            read_array_sw(*soln.v[0],"v.dump",Nx,Ny,Nz); 

            if (master()) fprintf(stdout,"Reading eta from eta.dump\n");
            read_array_sw(*soln.eta[0],"eta.dump",Nx,Ny,Nz); 
        }
    }
    else {
        plot_count = 1;
        soln.t = 0.0;

        if (master()) fprintf(stdout,"Reading u from %s\n", u_filename.c_str());
        read_array_sw(*soln.u[0],u_filename.c_str(),Nx,Ny,Nz); 

        if (master()) fprintf(stdout,"Reading v from %s\n", v_filename.c_str());
        read_array_sw(*soln.v[0],v_filename.c_str(),Nx,Ny,Nz); 

        if (master()) fprintf(stdout,"Reading eta from %s\n", eta_filename.c_str());
        read_array_sw(*soln.eta[0],eta_filename.c_str(),Nx,Ny,Nz); 
    }
    soln.next_plot_time = tplot*plot_count;

    // Read Basic State
    if ((compute_norms) or !(method_name == "nonlinear")) {
        //soln.initialize_background_q(qb_filename);
    }

    if (xgrid_type == "PERIODIC") {
        type_x = FOURIER;
        norm_x = 2.*M_PI/Lx;
    } else if (xgrid_type == "FREE_SLIP") {
        type_x = SINE; //FREE_SLIP;
        norm_x = M_PI/Lx;
    } else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    if (ygrid_type == "PERIODIC") {
        type_y = FOURIER;
        norm_y = 2.*M_PI/Ly;
    } else if (ygrid_type == "FREE_SLIP") {
        type_y = SINE; //FREE_SLIP;
        norm_y = M_PI/Ly;
    } else {
        if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
        MPI_Finalize(); exit(1);
    }

    // Initialize some constants now that initial conditions are available
    soln.initialize_constants();

    Trans1D X_xform(Nx, Nz, Ny, firstDim, type_x),
            Y_xform(Nx, Nz, Ny, thirdDim, type_y);
    TransWrapper XYZ_xform(Nx, Nz, Ny, type_x, NONE, type_y);

    Sz.X_xform = &X_xform;
    Sz.Y_xform = &Y_xform;
    Sz.XYZ_xform = &XYZ_xform;

    soln.X_xform = &X_xform;
    soln.Y_xform = &Y_xform;

    Sz.initialize(soln, xgrid_type, ygrid_type);
    soln.norm_3d = Sz.norm_3d;
    soln.norm_x = Sz.norm_x;
    soln.norm_y = Sz.norm_y;
    soln.norm_z = Sz.norm_z;

    // Write to a file
    apply_filter(soln, Sz);
    if (!restarting) soln.write_outputs(0);

    // Start step clock
    if (master()) soln.start_step_time = MPI_Wtime(); 
    if (master()) fprintf(stdout,"Startup time: %.12g\n", soln.start_step_time - soln.start_clock_time);

    /*
     *      BEGIN SOLVING
     */

    if (master()) fprintf(stdout,"---Initial State Values----\n");
    soln.write_initial_diagnostics(); 

    // Euler Step
    if (master()) fprintf(stdout,"---Beginning Euler Step----\n");
    step_euler(soln, flux, method, Sz);
    soln.update_after_step(plot_count);

    // AB2 Step
    if (master()) fprintf(stdout,"---Beginning AB2 Step------\n");
    step_ab2(soln, flux, method, Sz); 
    soln.update_after_step(plot_count);
    if (master()) fflush(stdout);

    if (master()) fprintf(stdout,"---Beginning AB3 Loop------\n");
    while (soln.t < tf) {

        // AB3 Step
        step_ab3(soln, flux, method, Sz); 

        // Write outputs if necessary
        if (soln.do_plot) {
            start_write = MPI_Wtime();
            soln.write_outputs(plot_count);
            soln.do_stitching();
            soln.next_plot_time += tplot;
            soln.do_plot = false;

            // Compute approximate clock-time for the step
            stop_write = MPI_Wtime();
            avg_write_time =    (avg_write_time*(plot_count - restart_sequence - 1) 
                    + (stop_write - start_write))
                /(plot_count - restart_sequence);
            avg_step_and_write_time =  
                avg_write_time + (stop_write - soln.start_step_time)/(soln.iterct+1);

            // Update some counters
            plot_count++;
        }

        // Write to dump file if necessary
        do_dump = 0;
        if (compute_time > 0.){
            time_remaining = compute_time - (stop_write - true_start_time);
            if (master() and (time_remaining < 3.*avg_step_and_write_time)) do_dump = 1;
            MPI_Bcast(&do_dump,1,MPI_INT,0,MPI_COMM_WORLD);
            if (do_dump == 1) {
                soln.finalize(plot_count,soln.t); // MUST be before dump
                soln.dump_if_needed(plot_count-1);
                if (compute_spectra) {
                    soln.stitch_spectra();
                    soln.close_spect();
                }
                soln.stitch_chain_diagnostics();
                soln.close_chain_diagnostics();

                soln.stitch_diagnostics();
                MPI_Finalize();
                exit(0);
            }
        }

        soln.update_after_step(plot_count);
    }

    if (master()) fprintf(stdout,"Finished at time %g!\n",soln.t);  

    // Tidy everything up and halt
    soln.finalize(plot_count,tf);
    if (compute_spectra) soln.close_spect();
    soln.close_chain_diagnostics();
    MPI_Finalize();
    return 0;
}

