
#ifndef QG_FUNCTIONS_HPP // Prevent double inclusion
#define QG_FUNCTIONS_HPP 1

#include "NSIntegrator.hpp"
#include "Science_qg.hpp"
#include "TArray.hpp"
#include "T_util.hpp"
#include "Par_util.hpp"
#include "Splits.hpp"
#include <iostream>
#include <fstream>
#include <blitz/array.h>
#include <vector>
#include "netcdf.h"
#include "netcdf_par.h"
#include "hdf5.h"

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

namespace qg_functions {

// Class to store solution variables
struct solnclass {

    private:
        TinyVector<int,3> local_lbound, local_extent;
        GeneralArrayStorage<3> local_storage;
        
        TinyVector<int,3> mlbound, mextent;
        GeneralArrayStorage<3> mstorage;
        
        // Blitz index placeholders
        blitz::firstIndex ii;
        blitz::secondIndex jj;
        blitz::thirdIndex kk;

    public:
        blitz::Range all;

        // Nonlinear Fields
        vector<DTArray *> q;
        vector<DTArray *> psi;
        vector<DTArray *> u;
        vector<DTArray *> v;
        vector<DTArray *> qx;
        vector<DTArray *> qy;

        // Background State
        vector<DTArray *> ub;
        vector<DTArray *> vb;
        vector<DTArray *> qxb;
        vector<DTArray *> qyb;
        vector<DTArray *> qb;
        vector<DTArray *> psib;

        // Zonal mean fields
        vector<DTArray *> qbar;
        vector<DTArray *> qbar_flux;
        vector<DTArray *> qbary;
        vector<DTArray *> ubar;
        vector<DTArray *> psibar;
        vector<DTArray *> meanvq;

        // Forcing fields
        vector<DTArray *> forcing;
        bool do_forcing;
        double force_time;

        // Some work arrays
        vector<DTArray *> work;
        vector<DTArray *> work_bar;
        double ** yz_slice;
        double ** yz_slice_red;

        // Grid arrays for netcdf
        double *x_vector, *y_vector, *z_vector;

        // Parameters for timestep
        double dx, dy, dz, t, next_plot_time;
        double w0,w1,w2;
        double dt0;
        double dt1;
        double dt2;
        bool do_plot;

        double U0;
        double V0;


        // Some problem parameters
        double g, H0, beta, N0, f0, tf, kappa;
        double Lr;
        int Nx, Ny, Nz;
        double Lx, Ly, Lz;
        bool restarting;
        TArrayn::S_EXP type_y, type_z, type_y_u;

        // Filter parameters
        double fstrength, fcutoff, forder; 

        // Diagnostics variables
        FILE * diagnostics;
        ifstream diagnostics_tmp;
        string diag_tmp;
        double max_q, min_q, q_pert_norm, q_norm;
        int iterct;
        double start_clock_time, 
               curr_clock_time, 
               start_step_time,
               step_time;
        bool compute_norms, compute_spectra;
        double next_diag_time, tdiag;
        double KE, PE, vol, rho0, PE_scale,
               KE_aniso, PE_aniso,
               *KE_aniso_array, *PE_aniso_array;
        double TE, Bu, Bu_x, Bu_y, KE_x, KE_y;
        double *level_mass;
        double curr_mass, curr_circ_N, curr_circ_S;
        double net_transport;
        double enstrophy;
        double include_bg_in_spectra;

        // Spectra and other vector diagnostics
        int offset_from, offset_to, N_spect;
        int prev_chain_write_count, chain_write_count;
        MPI_Status status;
        MPI_File spect_cnt_file;
        double **KE_2d_spect, *KE_1d_spect, *KE_x_spect, *KE_y_spect; 
        MPI_File KE_1d_spect_temp_file, KE_x_spect_temp_file,
                 KE_y_spect_temp_file, KE_2d_spect_temp_file,
                 KE_aniso_temp_file;
        MPI_File KE_1d_spect_final_file, KE_x_spect_final_file,
                 KE_y_spect_final_file, KE_2d_spect_final_file,
                 KE_aniso_final_file;
        double **PE_2d_spect, *PE_1d_spect, *PE_x_spect, *PE_y_spect; 
        MPI_File PE_1d_spect_temp_file, PE_x_spect_temp_file,
                 PE_y_spect_temp_file, PE_2d_spect_temp_file,
                 PE_aniso_temp_file;
        MPI_File PE_1d_spect_final_file, PE_x_spect_final_file,
                 PE_y_spect_final_file, PE_2d_spect_final_file,
                 PE_aniso_final_file;
        MPI_File level_mass_temp_file, level_mass_final_file;
        MPI_Offset old_chain_len;

        MPI_File ubar_temp_file, ubar_final_file,
                 qbar_temp_file, qbar_final_file,
                 psibar_temp_file, psibar_final_file;
        double *temp_bar;

        string method;

        int my_rank, rank, num_procs;

        // Constructor method
        solnclass(int sx, int sy, int sz, double lx, double ly, double lz,
                string const &flux_method, string const &ygrid_type, string const &zgrid_type) {

            start_clock_time = MPI_Wtime();
            MPI_Comm_rank(MPI_COMM_WORLD,&rank);
            MPI_Comm_rank(MPI_COMM_WORLD,&my_rank);
            MPI_Comm_size(MPI_COMM_WORLD,&num_procs);

            all = blitz::Range::all();

            Nx = sx;
            Ny = sy;
            Nz = sz;

            Lx = lx;
            Ly = ly;
            Lz = lz;

            w0 = 0.;
            w1 = 0.;
            w2 = 0.;

            do_forcing = false; // Default to no forcing.

            yz_slice = new double*[Nz];
            yz_slice_red = new double*[Nz];
            for (int II = 0; II < Nz; II++) {
                yz_slice[II] = new double[Ny];
                yz_slice_red[II] = new double[Ny];
            }

            dx = Lx/Nx;
            dy = Ly/Ny;
            dz = Lz/Nz;

            if (master()) {fprintf(stdout,"(Lx,Ly,Lz) = (%g,%g,%g)\n", Lx,Ly,Lz);}
            if (master()) {fprintf(stdout,"(Nx,Ny,Nz) = (%d,%d,%d)\n", Nx,Ny,Nz);}
            if (master()) {fprintf(stdout,"(dx,dy,dz) = (%g,%g,%g)\n", dx,dy,dz);}

            q_pert_norm = 0.0;
            q_norm = 0.0;

            local_lbound = alloc_lbound(Nx,Nz,Ny);
            local_extent = alloc_extent(Nx,Nz,Ny);
            local_storage = alloc_storage(Nx,Nz,Ny);

            mlbound = alloc_lbound(num_procs,Nz,Ny);
            mextent = alloc_extent(num_procs,Nz,Ny);
            mstorage = alloc_storage(num_procs,Nz,Ny);

            q.resize(1);
            psi.resize(1);
            u.resize(1);
            v.resize(1);
            qx.resize(1);
            qy.resize(1);

            // These are for the background state
            // and are needed for the nonlinear_pert
            // and linear flux functions. They are
            // initialized here 
            ub.resize(1);
            vb.resize(1);
            qxb.resize(1);
            qyb.resize(1);
            psib.resize(1);

            q[0]   = new DTArray(local_lbound,local_extent,local_storage);
            psi[0] = new DTArray(local_lbound,local_extent,local_storage);
            u[0]   = new DTArray(local_lbound,local_extent,local_storage);
            v[0]   = new DTArray(local_lbound,local_extent,local_storage);
            qx[0]  = new DTArray(local_lbound,local_extent,local_storage);
            qy[0]  = new DTArray(local_lbound,local_extent,local_storage);

            qbar.resize(1);
            qbar_flux.resize(3);
            qbary.resize(1);
            ubar.resize(1);
            psibar.resize(1);
            meanvq.resize(1);

            qbar[0] = new DTArray(mlbound, mextent, mstorage);
            qbar_flux[0] = new DTArray(mlbound, mextent, mstorage);
            qbar_flux[1] = new DTArray(mlbound, mextent, mstorage);
            qbar_flux[2] = new DTArray(mlbound, mextent, mstorage);
            qbary[0] = new DTArray(mlbound, mextent, mstorage);
            ubar[0] = new DTArray(mlbound, mextent, mstorage);
            psibar[0] = new DTArray(mlbound, mextent, mstorage);
            meanvq[0] = new DTArray(mlbound, mextent, mstorage);

            for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                    (*qbar[0])(rank,JJ,KK) = 0.;
                    (*qbar_flux[0])(rank,JJ,KK) = 0.;
                    (*qbar_flux[1])(rank,JJ,KK) = 0.;
                    (*qbar_flux[2])(rank,JJ,KK) = 0.;
                    (*qbary[0])(rank,JJ,KK) = 0.;
                    (*ubar[0])(rank,JJ,KK) = 0.;
                    (*psibar[0])(rank,JJ,KK) = 0.;
                    (*meanvq[0])(rank,JJ,KK) = 0.;
                }
            }

            work.resize(3);
            work[0] = new DTArray(local_lbound,local_extent,local_storage);
            work[1] = new DTArray(local_lbound,local_extent,local_storage);
            work[2] = new DTArray(local_lbound,local_extent,local_storage);

            work_bar.resize(1);
            work_bar[0] = new DTArray(mlbound, mextent, mstorage);

            dt0 = 1.0;
            dt1 = 1.0;
            dt2 = 1.0;
            do_plot = false;

            U0 = 0.0;
            V0 = 0.0;
            kappa = 0.0;
            g = 9.81;

            iterct = 0;

            next_diag_time = 0.0;
            chain_write_count = 0;
            rho0 = 1000.;
            vol  = Lx*Ly*Lz/(Nx*Ny*Nz);
            KE = 0.;
            PE = 0.;
            TE = 0.;
            enstrophy = 0.;
            KE_aniso = 0.;
            PE_aniso = 0.;
            Bu = 0.;

            include_bg_in_spectra = 0.;

            method = flux_method;

            if (ygrid_type == "PERIODIC") {
                type_y = FOURIER;
                type_y_u = FOURIER;
            } else if (ygrid_type == "FREE_SLIP") {
                type_y = SINE;
                type_y_u = COSINE;
            } else {
                if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
                MPI_Finalize(); exit(1);
            }

            if (zgrid_type == "FOURIER") {
                type_z = FOURIER;
            } else if (zgrid_type == "REAL") { 
                type_z = COSINE;
            } else if (zgrid_type == "CHEB") {
                type_z = NONE;
            } else if (zgrid_type == "NONE") {
                type_z = NONE;
            } else {
                if (master()) fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
                MPI_Finalize(); exit(1);
            }

        }

        // Initialize various constants
        void initialize_constants() {

            // Deformation radius
            Lr = pow(g*H0,0.5)/f0;

            // Now extract the zonal mean
            // This loop block computes the zonal mean
            for (int JJ = q[0]->lbound(secondDim); JJ <= q[0]->ubound(secondDim); JJ++) {
                for (int KK = q[0]->lbound(thirdDim); KK <= q[0]->ubound(thirdDim); KK++) {
                    yz_slice[JJ][KK] = 0.;
                    for (int II = q[0]->lbound(firstDim); II <= q[0]->ubound(firstDim); II++) {
                        yz_slice[JJ][KK] += (*q[0])(II,JJ,KK)/Nx;
                    }
                }
            }

            // Now communicate the mean
            sum_reduce_yz_slice();

            // Now subtract the mean and record it
            for (int JJ = q[0]->lbound(secondDim); JJ <= q[0]->ubound(secondDim); JJ++) {
                for (int KK = q[0]->lbound(thirdDim); KK <= q[0]->ubound(thirdDim); KK++) {
                    for (int II = q[0]->lbound(firstDim); II <= q[0]->ubound(firstDim); II++) {
                        (*q[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) - yz_slice_red[JJ][KK];
                    }
                    (*qbar[0])(rank,JJ,KK) = yz_slice_red[JJ][KK];
                }
            }

            if (method == "nonlinear") { include_bg_in_spectra = 0.; }

            //
            //// HANDLE RESTARTING
            //

            // If we're restarting, determine how what offset to use for the spectra.
            // and other chain diagnostics
            if (restarting) {
                if (Nz > 1) {
                    MPI_File_get_size(level_mass_final_file, &old_chain_len);
                    prev_chain_write_count = old_chain_len/sizeof(double)/Nz;
                }
                if (Nz == 1) {
                    MPI_File_get_size(ubar_final_file, &old_chain_len);
                    prev_chain_write_count = old_chain_len/sizeof(double)/Ny;
                }
            } else {
                prev_chain_write_count = 0;
            }

            // DEFINE GRIDS FOR NETCDF OUTPUTS
            x_vector = new double[Nx];
            y_vector = new double[Ny];
            z_vector = new double[Nz];

            for (int II = 0; II < Nx; II++) x_vector[II] = (II + 0.5)/Nx*Lx - Lx/2;
            for (int JJ = 0; JJ < Ny; JJ++) y_vector[JJ] = (JJ + 0.5)/Ny*Ly - Ly/2;

            if (type_z == NONE) {
                for (int KK = 0; KK < Nz; KK++) z_vector[KK] = Lz*cos(KK*M_PI/(Nz-1)) - Lz;
            } else {
                for (int KK = 0; KK < Nz; KK++) z_vector[KK] = (KK + 0.5)/Nz*Lz - Lz/2;
            }

        }

        // Sum-reduce yz_slice
        void sum_reduce_yz_slice() {
            for (int II = 0; II < Nz; II++) {
                MPI_Allreduce(yz_slice[II], yz_slice_red[II], Ny, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
            }
        }

        // Load background profiles
        void initialize_background_states(string const &ub_fn, string const &vb_fn, string const &qxb_fn, 
                string const &qyb_fn, string const &psib_fn) {

            local_lbound = alloc_lbound(Nx,Nz,Ny);
            local_extent = alloc_extent(Nx,Nz,Ny);
            local_storage = alloc_storage(Nx,Nz,Ny);

            ub[0] = new DTArray(local_lbound,local_extent,local_storage);
            vb[0] = new DTArray(local_lbound,local_extent,local_storage);
            qxb[0] = new DTArray(local_lbound,local_extent,local_storage);
            qyb[0] = new DTArray(local_lbound,local_extent,local_storage);
            psib[0] = new DTArray(local_lbound,local_extent,local_storage);

            if (master()) 
                fprintf(stdout,"Reading ub from %s\n", ub_fn.c_str());
            read_array_qg(*ub[0],ub_fn.c_str(),Nx,Ny,Nz);
            if (master()) 
                fprintf(stdout,"Reading vb from %s\n", vb_fn.c_str());
            read_array_qg(*vb[0],vb_fn.c_str(),Nx,Ny,Nz);
            if (master()) 
                fprintf(stdout,"Reading qxb from %s\n", qxb_fn.c_str());
            read_array_qg(*qxb[0],qxb_fn.c_str(),Nx,Ny,Nz);
            if (master()) 
                fprintf(stdout,"Reading qyb from %s\n", qyb_fn.c_str());
            read_array_qg(*qyb[0],qyb_fn.c_str(),Nx,Ny,Nz);
            if (master()) 
                fprintf(stdout,"Reading psib from %s\n", psib_fn.c_str());
            read_array_qg(*psib[0],psib_fn.c_str(),Nx,Ny,Nz);

            // Now determine upper bound of background velocities
            double tmp1, tmp2;
            tmp1 =  pvmax(*ub[0]);
            tmp2 = -pvmin(*ub[0]);
            if (tmp1 > tmp2) U0 = tmp1;
            else U0 = tmp2;

            tmp1 =  pvmax(*vb[0]);
            tmp2 = -pvmin(*vb[0]);
            if (tmp1 > tmp2) V0 = tmp1;
            else V0 = tmp2;

        }

        // Load background profile for computing norms
        void initialize_background_q(string const &qb_fn) {

            local_lbound = alloc_lbound(Nx,Nz,Ny);
            local_extent = alloc_extent(Nx,Nz,Ny);
            local_storage = alloc_storage(Nx,Nz,Ny);

            qb.resize(1);
            qb[0] = new DTArray(local_lbound,local_extent,local_storage);

            if (master()) 
                fprintf(stdout,"Reading qb from %s\n", qb_fn.c_str());
            read_array_qg(*qb[0],qb_fn.c_str(),Nx,Ny,Nz);
        }

        // Load forcing function if needed
        void initialize_forcing(string const &qforce) {

            local_lbound = alloc_lbound(Nx,Nz,Ny);
            local_extent = alloc_extent(Nx,Nz,Ny);
            local_storage = alloc_storage(Nx,Nz,Ny);

            forcing.resize(1); 
            forcing[0] = new DTArray(local_lbound,local_extent,local_storage);

            if (master()) { fprintf(stdout,"Reading q forcing from %s\n", qforce.c_str()); }
            read_array_qg(*forcing[0],qforce.c_str(),Nx,Ny,Nz);

        }

        // Error function for netcdf
        void ERR(int e) {
            fprintf(stderr, "Error: %s\n", nc_strerror(e));
        }
 
        // Create and write netcdf file
        void write_to_netcdf(int iter, bool write_psi, bool write_vels) {

            // Open the NETCDF file
            int FLAG = NC_NETCDF4 | NC_MPIIO;
            int ncid=0, retval;
            char buffer [50];
            snprintf(buffer, 50, "output_%04d.nc", iter);
            if (( retval = nc_create_par(buffer, FLAG, MPI_COMM_WORLD, MPI_INFO_NULL, &ncid) ))
                ERR(retval);

            // Define the dimensions
            int xdimid, ydimid, zdimid;
            if ((retval = nc_def_dim(ncid, "x", Nx, &xdimid)))
                ERR(retval);
            if ((retval = nc_def_dim(ncid, "y", Ny, &ydimid)))
                ERR(retval);
            if ((retval = nc_def_dim(ncid, "z", Nz, &zdimid)))
                ERR(retval);

            // Define coordinate variables
            int xvarid, yvarid, zvarid;
            if ((retval = nc_def_var(ncid, "x", NC_FLOAT, 1, &xdimid, &xvarid)))
                ERR(retval);
            if ((retval = nc_def_var(ncid, "y", NC_FLOAT, 1, &ydimid, &yvarid)))
                ERR(retval);
            if ((retval = nc_def_var(ncid, "z", NC_FLOAT, 1, &zdimid, &zvarid)))
                ERR(retval);

            // Transpose
            int dimids[3];
            int ndims = 3;
            dimids[2] = xdimid;
            dimids[1] = zdimid;
            dimids[0] = ydimid;

            // Declare variables
            int qvarid, psivarid, uvarid, vvarid;
            if ((retval = nc_def_var(ncid, "q", NC_DOUBLE, ndims, dimids, &qvarid)))
                ERR(retval);

            if (write_psi) {
                if ((retval = nc_def_var(ncid, "psi", NC_DOUBLE, ndims, dimids, &psivarid))) {
                    ERR(retval);
                }
            }

            if (write_vels) {
                if ((retval = nc_def_var(ncid, "u", NC_DOUBLE, ndims, dimids, &uvarid))) {
                    ERR(retval);
                }
                if ((retval = nc_def_var(ncid, "v", NC_DOUBLE, ndims, dimids, &vvarid))) {
                    ERR(retval);
                }
            }

            // End the definition
            if ((retval = nc_enddef(ncid))) { ERR(retval); }

            size_t start[3], count[3];
            start[2] = q[0]->lbound(firstDim);
            start[1] = q[0]->lbound(secondDim);
            start[0] = q[0]->lbound(thirdDim);
            count[2] = q[0]->ubound(firstDim);//-1;
            count[1] = q[0]->ubound(secondDim);//-1;
            count[0] = q[0]->ubound(thirdDim);//-1;

            cout << "Start";
            cout << start;
            cout << "Count";
            cout << count;

            // Put the coordinate variables
            if (master()) { // Grid needs to be written by master for reasons
                if ((retval = nc_put_var_double(ncid, xvarid, x_vector)))
                    ERR(retval);
                if ((retval = nc_put_var_double(ncid, yvarid, y_vector)))
                    ERR(retval);
                if ((retval = nc_put_var_double(ncid, zvarid, z_vector)))
                    ERR(retval);
            }
            
            // Put the variables
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK);
                        (*work[1])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK);
                        (*work[2])(II,JJ,KK) = (*u[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK);
                    }
                }
            }

            if ((retval = nc_put_vara_double(ncid, qvarid, start, count, work[0]->data())))
                ERR(retval);

            if (write_psi) {
                if ((retval = nc_put_vara_double(ncid, psivarid, start, count, work[1]->data()))) {
                    ERR(retval);
                }
            }

            if (write_vels) {
                if ((retval = nc_put_vara_double(ncid, uvarid, start, count, work[2]->data()))) {
                    ERR(retval);
                }
                if ((retval = nc_put_vara_double(ncid, vvarid, start, count, v[0]->data()))) {
                    ERR(retval);
                }
            }

            // Close the file
            if ((retval = nc_close(ncid))) { ERR(retval); }
        }


        // Save state
        void write_outputs(int iter, bool write_psi, bool write_vels) {

            // Temporary fix until I figure out how to add 2D to 3D with Blitz
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK);
                        (*work[1])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK);
                        (*work[2])(II,JJ,KK) = (*u[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK);
                    }
                }
            }
            write_array_qg(*work[0],"q",iter);
            if (!(write_psi == 0)) { write_array_qg(*work[1],"psi",iter); }
            if (!(write_vels == 0)) {
                write_array_qg(*work[2],"u",iter);
                write_array_qg(*v[0],"v",iter);
            }

            // Original version
            //write_array_qg(*q[0],"q",iter);
            //if (!(write_psi == 0)) { write_array_qg(*psi[0],"psi",iter); }
            //if (!(write_vels == 0)) {
            //    write_array_qg(*u[0],"u",iter);
            //    write_array_qg(*v[0],"v",iter);
            //}
        }

        // Initialize the diagnostics file
        void initialize_diagnostics_files(){

            if (master()) {

                // Only over-write diagnostics.txt if not restarting
                if (!restarting) {
                    diagnostics = fopen("diagnostics.txt", "w");
                    assert(diagnostics);
                    fprintf(diagnostics, "iteration, time, step_time, KE, APE" );
                    fprintf(diagnostics, ", enstrophy, max_q, min_q" );
                    if (compute_norms) { fprintf(diagnostics, ", q_pert_norm, q_norm" ); } 
                    fprintf(diagnostics, ", KE_aniso, PE_aniso" );
                    fprintf(diagnostics, ", Bu, Bu_x, Bu_y" );
                    fprintf(diagnostics, ", circ_N, circ_S, mass, net_transport\n" );
                }
                else {
                    diagnostics = fopen("diagnostics.txt", "a");
                    assert(diagnostics);
                }
                fclose(diagnostics);

                // Always over-write diagnostics_tmp.txt
                diagnostics = fopen("diagnostics_tmp.txt", "w");
                assert(diagnostics);
                fclose(diagnostics);
            }
        }

        void stitch_diagnostics() {
            if (master()) {
                diagnostics_tmp.open ("diagnostics_tmp.txt");
                diagnostics = fopen("diagnostics.txt","a");
                assert(diagnostics);

                // Copy everything from diagnostics_tmp.txt to diagnostics.txt
                while (getline (diagnostics_tmp,diag_tmp) ) {
                    fprintf(diagnostics, diag_tmp.c_str()); 
                    fprintf(diagnostics, "\n"); 
                }

                fclose(diagnostics);
                diagnostics_tmp.close();

                // Now wipe the diagnostics_tmp.txt file
                diagnostics = fopen("diagnostics_tmp.txt","w");
                assert(diagnostics);
                fclose(diagnostics);
            }
        }

        void initialize_chain_diagnostics_files() {

            if (Nz > 1) {
                // Initialize level_mass
                level_mass = new double[Nz];
                MPI_File_open(MPI_COMM_WORLD, "level_mass_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &level_mass_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "level_mass.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                        MPI_INFO_NULL, &level_mass_final_file);
            }

            if (Nz == 1) {
                // Initialize ubar, qbar, and psibar files
                temp_bar = new double[Ny];
                MPI_File_open(MPI_COMM_WORLD, "ubar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &ubar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "ubar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                        MPI_INFO_NULL, &ubar_final_file);

                MPI_File_open(MPI_COMM_WORLD, "qbar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &qbar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "qbar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                        MPI_INFO_NULL, &qbar_final_file);

                MPI_File_open(MPI_COMM_WORLD, "psibar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &psibar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "psibar.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                        MPI_INFO_NULL, &psibar_final_file);
            }

        }

        void initialize_spectra_files() {

            N_spect = (Nx >= Ny) ? Nx/2 : Ny/2;

            // Initialize KE anisotrophy array
            KE_aniso_array = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &KE_aniso_final_file);

            // Initialize 2D KE spectrum (depth averaged)
            KE_2d_spect = new double*[N_spect];
            for (int II = 0; II < N_spect; II++) { KE_2d_spect[II] = new double[N_spect]; }
            MPI_File_open(MPI_COMM_WORLD, "KE_2d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_2d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_2d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &KE_2d_spect_final_file);

            // Initialize azimuthally-integrated KE spectrum (depth averaged)
            KE_1d_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &KE_1d_spect_final_file);

            // Initialize x KE spectrum (yz averaged)
            KE_x_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "KE_x_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_x_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_x_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &KE_x_spect_final_file);

            // Initialize y KE spectrum (xz averaged)
            KE_y_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "KE_y_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_y_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_y_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &KE_y_spect_final_file);

            //
            //// Now initialize PE
            //

            // Initialize PE anisotrophy array
            PE_aniso_array = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_aniso.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &PE_aniso_final_file);

            // Initialize 2D PE spectrum (depth averaged)
            PE_2d_spect = new double*[N_spect];
            for (int II = 0; II < N_spect; II++) { PE_2d_spect[II] = new double[N_spect]; }
            MPI_File_open(MPI_COMM_WORLD, "PE_2d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_2d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_2d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &PE_2d_spect_final_file);

            // Initialize azimuthally-integrated PE spectrum (depth averaged)
            PE_1d_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &PE_1d_spect_final_file);

            // Initialize x PE spectrum (yz averaged)
            PE_x_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "PE_x_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_x_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_x_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &PE_x_spect_final_file);

            // Initialize y PE spectrum (xz averaged)
            PE_y_spect = new double[N_spect];
            MPI_File_open(MPI_COMM_WORLD, "PE_y_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_y_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_y_spect.bin", MPI_MODE_WRONLY | MPI_MODE_CREATE,
                    MPI_INFO_NULL, &PE_y_spect_final_file);

            //
            //// HANDLE THE BIN COUNTS (USEFUL FOR TESTING WITH PARCEVAL AFTER THE FACT)
            //

            // Compute the bin counts for the 1D spectra
            horiz_spectrum_bin_counts(*u[0], KE_1d_spect, KE_x_spect, KE_y_spect, Lx, Ly, FOURIER, type_y_u);

            // azimuthally integrated
            MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts.bin", 
                    MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
            if (master()) {
                MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
                MPI_File_write(spect_cnt_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);
            }
            MPI_File_close(&spect_cnt_file);

            // x
            MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts_x.bin", 
                    MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
            if (master()) {
                MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
                MPI_File_write(spect_cnt_file, KE_x_spect, N_spect, MPI_DOUBLE, &status);
            }
            MPI_File_close(&spect_cnt_file);

            // y
            MPI_File_open(MPI_COMM_WORLD, "spectra_bin_counts_y.bin", 
                    MPI_MODE_WRONLY | MPI_MODE_CREATE, MPI_INFO_NULL, &spect_cnt_file);
            if (master()) {
                MPI_File_seek(spect_cnt_file, 0, MPI_SEEK_SET);
                MPI_File_write(spect_cnt_file, KE_y_spect, N_spect, MPI_DOUBLE, &status);
            }
            MPI_File_close(&spect_cnt_file);

        }

        void update_energy_diagnostics() {

            // If Nz == 1, write ubar, qbar, and psibar
            if ((Nz == 1) and (master())) {
                for (int JJ = 0; JJ < Ny; JJ++) {
                    temp_bar[JJ] = (*ubar[0])(0,0,JJ);
                }
                MPI_File_seek(ubar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(ubar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);

                for (int JJ = 0; JJ < Ny; JJ++) {
                    temp_bar[JJ] = (*qbar[0])(0,0,JJ);
                }
                MPI_File_seek(qbar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(qbar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);

                for (int JJ = 0; JJ < Ny; JJ++) {
                    temp_bar[JJ] = (*psibar[0])(0,0,JJ);
                }
                MPI_File_seek(psibar_temp_file, Ny*chain_write_count*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(psibar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
            }

            // KE
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        if (method != "nonlinear") {
                            (*work[0])(II,JJ,KK) = (*u[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK) + (*ub[0])(II,JJ,KK);
                        } else {
                            (*work[0])(II,JJ,KK) = (*u[0])(II,JJ,KK) + (*ubar[0])(rank,JJ,KK);
                        }
                    }
                }
            }
            if (method != "nonlinear") {
                *work[1] = *(v[0]) + *(vb[0]);
            } else {
                *work[1] = *(v[0]);
            }
            net_transport = pssum(sum(*work[0]))*dx*dy*dz;

            // Compute circulation at North and South walls if appropriate
            if (type_y_u == COSINE) {
                curr_circ_N = pssum(sum((*work[0])(all,all,Ny-1))); 
                curr_circ_S = pssum(sum((*work[0])(all,all,0))); 
                //compute_circulation(&curr_circ_N, &curr_circ_S, *work[0], COSINE);
                if (Nz > 1) {
                    curr_circ_N *= dx*dz;
                    curr_circ_S *= dx*dz;
                }
                else{
                    curr_circ_N *= dx;
                    curr_circ_S *= dx;
                }
            }
            else {
                curr_circ_N = 0.;
                curr_circ_S = 0.;
            }
            KE = 0.5*rho0*vol*pssum(sum(((*work[0])*(*work[0]) + (*work[1])*(*work[1]))));
            KE_x = 0.5*rho0*vol*pssum(sum(((*work[0])*(*work[0]))));
            KE_y = 0.5*rho0*vol*pssum(sum(((*work[1])*(*work[1]))));

            if (compute_spectra) {

                // 1. Compute the spectra
                KE_spect(&KE_aniso, KE_aniso_array, u[0], v[0], ub[0], 
                        vb[0], ubar[0], work[0], work_bar[0],
                        KE_2d_spect, KE_1d_spect, KE_x_spect, KE_y_spect, 
                        include_bg_in_spectra, Lx, Ly, type_y);

                // 2. Scale the spectra
                for (int II = 0; II < N_spect; II++) {
                    for (int JJ = 0; JJ < N_spect; JJ++) {
                        KE_2d_spect[II][JJ] *= 0.5*rho0;
                    }
                    KE_1d_spect[II] *= 0.5*rho0;
                    KE_x_spect[II] *= 0.5*rho0;
                    KE_y_spect[II] *= 0.5*rho0;
                }

                // 3. Write spectra to a file
                if (master()) {
                    MPI_File_seek(KE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(KE_aniso_temp_file, KE_aniso_array, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(KE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(KE_1d_spect_temp_file, KE_1d_spect, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(KE_x_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(KE_x_spect_temp_file, KE_x_spect, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(KE_y_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(KE_y_spect_temp_file, KE_y_spect, N_spect, MPI_DOUBLE, &status);
                }
            }

            // PE
            for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                    for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                        if ( (method != "nonlinear") and (include_bg_in_spectra > 0.1) ) {
                            (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK) + (*psib[0])(II,JJ,KK);
                        } else {
                            (*work[0])(II,JJ,KK) = (*psi[0])(II,JJ,KK) + (*psibar[0])(rank,JJ,KK);
                        }
                    }
                }
            }

            if (Nz > 1) {
                dpsidz(*work[0], work[1], Lx, Ly, Lz, Nx, Ny, Nz);
                PE = 0.5*rho0*f0*f0/(N0*N0)*vol*pssum(sum((*work[1])*(*work[1]))); // 0.5*rho0*(dpsidz*f0/N0)^2

                compute_mass_per_level(work[1], level_mass); // For the sake of curiosity

                if (master()) {
                    MPI_File_seek(level_mass_temp_file, Nz*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(level_mass_temp_file, level_mass, Nz, MPI_DOUBLE, &status);
                }

                curr_mass = 0;
                for (int II = 0; II < Nz; II++) curr_mass += level_mass[II];
            } else {
                PE = 0.5*rho0*vol*pssum(sum((*work[0])*(*work[0])))/(Lr*Lr);
                curr_mass = pssum(sum(*work[0]))*dx*dy;
            }

            if (compute_spectra) {

                // 1. Compute the spectra
                PE_spect(&PE_aniso, PE_aniso_array, psi[0], psib[0], psibar[0], 
                        work[0], work[1], work_bar[0], PE_2d_spect, PE_1d_spect, PE_x_spect, PE_y_spect,
                        include_bg_in_spectra, Lx, Ly, Lz, f0, N0, type_y);

                // 2. Scale the spectra
                PE_scale = (Nz > 1) ? 0.5*rho0*f0*f0/(N0*N0) : 0.5*rho0/(Lr*Lr);
                for (int II = 0; II < N_spect; II++) {
                    for (int JJ = 0; JJ < N_spect; JJ++) {
                        PE_2d_spect[II][JJ] *= PE_scale;
                    }
                    PE_1d_spect[II] *= PE_scale;
                    PE_x_spect[II]  *= PE_scale;
                    PE_y_spect[II]  *= PE_scale;
                }

                // 3. Write spectra to a file
                if (master()) {
                    MPI_File_seek(PE_aniso_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(PE_aniso_temp_file, PE_aniso_array, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(PE_1d_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(PE_1d_spect_temp_file, PE_1d_spect, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(PE_x_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(PE_x_spect_temp_file, PE_x_spect, N_spect, MPI_DOUBLE, &status);

                    MPI_File_seek(PE_y_spect_temp_file, N_spect*chain_write_count*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(PE_y_spect_temp_file, PE_y_spect, N_spect, MPI_DOUBLE, &status);
                }
            }

            // Compute Burger number(s)
            Bu = 0.5*KE/PE;
            Bu_x = KE_x/PE;
            Bu_y = KE_y/PE;

            chain_write_count += 1;
        }

        void stitch_chain_diagnostics() {

            if (Nz > 1) {
                for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
                    offset_from = JJ*Nz;
                    offset_to   = (JJ + prev_chain_write_count)*Nz;

                    // level_mass
                    MPI_File_seek(level_mass_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                    MPI_File_read(level_mass_temp_file, level_mass, Nz, MPI_DOUBLE, &status);
                    MPI_File_seek(level_mass_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(level_mass_final_file, level_mass, Nz, MPI_DOUBLE, &status);
                }

                MPI_Barrier(MPI_COMM_WORLD);

                MPI_File_close(&level_mass_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "level_mass_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &level_mass_temp_file);
            }

            if (Nz == 1) {
                for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 
                    offset_from = JJ*Ny;
                    offset_to   = (JJ + prev_chain_write_count)*Ny;

                    // ubar
                    MPI_File_seek(ubar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                    MPI_File_read(ubar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
                    MPI_File_seek(ubar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(ubar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);

                    // qbar
                    MPI_File_seek(qbar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                    MPI_File_read(qbar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
                    MPI_File_seek(qbar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(qbar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);

                    // psibar
                    MPI_File_seek(psibar_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                    MPI_File_read(psibar_temp_file, temp_bar, Ny, MPI_DOUBLE, &status);
                    MPI_File_seek(psibar_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                    MPI_File_write(psibar_final_file, temp_bar, Ny, MPI_DOUBLE, &status);
                }

                MPI_Barrier(MPI_COMM_WORLD);

                MPI_File_close(&ubar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "ubar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &ubar_temp_file);

                MPI_File_close(&qbar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "qbar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &qbar_temp_file);

                MPI_File_close(&psibar_temp_file);
                MPI_File_open(MPI_COMM_WORLD, "psibar_temp.bin", 
                        MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                        MPI_INFO_NULL, &psibar_temp_file);

            }
        }

        void stitch_spectra() {

            for (int JJ = my_rank; JJ < chain_write_count; JJ += num_procs) { 

                offset_from = JJ*Nx/2;
                offset_to   = (JJ + prev_chain_write_count)*Nx/2;

                // KE anisotropy
                MPI_File_seek(KE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_aniso_temp_file, KE_aniso_array, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(KE_aniso_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_aniso_final_file, KE_aniso_array, Nx/2, MPI_DOUBLE, &status);

                // azimuthally average KE spectrum
                MPI_File_seek(KE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_1d_spect_temp_file, KE_1d_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(KE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_1d_spect_final_file, KE_1d_spect, Nx/2, MPI_DOUBLE, &status);

                // x KE spectrum
                MPI_File_seek(KE_x_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_x_spect_temp_file, KE_x_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(KE_x_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_x_spect_final_file, KE_x_spect, Nx/2, MPI_DOUBLE, &status);

                // y KE spectrum
                MPI_File_seek(KE_y_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(KE_y_spect_temp_file, KE_y_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(KE_y_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(KE_y_spect_final_file, KE_y_spect, Nx/2, MPI_DOUBLE, &status);

                // PE anisotropy
                MPI_File_seek(PE_aniso_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_aniso_temp_file, PE_aniso_array, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(PE_aniso_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_aniso_final_file, PE_aniso_array, Nx/2, MPI_DOUBLE, &status);

                // azimuthally averaged PE spectrum
                MPI_File_seek(PE_1d_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_1d_spect_temp_file, PE_1d_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(PE_1d_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_1d_spect_final_file, PE_1d_spect, Nx/2, MPI_DOUBLE, &status);

                // x PE spectrum
                MPI_File_seek(PE_x_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_x_spect_temp_file, PE_x_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(PE_x_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_x_spect_final_file, PE_x_spect, Nx/2, MPI_DOUBLE, &status);

                // y PE spectrum
                MPI_File_seek(PE_y_spect_temp_file, offset_from*sizeof(double), MPI_SEEK_SET);
                MPI_File_read(PE_y_spect_temp_file, PE_y_spect, Nx/2, MPI_DOUBLE, &status);
                MPI_File_seek(PE_y_spect_final_file, offset_to*sizeof(double), MPI_SEEK_SET);
                MPI_File_write(PE_y_spect_final_file, PE_y_spect, Nx/2, MPI_DOUBLE, &status);
            }

            MPI_Barrier(MPI_COMM_WORLD);

            MPI_File_close(&KE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_aniso_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_aniso_temp_file);

            MPI_File_close(&KE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_1d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_1d_spect_temp_file);

            MPI_File_close(&KE_x_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_x_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_x_spect_temp_file);
            
            MPI_File_close(&KE_y_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "KE_y_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &KE_y_spect_temp_file);

            MPI_File_close(&PE_aniso_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_aniso_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_aniso_temp_file);

            MPI_File_close(&PE_1d_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_1d_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_1d_spect_temp_file);

            MPI_File_close(&PE_x_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_x_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_x_spect_temp_file);

            MPI_File_close(&PE_y_spect_temp_file);
            MPI_File_open(MPI_COMM_WORLD, "PE_y_spect_temp.bin", 
                    MPI_MODE_RDWR | MPI_MODE_CREATE | MPI_MODE_DELETE_ON_CLOSE,
                    MPI_INFO_NULL, &PE_y_spect_temp_file);

        }

        void do_stitching() {

            if (compute_spectra) { stitch_spectra(); }
            stitch_chain_diagnostics();
            stitch_diagnostics();

            prev_chain_write_count += chain_write_count;
            chain_write_count = 0;
        }

        void close_chain_diagnostics() {
            if (Nz > 1) {
                MPI_File_close(&level_mass_temp_file);
                MPI_File_close(&level_mass_final_file);
            }

            if (Nz == 1) {
                MPI_File_close(&ubar_temp_file);
                MPI_File_close(&ubar_final_file);

                MPI_File_close(&qbar_temp_file);
                MPI_File_close(&qbar_final_file);

                MPI_File_close(&psibar_temp_file);
                MPI_File_close(&psibar_final_file);
            }
        }

        void close_spect() {
            MPI_File_close(&KE_aniso_temp_file);
            MPI_File_close(&KE_aniso_final_file);
            MPI_File_close(&KE_2d_spect_temp_file);
            MPI_File_close(&KE_2d_spect_final_file);
            MPI_File_close(&KE_1d_spect_temp_file);
            MPI_File_close(&KE_1d_spect_final_file);
            MPI_File_close(&KE_x_spect_temp_file);
            MPI_File_close(&KE_x_spect_final_file);
            MPI_File_close(&KE_y_spect_temp_file);
            MPI_File_close(&KE_y_spect_final_file);
            MPI_File_close(&PE_aniso_temp_file);
            MPI_File_close(&PE_aniso_final_file);
            MPI_File_close(&PE_2d_spect_temp_file);
            MPI_File_close(&PE_2d_spect_final_file);
            MPI_File_close(&PE_1d_spect_temp_file);
            MPI_File_close(&PE_1d_spect_final_file);
            MPI_File_close(&PE_x_spect_temp_file);
            MPI_File_close(&PE_x_spect_final_file);
            MPI_File_close(&PE_y_spect_temp_file);
            MPI_File_close(&PE_y_spect_final_file);
        }

        // Write initial diagnostics
        void write_initial_diagnostics(){

            update_energy_diagnostics();

            // Compute norms
            if (method == "nonlinear") { 
                for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                    for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                        for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                            (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK);
                        }
                    }
                }
                if (compute_norms) {
                    *work[1] = *(work[0]) - *(qb[0]);
                    q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                    q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                }
            } else {
                for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                    for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                        for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                            (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qb[0])(II,JJ,KK);
                        }
                    }
                }
                if (compute_norms) {
                    *work[1] = *(work[0]) - *(qb[0]);
                    q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]))), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                    q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                }
            }

            max_q = pvmax(*work[0]);
            min_q = pvmin(*work[0]);
            enstrophy = 0.5*pssum(sum( (*work[0])*(*work[0]) ));

            // Write the diagnostics
            if (master()) {
                fprintf(stdout,"t = %.4g days (%.3g%%, 0 outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                        t/86400.,100.*t/tf,KE,PE,dt0);
                fprintf(stdout,"\n");
                diagnostics = fopen("diagnostics_tmp.txt", "a");
                assert(diagnostics);
                fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
                fprintf(diagnostics,", %.16g, %.16g, %.16g", enstrophy, max_q, min_q);
                if (compute_norms) { fprintf(diagnostics,", %.16g, %.16g", q_pert_norm, q_norm); }
                fprintf(diagnostics,", %.16g, %.16g", KE_aniso, PE_aniso);
                fprintf(diagnostics,", %.16g, %.16g, %.16g", Bu, Bu_x, Bu_y);
                fprintf(diagnostics,", %.16g, %.16g, %.16g, %.16g", curr_circ_N, curr_circ_S, curr_mass, net_transport);
                fprintf(diagnostics,"\n");
                fclose(diagnostics);
            }

        }

        // Update after step
        void update_after_step(int plot_count){

            iterct++;
            t = t + dt0;
            curr_clock_time = MPI_Wtime();
            step_time = curr_clock_time - start_step_time;

            // If appropriate, do diagnostics
            // Note that tdiag = 0 (the default) will always satisfy this
            if (next_diag_time - t <= 0.0001*tdiag) {

                update_energy_diagnostics();

                // Compute norms
                if (method == "nonlinear") { 
                    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                                (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK);
                            }
                        }
                    }
                    if (compute_norms) {
                        *work[1] = *(work[0]) - *(qb[0]);
                        q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                        q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                    }
                } else {
                    for (int II = u[0]->lbound(firstDim); II <= u[0]->ubound(firstDim); II++) {
                        for (int JJ = u[0]->lbound(secondDim); JJ <= u[0]->ubound(secondDim); JJ++) {
                            for (int KK = u[0]->lbound(thirdDim); KK <= u[0]->ubound(thirdDim); KK++) {
                                (*work[0])(II,JJ,KK) = (*q[0])(II,JJ,KK) + (*qbar[0])(rank,JJ,KK) + (*qb[0])(II,JJ,KK);
                            }
                        }
                    }
                    if (compute_norms) {
                        *work[1] = *(work[0]) - *(qb[0]);
                        q_pert_norm = pow(pssum(sum( (*work[1])*(*work[1]))), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                        q_norm = pow(pssum(sum( (*work[0])*(*work[0]) )), 0.5)*(Lx*Ly*Lz)/(Nx*Ny*Nz);
                    }
                }

                max_q = pvmax(*work[0]);
                min_q = pvmin(*work[0]);
                enstrophy = 0.5*pssum(sum( (*work[0])*(*work[0]) ));

                // Write the diagnostics
                if (master()) {
                    fprintf(stdout,"t = %.4g days (%.3g%%, %d outputs), (KE,PE) = (%.3g, %.3g), dt = %.3g",
                            t/86400.,100.*t/tf,plot_count-1,KE,PE,dt0);
                    fprintf(stdout,"\n");
                    diagnostics = fopen("diagnostics_tmp.txt", "a");
                    assert(diagnostics);
                    fprintf(diagnostics,"%d, %.16g, %.16g, %.16g, %.16g", t, step_time, iterct, KE, PE);
                    fprintf(diagnostics,", %.16g, %.16g, %.16g", enstrophy, max_q, min_q);
                    if (compute_norms) { fprintf(diagnostics,", %.16g, %.16g", q_pert_norm, q_norm); }
                    fprintf(diagnostics,", %.16g, %.16g", KE_aniso, PE_aniso);
                    fprintf(diagnostics,", %.16g, %.16g, %.16g", Bu, Bu_x, Bu_y);
                    fprintf(diagnostics,", %.16g, %.16g, %.16g, %.16g", curr_circ_N, curr_circ_S, curr_mass, net_transport);
                    fprintf(diagnostics,"\n");
                    fclose(diagnostics);
                }
    
                // Update next diag time
                next_diag_time += tdiag;
            }
        }

        void dump_if_needed(int plot_count) {

            write_array_qg(*q[0],"q.dump",-1);

            // Write the dump time to a text file for restart purposes
            if (master()){
                FILE * dump_file;
                dump_file = fopen("dump_time.txt","w");
                assert(dump_file);
                fprintf(dump_file,"The dump time was:\n%.12g\n", t);
                fprintf(dump_file,"The dump index was:\n%d\n", plot_count);
                fclose(dump_file);
            }
            if (master()) fprintf(stdout,"Too close to end of alloted computation time, dump!\n");  

        }

        void finalize(int plot_count, double final_time) {
             
            // Update the dump file with with a final time too large so
            // that restarting doesn't occur.
            if (master()){
                FILE * dump_file;
                dump_file = fopen("dump_time.txt","w");
                assert(dump_file);
                fprintf(dump_file,"The dump 'time' was:\n%.12g\n", 2*final_time);
                fprintf(dump_file,"The dump index was:\n%d\n", plot_count);
                fclose(dump_file);
            }
            for (int II = 0; II < Nz; II++) {
                delete[] yz_slice[II];
                delete[] yz_slice_red[II];
            }
            delete[] yz_slice;
            delete[] yz_slice_red;
        }

};

// Transform information
struct Transgeom {

    private:   
        // Blitz index placeholders
        blitz::firstIndex ii;
        blitz::secondIndex jj;
        blitz::thirdIndex kk;

    public:
        TArrayn::S_EXP type_y, type_y_bar, type_z;

        vector <CTArray *> qh, qh_bar_c;
        // K2 is actually the inverse laplacian
        // K2h is the forward horizontal laplacian
        vector <DTArray *> K2, K2h, K2_bar, qh_bar_r;

        GeneralArrayStorage<3> spec_ordering;
        TinyVector<int,3> spec_lbound, spec_extent;

        GeneralArrayStorage<3> mspec_ordering;
        TinyVector<int,3> mspec_lbound, mspec_extent;
        
        double norm_3d, norm_3d_bar;
        double norm_x, norm_y, norm_z;
        double f0, N0;

        bool is_channel;

        int Nx, Ny, Nz;
        double Lx, Ly, Lz;

        Trans1D *X_xform,
                *Y_xform;
        TransWrapper *XYZ_xform;

        Trans1D *Y_xform_bar;
        TransWrapper *XYZ_xform_bar;

        Array<double,1> *kvec;
        Array<double,1> *lvec;
        Array<double,1> *mvec;
        Array<double,1> *kvec_bar;
        Array<double,1> *lvec_bar;
        Array<double,1> *mvec_bar;

        void initialize(solnclass &soln, string const &ygrid_type,
                string const &zgrid_type) {

            Nx = soln.Nx;
            Ny = soln.Ny;
            Nz = soln.Nz;

            Lx = soln.Lx;
            Ly = soln.Ly;
            Lz = soln.Lz;

            f0 = soln.f0;
            N0 = soln.N0;

            norm_x = (2.*M_PI/Lx);

            if (ygrid_type == "PERIODIC") {
                type_y = FOURIER;
                type_y_bar = FOURIER;
                norm_y = 2.*M_PI/Ly;
            } else if (ygrid_type == "FREE_SLIP") {
                type_y = SINE;
                type_y_bar = COSINE;
                norm_y = M_PI/Ly;
            } else {
                if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
                MPI_Finalize(); exit(1);
            }

            if (zgrid_type == "FOURIER") {
                type_z = FOURIER;
                norm_z = (2.*M_PI/Lz);
            } else if (zgrid_type == "REAL") { 
                type_z = COSINE;
                norm_z = (1.*M_PI/Lz);
            } else if (zgrid_type == "CHEB") {
                type_z = NONE;
                norm_z = (2./Lz);
            } else if (zgrid_type == "NONE") {
                type_z = NONE;
                norm_z = 0.;
            } else {
                if (master()) fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
                MPI_Finalize(); exit(1);
            }

            norm_3d = (*XYZ_xform).norm_factor();

            spec_ordering.ordering() = (*XYZ_xform).get_complex_temp()->ordering();
            spec_lbound = (*XYZ_xform).get_complex_temp()->lbound();
            spec_extent = (*XYZ_xform).get_complex_temp()->extent();

            qh.resize(1);
            K2.resize(1);
            K2h.resize(1);

            qh[0] = new CTArray(spec_lbound,spec_extent,spec_ordering);
            K2[0] = new DTArray(spec_lbound,spec_extent,spec_ordering);
            K2h[0] = new DTArray(spec_lbound,spec_extent,spec_ordering);

            norm_3d_bar = (*XYZ_xform_bar).norm_factor();
            qh_bar_r.resize(1);
            qh_bar_c.resize(1);
            if (is_channel) {
                mspec_ordering.ordering() = (*XYZ_xform_bar).get_real_temp()->ordering();
                mspec_lbound = (*XYZ_xform_bar).get_real_temp()->lbound();
                mspec_extent = (*XYZ_xform_bar).get_real_temp()->extent();

                qh_bar_r[0] = new DTArray(mspec_lbound,mspec_extent,mspec_ordering);
            } else {
                mspec_ordering.ordering() = (*XYZ_xform_bar).get_complex_temp()->ordering();
                mspec_lbound = (*XYZ_xform_bar).get_complex_temp()->lbound();
                mspec_extent = (*XYZ_xform_bar).get_complex_temp()->extent();

                qh_bar_c[0] = new CTArray(mspec_lbound,mspec_extent,mspec_ordering);
            }

            K2_bar.resize(1);
            K2_bar[0] = new DTArray(mspec_lbound,mspec_extent,mspec_ordering);

            // Define wavenumbers
            if (Nz > 1) {
                *K2[0]     = -1.0/(pow((*kvec)(ii),2) + pow((*lvec)(kk),2)     + pow((*mvec)(jj),2)     + 1e-100)/norm_3d;
                *K2_bar[0] = -1.0/(                     pow((*lvec_bar)(kk),2) + pow((*mvec_bar)(jj),2) + 1e-100)/norm_3d_bar;
            } else {
                *K2[0]     = -1.0/(pow((*kvec)(ii),2) + pow((*lvec)(kk),2)     + 1.0/pow(soln.Lr,2.0))/norm_3d;
                *K2_bar[0] = -1.0/(                     pow((*lvec_bar)(kk),2) + 1.0/pow(soln.Lr,2.0))/norm_3d_bar;
            }
            *K2h[0] = -1.0*(pow((*kvec)(ii),2) + pow((*lvec)(kk),2) + 0*jj )/norm_3d;

            // Change wavenumber at (0,0,0) from Inf to zero: Assumes mean of zero
            if ( (Nz > 1) or (f0 == 0.)) {
                if (K2[0]->lbound(firstDim) == 0 && K2[0]->lbound(secondDim) == 0 && K2[0]->lbound(thirdDim) == 0 )
                {  (*K2[0])(0,0,0) = 0.0; }

                // Since each processor does its own inversion, we need to zero out each corresponding entry.
                (*K2_bar[0])(soln.rank,0,0) = 0.0;
            }
        }

};

struct fluxclass {

    private:
        TinyVector<int,3> local_lbound, local_extent;
        GeneralArrayStorage<3> local_storage;
        
        // Blitz index placeholders
        blitz::firstIndex ii;
        blitz::secondIndex jj;
        blitz::thirdIndex kk;

    public:
        vector<DTArray *> q;

        // Constructor method
        fluxclass(int Nx, int Ny, int Nz) {

            local_lbound = alloc_lbound(Nx,Nz,Ny);
            local_extent = alloc_extent(Nx,Nz,Ny);
            local_storage = alloc_storage(Nx,Nz,Ny);

            q.resize(3);

            q[0] = new DTArray(local_lbound,local_extent,local_storage);
            q[1] = new DTArray(local_lbound,local_extent,local_storage);
            q[2] = new DTArray(local_lbound,local_extent,local_storage);

            *q[0] = 0*ii + 0*jj + 0*kk;
            *q[1] = 0*ii + 0*jj + 0*kk;
            *q[2] = 0*ii + 0*jj + 0*kk;
        }

};



// Class to define flux function
struct methodclass {

    void (*flux)(solnclass & soln, DTArray & flux,
            Array<double,1> & ygrid, Transgeom & Sz); 

};

};

using namespace qg_functions;

// Declare some functions
void compute_dt(solnclass & soln);

void compute_weights2(solnclass & soln);

void compute_weights3(solnclass & soln);

void write_grids(DTArray & tmp, double Lx, double Ly, double Lz,
        int Nx, int Ny, int Nz, Transgeom & Sz);

void vertical_solve(CTArray & psihat, CTArray & qhat, DTArray & K2, solnclass & soln);

void compute_vels_and_psi(solnclass & soln, Transgeom & Sz);

void nonlinear(solnclass & soln, DTArray & flux, Array<double,1> & ygrid, Transgeom & Sz); 

void linear(solnclass & soln, DTArray & flux, Array<double,1> & ygrid, Transgeom & Sz); 

void nonlinear_pert(solnclass & soln, DTArray & flux, Array<double,1> & ygrid, Transgeom & Sz); 

void apply_filter(solnclass & soln, Transgeom & Sz);

void step_euler(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz);

void step_ab2(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz);

void step_ab3(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz);

void initialize_diagnostics_files();

#endif
