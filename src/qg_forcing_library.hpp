
#include "qg_functions.hpp"
#include "QG_Forcing_Library/null_forcing.hpp"
#include "QG_Forcing_Library/DEMO_linear_wall.hpp"
#include "QG_Forcing_Library/DEMO_zonal_launcher.hpp"
#include "QG_Forcing_Library/DEMO_zonal_jet.hpp"
#include "QG_Forcing_Library/DEMO_periodic_meridional_launcher.hpp"

#ifndef QG_FORCING_LIBRARY_HPP // Prevent double inclusion
#define QG_FORCING_LIBRARY_HPP 1

void assign_forcing_functions(solnclass & soln, string const &choice);

#endif
