/*
 *
 * Contains functions and class definitions for
 * the qg3D SPINS file.
 *
 */

#include "qg_functions.hpp"
#include "TArray.hpp"
#include "T_util.hpp"
#include "Par_util.hpp"
#include "Splits.hpp"
#include "gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace qg_functions;

// Compute psi and velocities
// Mainly only needed to output u.0, v.0, and psi.0
// when desired.
void compute_vels_and_psi(solnclass & soln, Transgeom & Sz)
{

    if (soln.debug and master()) { fprintf(stdout, "Entering compute_vels_and_psi\n"); }

    // Make nice references for arrays used
    DTArray &q = *soln.q[0], &psi = *soln.psi[0];
    DTArray &u = *soln.u[0], &v = *soln.v[0];
    DTArray &K2 = *Sz.K2[0];
    CTArray &qh = *Sz.qh[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &ubar = *soln.ubar[0];
    DTArray &K2_bar = *Sz.K2_bar[0];
    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    double &norm_3d = Sz.norm_3d,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Compute psi
    if (soln.debug and master()) { fprintf(stdout, "  compute_vels_and_psi -> streamfunction\n"); }
    XYZ_xform.forward_transform(&q,FOURIER,Sz.type_z,Sz.type_y); 
    if ((Sz.type_z != NONE) or (Sz.norm_z == 0)) {
        qh = *(XYZ_xform.get_complex_temp());
        qh = qh*K2;  
        *(XYZ_xform.get_complex_temp()) = qh;
    }
    else if (Sz.norm_z > 0.) {
        vertical_solve(*(XYZ_xform.get_complex_temp()), qh, K2, soln);
    }
    XYZ_xform.back_transform(&psi,FOURIER,Sz.type_z,Sz.type_y);

    // Geostrophic Velocity
    if (soln.debug and master()) { fprintf(stdout, "  compute_vels_and_psi -> geostrophic velocity\n"); }
    if (Sz.type_y == FOURIER)   {deriv_fft(psi,Y_xform,u);}
    else if (Sz.type_y == SINE) {deriv_dst(psi,Y_xform,u);}
    u *= -norm_y;                                                  // calc u
    deriv_fft(psi,X_xform,v);  v *=  norm_x;                       // calc v

    if (soln.use_zonal_decomp) {
        // Invert for psibar  (BAS: Not implemented for CHEB)
        XYZ_xform_bar.forward_transform(&qbar,NONE,Sz.type_z,Sz.type_y_bar); 
        if (XYZ_xform_bar.use_complex()) {
            qh_bar_c = *(XYZ_xform_bar.get_complex_temp());
            qh_bar_c = qh_bar_c*K2_bar;  
            *(XYZ_xform_bar.get_complex_temp()) = qh_bar_c;
        } else {
            qh_bar_r = *(XYZ_xform_bar.get_real_temp());
            qh_bar_r = qh_bar_r*K2_bar;  
            *(XYZ_xform_bar.get_real_temp()) = qh_bar_r;
        }
        XYZ_xform_bar.back_transform(&psibar,NONE,Sz.type_z,Sz.type_y_bar);

        // Compute ubar
        if (Sz.type_y_bar == FOURIER)   {deriv_fft(psibar,Y_xform_bar,ubar);}
        else if (Sz.type_y_bar == COSINE) {deriv_dct(psibar,Y_xform_bar,ubar);}
        ubar *= -norm_y;                                                  // calc ubar
    }

}


// Nonlinear Flux Method
void nonlinear(solnclass & soln, DTArray & flux, Transgeom & Sz)
{
    if (soln.debug and master()) { fprintf(stdout, "Entering nonlinear\n"); }

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],   &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],   &v = *soln.v[0];
    DTArray &qx = *soln.qx[0], &qy = *soln.qy[0];
    CTArray &qh = *Sz.qh[0];

    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &qbar_flux_0 = *soln.qbar_flux[0];
    DTArray &qbar_flux_1 = *soln.qbar_flux[1];
    DTArray &qbar_flux_2 = *soln.qbar_flux[2];

    DTArray &qbary = *soln.qbary[0], &ubar = *soln.ubar[0];
    DTArray &meanvq = *soln.meanvq[0];

    DTArray &qlaut = *soln.qlaut[0], &qlauty = *soln.qlauty[0];
    DTArray &ulaut = *soln.ulaut[0], &psilaut = *soln.psilaut[0];

    double &norm_3d = Sz.norm_3d,
           &norm_3d_bar = Sz.norm_3d_bar,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Gradient of PV
    if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> gradient of PV\n"); }
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        
    if (Sz.type_y == FOURIER)   {deriv_fft(q, Y_xform, qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q, Y_xform, qy);}
    qy *= norm_y;                                                 

    compute_vels_and_psi(soln, Sz);
    if (soln.use_zonal_decomp) { soln.compute_laut_fields(); }
    soln.compute_requested_derivatives();

    if (soln.use_zonal_decomp) {
        // Compute <vq>
        //    Each processor will have a copy of the information
        //    While technically this is redundant and requires more solves
        //    than strictly necessary, in order to have the parallelized
        //    declaration of the DTArrays and Transforms play nice,
        //    it seemed to be necessary.
        //
        //    <v(qhat+qbar)> = <vqhat> + <vqbar> = <vqhat> + <v>qbar = <vqhat>
        //    since <v> = 0
        if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> computing <vq>\n"); }
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                soln.yz_slice[JJ][KK] = 0.;
                for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
                    soln.yz_slice[JJ][KK] += v(II,JJ,KK)*q(II,JJ,KK)/soln.Nx;    // vq
                }
            }
        }
        soln.sum_reduce_yz_slice();
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                meanvq(soln.rank,JJ,KK) = soln.yz_slice_red[JJ][KK];   // < vq >
            }
        }

        if (Sz.type_y == FOURIER)   {deriv_fft(meanvq,Y_xform_bar,qbar_flux_0);}
        else if (Sz.type_y == SINE) {deriv_dst(meanvq,Y_xform_bar,qbar_flux_0);}
        qbar_flux_0 *= norm_y;                                                  // calc ddy< vq >

        // Account for forcing term in the bar field
        if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> bar forcing field\n"); }
        for (int JJ = 0; JJ < soln.Ny; JJ++) {
            for (int KK = 0; KK < soln.Nz; KK++) {
                qbar_flux_0(soln.rank, KK, JJ) += 
                    + soln.forcing_field_3(JJ, KK, soln)
                    - soln.forcing_field_4(JJ, KK, soln);
            }
        }

        // Update the circulation terms
        // BAS: Update to AB3
        if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> updating laut fields\n"); }
        for (int KK = 0; KK < soln.Nz; KK++) {
            soln.uS[KK] += soln.dt0*soln.duSdt(KK, soln);
            soln.uN[KK] += soln.dt0*soln.duNdt(KK, soln);
            soln.alaut[KK] += soln.dt0*soln.dalautdt(KK, soln);
            soln.blaut[KK] += soln.dt0*soln.dblautdt(KK, soln);
            soln.claut[KK] += soln.dt0*soln.dclautdt(KK, soln);
        }


        // Gradient of qbar
        if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> gradient of qbar\n"); }
        if (Sz.type_y == FOURIER)   {deriv_fft(qbar, Y_xform_bar, qbary);}
        else if (Sz.type_y_bar == COSINE) {deriv_dct(qbar, Y_xform_bar, qbary);}
        qbary *= norm_y;                                                  // calc qbar_y

        // Evolve qbar
        if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> evolve qbar\n"); }
        qbar = qbar - soln.w0*qbar_flux_0 - soln.w1*qbar_flux_1 - soln.w2*qbar_flux_2;
        qbar_flux_2 = qbar_flux_1;
        qbar_flux_1 = qbar_flux_0;
    }


    // Compute flux
    if (soln.debug and master()) { fprintf(stdout, "   nonlinear -> computing flux\n"); }
    for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                if (soln.use_zonal_decomp) {
                    flux(II,JJ,KK) = 
                        (u(II,JJ,KK) + ubar(soln.rank,JJ,KK) + ulaut(soln.rank,JJ,KK))*qx(II,JJ,KK)     // u*q_x
                        + v(II,JJ,KK)*(qy(II,JJ,KK) + qbary(soln.rank,JJ,KK) + qlauty(soln.rank,JJ,KK) + soln.beta) // v*(q_y + beta)
                        - (qbar_flux_0(soln.rank,JJ,KK) + soln.forcing_field_4(KK, JJ, soln)  )// < v*q >, one forcing component
                        - soln.forcing_field_1(II, KK, JJ, soln) // forcing components for the hat fields
                        + soln.forcing_field_2(II, KK, JJ, soln);
                } else {
                    flux(II,JJ,KK) = 
                        u(II,JJ,KK)*qx(II,JJ,KK)     // u*q_x
                        + v(II,JJ,KK)*(qy(II,JJ,KK) + soln.beta) // v*(q_y + beta)
                        - soln.forcing_field_1(II, KK, JJ, soln)  // these forcing terms 
                        + soln.forcing_field_2(II, KK, JJ, soln); // influence the full field
                }
            }
        }
    }
};


// Linear Flux Method
void linear(solnclass & soln, DTArray & flux, Transgeom & Sz)
{
    if (soln.debug and master()) { fprintf(stdout, "Entering linear\n"); }

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],     &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],     &v = *soln.v[0];
    DTArray &qx = *soln.qx[0],   &qy = *soln.qy[0];
    DTArray &ub = *soln.ub[0],   &vb = *soln.vb[0];
    DTArray &qb = *soln.qb[0];
    DTArray &qxb = *soln.qxb[0], &qyb = *soln.qyb[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &qbar_flux_0 = *soln.qbar_flux[0];
    DTArray &qbar_flux_1 = *soln.qbar_flux[1];
    DTArray &qbar_flux_2 = *soln.qbar_flux[2];

    DTArray &qbary = *soln.qbary[0], &ubar = *soln.ubar[0];
    DTArray &meanvq = *soln.meanvq[0];

    double &norm_3d = Sz.norm_3d,
           &norm_3d_bar = Sz.norm_3d_bar,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Gradient of PV
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        // calc q_x
    if (Sz.type_y == FOURIER)   {deriv_fft(q,Y_xform,qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q,Y_xform,qy);}
    qy *= norm_y;                                                  // calc q_y

    compute_vels_and_psi(soln, Sz);
    soln.compute_requested_derivatives();

    if (soln.use_zonal_decomp) {
        // Compute <vq>
        //    Each processor will have a copy of the information
        //    While technically this is redundant and requires more solves
        //    than strictly necessary, in order to have the parallelized
        //    declaration of the DTArrays and Transforms play nice,
        //    it seemed to be necessary.
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                soln.yz_slice[JJ][KK] = 0.;
                for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
                    soln.yz_slice[JJ][KK] += (v(II,JJ,KK)*qb(II,JJ,KK) + vb(II,JJ,KK)*q(II,JJ,KK))/soln.Nx;    // vq
                }
            }
        }
        soln.sum_reduce_yz_slice();
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                meanvq(soln.rank,JJ,KK) = soln.yz_slice_red[JJ][KK];   // < vq >
            }
        }

        if (Sz.type_y == FOURIER)   {deriv_fft(meanvq,Y_xform_bar,qbar_flux_0);}
        else if (Sz.type_y == SINE) {deriv_dst(meanvq,Y_xform_bar,qbar_flux_0);}
        qbar_flux_0 *= norm_y;                                                  // calc ddy< vq >

        // Gradient of qbar
        if (Sz.type_y == FOURIER)   {deriv_fft(qbar, Y_xform_bar, qbary);}
        else if (Sz.type_y_bar == COSINE) {deriv_dct(qbar, Y_xform_bar, qbary);}
        qbary *= norm_y;                                                  // calc qbar_y

        // Evolve qbar
        qbar = qbar - soln.w0*qbar_flux_0 - soln.w1*qbar_flux_1 - soln.w2*qbar_flux_2;
        qbar_flux_2 = qbar_flux_1;
        qbar_flux_1 = qbar_flux_0;
    }

    // Compute flux
    for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                if (soln.use_zonal_decomp) {
                    flux(II,JJ,KK) = 
                        ub(II,JJ,KK)*qx(II,JJ,KK) 
                        + vb(II,JJ,KK)*(qy(II,JJ,KK) + qbary(soln.rank,JJ,KK))
                        + (u(II,JJ,KK) + ubar(soln.rank,JJ,KK))*qxb(II,JJ,KK)
                        + v(II,JJ,KK)*(qyb(II,JJ,KK) + soln.beta)
                        - qbar_flux_0(soln.rank,JJ,KK);
                } else {
                    flux(II,JJ,KK) = 
                        ub(II,JJ,KK)*qx(II,JJ,KK) + vb(II,JJ,KK)*qy(II,JJ,KK)
                        + u(II,JJ,KK)*qxb(II,JJ,KK) + v(II,JJ,KK)*(qyb(II,JJ,KK) + soln.beta);
                }
            }
        }
    }

};

// Nonlinear Perturbation Flux Method
void nonlinear_pert(solnclass & soln, DTArray & flux, Transgeom & Sz)
{
    if (soln.debug and master()) { fprintf(stdout, "Entering nonlinear_pert\n"); }

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],     &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],     &v = *soln.v[0];
    DTArray &qx = *soln.qx[0],   &qy = *soln.qy[0];
    DTArray &UB = *soln.ub[0],   &QBY = *soln.qyb[0];

    DTArray &K2 = *Sz.K2[0],    &K2h = *Sz.K2h[0];
    CTArray &qh = *Sz.qh[0];

    DTArray &K2_bar = *Sz.K2_bar[0];
    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &qbar_flux_0 = *soln.qbar_flux[0];
    DTArray &qbar_flux_1 = *soln.qbar_flux[1];
    DTArray &qbar_flux_2 = *soln.qbar_flux[2];

    DTArray &qbary = *soln.qbary[0], &ubar = *soln.ubar[0];
    DTArray &meanvq = *soln.meanvq[0];

    double &norm_3d = Sz.norm_3d,
           &norm_3d_bar = Sz.norm_3d_bar,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Gradient of PV
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        // calc q_x
    if (Sz.type_y == FOURIER)   {deriv_fft(q,Y_xform,qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q,Y_xform,qy);}
    qy *= norm_y;                                                  // calc q_y

    compute_vels_and_psi(soln, Sz);
    soln.compute_requested_derivatives();

    if (soln.use_zonal_decomp) {
        // Compute <vq>
        //    Each processor will have a copy of the information
        //    While technically this is redundant and requires more solves
        //    than strictly necessary, in order to have the parallelized
        //    declaration of the DTArrays and Transforms play nice,
        //    it seemed to be necessary.
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                soln.yz_slice[JJ][KK] = 0.;
                for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
                    soln.yz_slice[JJ][KK] += v(II,JJ,KK)*q(II,JJ,KK)/soln.Nx;
                }
            }
        }
        soln.sum_reduce_yz_slice();
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                meanvq(soln.rank,JJ,KK) = soln.yz_slice_red[JJ][KK];
            }
        }

        if (Sz.type_y == FOURIER)   {deriv_fft(meanvq,Y_xform_bar,qbar_flux_0);}
        else if (Sz.type_y == SINE) {deriv_dst(meanvq,Y_xform_bar,qbar_flux_0);}
        qbar_flux_0 *= norm_y;                                                  // calc ddy< vq >

        // Gradient of qbar
        if (Sz.type_y == FOURIER)   {deriv_fft(qbar, Y_xform_bar, qbary);}
        else if (Sz.type_y_bar == COSINE) {deriv_dct(qbar, Y_xform_bar, qbary);}
        qbary *= norm_y;                                                  // calc qbar_y

        // Evolve qbar
        qbar = qbar - soln.w0*qbar_flux_0 - soln.w1*qbar_flux_1 - soln.w2*qbar_flux_2;
        qbar_flux_2 = qbar_flux_1;
        qbar_flux_1 = qbar_flux_0;
    }

    // Compute flux
    flux = u*qx + UB*qx + v*(qy + QBY + soln.beta);
    if (soln.use_zonal_decomp) {
        for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
            for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
                for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                    flux(II,JJ,KK) = flux(II,JJ,KK) 
                        + ubar(soln.rank,JJ,KK)*qx(II,JJ,KK) 
                        + v(II,JJ,KK)*qbary(soln.rank,JJ,KK)
                        - qbar_flux_0(soln.rank,JJ,KK);
                }
            }
        }
    }

}

// Apply the CFL condition to compute dt
void compute_dt(solnclass & soln) {  
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_dt\n"); }

    // Compute restrictions
    double tmp1, tmp2;
    double max_u, max_v;
    double dt;

    tmp1 =  pvmax(*soln.u[0]);
    tmp2 = -pvmin(*soln.u[0]);
    max_u = max(tmp1,tmp2) + soln.U0;

    if (soln.use_zonal_decomp) {
        tmp1 =  pvmax(*soln.ubar[0]);
        tmp2 = -pvmin(*soln.ubar[0]);
        max_u += max(tmp1, tmp2);

        soln.compute_laut_fields();
        tmp1 =  pvmax(*soln.ulaut[0]);
        tmp2 = -pvmin(*soln.ulaut[0]);
        max_u += max(tmp1, tmp2);
    }

    tmp1 =  pvmax(*soln.v[0]);
    tmp2 = -pvmin(*soln.v[0]);
    max_v = max(tmp1, tmp2) + soln.V0;

    dt = min(soln.dx/max_u, soln.dy/max_v)/5.;

    if (dt < 1.0) { // BAS: Why 1?
        if (master()) {
            fprintf(stderr, "Time step is %g seconds, too small to continue. Force exit!\n",dt);
            fprintf(stderr, "max_u = %.12g, max_v = %.12g\n", max_u, max_v);
        }
        MPI_Finalize(); exit(1);
    }
    else if(dt > 15000.0) { // BAS: Why 15000?
        dt = 15000.0;
    }

    if (dt + soln.t >= soln.next_plot_time) {
        dt  = soln.next_plot_time - soln.t;
        soln.do_plot = true;
    }

    soln.dt2 = soln.dt1;
    soln.dt1 = soln.dt0;
    soln.dt0 = dt;

    return;
}

void apply_filter(solnclass & soln,Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering apply_filter\n"); }

    DTArray &q    = *soln.q[0],
            &qbar = *soln.qbar[0];

    TransWrapper &XYZ_xform = *Sz.XYZ_xform,
                 &XYZ_xform_bar = *Sz.XYZ_xform_bar; 

    double &fcut = soln.fcutoff,
           &ford = soln.forder,
           &fstr = soln.fstrength;

    // Filter q and qbar separately.
    // Since filtering is a linear operator, this is okay
    filter3(q, XYZ_xform, FOURIER, Sz.type_z, Sz.type_y, fcut, ford, fstr); 
    if (soln.use_zonal_decomp) {
        filter_bar(qbar,XYZ_xform_bar, Sz.type_z, Sz.type_y, fcut, ford, fstr); 
    }

    // Now that we've filtered q, compute the filtered u, v, and psi fields.
    compute_vels_and_psi(soln, Sz);

}

// Apply the Euler step
void step_euler(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_euler\n"); }

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[0];
    DTArray &qbar = *soln.qbar[0];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute new timestep, decrease for euler
    compute_dt(soln);
    soln.dt0 *= 0.1;
    soln.w0 = soln.dt0;
    soln.w1 = 0;
    soln.w2 = 0;

    // Compute flux
    method.flux(soln,fluxq,Sz);

    // Evolve the forcing, if needed
    //if (soln.do_forcing) {
    //    method.force_evolve(soln, Sz);
    //}

    // Advance solution
    q = q - soln.dt0*fluxq;
    //if (soln.do_forcing) {
    //    q    = q    - pow(soln.dt0, 0.5)*(*soln.forcing[0]);
    //    if (soln.use_zonal_decomp) {
    //        qbar = qbar - pow(soln.dt0, 0.5)*(*soln.forcing_bar[0]);
    //    }
    //}

    // Apply exponential filter
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[1] = fluxq; 

}

// Compute the weights for the AB2 method
void compute_weights2(solnclass & soln) {
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_weights2\n"); }

    double a,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn  = soln.t;

    a  = soln.t - soln.dt1;
    ts = soln.t;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w0 = w;

    a  = soln.t;
    ts = soln.t - soln.dt1;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w1 = w;

    soln.w2 = 0;

    return;

}

// Apply the AB2 step
void step_ab2(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_ab2\n"); }

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[1];
    DTArray &qbar = *soln.qbar[0];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute new timestep, decrease for ab2
    compute_dt(soln);
    soln.dt0 *= 0.1;
    compute_weights2(soln);

    // Compute flux
    method.flux(soln,fluxq,Sz);

    // Evolve the forcing, if needed
    //if (soln.do_forcing) {
    //    method.force_evolve(soln, Sz);
    //}

    // Advance solution
    q = q - soln.w0*fluxq - soln.w1*(*flux.q[1]);
    //if (soln.do_forcing) {
    //    q    = q    - pow(soln.dt0, 0.5)*(*soln.forcing[0]);
    //    if (soln.use_zonal_decomp) {
    //        qbar = qbar - pow(soln.dt0, 0.5)*(*soln.forcing_bar[0]);
    //    }
    //}

    // Exponential Filter 
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[0] = *flux.q[1];
    *flux.q[1] = fluxq; 

}

// Compute the weights for the AB3 method
void compute_weights3(solnclass & soln) {
    if (soln.debug and master()) { fprintf(stdout, "Entering compute_weights3\n"); }

    double a,b,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn = soln.t;

    a  = soln.t - soln.dt1;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w0 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t - soln.dt1;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w1 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1;
    ts = soln.t - soln.dt1 - soln.dt2;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w2 = w;


    return;

}


// Apply the AB3 step
void step_ab3(solnclass & soln, fluxclass & flux, methodclass & method, Transgeom & Sz) {
    if (soln.debug and master()) { fprintf(stdout, "Entering step_ab3\n"); }

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[2];
    DTArray &qbar = *soln.qbar[0];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute new timestep, full dt for ab3
    compute_dt(soln);
    compute_weights3(soln);

    // Compute flux
    method.flux(soln,fluxq,Sz);

    // Evolve the forcing, if needed
    //if (soln.do_forcing) {
    //    method.force_evolve(soln, Sz);
    //}

    // Advance solution
    q = q - soln.w0*fluxq - soln.w1*(*flux.q[1]) - soln.w2*(*flux.q[0]);
    //if (soln.do_forcing) {
    //    q    = q    - pow(soln.dt0, 0.5)*(*soln.forcing[0]);
    //    if (soln.use_zonal_decomp) {
    //        qbar = qbar - pow(soln.dt0, 0.5)*(*soln.forcing_bar[0]);
    //    }
    //}

    // Exponential Filter
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[0] = *flux.q[1];
    *flux.q[1] = fluxq; 
}

// Write the grids to the corresponding files. Assumes uniform grids.
void write_grids(DTArray & tmp, double Lx, double Ly, double Lz,
        int Nx, int Ny, int Nz, Transgeom & Sz) {

    bool debug = false;
    if (debug and master()) { fprintf(stdout, "Entering write_grids\n"); }

    // Blitz index placeholders
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    if (Nx > 1) {
        tmp = (ii + 0.5)/Nx*Lx + 0*jj + 0*kk - Lx/2;
        write_array_qg(tmp,"xgrid");
    }

    if (Ny > 1) {
        tmp = 0*ii + (kk + 0.5)/Ny*Ly + 0*jj - Ly/2;
        write_array_qg(tmp,"ygrid");
    }

    if (Nz > 1) {
        if (Sz.type_z == NONE) {
            tmp = 0*ii + 0*kk + Lz*(0.5 + 0.5*cos(jj*M_PI/(Nz-1))) - Lz;
        } else {
            tmp = 0*ii + 0*kk + (jj + 0.5)/Nz*Lz - Lz;
        }
        write_array_qg(tmp,"zgrid");
    }
}

void vertical_solve(CTArray & psihat, CTArray & qhat, DTArray & K2, solnclass & soln) {
    if (soln.debug and master()) { fprintf(stdout, "Entering vertical_solve (SHOULD NOT HAPPEN!!)\n"); }
    // Solves the Nx*Ny vertical problems for the inversion of
    // the Laplacian-of-q to give the streamfunction

    // The root mathematical problem being solved here is:

    // q = (Psi_xx + Psi_yy) + (f0^2/N^2) Psi_zz, or in Fourier space:

    // qhat = (-k^2 - l^2) psihat + (f0^2/N^2) psihat_zz

    // This gives a number of 1D Helmholtz-type problems, which we can solve with
    // poisson_1d in src/gmres_1d_solver.[ch]pp

    // This proceeds through 1D arrays, so allocate those
    blitz::Array<double,1> psihat1d(soln.Nz), rhs(soln.Nz);

    // We can rearrange the previous form for a "clean" second-derivative
    // by multiplying by N^2/f0^2, giving:

    // (N^2/f^2) (-k^2 - l^2) psihat + psihat_zz = (N^2/f^2) qhat

    // so in terms of parameters for poisson_1d, borrowing matlab-style notation:

    // rhs = resid = (N^2/f^2) qhat(i,:,j)
    // length = Lz
    // helmholtz = (-)N^2/f^2*(-k^2 - l^2) = -N^2/(f^2*K2(i,0,j))
    //             (poisson_1d expects a positive parameter for a negative term,
    //              on the idea that negative constant parameters here are well-
    //              defined.)
    // a_top = a_bot = 0 // Dirichlet portion of boundary conditions
    // b_top = b_bot = 1 // Neumann portion of boundary conditions

    // And finally, to respect the boundary conditions the first and last
    // entries of the residual/rhs must be replaced with 0.

    double norm = soln.N0*soln.N0/(soln.f0*soln.f0); // To keep this ratio handy

    // Range for assignment between 1D and 3D arrays
    blitz::Range all = blitz::Range::all();

    // BAS: Changed order for 2nd and 3rd dimensions

    for (int iii = qhat.lbound(firstDim); iii <= qhat.ubound(firstDim); iii++) {
        for (int jjj = qhat.lbound(thirdDim); jjj <= qhat.ubound(thirdDim); jjj++) {
            // The input and output arrays are complex, but poisson_1d works
            // on real arrays; we need to separate these out.

            rhs = norm*real(qhat(iii,all,jjj)); // Grab the real part
            rhs(0) = 0; // Assign boundary conditions
            rhs(soln.Nz-1) = 0;

            int iter_count = poisson_1d(rhs,psihat1d,
                    soln.Lz, // Vertical grid length
                    -norm/K2(iii,0,jjj), // Helmholtz parameter
                    0.0, 0.0, 1.0, 1.0 // Boundary conditions
                    );

            if (iter_count <= 0) { // The solve failed for some reason
                fprintf(stderr,"Vertical solve(%d,%d) real (constant %g) failed, returning %d\n",iii,jjj,-norm/K2(iii,0,jjj),iter_count);
                assert(0);
            }

            real(psihat(iii,all,jjj)) = psihat1d;

            // Repeat for the imaginary part
            rhs = norm*imag(qhat(iii,all,jjj));
            rhs(0) = 0; rhs(soln.Nz-1) = 0;
            iter_count = poisson_1d(rhs,psihat1d,soln.Lz,-norm/K2(iii,0,jjj),0,0,1.0,1.0);
            if (iter_count <= 0) { // The solve failed for some reason
                fprintf(stderr,"Vertical solve(%d,%d) imag (constant %g) failed, returning %d\n",iii,jjj,-norm/K2(iii,0,jjj),iter_count);
                assert(0);
            }



        }
    }
}


void filter_bar(DTArray & source, TransWrapper & tform, 
        S_EXP dim2_type, S_EXP dim3_type,
        double cutoff, double order, double strength) {

    bool debug = false;
    if (debug and master()) { fprintf(stdout, "Entering filter_bar\n"); }

    if (strength == 0) return;

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    /* Transform */
    tform.forward_transform(&source, NONE, dim2_type, dim3_type);

    /* Get wavenumbers and find their maximums */
    Array<double,1> lvec = tform.wavenums(secondDim),
        mvec = tform.wavenums(thirdDim);
    double lmax = tform.max_wavenum(secondDim),
           mmax = tform.max_wavenum(thirdDim);

    if (lmax == 0) lmax = 1;
    if (mmax == 0) mmax = 1;

    double lcut = cutoff*lmax, mcut = cutoff*mmax;
    double norm = tform.norm_factor();
    double strength_y = strength, strength_z = strength;

    lvec = where(abs(lvec) <= cutoff*lmax, 1, 
            exp(-strength*pow((abs(lvec)-lcut)/(lmax-lcut), order)));
    mvec = where(abs(mvec) <= cutoff*mmax, 1, 
            exp(-strength*pow((abs(mvec)-mcut)/(mmax-mcut), order)));

    // Multiply the proper temporary array by the transformation coefficients
    if (tform.use_complex()) {
        CTArray & temp = *(tform.get_complex_temp());
        temp = temp * lvec(jj) * mvec(kk) / norm;
    } else {
        DTArray & temp = *(tform.get_real_temp());
        temp = temp * lvec(jj) * mvec(kk) / norm;
    }

    // Transform back to overwrite source.
    tform.back_transform(&source, NONE, dim2_type, dim3_type);
}
