#include "qg_forcing_library.hpp"

void assign_forcing_functions(solnclass & soln, string const &choice) {

    if (choice == "DEMO_linear_wall") {
        soln.forcing_field_1 = &(linear_wall::forcing_field_1);
        soln.forcing_field_2 = &(linear_wall::forcing_field_2);
        soln.forcing_field_3 = &(linear_wall::forcing_field_3);
        soln.forcing_field_4 = &(linear_wall::forcing_field_4);
        soln.duSdt           = &(linear_wall::duSdt);
        soln.duNdt           = &(linear_wall::duNdt);
        soln.dalautdt        = &(linear_wall::dalautdt);
        soln.dblautdt        = &(linear_wall::dblautdt);
        soln.dclautdt        = &(linear_wall::dclautdt);

        // This case uses Rayleigh damping, so we need velocity gradients
        soln.compute_u_derivs[soln.Y_IND] = true;
    }
    else if (choice == "DEMO_zonal_launcher") {
        soln.forcing_field_1 = &(zonal_launcher::forcing_field_1);
        soln.forcing_field_2 = &(zonal_launcher::forcing_field_2);
        soln.forcing_field_3 = &(zonal_launcher::forcing_field_3);
        soln.forcing_field_4 = &(zonal_launcher::forcing_field_4);
        soln.duSdt           = &(zonal_launcher::duSdt);
        soln.duNdt           = &(zonal_launcher::duNdt);
        soln.dalautdt        = &(zonal_launcher::dalautdt);
        soln.dblautdt        = &(zonal_launcher::dblautdt);
        soln.dclautdt        = &(zonal_launcher::dclautdt);

        // This case uses Rayleigh damping, so we need velocity gradients
        soln.compute_u_derivs[soln.Y_IND] = true;
    }
    else if (choice == "DEMO_zonal_jet") {
        soln.forcing_field_1 = &(zonal_jet::forcing_field_1);
        soln.forcing_field_2 = &(zonal_jet::forcing_field_2);
        soln.forcing_field_3 = &(zonal_jet::forcing_field_3);
        soln.forcing_field_4 = &(zonal_jet::forcing_field_4);
        soln.duSdt           = &(zonal_jet::duSdt);
        soln.duNdt           = &(zonal_jet::duNdt);
        soln.dalautdt        = &(zonal_jet::dalautdt);
        soln.dblautdt        = &(zonal_jet::dblautdt);
        soln.dclautdt        = &(zonal_jet::dclautdt);

        // This case uses Rayleigh damping, so we need velocity gradients
        soln.compute_u_derivs[soln.Y_IND] = true;
    }
    else if (choice == "DEMO_periodic_meridional_launcher") {
        soln.forcing_field_1 = &(periodic_meridional_launcher::forcing_field_1);
        soln.forcing_field_2 = &(periodic_meridional_launcher::forcing_field_2);
        soln.forcing_field_3 = &(periodic_meridional_launcher::forcing_field_3);
        soln.forcing_field_4 = &(periodic_meridional_launcher::forcing_field_4);
        soln.duSdt           = &(periodic_meridional_launcher::duSdt);
        soln.duNdt           = &(periodic_meridional_launcher::duNdt);
        soln.dalautdt        = &(periodic_meridional_launcher::dalautdt);
        soln.dblautdt        = &(periodic_meridional_launcher::dblautdt);
        soln.dclautdt        = &(periodic_meridional_launcher::dclautdt);
    } else {
        if (master()) { fprintf(stdout, "Note: Unforced simulation.\n"); }
        soln.forcing_field_1 = &(null_forcing::forcing_field_1);
        soln.forcing_field_2 = &(null_forcing::forcing_field_2);
        soln.forcing_field_3 = &(null_forcing::forcing_field_3);
        soln.forcing_field_4 = &(null_forcing::forcing_field_4);
        soln.duSdt           = &(null_forcing::duSdt);
        soln.duNdt           = &(null_forcing::duNdt);
        soln.dalautdt        = &(null_forcing::dalautdt);
        soln.dblautdt        = &(null_forcing::dblautdt);
        soln.dclautdt        = &(null_forcing::dclautdt);
    }

}
