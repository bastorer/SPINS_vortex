#include "../BaseCase.hpp"
#include "../NSIntegrator.hpp"
#include "../TArray.hpp"
#include <blitz/array.h>

using namespace NSIntegrator;
using blitz::Array;
using std::vector;

/* Parse the Chain/Slice Inputs */
void BaseCase::specify_chains_and_slices(int * num_chains, double *** chain_coords,
       int * num_slices, double ** slice_coords,
       double ** c_data_buf, 
       vector<double> x_chain_y_coords, vector<double> x_chain_z_coords,
       vector<double> y_chain_x_coords, vector<double> y_chain_z_coords,
       vector<double> z_chain_x_coords, vector<double> z_chain_y_coords,
       vector<double> xy_slice_z_coords,
       vector<double> yz_slice_x_coords,
       vector<double> xz_slice_y_coords,
       DTArray & u) {

    num_chains[x_ind()] = x_chain_y_coords.size(); // Number of x-chains
    num_chains[y_ind()] = y_chain_x_coords.size(); // Number of y-chains
    num_chains[z_ind()] = z_chain_x_coords.size(); // Number of z-chains

    c_data_buf[x_ind()] = new double[u.extent(firstDim)];
    c_data_buf[y_ind()] = new double[u.extent(secondDim)];
    c_data_buf[z_ind()] = new double[u.extent(thirdDim)];

    chain_coords[x_ind()] = new double*[3];
    chain_coords[y_ind()] = new double*[3];
    chain_coords[z_ind()] = new double*[3];
    chain_coords[x_ind()][y_ind()] = new double[num_chains[x_ind()]]; // y coord for x-chains
    chain_coords[x_ind()][z_ind()] = new double[num_chains[x_ind()]]; // z coord for x-chains
    chain_coords[y_ind()][x_ind()] = new double[num_chains[y_ind()]]; // x coord for y-chains
    chain_coords[y_ind()][z_ind()] = new double[num_chains[y_ind()]]; // z coord for y-chains
    chain_coords[z_ind()][x_ind()] = new double[num_chains[z_ind()]]; // x coord for z-chains
    chain_coords[z_ind()][y_ind()] = new double[num_chains[z_ind()]]; // y coord for z-chains

    for (int II = 0; II < num_chains[x_ind()]; II++) {
        chain_coords[x_ind()][y_ind()][II] = x_chain_y_coords[II];
        chain_coords[x_ind()][z_ind()][II] = x_chain_z_coords[II];
    }

    for (int II = 0; II < num_chains[y_ind()]; II++) {
        chain_coords[y_ind()][x_ind()][II] = y_chain_x_coords[II];
        chain_coords[y_ind()][z_ind()][II] = y_chain_z_coords[II];
    }

    for (int II = 0; II < num_chains[z_ind()]; II++) {
        chain_coords[z_ind()][x_ind()][II] = z_chain_x_coords[II];
        chain_coords[z_ind()][y_ind()][II] = z_chain_y_coords[II];
    }

    num_slices[x_ind()] = yz_slice_x_coords.size(); // Number of yz-slices
    num_slices[y_ind()] = xz_slice_y_coords.size(); // Number of xz-slices
    num_slices[z_ind()] = xy_slice_z_coords.size(); // Number of xy-slices

    slice_coords[x_ind()] = new double[num_slices[x_ind()]]; // x coord for yz-slices
    slice_coords[y_ind()] = new double[num_slices[y_ind()]]; // y coord for yz-slices
    slice_coords[z_ind()] = new double[num_slices[z_ind()]]; // z coord for xy-slices

    for (int II = 0; II < num_slices[x_ind()]; II++) {
        slice_coords[x_ind()][II] = yz_slice_x_coords[II];
    }

    for (int II = 0; II < num_slices[y_ind()]; II++) {
        slice_coords[y_ind()][II] = xz_slice_y_coords[II];
    }

    for (int II = 0; II < num_slices[z_ind()]; II++) {
        slice_coords[z_ind()][II] = xy_slice_z_coords[II];
    }
}

