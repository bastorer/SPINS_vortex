/*
 *
 * Contains functions and class definitions for
 * the qg3D SPINS file.
 *
 */

#include "qg_functions_netcdf.hpp"
#include "TArray.hpp"
#include "T_util.hpp"
#include "Par_util.hpp"
#include "Splits.hpp"
#include "gmres_1d_solver.hpp"
#include <blitz/array.h>
#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

using namespace qg_functions;

// Compute psi and velocities
// Mainly only needed to output u.0, v.0, and psi.0
// when desired.
// BAS: Could also be used to tidy up the flux functions
void compute_vels_and_psi(solnclass & soln, Transgeom & Sz)
{
    // Make nice references for arrays used
    DTArray &q = *soln.q[0], &psi = *soln.psi[0];
    DTArray &u = *soln.u[0], &v = *soln.v[0];
    DTArray &K2 = *Sz.K2[0];
    CTArray &qh = *Sz.qh[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &ubar = *soln.ubar[0];
    DTArray &K2_bar = *Sz.K2_bar[0];
    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    double &norm_3d = Sz.norm_3d,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Compute psi
    XYZ_xform.forward_transform(&q,FOURIER,Sz.type_z,Sz.type_y); 
    if ((Sz.type_z != NONE) or (Sz.norm_z == 0)) {
        qh = *(XYZ_xform.get_complex_temp());
        qh = qh*K2;  
        *(XYZ_xform.get_complex_temp()) = qh;
    }
    else if (Sz.norm_z > 0.) {
        vertical_solve(*(XYZ_xform.get_complex_temp()), qh, K2, soln);
    }
    XYZ_xform.back_transform(&psi,FOURIER,Sz.type_z,Sz.type_y);

    // Geostrophic Velocity
    if (Sz.type_y == FOURIER)   {deriv_fft(psi,Y_xform,u);}
    else if (Sz.type_y == SINE) {deriv_dst(psi,Y_xform,u);}
    u *= -norm_y;                                                  // calc u
    deriv_fft(psi,X_xform,v);  v *=  norm_x;                       // calc v

    // Invert for psibar
    // BAS: Not implemented for CHEB
    XYZ_xform_bar.forward_transform(&qbar,NONE,Sz.type_z,Sz.type_y_bar); 
    if (Sz.is_channel) {
        qh_bar_r = *(XYZ_xform_bar.get_real_temp());
        qh_bar_r = qh_bar_r*K2_bar;  
        *(XYZ_xform_bar.get_real_temp()) = qh_bar_r;
    } else {
        qh_bar_c = *(XYZ_xform_bar.get_complex_temp());
        qh_bar_c = qh_bar_c*K2_bar;  
        *(XYZ_xform_bar.get_complex_temp()) = qh_bar_c;
    }
    XYZ_xform_bar.back_transform(&psibar,NONE,Sz.type_z,Sz.type_y_bar);

    // Compute ubar
    if (Sz.type_y == FOURIER)   {deriv_fft(psibar,Y_xform_bar,ubar);}
    else if (Sz.type_y_bar == COSINE) {deriv_dct(psibar,Y_xform_bar,ubar);}
    ubar *= -norm_y;                                                  // calc ubar

}


// Nonlinear Flux Method
void nonlinear(solnclass & soln, DTArray & flux,
        Array<double,1> & ygrid, Transgeom & Sz)
{

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],   &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],   &v = *soln.v[0];
    DTArray &qx = *soln.qx[0], &qy = *soln.qy[0];
    DTArray &visc = *soln.work[0];
    DTArray &K2 = *Sz.K2[0],   &K2h = *Sz.K2h[0];
    CTArray &qh = *Sz.qh[0];
 
    DTArray &K2_bar = *Sz.K2_bar[0];
    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &qbar_flux_0 = *soln.qbar_flux[0];
    DTArray &qbar_flux_1 = *soln.qbar_flux[1];
    DTArray &qbar_flux_2 = *soln.qbar_flux[2];

    DTArray &qbary = *soln.qbary[0], &ubar = *soln.ubar[0];
    DTArray &meanvq = *soln.meanvq[0];

    double &norm_3d = Sz.norm_3d,
           &norm_3d_bar = Sz.norm_3d_bar,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Gradient of PV
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        
    if (Sz.type_y == FOURIER)   {deriv_fft(q, Y_xform, qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q, Y_xform, qy);}
    qy *= norm_y;                                                 

    compute_vels_and_psi(soln, Sz);

    // Compute <vq>
    //    Each processor will have a copy of the information
    //    While technically this is redundant and requires more solves
    //    than strictly necessary, in order to have the parallelized
    //    declaration of the DTArrays and Transforms play nice,
    //    it seemed to be necessary.
    for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
        for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
            soln.yz_slice[JJ][KK] = 0.;
            for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
                soln.yz_slice[JJ][KK] += v(II,JJ,KK)*(q(II,JJ,KK) + qbar(soln.rank,JJ,KK))/soln.Nx;
            }
        }
    }
    soln.sum_reduce_yz_slice();
    for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
        for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
            meanvq(soln.rank,JJ,KK) = soln.yz_slice_red[JJ][KK];
        }
    }

    if (Sz.type_y == FOURIER)   {deriv_fft(meanvq,Y_xform_bar,qbar_flux_0);}
    else if (Sz.type_y == SINE) {deriv_dst(meanvq,Y_xform_bar,qbar_flux_0);}
    qbar_flux_0 *= norm_y;                                                  // calc ddy< vq >

    // Gradient of qbar
    if (Sz.type_y == FOURIER)   {deriv_fft(qbar, Y_xform_bar, qbary);}
    else if (Sz.type_y_bar == COSINE) {deriv_dct(qbar, Y_xform_bar, qbary);}
    qbary *= norm_y;                                                  // calc qbar_y
    
    // Evolve qbar
    qbar = qbar - soln.w0*qbar_flux_0 - soln.w1*qbar_flux_1 - soln.w2*qbar_flux_2;
    qbar_flux_2 = qbar_flux_1;
    qbar_flux_1 = qbar_flux_0;

    // Compute flux
    for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                flux(II,JJ,KK) = 
                   (u(II,JJ,KK) + ubar(soln.rank,JJ,KK))*qx(II,JJ,KK)     // u*q_x
                  + v(II,JJ,KK)*(qy(II,JJ,KK) + qbary(soln.rank,JJ,KK) + soln.beta) // v*(q_y + beta)
                  - qbar_flux_0(soln.rank,JJ,KK); // bar(v*q)
                if (soln.do_forcing) { // Add forcing if necessary
                    flux(II,JJ,KK) += (*soln.forcing[0])(II,JJ,KK);
                }
            }
        }
    }
};


// Linear Flux Method
void linear(solnclass & soln, DTArray & flux,
        Array<double,1> & ygrid, Transgeom & Sz)
{

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],     &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],     &v = *soln.v[0];
    DTArray &qx = *soln.qx[0],   &qy = *soln.qy[0];
    DTArray &ub = *soln.ub[0],   &vb = *soln.vb[0];
    DTArray &qxb = *soln.qxb[0], &qyb = *soln.qyb[0];

    double &norm_3d = Sz.norm_3d,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;

    DTArray &visc = *soln.work[0];
    DTArray &K2 = *Sz.K2[0],   &K2h = *Sz.K2h[0];
    CTArray &qh = *Sz.qh[0];

    // Gradient of PV
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        // calc q_x
    if (Sz.type_y == FOURIER)   {deriv_fft(q,Y_xform,qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q,Y_xform,qy);}
    qy *= norm_y;                                                  // calc q_y

    // Compute psi
    XYZ_xform.forward_transform(&q,FOURIER,Sz.type_z,Sz.type_y); 
    if ((Sz.type_z != NONE) or (Sz.norm_z == 0)) {
        qh = *(XYZ_xform.get_complex_temp());
        qh = qh*K2;  
        *(XYZ_xform.get_complex_temp()) = qh;
    }
    else if (Sz.norm_z > 0.) {
        vertical_solve(*(XYZ_xform.get_complex_temp()), qh, K2, soln);
    }
    XYZ_xform.back_transform(&psi,FOURIER,Sz.type_z,Sz.type_y);

    // Geostrophic Velocity
    if (Sz.type_y == FOURIER)   {deriv_fft(psi,Y_xform,u);}
    else if (Sz.type_y == SINE) {deriv_dst(psi,Y_xform,u);}
    u *= -norm_y;                                                  // calc u
    deriv_fft(psi,X_xform,v);  v *=  norm_x;                       // calc v

    // Compute flux
    flux = ub*qx + vb*qy + u*qxb + v*(qyb + soln.beta);               
};

// Nonlinear Perturbation Flux Method
void nonlinear_pert(solnclass & soln, DTArray & flux,
        Array<double,1> & ygrid, Transgeom & Sz)
{

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // Make nice references for arrays used
    DTArray &q = *soln.q[0],     &psi = *soln.psi[0];
    DTArray &u = *soln.u[0],     &v = *soln.v[0];
    DTArray &qx = *soln.qx[0],   &qy = *soln.qy[0];
    DTArray &UB = *soln.ub[0],   &QBY = *soln.qyb[0];

    DTArray &K2 = *Sz.K2[0],    &K2h = *Sz.K2h[0];
    CTArray &qh = *Sz.qh[0];

    DTArray &K2_bar = *Sz.K2_bar[0];
    CTArray &qh_bar_c = *Sz.qh_bar_c[0];
    DTArray &qh_bar_r = *Sz.qh_bar_r[0];

    DTArray &qbar = *soln.qbar[0], &psibar = *soln.psibar[0];
    DTArray &qbar_flux_0 = *soln.qbar_flux[0];
    DTArray &qbar_flux_1 = *soln.qbar_flux[1];
    DTArray &qbar_flux_2 = *soln.qbar_flux[2];

    DTArray &qbary = *soln.qbary[0], &ubar = *soln.ubar[0];
    DTArray &meanvq = *soln.meanvq[0];

    double &norm_3d = Sz.norm_3d,
           &norm_3d_bar = Sz.norm_3d_bar,
           &norm_x  = Sz.norm_x,
           &norm_y  = Sz.norm_y;
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 
    Trans1D &X_xform = *Sz.X_xform,
            &Y_xform = *Sz.Y_xform;
    TransWrapper &XYZ_xform_bar = *Sz.XYZ_xform_bar; 
    Trans1D &Y_xform_bar = *Sz.Y_xform_bar;

    // Gradient of PV
    deriv_fft(q,X_xform,qx);  qx *= norm_x;                        // calc q_x
    if (Sz.type_y == FOURIER)   {deriv_fft(q,Y_xform,qy);}
    else if (Sz.type_y == SINE) {deriv_dst(q,Y_xform,qy);}
    qy *= norm_y;                                                  // calc q_y

    compute_vels_and_psi(soln, Sz);

    // Compute <vq>
    //    Each processor will have a copy of the information
    //    While technically this is redundant and requires more solves
    //    than strictly necessary, in order to have the parallelized
    //    declaration of the DTArrays and Transforms play nice,
    //    it seemed to be necessary.
    for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
        for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
            soln.yz_slice[JJ][KK] = 0.;
            for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
                soln.yz_slice[JJ][KK] += v(II,JJ,KK)*q(II,JJ,KK)/soln.Nx;
            }
        }
    }
    soln.sum_reduce_yz_slice();
    for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
        for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
            meanvq(soln.rank,JJ,KK) = soln.yz_slice_red[JJ][KK];
        }
    }

    if (Sz.type_y == FOURIER)   {deriv_fft(meanvq,Y_xform_bar,qbar_flux_0);}
    else if (Sz.type_y == SINE) {deriv_dst(meanvq,Y_xform_bar,qbar_flux_0);}
    qbar_flux_0 *= norm_y;                                                  // calc ddy< vq >

    // Gradient of qbar
    if (Sz.type_y == FOURIER)   {deriv_fft(qbar, Y_xform_bar, qbary);}
    else if (Sz.type_y_bar == COSINE) {deriv_dct(qbar, Y_xform_bar, qbary);}
    qbary *= norm_y;                                                  // calc qbar_y

    // Evolve qbar
    qbar = qbar - soln.w0*qbar_flux_0 - soln.w1*qbar_flux_1 - soln.w2*qbar_flux_2;
    qbar_flux_2 = qbar_flux_1;
    qbar_flux_1 = qbar_flux_0;

    // Compute flux
    for (int II = u.lbound(firstDim); II <= u.ubound(firstDim); II++) {
        for (int JJ = u.lbound(secondDim); JJ <= u.ubound(secondDim); JJ++) {
            for (int KK = u.lbound(thirdDim); KK <= u.ubound(thirdDim); KK++) {
                flux(II,JJ,KK) = 
                    (u(II,JJ,KK) + UB(II,JJ,KK) + ubar(soln.rank,JJ,KK))*qx(II,JJ,KK)
                   + v(II,JJ,KK)*(qy(II,JJ,KK) + QBY(II,JJ,KK) + qbary(soln.rank,JJ,KK) + soln.beta)
                   - qbar_flux_0(soln.rank,JJ,KK);
            }
        }
    }

}

// Apply the CFL condition to compute dt
void compute_dt(solnclass & soln) {  

    // Compute restrictions
    double tmp1, tmp2;
    double max_u, max_v;
    double dt;

    tmp1 =  pvmax(*soln.u[0]);
    tmp2 = -pvmin(*soln.u[0]);
    max_u = max(tmp1,tmp2) + soln.U0;

    tmp1 =  pvmax(*soln.ubar[0]);
    tmp2 = -pvmin(*soln.ubar[0]);
    max_u += max(tmp1, tmp2);

    tmp1 =  pvmax(*soln.v[0]);
    tmp2 = -pvmin(*soln.v[0]);
    max_v = max(tmp1, tmp2) + soln.V0;

    dt = min(soln.dx/max_u, soln.dy/max_v)/5.;

    if (dt < 1.0) { // BAS: Why 1?
        if (master()) {
            tmp1 =  pvmax(*soln.ubar[0]);
            tmp2 = -pvmin(*soln.ubar[0]);
            fprintf(stderr,"Time step is %g seconds, too small to continue. Force exit!\n",dt);
            fprintf(stderr,"   max(u, ubar, v) = (%g, %g, %g)\n", max_u - max(tmp1,tmp2), max(tmp1,tmp2), max_v);
            fprintf(stderr,"   (dx, dy) = (%g, %g)\n", soln.dx, soln.dy);
        }
        MPI_Finalize(); exit(1);
    }
    else if(dt > 15000.0) { // BAS: Why 15000?
        dt = 15000.0;
    }

    if (dt + soln.t >= soln.next_plot_time) {
        dt  = soln.next_plot_time - soln.t;
        soln.do_plot = true;
    }

    soln.dt2 = soln.dt1;
    soln.dt1 = soln.dt0;
    soln.dt0 = dt;

    return;
}

void apply_filter(solnclass & soln,Transgeom & Sz) {

    DTArray &q    = *soln.q[0],
            &qbar = *soln.qbar[0];

    TransWrapper &XYZ_xform = *Sz.XYZ_xform,
                 &XYZ_xform_bar = *Sz.XYZ_xform_bar; 

    double &fcut = soln.fcutoff,
           &ford = soln.forder,
           &fstr = soln.fstrength;

    // Filter q and qbar separately.
    // Since filtering is a linear operator, this is okay
    filter3(q,   XYZ_xform,     FOURIER, Sz.type_z, Sz.type_y, fcut, ford, fstr); 
    filter3(qbar,XYZ_xform_bar, NONE,    Sz.type_z, Sz.type_y, fcut, ford, fstr); 

    // Now that we've filtered q, compute the filtered u, v, and psi fields.
    compute_vels_and_psi(soln, Sz);

}

// Apply the Euler step
void step_euler(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz) {

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[0];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute flux
    method.flux(soln,fluxq,ygrid,Sz);

    // Compute new timestep, decrease for euler
    compute_dt(soln);
    soln.dt0 *= 0.1;
    soln.w0 = soln.dt0;
    soln.w1 = 0;
    soln.w2 = 0;

    // Advance solution
    q = q - soln.dt0*fluxq;

    // Apply exponential filter
    //filter3(q,XYZ_xform,FOURIER,Sz.type_z,Sz.type_y,soln.fcutoff,soln.forder,soln.fstrength); 
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[1] = fluxq; 

}

// Compute the weights for the AB2 method
void compute_weights2(solnclass & soln) {

    double a,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn  = soln.t;

    a  = soln.t - soln.dt1;
    ts = soln.t;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w0 = w;

    a  = soln.t;
    ts = soln.t - soln.dt1;
    w  = (  0.5*(tnp*tnp - tn*tn) 
            - a*(tnp-tn) )
        /(ts-a);
    soln.w1 = w;

    soln.w2 = 0;

    return;

}

// Apply the AB2 step
void step_ab2(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz) {

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[1];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute flux
    method.flux(soln,fluxq,ygrid,Sz);

    // Compute new timestep, decrease for ab2
    compute_dt(soln);
    soln.dt0 *= 0.1;

    // Advance solution
    compute_weights2(soln);
    q = q - soln.w0*fluxq - soln.w1*(*flux.q[1]);

    // Exponential Filter 
    //filter3(q,XYZ_xform,FOURIER,Sz.type_z,Sz.type_y,soln.fcutoff,soln.forder,soln.fstrength); 
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[0] = *flux.q[1];
    *flux.q[1] = fluxq; 

}

// Compute the weights for the AB3 method
void compute_weights3(solnclass & soln) {

    double a,b,w,ts;
    double tnp = soln.t + soln.dt0;
    double tn = soln.t;

    a  = soln.t - soln.dt1;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w0 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1 - soln.dt2;
    ts = soln.t - soln.dt1;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w1 = w;

    a  = soln.t;
    b  = soln.t - soln.dt1;
    ts = soln.t - soln.dt1 - soln.dt2;
    w  = (    (1./3)*(tnp*tnp*tnp - tn*tn*tn) 
            -    0.5*(a+b)*(tnp*tnp - tn*tn) 
            + (a*b*(tnp-tn)) )
        /((ts-a)*(ts-b));
    soln.w2 = w;


    return;

}


// Apply the AB3 step
void step_ab3(solnclass & soln, fluxclass & flux, methodclass & method,
        Array<double,1> & xgrid, Array<double,1> & ygrid, Transgeom & Sz) {

    // Make nice references for arrays
    DTArray &q = *soln.q[0], &u = *soln.u[0], &v = *soln.v[0], &fluxq = *flux.q[2];
    TransWrapper &XYZ_xform = *Sz.XYZ_xform; 

    // Compute flux
    method.flux(soln,fluxq,ygrid,Sz);

    // Compute new timestep, full dt for ab3
    compute_dt(soln);

    // Advance solution
    compute_weights3(soln);
    q = q - soln.w0*fluxq - soln.w1*(*flux.q[1]) - soln.w2*(*flux.q[0]);

    // Exponential Filter
    //filter3(q,XYZ_xform,FOURIER,Sz.type_z,Sz.type_y,soln.fcutoff,soln.forder,soln.fstrength); 
    apply_filter(soln, Sz);

    // Record fluxes for future
    *flux.q[0] = *flux.q[1];
    *flux.q[1] = fluxq; 
}

// Write the grids to the corresponding files. Assumes uniform grids.
void write_grids(DTArray & tmp, double Lx, double Ly, double Lz,
        int Nx, int Ny, int Nz, Transgeom & Sz) {

    // Blitz index placeholders
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    if (Nx > 1) {
        tmp = (ii + 0.5)/Nx*Lx + 0*jj + 0*kk - Lx/2;
        write_array_qg(tmp,"xgrid");
    }

    if (Ny > 1) {
        tmp = 0*ii + (kk + 0.5)/Ny*Ly + 0*jj - Ly/2;
        write_array_qg(tmp,"ygrid");
    }

    if (Nz > 1) {
        if (Sz.type_z == NONE) {
            tmp = 0*ii + 0*kk + Lz*(0.5 + 0.5*cos(jj*M_PI/(Nz-1))) - Lz;
        } else {
            tmp = 0*ii + 0*kk + (jj + 0.5)/Nz*Lz - Lz;
        }
        write_array_qg(tmp,"zgrid");
    }
}

void vertical_solve(CTArray & psihat, CTArray & qhat, DTArray & K2, solnclass & soln) {
    // Solves the Nx*Ny vertical problems for the inversion of
    // the Laplacian-of-q to give the streamfunction

    // The root mathematical problem being solved here is:

    // q = (Psi_xx + Psi_yy) + (f0^2/N^2) Psi_zz, or in Fourier space:

    // qhat = (-k^2 - l^2) psihat + (f0^2/N^2) psihat_zz

    // This gives a number of 1D Helmholtz-type problems, which we can solve with
    // poisson_1d in src/gmres_1d_solver.[ch]pp

    // This proceeds through 1D arrays, so allocate those
    blitz::Array<double,1> psihat1d(soln.Nz), rhs(soln.Nz);

    // We can rearrange the previous form for a "clean" second-derivative
    // by multiplying by N^2/f0^2, giving:

    // (N^2/f^2) (-k^2 - l^2) psihat + psihat_zz = (N^2/f^2) qhat

    // so in terms of parameters for poisson_1d, borrowing matlab-style notation:

    // rhs = resid = (N^2/f^2) qhat(i,:,j)
    // length = Lz
    // helmholtz = (-)N^2/f^2*(-k^2 - l^2) = -N^2/(f^2*K2(i,0,j))
    //             (poisson_1d expects a positive parameter for a negative term,
    //              on the idea that negative constant parameters here are well-
    //              defined.)
    // a_top = a_bot = 0 // Dirichlet portion of boundary conditions
    // b_top = b_bot = 1 // Neumann portion of boundary conditions

    // And finally, to respect the boundary conditions the first and last
    // entries of the residual/rhs must be replaced with 0.

    double norm = soln.N0*soln.N0/(soln.f0*soln.f0); // To keep this ratio handy

    // Range for assignment between 1D and 3D arrays
    blitz::Range all = blitz::Range::all();

    // BAS: Changed order for 2nd and 3rd dimensions

    for (int iii = qhat.lbound(firstDim); iii <= qhat.ubound(firstDim); iii++) {
        for (int jjj = qhat.lbound(thirdDim); jjj <= qhat.ubound(thirdDim); jjj++) {
            // The input and output arrays are complex, but poisson_1d works
            // on real arrays; we need to separate these out.

            rhs = norm*real(qhat(iii,all,jjj)); // Grab the real part
            rhs(0) = 0; // Assign boundary conditions
            rhs(soln.Nz-1) = 0;

            int iter_count = poisson_1d(rhs,psihat1d,
                    soln.Lz, // Vertical grid length
                    -norm/K2(iii,0,jjj), // Helmholtz parameter
                    0.0, 0.0, 1.0, 1.0 // Boundary conditions
                    );

            if (iter_count <= 0) { // The solve failed for some reason
                fprintf(stderr,"Vertical solve(%d,%d) real (constant %g) failed, returning %d\n",iii,jjj,-norm/K2(iii,0,jjj),iter_count);
                assert(0);
            }

            real(psihat(iii,all,jjj)) = psihat1d;

            // Repeat for the imaginary part
            rhs = norm*imag(qhat(iii,all,jjj));
            rhs(0) = 0; rhs(soln.Nz-1) = 0;
            iter_count = poisson_1d(rhs,psihat1d,soln.Lz,-norm/K2(iii,0,jjj),0,0,1.0,1.0);
            if (iter_count <= 0) { // The solve failed for some reason
                fprintf(stderr,"Vertical solve(%d,%d) imag (constant %g) failed, returning %d\n",iii,jjj,-norm/K2(iii,0,jjj),iter_count);
                assert(0);
            }



        }
    }
}


