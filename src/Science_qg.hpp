/* WARNING: Science Content!

   Collection of various analysis routines that are general enough to be useful
   over more than one project */

#ifndef SCIENCE_HPP
#define SCIENCE_HPP 1

#include "T_util.hpp"
#include "TArray.hpp"
#include <blitz/array.h>
#include <blitz/tinyvec-et.h>
#include "math.h"
#include "Par_util.hpp"
#include "stdio.h"
#include "Split_reader.hpp"
#include <stdlib.h>
#include <complex>
#include "NSIntegrator.hpp"
#include <string>
#include "Parformer.hpp"

//using blitz::Array;
//using blitz::cos;
//using namespace TArrayn;
//using namespace NSIntegrator;
//using namespace Transformer;

/* Compute dpsidz, dpsidx, dpsidy (for QG) */
void dpsidz(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz);

void dpsidz_bar(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz);

// Compute circulation about the North and South walls
void compute_circulation(double * circ_N, double * circ_S,
        DTArray & u, Transformer :: S_EXP type_y, MPI_Comm c = MPI_COMM_WORLD);

// Depth-averaged and azimuthally-integrated KE spectrum
void horiz_spectrum_bin_counts(DTArray & var, 
        double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, Transformer::S_EXP type_x, 
        Transformer:: S_EXP type_y, MPI_Comm c = MPI_COMM_WORLD);

void compute_mass_per_level(DTArray * rho, double * level_mass, MPI_Comm c = MPI_COMM_WORLD);

void KE_spect(double * aniso, double * aniso_array, DTArray * u, DTArray * v, DTArray * ub, DTArray * vb,
        DTArray * ubar, DTArray * work, DTArray * work2, bool use_zonal_decomp,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, double include_bg_in_spectra, bool compute_aniso,
        double Lx, double Ly, TArrayn::S_EXP type_y, double fcutoff, MPI_Comm c = MPI_COMM_WORLD);

void PE_spect(double * aniso, double * aniso_array, DTArray * psi, DTArray * psib, DTArray * psibar,
        DTArray * work, DTArray * work2, DTArray * work3, bool use_zonal_decomp,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, double include_bg_in_spectra, bool compute_aniso,
        double Lx, double Ly, double Lz, double f0, double N0, TArrayn::S_EXP type_y, double fcutoff, MPI_Comm c = MPI_COMM_WORLD);

void Enstrophy_spect(double * aniso, double * aniso_array, DTArray * q, DTArray * qb, DTArray * qbar,
        DTArray * work, DTArray * work_bar, bool use_zonal_decomp,
        DTArray * temp_2d, double * spect, double * spect_x, double * spect_y, double include_bg_in_spectra, bool compute_aniso,
        double Lx, double Ly, TArrayn::S_EXP type_y, double fcutoff, MPI_Comm c = MPI_COMM_WORLD);
 
// Equation of state for seawater, polynomial fit from
// Brydon, Sun, Bleck (1999) (JGR)
inline double eqn_of_state(double T, double S){
   // Returns the density anomaly (kg/m^3) for water at the given
   // temperature T (degrees celsius) and salinity S (PSU?)

   // Constants are from table 4 of the above paper, at pressure 0
   // (This is appropriate since this is currently an incompressible
   // model)
   const double c1 = -9.20601e-2; // constant term
   const double c2 =  5.10768e-2; // T term
   const double c3 =  8.05999e-1; // S term
   const double c4 = -7.40849e-3; // T^2 term
   const double c5 = -3.01036e-3; // ST term
   const double c6 =  3.32267e-5; // T^3 term
   const double c7 =  3.21931e-5; // ST^2 term

   return c1 + c2*T + c3*S + c4*T*T + c5*S*T + c6*T*T*T + c7*S*T*T;
}

// Define a Blitz-friendly operator
BZ_DECLARE_FUNCTION2(eqn_of_state)

inline double eqn_of_state_t(double T){
   // Specialize for freshwater with S=0
   return eqn_of_state(T,0.0);
}
BZ_DECLARE_FUNCTION(eqn_of_state_t)

#endif
