/* WARNING: Science Content!

   Collection of various analysis routines that are general enough to be useful
   over more than one project */

#ifndef SCIENCE_HPP
#define SCIENCE_HPP 1

#include <blitz/array.h>
#include "TArray.hpp"
#include "NSIntegrator.hpp"
#include <string>
#include "Parformer.hpp"

namespace NSI = NSIntegrator;

// Marek's Overturning Diagnostic
blitz::Array<double,3> overturning_2d(blitz::Array<double,3> const & rho, 
      blitz::Array<double,1> const & zgrid, TArrayn::Dimension reduce = TArrayn::thirdDim );

// Read in a 2D file and interpret it as a 2D slice of a 3D array, for
// initialization with read-in-data from a program like MATLAB
void read_2d_slice(blitz::Array<double,3> & fillme, const char * filename, 
                  int Nx, int Ny);

void read_2d_restart(blitz::Array<double,3>& fillme, const char* filename,
                  int Nx, int Ny);

/* Compute vorticity */
void vorticity(TArrayn::DTArray & u, TArrayn::DTArray & v, 
      TArrayn::DTArray & w, TArrayn::DTArray * & w_x, TArrayn::DTArray * & w_y,
      TArrayn::DTArray * & w_z, double Lx, double Ly, double Lz,
      int szx, int szy, int szz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, 
      NSI::DIMTYPE DIM_Z);

/* Compute dudx */
void dudx(TArrayn::DTArray & u, TArrayn::DTArray * & out, 
      double Lx, double Ly, double Lz,
      int szx, int szy, int szz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, 
      NSI::DIMTYPE DIM_Z);

/* Compute dudy */
void dudy(TArrayn::DTArray & u, TArrayn::DTArray * & out, 
      double Lx, double Ly, double Lz,
      int szx, int szy, int szz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, 
      NSI::DIMTYPE DIM_Z);

/* Compute dudz */
void dudz(TArrayn::DTArray & u, TArrayn::DTArray * & out, 
      double Lx, double Ly, double Lz,
      int szx, int szy, int szz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, 
      NSI::DIMTYPE DIM_Z);

/* Compute dpsidz, dpsidx, dpsidy (for QG) */
/*
void dpsidz(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz);
void dpsidx(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz);
void dpsidy(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz);
*/

/* Compute ertel pv */
void ertel_pv(DTArray & u, DTArray & v, DTArray & w, DTArray & rho,
	                DTArray * e_pv, DTArray & temp_a, DTArray & temp_b,
                    double f0, double N0, double Lx, double Ly, double Lz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z,
      MPI_Comm c = MPI_COMM_WORLD);

// Quadrature weights
void compute_quadweights(int szx, int szy, int szz, 
      double Lx, double Ly, double Lz,
      NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y,
      NSI::DIMTYPE DIM_Z);

const blitz::Array<double,1> * get_quad_x();
const blitz::Array<double,1> * get_quad_y();
const blitz::Array<double,1> * get_quad_z();

// Depth-averaged and azimuthally-integrated KE spectrum
void horiz_spectrum_depth_avg(double * aniso, DTArray & var, 
        double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, Transformer::S_EXP type_x, 
        Transformer:: S_EXP type_y, MPI_Comm c = MPI_COMM_WORLD);

void horiz_spectrum_bin_counts(DTArray & var, 
        double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, Transformer::S_EXP type_x, 
        Transformer:: S_EXP type_y, MPI_Comm c = MPI_COMM_WORLD);

// Compute horizontal KE spectra 
void KE_spect(double * aniso, double * aniso_array, DTArray & u, DTArray & v, DTArray & w, DTArray & temp,
        double ** spect_2d, double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, const char * type_x, const char * type_y, MPI_Comm c = MPI_COMM_WORLD);

// Compute horizontal PE spectra 
void PE_spect(double * aniso, double * aniso_array, DTArray & b, DTArray & temp,
        double ** spect_2d, double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, const char * type_x, const char * type_y, MPI_Comm c = MPI_COMM_WORLD);

// Measure the level of QG balance
double measure_geostrophic_balance(DTArray * eps_geo, DTArray & p, DTArray & u, DTArray & v, 
        DTArray & w, DTArray & b, DTArray & temp_a, DTArray & temp_b, 
        double rho0, double f0, double N0, double Lx, double Ly,
        MPI_Comm c = MPI_COMM_WORLD);

double measure_cyclostrophic_balance(DTArray * eps_cyclo, DTArray & p, DTArray & u, DTArray & v, 
        DTArray & w, DTArray & b, DTArray & temp_a, DTArray & temp_b, DTArray & temp_c,
        double rho0, double f0, double N0, double Lx, double Ly, double & div_adv_rms, double & zeta_rms, double & lap_p_rms,
        MPI_Comm c = MPI_COMM_WORLD);

void estimate_bu(double * Bu, DTArray & b, DTArray & u, DTArray & v, DTArray & w, 
        DTArray & temp_a, DTArray & tmep_b, DTArray & temp_c, double rho0, 
        double Lx, double Ly, double Lz, double N0, double f0, const char * type_x, const char * type_y, MPI_Comm c = MPI_COMM_WORLD);

void estimate_Ro(double * Bu, DTArray & b, DTArray & u, DTArray & v, DTArray & w, 
        DTArray & temp_a, DTArray & tmep_b, DTArray & temp_c, double rho0, 
        double Lx, double Ly, double N0, double f0, const char * type_x, const char * type_y, MPI_Comm c = MPI_COMM_WORLD);

// Equation of state for seawater, polynomial fit from
// Brydon, Sun, Bleck (1999) (JGR)

inline double eqn_of_state(double T, double S){
   // Returns the density anomaly (kg/m^3) for water at the given
   // temperature T (degrees celsius) and salinity S (PSU?)

   // Constants are from table 4 of the above paper, at pressure 0
   // (This is appropriate since this is currently an incompressible
   // model)
   const double c1 = -9.20601e-2; // constant term
   const double c2 =  5.10768e-2; // T term
   const double c3 =  8.05999e-1; // S term
   const double c4 = -7.40849e-3; // T^2 term
   const double c5 = -3.01036e-3; // ST term
   const double c6 =  3.32267e-5; // T^3 term
   const double c7 =  3.21931e-5; // ST^2 term

   return c1 + c2*T + c3*S + c4*T*T + c5*S*T + c6*T*T*T + c7*S*T*T;
}

// Define a Blitz-friendly operator
BZ_DECLARE_FUNCTION2(eqn_of_state)

inline double eqn_of_state_t(double T){
   // Specialize for freshwater with S=0
   return eqn_of_state(T,0.0);
}
BZ_DECLARE_FUNCTION(eqn_of_state_t)

#endif
