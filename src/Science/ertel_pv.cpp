#include "../Science.hpp"

namespace TA  = TArrayn;
namespace Tf  = Transformer;
namespace NSI = NSIntegrator;

void ertel_pv(DTArray & u, DTArray & v, DTArray & w, DTArray & rho,
        DTArray * e_pv, DTArray & temp_a, DTArray & temp_b, double f0, double N0,
        double Lx, double Ly, double Lz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z, MPI_Comm c) {

    int Nx, Ny, Nz;
    Nx = pssum(v.extent(Tf::firstDim),c);
    Ny = v.extent(Tf::secondDim);
    Nz = v.extent(Tf::thirdDim);

    static Tf::Trans1D trans_x(Nx,Ny,Nz,Tf::firstDim, (DIM_X == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL)),
                       trans_y(Nx,Ny,Nz,Tf::secondDim, (DIM_Y == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL)),
                       trans_z (Nx,Ny,Nz,Tf::thirdDim, (DIM_Z == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL));
    
    /* x-vorticity is w_y - v_z */
    /* b_x*(w_y - v_z) */
    *e_pv = 0;
    if (Nx > 1) { // b_x
        //if (master()) {fprintf(stdout, "b_x\n");}
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(rho,trans_x,temp_b);
            temp_b *= 2*M_PI/Lx;
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(rho,trans_x,temp_b);
            temp_b *= M_PI/Lx;
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(rho,trans_x,temp_b);
            temp_b *= -2/Lx;
        }
    }
    if (Ny > 1) { // w_y
        //if (master()) {fprintf(stdout, "w_y\n");}
        if (DIM_Y == NSI::PERIODIC) {
            deriv_fft(w,trans_y,temp_a);
            *e_pv += temp_a*temp_b*(2*M_PI/Ly);
        } else if (DIM_Y == NSI::FREE_SLIP) {
            deriv_dct(w,trans_y,temp_a);
            *e_pv += temp_a*temp_b*(M_PI/Ly);
        } else {
            assert(DIM_Y == NSI::NO_SLIP);
            deriv_cheb(w,trans_y,temp_a);
            *e_pv += temp_a*temp_b*(-2/Ly);
        }
    }
    if (Nz > 1) { // v_z
        //if (master()) {fprintf(stdout, "v_z\n");}
        if (DIM_Z == NSI::PERIODIC) {
            deriv_fft(v,trans_z,temp_a);
            *e_pv -= temp_a*temp_b*(2*M_PI/Lz);
        } else if (DIM_Z == NSI::FREE_SLIP) {
            deriv_dct(v,trans_z,temp_a);
            *e_pv -= temp_a*temp_b*(M_PI/Lz);
        } else {
            assert(DIM_Z == NSI::NO_SLIP);
            deriv_cheb(v,trans_z,temp_a);
            *e_pv -= temp_a*temp_b*(-2/Lz);
        }
    }

    // y-vorticity is u_z - w_x
    /* b_y*(u_z - w_x) */
    if (Ny > 1) { // b_y
        //if (master()) {fprintf(stdout, "b_y\n");}
        if (DIM_Y == NSI::PERIODIC) {
            deriv_fft(rho,trans_y,temp_b);
            temp_b *= 2*M_PI/Ly;
        } else if (DIM_Y == NSI::FREE_SLIP) {
            deriv_dct(rho,trans_y,temp_b);
            temp_b *= M_PI/Ly;
        } else {
            assert(DIM_Y == NSI::NO_SLIP);
            deriv_cheb(rho,trans_y,temp_b);
            temp_b *= -2/Ly;
        }
    }
    if (Nz > 1) { // u_z
        //if (master()) {fprintf(stdout, "u_z\n");}
        if (DIM_Z == NSI::PERIODIC) {
            deriv_fft(u,trans_z,temp_a);
            *e_pv += temp_a*temp_b*(2*M_PI/Lz);
        } else if (DIM_Z == NSI::FREE_SLIP) {
            deriv_dct(u,trans_z,temp_a);
            *e_pv += temp_a*temp_b*(M_PI/Lz);
        } else {
            assert(DIM_Z == NSI::NO_SLIP);
            deriv_cheb(u,trans_z,temp_a);
            *e_pv += temp_a*temp_b*(-2/Lz);
        }
    }
    if (Nx > 1) { // w_x
        //if (master()) {fprintf(stdout, "w_x\n");}
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(w,trans_x,temp_a);
            *e_pv -= temp_a*temp_b*(2*M_PI/Lx);
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(w,trans_x,temp_a);
            *e_pv -= temp_a*temp_b*(M_PI/Lx);
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(w,trans_x,temp_a);
            *e_pv -= temp_a*temp_b*(-2/Lx);
        }
    }

    // And finally, vort_z is v_x - u_y
    // b_z*(v_x - u_y)
    if (Nz > 1) { // b_z
        //if (master()) {fprintf(stdout, "b_z\n");}
        if (DIM_Z == NSI::PERIODIC) {
            deriv_fft(rho,trans_z,temp_b);
            temp_b *= 2*M_PI/Lz;
        } else if (DIM_Z == NSI::FREE_SLIP) {
            deriv_dct(rho,trans_z,temp_b);
            temp_b *= M_PI/Lz;
        } else {
            assert(DIM_Z == NSI::NO_SLIP);
            deriv_cheb(rho,trans_z,temp_b);
            temp_b *= -2/Lz;
        }
        temp_b += N0*N0;
        *e_pv += f0*temp_b;
    }
    if (Nx > 1) { // v_x
        //if (master()) {fprintf(stdout, "v_x\n");}
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(v,trans_x,temp_a);
            *e_pv += temp_a*temp_b*(2*M_PI/Lx);
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(v,trans_x,temp_a);
            *e_pv += temp_a*temp_b*(M_PI/Lx);
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(v,trans_x,temp_a);
            *e_pv += temp_a*temp_b*(-2/Lx);
        }
    }
    if (Ny > 1) { // u_y
        //if (master()) {fprintf(stdout, "u_y\n");}
        if (DIM_Y == NSI::PERIODIC) {
            deriv_fft(u,trans_y,temp_a);
            *e_pv -= temp_a*temp_b*(2*M_PI/Ly);
        } else if (DIM_Y == NSI::FREE_SLIP) {
            deriv_dct(u,trans_y,temp_a);
            *e_pv -= temp_a*temp_b*(M_PI/Ly);
        } else {
            assert(DIM_Y == NSI::NO_SLIP);
            deriv_cheb(u,trans_y,temp_a);
            *e_pv -= temp_a*temp_b*(-2/Ly);
        }
    }
    return;
}
