#include "../Science.hpp"

namespace TA  = TArrayn;
namespace Tf  = Transformer;
namespace NSI = NSIntegrator;

void dudz(TA::DTArray & u, TA::DTArray * & out,
        double Lx, double Ly, double Lz,
        int szx, int szy, int szz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z) {

    static Tf::Trans1D trans_z (szx, szy, szz, Tf::thirdDim, (DIM_Z == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL));

    if (DIM_Z == NSI::PERIODIC) {
        deriv_fft(u,trans_z,*out);
        *out *= 2*M_PI/Lz;
    } else if (DIM_Z == NSI::FREE_SLIP) {
        deriv_dct(u,trans_z,*out);
        *out *= M_PI/Lz;
    } else {
        assert(DIM_Z == NSI::NO_SLIP);
        deriv_cheb(u,trans_z,*out);
        *out *= -2/Lz;
    }
    return;
}
