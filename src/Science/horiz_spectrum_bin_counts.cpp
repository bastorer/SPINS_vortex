#include "../Science.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

// Compute the bins counts for the spectra
void horiz_spectrum_bin_counts(DTArray & var, double * spect, 
        double * spect_x, double * spect_y, double Lx, double Ly, 
        Tf::S_EXP type_x, Tf::S_EXP type_y, MPI_Comm c) {
    
    int Nx, Ny, Nz, N, numproc, ub, lb, my_rank;
    int tmpII;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(var.extent(Tf::firstDim),c);
    Ny = var.extent(Tf::secondDim);
    Nz = var.extent(Tf::thirdDim);

    // Create the forward transformation wrapper and compute the wave numbers
    static Tf::TransWrapper XY_xform = Tf::TransWrapper(Nx,Ny,Nz,type_x,type_y,Tf::NONE);

    double norm_x, norm_y;
    norm_x = (type_x == Tf::FOURIER) ? 2*M_PI/Lx : M_PI/Lx;
    norm_y = (type_y == Tf::FOURIER) ? 2*M_PI/Ly : M_PI/Ly;

    double kxmax, kymax, kmax;
    kxmax = XY_xform.max_wavenum(Tf::firstDim)*norm_x;
    kymax = XY_xform.max_wavenum(Tf::secondDim)*norm_y;
    kmax = max(kxmax,kymax);

    N = max(Nx,Ny);
    int N_spect = static_cast<int>(N/2);
    double dk = kmax/(N/2);
    double kub, klb, kx, ky, kh;

    // Some awkwardness because we need to handle real and complex arrays independently
    TA::CTArray * var_hat_c;
    TA::DTArray * var_hat_r;
    bool is_complex;
    int Ilb, Iub, Jlb, Jub, Klb, Kub;
    if ( (type_x == Tf::FOURIER) or (type_y == Tf::FOURIER)) {
        is_complex = true;
        XY_xform.forward_transform(&var, type_x, type_y, Tf::NONE);
        var_hat_c = XY_xform.get_complex_temp();
    } else {
        is_complex = false;
        XY_xform.forward_transform(&var, type_x, type_y, Tf::NONE);
        var_hat_r = XY_xform.get_real_temp();
    }
    Ilb = is_complex ? (*var_hat_c).lbound(Tf::firstDim) : (*var_hat_r).lbound(Tf::firstDim);
    Iub = is_complex ? (*var_hat_c).ubound(Tf::firstDim) : (*var_hat_r).ubound(Tf::firstDim);
    Jlb = is_complex ? (*var_hat_c).lbound(Tf::secondDim) : (*var_hat_r).lbound(Tf::secondDim);
    Jub = is_complex ? (*var_hat_c).ubound(Tf::secondDim) : (*var_hat_r).ubound(Tf::secondDim);
    Klb = is_complex ? (*var_hat_c).lbound(Tf::thirdDim) : (*var_hat_r).lbound(Tf::thirdDim);
    Kub = is_complex ? (*var_hat_c).ubound(Tf::thirdDim) : (*var_hat_r).ubound(Tf::thirdDim);

    Array<double,1> kvec = XY_xform.wavenums(Tf::firstDim),
        lvec = XY_xform.wavenums(Tf::secondDim);
    blitz::firstIndex ii;
    blitz::secondIndex jj;

    // Now apply azimuthal integration and depth averaging
    double * cnt = new double[N_spect];
    double * cnt_x = new double[N_spect];
    double * cnt_y = new double[N_spect];

    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    bool chopped_x, chopped_y;
    if (type_y == Tf::FOURIER) {chopped_y = true; chopped_x = false;}
    else if (type_x == Tf::FOURIER) {chopped_x = true; chopped_y = false;}
    else {chopped_x = false; chopped_y = false;}

    double mult = 1.;
    for (int II = 0; II < N_spect; II++) {
        cnt[II] = 0.;
        cnt_x[II] = 0.;
        cnt_y[II] = 0.;
    }

    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        if (chopped_x) {
            if ((kvec(I) == 0) or (kvec(I) == Iub)) {
                mult = 1.; }
            else {  mult = 2.; }
        }

        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            if ((type_y == Tf::SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            if (chopped_y) {
                if ((lvec(J) == 0) or (lvec(J) == Jub)) {
                    mult = 1.; }
                else { mult = 2.; }
            }

            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.49);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                cnt[tmpII] += mult;
            }

            // x spectrum
            tmpII = floor(abs(kx)/dk + 0.49);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                cnt_x[tmpII] += mult;
            }

            // y spectrum
            tmpII = floor(abs(ky)/dk + 0.49);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                cnt_y[tmpII] += mult;
            }

        }
    }

    double * full_cnt = new double[N_spect];
    MPI_Reduce(cnt, full_cnt, N_spect, MPI_DOUBLE, MPI_SUM, 0, c);
    double * full_cnt_x = new double[N_spect];
    MPI_Reduce(cnt_x, full_cnt_x, N_spect, MPI_DOUBLE, MPI_SUM, 0, c);
    double * full_cnt_y = new double[N_spect];
    MPI_Reduce(cnt_y, full_cnt_y, N_spect, MPI_DOUBLE, MPI_SUM, 0, c);

    // Confirm that the total number of binned wavenumbers equals
    // the total number of wavenumbers.
    if (master()) {
        double check = 0.;
        double check_x = 0.;
        double check_y = 0.;
        for (int II = 0; II < N_spect; II++) { 
            check += full_cnt[II];
            check_x += full_cnt_x[II];
            check_y += full_cnt_y[II];
        }
        if ( (abs(check   - (Nx*Ny*Nz)) > 0.1)
          or (abs(check_x - (Nx*Ny*Nz)) > 0.1)
          or (abs(check_y - (Nx*Ny*Nz)) > 0.1) ) {
            fprintf(stderr, "Count is wrong! %g instead of %d\n", check, Nx*Ny*Nz);
        }
    }

    if (master()) {
        for (int II = 0; II < N_spect; II++) {
            spect[II] = full_cnt[II];
            spect_x[II] = full_cnt_x[II];
            spect_y[II] = full_cnt_y[II];
        }
    }

    delete[] full_cnt;
    delete[] cnt;
    delete[] full_cnt_x;
    delete[] cnt_x;
    delete[] full_cnt_y;
    delete[] cnt_y;

}

