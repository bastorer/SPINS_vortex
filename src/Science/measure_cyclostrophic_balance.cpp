#include "../Science.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

double measure_cyclostrophic_balance(DTArray * eps_cyclo, DTArray & p, DTArray & u, DTArray & v, 
        DTArray & w, DTArray & b, DTArray & temp_a, DTArray & temp_b, DTArray & temp_c,
        double rho0, double f0, double N0, double Lx, double Ly, double & div_adv_rms, double & zeta_rms, double & lap_p_rms,
        MPI_Comm c) {

    // eps_cyclo = | -div_adv + f*zeta^z - 1/rho*(dxx+dyy)p   | / ( |div_adv| + f|zeta^z| + |1/rho*(dxx+dyy)|  + mu )
    //
    // mu = f*zeta^z_RMS + 1/rho*[(dxx+dyy)p]_RMS
    // div_adv = div_H(u_H dot grad_H(u_H))

    int Nx, Ny, Nz;
    Nx = pssum(v.extent(Tf::firstDim),c);
    Ny = v.extent(Tf::secondDim);
    Nz = v.extent(Tf::thirdDim);

    // Currently assumes Fourier in the horizontal
    static Tf::Trans1D X_xform(Nx,Ny,Nz,Tf::firstDim,Tf::FOURIER),
        Y_xform(Nx,Ny,Nz,Tf::secondDim,Tf::FOURIER);
    static Tf::TransWrapper XY_xform(Nx, Ny, Nz, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
        Array<double,1> kvec = XY_xform.wavenums(Tf::firstDim),
        lvec = XY_xform.wavenums(Tf::secondDim);
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    static GeneralArrayStorage<3> spec_ordering;
    static TinyVector<int,3> spec_lbound, spec_extent;
    spec_ordering.ordering() = XY_xform.get_complex_temp()->ordering();
    spec_lbound = XY_xform.get_complex_temp()->lbound();
    spec_extent = XY_xform.get_complex_temp()->extent();

    static TA::CTArray c_temp(spec_lbound, spec_extent, spec_ordering),
                       c_temp2(spec_lbound, spec_extent, spec_ordering);
    double Norm_x, Norm_y;
    Norm_x = 2.0*M_PI/Lx;
    Norm_y = 2.0*M_PI/Ly;

    complex<double> onej(0.,1.0);

    // div_H(u_H dot grad_H(u_H)) = (dxu)^2 + udxxu + 2*dxvdyu + vdxyu + udxyv + vdyyv + (dyv)^2 = temp_a
    XY_xform.forward_transform(&u, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    c_temp  = *(XY_xform.get_complex_temp());
    XY_xform.forward_transform(&v, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    c_temp2 = *(XY_xform.get_complex_temp());

    // (dxu)^2
    *(XY_xform.get_complex_temp()) = onej*(kvec(ii)*Norm_x + 0*jj + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a = temp_b*temp_b;

    // udxxu
    *(XY_xform.get_complex_temp()) = -(pow(kvec(ii)*Norm_x,2.0) + 0*jj + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a += u*temp_b;

    // vdxyu
    *(XY_xform.get_complex_temp()) = -(kvec(ii)*Norm_x*lvec(jj)*Norm_y + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a += v*temp_b;

    // 2*dxvdyu
    *(XY_xform.get_complex_temp()) = onej*(0*ii + lvec(jj)*Norm_y + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    *(XY_xform.get_complex_temp()) = onej*(kvec(ii)*Norm_x + 0*jj + 0*kk)*c_temp2(ii,jj,kk);
    XY_xform.back_transform(&temp_c, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_c *= 1.0/XY_xform.norm_factor();
    temp_a += 2.0*temp_b*temp_c;

    // udxyv
    *(XY_xform.get_complex_temp()) = -(kvec(ii)*Norm_x*lvec(jj)*Norm_y + 0*kk)*c_temp2(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a += u*temp_b;

    // vdyyv
    *(XY_xform.get_complex_temp()) = -(0*kk + pow(lvec(jj)*Norm_y,2.0) + 0*kk)*c_temp2(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a += v*temp_b;

    // (dyv)^2
    *(XY_xform.get_complex_temp()) = onej*(0*ii + lvec(jj)*Norm_y + 0*kk)*c_temp2(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/XY_xform.norm_factor();
    temp_a += temp_b*temp_b;

    // f0*zeta^z = f0*(v_x - u_y) = temp_b
    deriv_fft(v,X_xform,temp_b);
    deriv_fft(u,Y_xform,temp_c);
    temp_b = f0*(temp_b*Norm_x - temp_c*Norm_y);

    // (dxx + dyy)p/rho0 = temp_c
    XY_xform.forward_transform(&p, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    c_temp = *(XY_xform.get_complex_temp());
    *(XY_xform.get_complex_temp()) = -(pow(kvec(ii)*Norm_x, 2.0) + pow(lvec(jj)*Norm_y,2.0) + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_c, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_c *= 1.0/(XY_xform.norm_factor()*rho0);

    div_adv_rms = pow(pssum(sum(temp_a*temp_a))/(Nx*Ny*Nz), 0.5);
    zeta_rms    = pow(pssum(sum(temp_b*temp_b))/(Nx*Ny*Nz), 0.5);
    lap_p_rms   = pow(pssum(sum(temp_c*temp_c))/(Nx*Ny*Nz), 0.5);

    *eps_cyclo = abs( -temp_a(ii,jj,kk) + temp_b(ii,jj,kk) - temp_c(ii,jj,kk) ) /
        ( abs(temp_a(ii,jj,kk)) + abs(temp_b(ii,jj,kk))  + abs(temp_c(ii,jj,kk)) + zeta_rms + lap_p_rms );

    // Now compute the energy-weighted average of eps_cyclo
    // This should provide a reasonable scalar measure of 
    // the over-all balance. 
    double en_wei_avg = pssum(sum((*eps_cyclo)*
                (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
                      + ( 0.5*rho0*b*b/N0/N0 ))))
        /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));
    return en_wei_avg;

}
