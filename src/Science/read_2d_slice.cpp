#include "../Science.hpp"
#include "../Split_reader.hpp"

namespace Tf  = Transformer;

// Read in a 2D file and interpret it as a 2D slice of a 3D array, for
// initialization with read-in-data from a program like MATLAB
void read_2d_slice(Array<double,3> & fillme, const char * filename,  int Nx, int Ny) {

    //using blitz::ColumnMajorArray;

    /* Get the local ranges we're interested in */
    blitz::Range xrange(fillme.lbound(Tf::firstDim), fillme.ubound(Tf::firstDim));
    blitz::Range zrange(fillme.lbound(Tf::thirdDim), fillme.ubound(Tf::thirdDim));

    /* Read the 2D slice from disk.  Matlab uses Column-Major array storage */

    blitz::GeneralArrayStorage<2> storage_order = blitz::ColumnMajorArray<2>();
    blitz::Array<double,2> * sliced = read_2d_slice<double>(filename, Nx, Ny, xrange, zrange, storage_order);

    /* Now, assign the slice to fill the 3D array */
    for(int y = fillme.lbound(Tf::secondDim); y <= fillme.ubound(Tf::secondDim); y++) {
        fillme(xrange,y,zrange) = (*sliced)(xrange,zrange);
    }
    delete sliced; 
}

