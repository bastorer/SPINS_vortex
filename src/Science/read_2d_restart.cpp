#include "../Science.hpp"
#include "../Split_reader.hpp"

using blitz::Array;
namespace Tf = Transformer;

// Read in a 2D-array from file and extend it to fill a full, 3D array in
// memory.  Unlike read_2d_slice, this uses the standard C storage
// order -- matlab uses the transpose of column-major ordering
void read_2d_restart(Array<double,3> & fillme, const char * filename, int Nx, int Ny) {

    /* Get the local ranges we're interested in */
    blitz::Range xrange(fillme.lbound(Tf::firstDim), fillme.ubound(Tf::firstDim));
    blitz::Range zrange(fillme.lbound(Tf::thirdDim), fillme.ubound(Tf::thirdDim));

    /* Read the 2D slice from disk.  Matlab uses Column-Major array storage */

    blitz::GeneralArrayStorage<2> storage_order;
    blitz::Array<double,2> * sliced =  read_2d_slice<double>(filename, Nx, Ny, xrange, zrange, storage_order);

    /* Now, assign the slice to fill the 3D array */
    for(int y = fillme.lbound(Tf::secondDim); y <= fillme.ubound(Tf::secondDim); y++) {
        fillme(xrange,y,zrange) = (*sliced)(xrange, zrange);
    }
    delete sliced; 
}

