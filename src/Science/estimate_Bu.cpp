#include "../Science.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

void estimate_bu(double * Bu, DTArray & b, DTArray & u, DTArray & v, DTArray & w,
        DTArray & temp_a, DTArray & temp_b, DTArray & temp_c, double rho0,
        double Lx, double Ly, double Lz, double N0, double f0, const char * type_x, const char * type_y, MPI_Comm c) {

    int Nx, Ny, Nz, N, numproc, ub, lb, my_rank;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(b.extent(Tf::firstDim),c);
    Ny = b.extent(Tf::secondDim);
    Nz = b.extent(Tf::thirdDim);

    Tf::S_EXP x_form = (type_x == "PERIODIC") ? Tf::FOURIER : Tf::COSINE;
    Tf::S_EXP y_form = (type_y == "PERIODIC") ? Tf::FOURIER : Tf::COSINE;

    static Tf::Trans1D trans_x(Nx, Ny, Nz, Tf::firstDim,  x_form);
    static Tf::Trans1D trans_y(Nx, Ny, Nz, Tf::secondDim, y_form);
    static Tf::Trans1D trans_z(Nx, Ny, Nz, Tf::thirdDim,  Tf::COSINE);

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    // b_x
    if (Nx > 1) {
        if (x_form == Tf::FOURIER) { deriv_fft(b,trans_x,temp_a); temp_a *= 2*M_PI/Lx; }
        else if (x_form == Tf::COSINE) { deriv_dct(b,trans_x,temp_a); temp_a *= M_PI/Lx; }
    }

    // b_y
    if (Ny > 1) {
        if (y_form == Tf::FOURIER) { deriv_fft(b,trans_y,temp_b); temp_b *= 2*M_PI/Ly; }
        else if (y_form == Tf::COSINE) { deriv_dct(b,trans_y,temp_b); temp_b *= M_PI/Ly; }
    }

    // | del_H ( b ) |
    temp_a = pow(temp_a*temp_a + temp_b*temp_b , 0.5);

    // b_z
    deriv_dct(b,trans_z,temp_b);
    temp_b *= M_PI/Lz;

    // double bu_mu = pow((N0/f0)*pssum(sum(temp_a))/pssum(sum(abs(temp_b))), 2.0);

    double Linv = pssum(sum((temp_a)*
                (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
                      + ( 0.5*rho0*b*b/N0/N0 ))))
        /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));
    double Hinv = pssum(sum((abs(temp_b))*
                (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
                      + ( 0.5*rho0*b*b/N0/N0 ))))
        /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));

    double bu_mu = pow((N0/f0)*Linv/Hinv, 2.0);

    // ( (N0/f0) * | del_H ( b ) | / | d_z ( b ) | ) ^ 2
    //temp_c = pow((N0/f0)*temp_a(ii,jj,kk)/(abs(temp_b(ii,jj,kk)) + 1e-10), 2.0);

    //double bu_mu = pssum(sum(temp_a))/(Nx*Ny*Nz);
    //double bu_mu = pssum(sum((temp_c)*
    //            (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
    //                  + ( 0.5*rho0*b*b/N0/N0 ))))
    //    /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));

    // Now compute deviation
    //temp_b = pow(temp_c(ii,jj,kk) - bu_mu, 2.0);

    //double bu_sigma = pow(pssum(sum(temp_b))/(Nx*Ny*Nz), 0.5);

    *Bu = bu_mu;
}
