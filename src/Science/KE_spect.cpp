#include "../Science.hpp"


// This code is a bit lengthy, so I'm going to add some discussion here about the general idea
//
// Purpose: this code produces power spectra of KE, assuming that NONE OF THE DIRECTIONS ARE CHEB
//          in total, three spectra are returned: 
//                  1) spect  : depth averaged and azimuthally integrated (i.e. function of kr only)
//                  2) spect_x: depth averaged and integrated in y (i.e. function of kx only)
//                  3) spect_y: depth averaged and integrated in x (i.e. function of ky only)
//          Since we are computing spectra, we can also measure anisotropy
//
// Method:
//      For each of u, v, w, do the following:
//          1) transform into spectral space using the appropriate transform type
//          2) loop through each point in spectral space
//              a) compute the corresponding wavenumber (accounting for the DFT, DCT, DST differences)
//              b) determine the corresponding index in the spectrum vector
//              c) add the contribution (following Parceval's theorem)
//                  i) do this for the azimuthally-, x-, and y- integrated spectra
//
//      For anisotropy:
//          While computing the integrated spectra, also compute one that is just depth-integrated
//              (i.e. retains x-y information). This will be stored in temp, which is a 3D DTArray
//          For a given circle (kx^2 + ky^2 = const.), we can then compare the mean (mu) with the standard
//              deviation (sigma) in power. The ration sigma / mu is then our (dimensionless) measure of 
//              anisotropy

using namespace TArrayn;
using namespace NSIntegrator;
using namespace Transformer;

//namespace TA = TArrayn;
//namespace Tf = Transformer;

// Compute the horizontal KE spectrum
void KE_spect(double * aniso, double * aniso_array, DTArray & u, DTArray & v, DTArray & w, DTArray & temp,
        double ** spect_2d, double * spect, double * spect_x, double * spect_y,
        double Lx, double Ly, const char * type_x, const char * type_y, MPI_Comm c) {

    bool debug = false;

    if (debug and master()) { fprintf(stdout, "Entering KE_spect\n");  }
    
    // Start by extracting problem information
    int Nx, Ny, Nz, N, numproc, ub, lb, my_rank;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(v.extent(firstDim),c);
    Ny = v.extent(secondDim);
    Nz = v.extent(thirdDim);

    // Declarations for internal variables
    S_EXP x_form, y_form;

    CTArray * var_hat_c;
    DTArray * var_hat_r;
    bool is_complex;
    int Ilb, Iub, Jlb, Jub, Klb, Kub;
    double norm_x, norm_y;
    double kxmax, kymax, kmax;
    double kx, ky, dk, dkx, dky;
    bool chopped_x, chopped_y;
    double mult_x, mult_y, curr_cnt;

    double kh;
    int tmpII, tmpJJ;
    
    ////
    //// Arrays for storing the many and several spectra
    ////
    N = min(Nx,Ny);
    int N_spect  = static_cast<int>(N/2);
    int Nx_spect = static_cast<int>(Nx/2);
    int Ny_spect = static_cast<int>(Ny/2);

    // Azimuthally integrated spectrum
    double ** ans = new double*[N_spect];
    double * cnt_u = new double[N_spect];
    double * cnt_v = new double[N_spect];
    double * cnt_w = new double[N_spect];

    // x spectrum
    double ** ans_x = new double*[Nx_spect];
    double * cnt_x_u = new double[Nx_spect];
    double * cnt_x_v = new double[Nx_spect];
    double * cnt_x_w = new double[Nx_spect];

    // y spectrum
    double ** ans_y = new double*[Ny_spect];
    double * cnt_y_u = new double[Ny_spect];
    double * cnt_y_v = new double[Ny_spect];
    double * cnt_y_w = new double[Ny_spect];

    for (int II = 0; II < N_spect; II++) {  ans[II] = new double[Nz]; }
    for (int II = 0; II < Nx_spect; II++) { ans_x[II] = new double[Nz];  }
    for (int II = 0; II < Ny_spect; II++) { ans_y[II] = new double[Nz];  }

    x_form = (type_x == "PERIODIC") ? FOURIER : SINE;
    y_form = (type_y == "PERIODIC") ? FOURIER : COSINE;

    norm_x = (x_form == FOURIER) ? 2*M_PI/Lx : M_PI/Lx;
    norm_y = (y_form == FOURIER) ? 2*M_PI/Ly : M_PI/Ly;

    //
    ////
    ////// First, we deal with u, and incorporate it's portion
    /////  into the 2D spectrum.
    ////
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  preparing u transforms\n");  }

    // Create the forward transformation wrapper and compute the wave numbers
    static TransWrapper XY_xform_u = TransWrapper(Nx, Ny, Nz, x_form, y_form, NONE);

    kxmax = XY_xform_u.max_wavenum(firstDim)*norm_x;
    kymax = XY_xform_u.max_wavenum(secondDim)*norm_y;
    kmax = max(kxmax,kymax);
    dkx = kxmax/(Nx_spect);
    dky = kymax/(Ny_spect);
    dk  = kmax/(N_spect);

    // Some awkwardness because we need to handle real and complex arrays independently
    is_complex = ( (x_form == FOURIER) or (y_form == FOURIER) );
    XY_xform_u.forward_transform(&u, x_form, y_form, NONE);
    if ( is_complex ) {
        var_hat_c = XY_xform_u.get_complex_temp();
    } else {
        var_hat_r = XY_xform_u.get_real_temp();
    }
    Ilb = is_complex ? (*var_hat_c).lbound(firstDim) : (*var_hat_r).lbound(firstDim);
    Iub = is_complex ? (*var_hat_c).ubound(firstDim) : (*var_hat_r).ubound(firstDim);
    Jlb = is_complex ? (*var_hat_c).lbound(secondDim) : (*var_hat_r).lbound(secondDim);
    Jub = is_complex ? (*var_hat_c).ubound(secondDim) : (*var_hat_r).ubound(secondDim);
    Klb = is_complex ? (*var_hat_c).lbound(thirdDim) : (*var_hat_r).lbound(thirdDim);
    Kub = is_complex ? (*var_hat_c).ubound(thirdDim) : (*var_hat_r).ubound(thirdDim);

    Array<double,1> kvec = XY_xform_u.wavenums(firstDim),
        lvec = XY_xform_u.wavenums(secondDim);
    blitz::firstIndex ii;
    blitz::secondIndex jj;

    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    chopped_y = (y_form == FOURIER);
    chopped_x = (y_form != FOURIER) and (x_form == FOURIER);

    // To make life interesting, the DST and DCT have extra multiplicative factors
    // that need to be accounted for depending on the wavenumber. mult_x and mult_y
    // are going to do just that.

    // Zero everything out before we start
    for (int II = 0; II < N_spect; II++) {
        for (int KK = 0; KK < Nz; KK++) {
            ans[II][KK] = 0.;
            ans_y[II][KK] = 0.;
        }
        cnt_u[II] = 0.;
    }
    for (int II = 0; II < Nx_spect; II++) {
        for (int KK = 0; KK < Nz; KK++) {
            ans_x[II][KK] = 0.;
        }
        cnt_x_u[II] = 0.;
    }
    for (int II = 0; II < Ny_spect; II++) {
        for (int KK = 0; KK < Nz; KK++) {
            ans_y[II][KK] = 0.;
        }
        cnt_y_u[II] = 0.;
    }
    curr_cnt = 1.;

    // First loop: through the first index of the transformed field
    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        mult_x = 1.;
        if ((x_form == SINE) or (x_form == COSINE)) {
            mult_x = 0.5;
            if ((I == 0) and (x_form == COSINE)) {mult_x *= 0.5;}
        } else if (chopped_x) {
            if ((kvec(I) == 0) or (kx == kxmax)) {
                mult_x *= 1.; curr_cnt = 1.;}
            else {  mult_x *= 2.; curr_cnt = 2.;}
        }

        // Second loop: through the second index of the transformed field
        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            mult_y = 1.;
            if ((y_form == SINE) or (y_form == COSINE)) {
                mult_y = 0.5;
                if ((J == 0) and (y_form == COSINE)) {mult_y *= 0.5;}
            } else if (chopped_y) {
                if ((lvec(J) == 0) or (ky == kymax)) {
                    mult_y *= 1.; curr_cnt = 1.;}
                else {  mult_y *= 2.; curr_cnt = 2.;}
            }

            // 2D spectrum
            for (int K = Klb; K<= Kub; K++) {
                if (is_complex) { temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else {            temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
            }

            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_u[tmpII] += curr_cnt;
            }

            // x spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_x_u[tmpII] += curr_cnt;
            }

            // y spectrum
            tmpII = floor(abs(ky)/dky + 0.5);
            if (tmpII >= Ny_spect) { tmpII = Ny_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_y_u[tmpII] += curr_cnt;
            }
        }
    }

    //
    ////
    ////// Next, we deal with v, and incorporate it's portion
    /////  into the 2D spectrum.
    ////
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  preparing v transforms\n");  }

    // Create the forward transformation wrapper
    x_form = (type_x == "PERIODIC") ? FOURIER : COSINE;
    y_form = (type_y == "PERIODIC") ? FOURIER : SINE;

    static TransWrapper XY_xform_v = TransWrapper(Nx, Ny, Nz, x_form, y_form, NONE);

    // Some awkwardness because we need to handle real and complex arrays independently
    is_complex = ( (x_form == FOURIER) or (y_form == FOURIER) );
    XY_xform_v.forward_transform(&v, x_form, y_form, NONE);
    if ( is_complex ) {
        var_hat_c = XY_xform_v.get_complex_temp();
    } else {
        var_hat_r = XY_xform_v.get_real_temp();
    }
    Ilb = is_complex ? (*var_hat_c).lbound(firstDim) : (*var_hat_r).lbound(firstDim);
    Iub = is_complex ? (*var_hat_c).ubound(firstDim) : (*var_hat_r).ubound(firstDim);
    Jlb = is_complex ? (*var_hat_c).lbound(secondDim) : (*var_hat_r).lbound(secondDim);
    Jub = is_complex ? (*var_hat_c).ubound(secondDim) : (*var_hat_r).ubound(secondDim);
    Klb = is_complex ? (*var_hat_c).lbound(thirdDim) : (*var_hat_r).lbound(thirdDim);
    Kub = is_complex ? (*var_hat_c).ubound(thirdDim) : (*var_hat_r).ubound(thirdDim);

    kvec = XY_xform_v.wavenums(firstDim);
    lvec = XY_xform_v.wavenums(secondDim);


    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    chopped_y = (y_form == FOURIER);
    chopped_x = (y_form != FOURIER) and (x_form == FOURIER);

    // To make life interesting, the DST and DCT have extra multiplicative factors
    // that need to be accounted for depending on the wavenumber. mult_x and mult_y
    // are going to do just that.

    for (int II = 0; II < N_spect; II++) {  cnt_v[II] = 0.; }
    for (int II = 0; II < Nx_spect; II++) { cnt_x_v[II] = 0.; }
    for (int II = 0; II < Ny_spect; II++) { cnt_y_v[II] = 0.; }

    // First loop: through the first index of the transformed field
    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        mult_x = 1.;
        if ((x_form == SINE) or (x_form == COSINE)) {
            mult_x = 0.5;
            if ((I == 0) and (x_form == COSINE)) {mult_x *= 0.5;}
        } else if (chopped_x) {
            if ((kvec(I) == 0) or (kx == kxmax)) {
                mult_x *= 1.; curr_cnt = 1.;}
            else {  mult_x *= 2.; curr_cnt = 2.;}
        }

        // Second loop: through the second index of the transformed field
        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            mult_y = 1.;
            if ((y_form == SINE) or (y_form == COSINE)) {
                mult_y = 0.5;
                if ((J == 0) and (y_form == COSINE)) {mult_y *= 0.5;}
            } else if (chopped_y) {
                if ((lvec(J) == 0) or (ky == kymax)) {
                    mult_y *= 1.; curr_cnt = 1.;}
                else {  mult_y *= 2.; curr_cnt = 2.;}
            }

            // 2D spectrum
            for (int K = Klb; K<= Kub; K++) {
                if (is_complex) { temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else {            temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
            }

            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_v[tmpII] += curr_cnt;
            }

            // x spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_x_v[tmpII] += curr_cnt;
            }

            // y spectrum
            tmpII = floor(abs(ky)/dky + 0.5);
            if (tmpII >= Ny_spect) { tmpII = Ny_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_y_v[tmpII] += curr_cnt;
            }
        }
    }

    //
    ////
    ////// Next, we deal with w, and incorporate it's portion
    /////  into the 2D spectrum.
    ////
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  preparing w transforms\n");  }

    // Create the forward transformation wrapper
    x_form = (type_x == "PERIODIC") ? FOURIER : COSINE;
    y_form = (type_y == "PERIODIC") ? FOURIER : COSINE;

    static TransWrapper XY_xform_w = TransWrapper(Nx, Ny, Nz, x_form, y_form, NONE);

    // Some awkwardness because we need to handle real and complex arrays independently
    is_complex = ( (x_form == FOURIER) or (y_form == FOURIER) );
    XY_xform_w.forward_transform(&w, x_form, y_form, NONE);
    if ( is_complex ) {
        var_hat_c = XY_xform_w.get_complex_temp();
    } else {
        var_hat_r = XY_xform_w.get_real_temp();
    }
    Ilb = is_complex ? (*var_hat_c).lbound(firstDim) : (*var_hat_r).lbound(firstDim);
    Iub = is_complex ? (*var_hat_c).ubound(firstDim) : (*var_hat_r).ubound(firstDim);
    Jlb = is_complex ? (*var_hat_c).lbound(secondDim) : (*var_hat_r).lbound(secondDim);
    Jub = is_complex ? (*var_hat_c).ubound(secondDim) : (*var_hat_r).ubound(secondDim);
    Klb = is_complex ? (*var_hat_c).lbound(thirdDim) : (*var_hat_r).lbound(thirdDim);
    Kub = is_complex ? (*var_hat_c).ubound(thirdDim) : (*var_hat_r).ubound(thirdDim);

    kvec = XY_xform_w.wavenums(firstDim);
    lvec = XY_xform_w.wavenums(secondDim);

    // In the event that at least one horizontal dimension is periodic,
    // the tranformer will throw away half -1 of the wavenumbers
    // due to non-uniqueness. We need to deal with that now.
    chopped_y = (y_form == FOURIER);
    chopped_x = (y_form != FOURIER) and (x_form == FOURIER);

    // To make life interesting, the DST and DCT have extra multiplicative factors
    // that need to be accounted for depending on the wavenumber. mult_x and mult_y
    // are going to do just that.

    for (int II = 0; II < N_spect; II++) {  cnt_w[II] = 0.;   }
    for (int II = 0; II < Nx_spect; II++) { cnt_x_w[II] = 0.; }
    for (int II = 0; II < Ny_spect; II++) { cnt_y_w[II] = 0.; }

    // First loop: through the first index of the transformed field
    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        mult_x = 1.;
        if ((x_form == SINE) or (x_form == COSINE)) {
            mult_x = 0.5;
            if ((I == 0) and (x_form == COSINE)) {mult_x *= 0.5;}
        } else if (chopped_x) {
            if ((kvec(I) == 0) or (kx == kxmax)) {
                mult_x *= 1.; curr_cnt = 1.;}
            else {  mult_x *= 2.; curr_cnt = 2.;}
        }

        // Second loop: through the second index of the transformed field
        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            if ((y_form == SINE) and (lvec(J) == Ny)) { ky = 0. ;} // This makes DSTs and DCTs play well together
            mult_y = 1.;
            if ((y_form == SINE) or (y_form == COSINE)) {
                mult_y = 0.5;
                if ((J == 0) and (y_form == COSINE)) {mult_y *= 0.5;}
            } else if (chopped_y) {
                if ((lvec(J) == 0) or (ky == kymax)) {
                    mult_y *= 1.; curr_cnt = 1.;}
                else {  mult_y *= 2.; curr_cnt = 2.;}
            }

            // 2D spectrum
            for (int K = Klb; K<= Kub; K++) {
                if (is_complex) { temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else {            temp(I,J,K) += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
            }

            // Azimuthally integrated spectrum
            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if (tmpII >= N_spect) { tmpII = N_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_w[tmpII] += curr_cnt;
            }

            // x spectrum
            tmpII = floor(abs(kx)/dkx + 0.5);
            if (tmpII >= Nx_spect) { tmpII = Nx_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_x[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_x_w[tmpII] += curr_cnt;
            }

            // y spectrum
            tmpII = floor(abs(ky)/dky + 0.5);
            if (tmpII >= Ny_spect) { tmpII = Ny_spect - 1; }
            for (int K = Klb; K <= Kub; K++) {
                if (is_complex) { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_c)(I,J,K)), 2.0); }
                else { ans_y[tmpII][K] += mult_x*mult_y*pow(abs((*var_hat_r)(I,J,K)), 2.0); }
                cnt_y_w[tmpII] += curr_cnt;
            }
        }
    }

    //
    ////
    ////// Now that the transforms are done
    /////  we communicate to collect.
    ////
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  preparing to collect results\n");  }

    // Azimuthally integrated spectrum
    double ** full_ans = new double*[N_spect];
    double * full_cnt_u = new double[N_spect];
    double * full_cnt_v = new double[N_spect];
    double * full_cnt_w = new double[N_spect];
    for (int II = 0; II < N_spect; II++) {
        full_ans[II] = new double[Nz];
        MPI_Allreduce(ans[II], full_ans[II], Nz, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_u, full_cnt_u, N_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_v, full_cnt_v, N_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_w, full_cnt_w, N_spect, MPI_DOUBLE, MPI_SUM, c);

    // x spectrum
    double ** full_ans_x = new double*[Nx_spect];
    double * full_cnt_x_u = new double[Nx_spect];
    double * full_cnt_x_v = new double[Nx_spect];
    double * full_cnt_x_w = new double[Nx_spect];
    for (int II = 0; II < Nx_spect; II++) {
        full_ans_x[II] = new double[Nz];
        MPI_Allreduce(ans_x[II], full_ans_x[II], Nz, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_x_u, full_cnt_x_u, Nx_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_x_v, full_cnt_x_v, Nx_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_x_w, full_cnt_x_w, Nx_spect, MPI_DOUBLE, MPI_SUM, c);

    // y_spectrum
    double ** full_ans_y = new double*[Ny_spect];
    double * full_cnt_y_u = new double[Ny_spect];
    double * full_cnt_y_v = new double[Ny_spect];
    double * full_cnt_y_w = new double[Ny_spect];
    for (int II = 0; II < Ny_spect; II++) {
        full_ans_y[II] = new double[Nz];
        MPI_Allreduce(ans_y[II], full_ans_y[II], Nz, MPI_DOUBLE, MPI_SUM, c);
    }
    MPI_Allreduce(cnt_y_u, full_cnt_y_u, Ny_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_y_v, full_cnt_y_v, Ny_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(cnt_y_w, full_cnt_y_w, Ny_spect, MPI_DOUBLE, MPI_SUM, c);

    // Confirm that the total number of binned wavenumbers equals
    // the total number of wavenumbers.
    if (master()) {
        double check_u = 0.; 
        double check_v = 0.; 
        double check_w = 0.; 
        double check_x_u = 0.;
        double check_x_v = 0.;
        double check_x_w = 0.;
        double check_y_u = 0.;
        double check_y_v = 0.;
        double check_y_w = 0.;
        for (int II = 0; II < N_spect; II++) { 
            check_u   += full_cnt_u[II];
            check_v   += full_cnt_v[II];
            check_w   += full_cnt_w[II];
        }
        for (int II = 0; II < Nx_spect; II++) { 
            check_x_u += full_cnt_x_u[II];
            check_x_v += full_cnt_x_v[II];
            check_x_w += full_cnt_x_w[II];
        }
        for (int II = 0; II < Ny_spect; II++) { 
            check_y_u += full_cnt_y_u[II];
            check_y_v += full_cnt_y_v[II];
            check_y_w += full_cnt_y_w[II];
        }

        if ( (abs(check_u   - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_u is wrong! %.13g instead of %d\n", check_u, Nx*Ny*Nz); }
        if ( (abs(check_v   - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_v is wrong! %.13g instead of %d\n", check_v, Nx*Ny*Nz); }
        if ( (abs(check_w   - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_w is wrong! %.13g instead of %d\n", check_w, Nx*Ny*Nz); }

        if ((abs(check_x_u - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_u is wrong! %.13g instead of %d\n", check_x_u, Nx*Ny*Nz); }
        if ((abs(check_x_v - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_v is wrong! %.13g instead of %d\n", check_x_v, Nx*Ny*Nz); }
        if ((abs(check_x_w - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_w is wrong! %.13g instead of %d\n", check_x_w, Nx*Ny*Nz); }

        if ((abs(check_y_u - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_u is wrong! %.13g instead of %d\n", check_y_u, Nx*Ny*Nz); }
        if ((abs(check_y_v - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_v is wrong! %.13g instead of %d\n", check_y_v, Nx*Ny*Nz); }
        if ((abs(check_y_w - (Nx*Ny*Nz)) > 0.1) ) 
        { fprintf(stderr, "Count_x_w is wrong! %.13g instead of %d\n", check_y_w, Nx*Ny*Nz); }
    }

    //
    //// Pass back the depth-averaged answers
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  returning the results\n");  }

    for (int II = 0; II < N_spect; II++) {
        spect[II] = 0.;
        for (int KK = 0; KK < Nz; KK++) {
            spect[II] += full_ans[II][KK];
        }
    }

    for (int II = 0; II < Nx_spect; II++) {
        spect_x[II] = 0.;
        for (int KK = 0; KK < Nz; KK++) {
            spect_x[II] += full_ans_x[II][KK];
        }
    }
    for (int II = 0; II < Ny_spect; II++) {
        spect_y[II] = 0.;
        for (int KK = 0; KK < Nz; KK++) {
            spect_y[II] += full_ans_y[II][KK];
        }
    }

    //
    //// Compute the anisotropy
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  computing anisotropy\n");  }
    
    double the_sum = 0.;
    double the_cnt = 0.;
    double *aniso_tmp = new double[N_spect];
    double *aniso_cnt = new double[N_spect];

    double the_sum_full = 0.;
    double the_cnt_full = 0.;
    double *aniso_tmp_full = new double[N_spect];
    double *aniso_cnt_full = new double[N_spect];

    double mean_val, spect_val;

    int Ix, Iy;

    // Zero out the array
    for (int II = 0; II < N_spect; II++) {
        aniso_tmp[II] = 0.;
        aniso_cnt[II] = 0.;
    }

    for (int I = Ilb; I <= Iub; I++) {

        kx = norm_x*kvec(I);
        Ix = floor(abs(kx)/dkx + 0.5);
        if ( Ix >= Nx_spect)   { Ix = Nx_spect - 1; }

        for (int J = Jlb; J <= Jub; J++) {

            ky = norm_y*lvec(J);
            Iy = floor(abs(ky)/dky + 0.5);
            if ( Iy >= Ny_spect)   { Iy = Ny_spect - 1; }

            kh = pow(kx*kx + ky*ky, 0.5);
            tmpII = floor(kh/dk + 0.5);
            if ( tmpII >= N_spect) { tmpII = N_spect - 1; }

            for (int K = 0; K < Nz; K++) {
                mean_val = full_ans[tmpII][K]/(full_cnt_u[tmpII]/Nz);
                spect_val = temp(I,J,K);

                the_sum          += pow(spect_val - mean_val,   2.0);
                aniso_tmp[tmpII] += pow(spect_val/mean_val - 1, 2.0);

                the_cnt          += 1;
                aniso_cnt[tmpII] += 1;
            }
        }
    }

    MPI_Allreduce(aniso_tmp, aniso_tmp_full, N_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(aniso_cnt, aniso_cnt_full, N_spect, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(&the_sum,  &the_sum_full, 1, MPI_DOUBLE, MPI_SUM, c);
    MPI_Allreduce(&the_cnt,  &the_cnt_full, 1, MPI_DOUBLE, MPI_SUM, c);

    for (int II = 0; II < N_spect; II++) {
        // Divide by the count before computing the square root
        aniso_array[II] = (aniso_cnt[II] == 0) ? 0. : pow(aniso_tmp[II]/aniso_cnt[II],0.5);
    }
    if (the_cnt_full != 0.) { *aniso = pow(the_sum_full/the_cnt_full, 0.5); }

    //
    //// Now delete all of the arrays that we have created
    //
    if (debug and master()) { fprintf(stdout, "  KE_spect:  deleting arrays\n");  }

    delete[] aniso_tmp;
    delete[] aniso_cnt;
    delete[] aniso_tmp_full;
    delete[] aniso_cnt_full;

    for (int II = 0; II < N_spect; II++) {
        delete[] ans[II];
        delete[] full_ans[II];
    }

    for (int II = 0; II < Nx_spect; II++) {
        delete[] ans_x[II];
        delete[] full_ans_x[II];
    }

    for (int II = 0; II < Ny_spect; II++) {
        delete[] ans_y[II];
        delete[] full_ans_y[II];
    }

    delete[] full_cnt_u;
    delete[] full_cnt_v;
    delete[] full_cnt_w;
    delete[] cnt_u;
    delete[] cnt_v;
    delete[] cnt_w;

    delete[] full_cnt_x_u;
    delete[] full_cnt_x_v;
    delete[] full_cnt_x_w;
    delete[] cnt_x_u;
    delete[] cnt_x_v;
    delete[] cnt_x_w;

    delete[] full_cnt_y_u;
    delete[] full_cnt_y_v;
    delete[] full_cnt_y_w;
    delete[] cnt_y_u;
    delete[] cnt_y_v;
    delete[] cnt_y_w;

    delete[] ans;
    delete[] ans_x;
    delete[] ans_y;
    delete[] full_ans;
    delete[] full_ans_x;
    delete[] full_ans_y;

    if (debug and master()) { fprintf(stdout, "  KE_spect:  finished\n");  }
}

