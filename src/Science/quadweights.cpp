
#include "../Science.hpp"
#include "../T_util.hpp"
#include "../TArray.hpp"
#include <blitz/array.h>
#include <blitz/tinyvec-et.h>
#include "math.h"
#include "../Par_util.hpp"
#include "stdio.h"
#include "../Split_reader.hpp"
#include "../Parformer.hpp"
#include <stdlib.h>
#include <complex>
#include <string>


using blitz::Array;
using blitz::cos;
using namespace TArrayn;
using namespace NSIntegrator;
using namespace Transformer;

namespace Tf  = Transformer;
namespace NSI = NSIntegrator;

// Global arrays to store quadrature weights
Array<double,1> _quadw_x, _quadw_y, _quadw_z;

const blitz::Array<double,1> * get_quad_x() { 
   // Check whether the quad weight has been initialized
   if (_quadw_x.length(Tf::firstDim) <= 0) {
      assert(0 && "Error: quadrature weights were not initalized before use");
   }
   return &_quadw_x;
}

const blitz::Array<double,1> * get_quad_y() {
   if (_quadw_y.length(Tf::firstDim) <= 0) {
      assert(0 && "Error: quadrature weights were not initalized before use");
   }
   return &_quadw_y;
}

const blitz::Array<double,1> * get_quad_z() {
   if (_quadw_z.length(Tf::firstDim) <= 0) {
      assert(0 && "Error: quadrature weights were not initalized before use");
   }
   return &_quadw_z;
}

// Compute quadrature weights
void compute_quadweights(int szx, int szy, int szz, 
        double Lx, double Ly, double Lz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z) {

    _quadw_x.resize(split_range(szx));
    _quadw_y.resize(szy); 
    _quadw_z.resize(szz);

    if (DIM_X == NSI::NO_SLIP) {
        blitz::firstIndex ii;
        _quadw_x = 1;
        for (int k = 1; k <= (szx-2)/2; k++) {
            // From Trefethen, Spectral Methods in MATLAB
            // clenshaw-curtis quadrature weights
            _quadw_x -= 2*cos(2*k*M_PI*ii/(szx-1))/(4*k*k-1);
        }
        if ((szx%2))
            _quadw_x -= cos(M_PI*ii)/((szx-1)*(szx-1)-1);
        _quadw_x = 2*_quadw_x/(szx-1);
        if (_quadw_x.lbound(firstDim) == 0) {
            _quadw_x(0) = 1.0/(szx-1)/(szx-1);
        }
        if (_quadw_x.ubound(firstDim) == (szx-1)) {
            _quadw_x(szx-1) = 1.0/(szx-1)/(szx-1);
        }
        _quadw_x *= Lx/2;
    } else {
        // Trapezoid rule
        _quadw_x = Lx/szx;
    }
    if (DIM_Y == NSI::NO_SLIP) {
        blitz::firstIndex ii;
        _quadw_y = 1;
        for (int k = 1; k <= (szy-2)/2; k++) {
            // From Trefethen, Spectral Methods in MATLAB
            // clenshaw-curtis quadrature weights
            _quadw_y -= 2*cos(2*k*(M_PI*(ii)/(szy-1)))/(4*k*k-1);
        }
        if ((szy%2))
            _quadw_y -= cos(M_PI*ii)/((szy-1)*(szy-1)-1);
        _quadw_y = 2*_quadw_y/(szy-1);
        _quadw_y(0) = 1.0/(szy-1)/(szy-1);
        _quadw_y(szy-1) = 1.0/(szy-1)/(szy-1);
        _quadw_y *= Ly/2;
    } else {
        // Trapezoid rule
        _quadw_y = Ly/szy;
    }
    if (DIM_Z == NSI::NO_SLIP) {
        blitz::firstIndex ii;
        _quadw_z = 1;
        for (int k = 1; k <= (szz-2)/2; k++) {
            // From Trefethen, Spectral Methods in MATLAB
            // clenshaw-curtis quadrature weights
            _quadw_z -= 2*cos(2*k*(M_PI*(ii)/(szz-1)))/(4*k*k-1);
        }
        if ((szz%2))
            _quadw_z -= cos(M_PI*ii)/((szz-1)*(szz-1)-1);
        _quadw_z = 2*_quadw_z/(szz-1);
        _quadw_z(0) = 1.0/(szz-1)/(szz-1);
        _quadw_z(szz-1) = 1.0/(szz-1)/(szz-1);
        _quadw_z *= Lz/2;
    } else {
        // Trapezoid rule
        _quadw_z = Lz/szz;
    }
}

/*
void dpsidz(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz) {

    //BAS: At the moment, hardcoded for free slip
    static NSIntegrator::DIMTYPE DIM_Z = FREE_SLIP;
    static Transformer::Trans1D trans_z (szx,szy,szz,thirdDim, (DIM_Z == PERIODIC ? FOURIER : REAL));

    if (DIM_Z == FREE_SLIP) {
        deriv_dct(psi,trans_z,*out);
        *out *= M_PI/Lz;
    }
    return;
}

void dpsidx(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz) {

    //BAS: At the moment, hardcoded for periodic
    static NSIntegrator::DIMTYPE DIM_X = PERIODIC;
    static Transformer::Trans1D trans_x (szx,szy,szz,firstDim, (DIM_X == PERIODIC ? FOURIER : REAL));

    if (DIM_X == PERIODIC) {
        deriv_fft(psi,trans_x,*out);
        *out *= 2*M_PI/Lx;
    } 
    return;
}

void dpsidy(TArrayn::DTArray & psi, TArrayn::DTArray * & out,
          double Lx, double Ly, double Lz, int szx, int szy, int szz) {

    //BAS: At the moment, hardcoded for periodic
    static NSIntegrator::DIMTYPE DIM_Y = PERIODIC;
    static Transformer::Trans1D trans_y (szx,szy,szz,secondDim, (DIM_Y == PERIODIC ? FOURIER : REAL));

    if (DIM_Y == PERIODIC) {
        deriv_fft(psi,trans_y,*out);
        *out *= 2*M_PI/Ly;
    } 
    return;
}
*/
