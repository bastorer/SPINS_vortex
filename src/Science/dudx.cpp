#include "../Science.hpp"

namespace TA  = TArrayn;
namespace Tf  = Transformer;
namespace NSI = NSIntegrator;

void dudx(TA::DTArray & u, TA::DTArray * & out,
        double Lx, double Ly, double Lz,
        int szx, int szy, int szz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z) {

    static Tf::Trans1D trans_x(szx, szy, szz, Tf::firstDim, (DIM_X == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL));

    if (DIM_X == NSI::PERIODIC) {
        deriv_fft(u,trans_x,*out);
        *out *= 2*M_PI/Lx;
    } else if (DIM_X == NSI::FREE_SLIP) {
        deriv_dst(u,trans_x,*out);
        *out *= M_PI/Lx;
    } else {
        assert(DIM_X == NSI::NO_SLIP);
        deriv_cheb(u,trans_x,*out);
        *out *= -2/Lx;
    }
    return;
}
