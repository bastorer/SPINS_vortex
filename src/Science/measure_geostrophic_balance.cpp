#include "../Science.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

double measure_geostrophic_balance(DTArray * eps_geo, DTArray & p, DTArray & u, DTArray & v, 
        DTArray & w, DTArray & b, DTArray & temp_a, DTArray & temp_b, 
        double rho0, double f0, double N0, double Lx, double Ly,
        MPI_Comm c) {

    // eps_geo = |  f*zeta^z - 1/rho*(dxx+dyy)p   | / ( f|zeta^z| + |1/rho*(dxx+dyy)|  + mu )
    //
    // mu = f*zeta^z_RMS + 1/rho*[(dxx+dyy)p]_RMS

    int Nx, Ny, Nz;
    Nx = pssum(v.extent(Tf::firstDim),c);
    Ny = v.extent(Tf::secondDim);
    Nz = v.extent(Tf::thirdDim);

    // Currently assumes Fourier in the horizontal
    static Tf::Trans1D X_xform(Nx,Ny,Nz,Tf::firstDim,Tf::FOURIER),
        Y_xform(Nx,Ny,Nz,Tf::secondDim,Tf::FOURIER);
    static Tf::TransWrapper XY_xform(Nx, Ny, Nz, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
        Array<double,1> kvec = XY_xform.wavenums(Tf::firstDim),
        lvec = XY_xform.wavenums(Tf::secondDim);
    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    static GeneralArrayStorage<3> spec_ordering;
    static TinyVector<int,3> spec_lbound, spec_extent;
    spec_ordering.ordering() = XY_xform.get_complex_temp()->ordering();
    spec_lbound = XY_xform.get_complex_temp()->lbound();
    spec_extent = XY_xform.get_complex_temp()->extent();

    static TA::CTArray c_temp(spec_lbound, spec_extent, spec_ordering);
    double Norm_x, Norm_y;
    Norm_x = 2.0*M_PI/Lx;
    Norm_y = 2.0*M_PI/Ly;

    // f0*zeta^z = f0*(v_x - u_y) = temp_a
    deriv_fft(v,X_xform,temp_a);
    deriv_fft(u,Y_xform,temp_b);
    temp_a = f0*(temp_a*Norm_x - temp_b*Norm_y);

    // (dxx + dyy)p/rho0 = temp_b
    XY_xform.forward_transform(&p, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    c_temp = *(XY_xform.get_complex_temp());
    *(XY_xform.get_complex_temp()) = -(pow(kvec(ii)*Norm_x, 2.0) + pow(lvec(jj)*Norm_y,2.0) + 0*kk)*c_temp(ii,jj,kk);
    XY_xform.back_transform(&temp_b, Tf::FOURIER, Tf::FOURIER, Tf::NONE);
    temp_b *= 1.0/(XY_xform.norm_factor()*rho0);

    double zeta_rms  = pow(pssum(sum(temp_a*temp_a))/(Nx*Ny*Nz), 0.5);
    double lap_p_rms = pow(pssum(sum(temp_b*temp_b))/(Nx*Ny*Nz), 0.5);

    *eps_geo = abs( temp_a(ii,jj,kk) - temp_b(ii,jj,kk) ) / ( abs(temp_a(ii,jj,kk)) + abs(temp_b(ii,jj,kk)) + zeta_rms + lap_p_rms );

    // Now compute the energy-weighted average of eps_geo
    // This should provide a reasonable scalar measure of 
    // the over-all balance. 
    double en_wei_avg = pssum(sum((*eps_geo)*
                (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
                      + ( 0.5*rho0*b*b/N0/N0 ))))
        /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));
    return en_wei_avg;

}
