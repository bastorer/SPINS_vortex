#include "../Science.hpp"

namespace TA = TArrayn;
namespace Tf = Transformer;

void estimate_Ro(double * Ro, DTArray & b, DTArray & u, DTArray & v, DTArray & w,
        DTArray & temp_a, DTArray & temp_b, DTArray & temp_c, double rho0,
        double Lx, double Ly, double N0, double f0, const char * type_x, const char * type_y, MPI_Comm c) {

    int Nx, Ny, Nz, N, numproc, ub, lb, my_rank;
    MPI_Comm_rank(c,&my_rank);
    MPI_Comm_size(c,&numproc);

    Nx = pssum(b.extent(Tf::firstDim),c);
    Ny = b.extent(Tf::secondDim);
    Nz = b.extent(Tf::thirdDim);

    Tf::S_EXP x_form = (type_x == "PERIODIC") ? Tf::FOURIER : Tf::COSINE;
    Tf::S_EXP y_form = (type_y == "PERIODIC") ? Tf::FOURIER : Tf::COSINE;

    static Tf::Trans1D trans_x(Nx, Ny, Nz, Tf::firstDim,  x_form);
    static Tf::Trans1D trans_y(Nx, Ny, Nz, Tf::secondDim, y_form);

    blitz::firstIndex ii;
    blitz::secondIndex jj;
    blitz::thirdIndex kk;

    if (x_form == Tf::FOURIER) { 
        deriv_fft(v,trans_x,temp_a); 
        deriv_fft(u,trans_y,temp_b);
        temp_a *= 2*M_PI/Lx;
        temp_b *= 2*M_PI/Ly;
    }
    else if (x_form == Tf::COSINE) {
        deriv_dct(v,trans_x,temp_a); 
        deriv_dct(u,trans_y,temp_b);
        temp_a *= M_PI/Lx;
        temp_b *= M_PI/Ly;
    }

    // | del_H ( b ) |
    temp_c = abs(temp_a - temp_b)/f0;

    double Ro_mu = pow(pssum(sum(temp_c*temp_c))/(Nx*Ny,Nz), 0.5);

    //double Ro_mu = pssum(sum((temp_c)*
    //            (     ( 0.5*rho0*(u*u + v*v + w*w) ) 
    //                  + ( 0.5*rho0*b*b/N0/N0 ))))
    //    /pssum(sum( ( 0.5*rho0*(u*u + v*v + w*w) ) + ( 0.5*rho0*b*b/(N0*N0) ) ));

    // Now compute deviation
    //temp_b = pow(temp_c(ii,jj,kk) - Ro_mu, 2.0);

    //double Ro_sigma = pow(pssum(sum(temp_b))/(Nx*Ny*Nz), 0.5);

    *Ro = Ro_mu;
}
