#include "../Science.hpp"

namespace TA  = TArrayn;
namespace Tf  = Transformer;
namespace NSI = NSIntegrator;
namespace bz = blitz;

void vorticity(TA::DTArray & u, TA::DTArray & v, TA::DTArray & w, 
        TA::DTArray * & vor_x, TA::DTArray * & vor_y, TA::DTArray * & vor_z, 
        double Lx, double Ly, double Lz,
        int szx, int szy, int szz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z) {

    static int Nx = 0, Ny = 0, Nz = 0;

    static Tf::Trans1D trans_x(szx, szy, szz, Tf::firstDim, (DIM_X == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL)),
                       trans_y(szx, szy, szz, Tf::secondDim, (DIM_Y == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL)),
                       trans_z(szx, szy, szz, Tf::thirdDim, (DIM_Z == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL));

    static bz::TinyVector<int,3> 
        local_lbounds(alloc_lbound(szx,szy,szz)),
        local_extent(alloc_extent(szx,szy,szz));

    static bz::GeneralArrayStorage<3> 
        local_storage(alloc_storage(szx,szy,szz));

    static TA::DTArray vort_x(local_lbounds,local_extent,local_storage),
                       vort_y(local_lbounds,local_extent,local_storage),
                       vort_z(local_lbounds,local_extent,local_storage),
                       temp_a(local_lbounds,local_extent,local_storage);
    /* Initialization */
    if (Nx == 0 || Ny == 0 || Nz == 0) {
        Nx = szx; Ny = szy; Nz = szz;
    }
    assert (Nx == szx && Ny == szy && Nz == szz);

    /* x-vorticity is w_y - v_z */
    vort_x = 0;
    if (szy > 1) { // w_y
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(w,trans_y,temp_a);
            vort_x = temp_a*(2*M_PI/Ly);
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(w,trans_y,temp_a);
            vort_x = temp_a*(M_PI/Ly);
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(w,trans_y,temp_a);
            vort_x = temp_a*(-2/Ly);
        }
    }
 
    if (szz > 1) { // v_z
        if (DIM_Z == NSI::PERIODIC) {
            deriv_fft(v,trans_z,temp_a);
            vort_x -= temp_a*(2*M_PI/Lz);
        } else if (DIM_Z == NSI::FREE_SLIP) {
            deriv_dct(v,trans_z,temp_a);
            vort_x -= temp_a*(M_PI/Lz);
        } else {
            assert(DIM_Z == NSI::NO_SLIP);
            deriv_cheb(v,trans_z,temp_a);
            vort_x -= temp_a*(-2/Lz);
        }
    }

    //
    // y-vorticity is u_z - w_x
    vort_y = 0;
    if (szz > 1) { // u_z
        if (DIM_Z == NSI::PERIODIC) {
            deriv_fft(u,trans_z,temp_a);
            vort_y = temp_a*(2*M_PI/Lz);
        } else if (DIM_Z == NSI::FREE_SLIP) {
            deriv_dct(u,trans_z,temp_a);
            vort_y = temp_a*(M_PI/Lz);
        } else {
            assert(DIM_Z == NSI::NO_SLIP);
            deriv_cheb(u,trans_z,temp_a);
            vort_y = temp_a*(-2/Lz);
        }
    }
 
    if (szx > 1) { // w_x
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(w,trans_x,temp_a);
            vort_y -= temp_a*(2*M_PI/Lx);
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(w,trans_x,temp_a);
            vort_y -= temp_a*(M_PI/Lx);
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(w,trans_x,temp_a);
            vort_y -= temp_a*(-2/Lx);
        }
    }
    
    //
    // And finally, vort_z is v_x - u_y
    vort_z = 0;
    if (szx > 1) { // v_x
        if (DIM_X == NSI::PERIODIC) {
            deriv_fft(v,trans_x,temp_a);
            vort_z = temp_a*(2*M_PI/Lx);
        } else if (DIM_X == NSI::FREE_SLIP) {
            deriv_dct(v,trans_x,temp_a);
            vort_z = temp_a*(M_PI/Lx);
        } else {
            assert(DIM_X == NSI::NO_SLIP);
            deriv_cheb(v,trans_x,temp_a);
            vort_z = temp_a*(-2/Lx);
        }
    }
 
    if (szy > 1) { // u_y
        if (DIM_Y == NSI::PERIODIC) {
            deriv_fft(u,trans_y,temp_a);
            vort_z -= temp_a*(2*M_PI/Ly);
        } else if (DIM_Y == NSI::FREE_SLIP) {
            deriv_dct(u,trans_y,temp_a);
            vort_z -= temp_a*(M_PI/Ly);
        } else {
            assert(DIM_Y == NSI::NO_SLIP);
            deriv_cheb(u,trans_y,temp_a);
            vort_z -= temp_a*(-2/Ly);
        }
    }

    vor_x = &vort_x;
    vor_y = &vort_y;
    vor_z = &vort_z;

    return;
}
