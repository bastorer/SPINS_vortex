#include "../Science.hpp"

namespace TA  = TArrayn;
namespace Tf  = Transformer;
namespace NSI = NSIntegrator;

void dudy(TA::DTArray & u, TA::DTArray * & out,
        double Lx, double Ly, double Lz,
        int szx, int szy, int szz,
        NSI::DIMTYPE DIM_X, NSI::DIMTYPE DIM_Y, NSI::DIMTYPE DIM_Z) {

    static Tf::Trans1D trans_y(szx, szy, szz, Tf::secondDim, (DIM_Y == NSI::PERIODIC ? Tf::FOURIER : NSI::REAL));

    if (DIM_Y == NSI::PERIODIC) {
        deriv_fft(u,trans_y,*out);
        *out *= 2*M_PI/Ly;
    } else if (DIM_Y == NSI::FREE_SLIP) {
        deriv_dct(u,trans_y,*out);
        *out *= M_PI/Ly;
    } else {
        assert(DIM_Y == NSI::NO_SLIP);
        deriv_cheb(u,trans_y,*out);
        *out *= -2/Ly;
    }
    return;
}

