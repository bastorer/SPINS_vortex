
#ifndef QG_FUNCTIONS_HPP // Prevent double inclusion
#define QG_FUNCTIONS_HPP 1

#include "NSIntegrator.hpp"
#include "Science_qg.hpp"
#include "TArray.hpp"
#include "T_util.hpp"
#include "Par_util.hpp"
#include "Splits.hpp"
#include <iostream>
#include <fstream>
#include <blitz/array.h>
#include <vector>

using namespace TArrayn;
using namespace Transformer;

using blitz::Array;
using blitz::TinyVector;
using blitz::GeneralArrayStorage;

namespace qg_functions {

    // Class to store solution variables
    struct solnclass {

        private:
            TinyVector<int,3> local_lbound, local_extent;
            GeneralArrayStorage<3> local_storage;

            TinyVector<int,3> mlbound, mextent;
            GeneralArrayStorage<3> mstorage;

            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            bool debug;

            blitz::Range all;

            // Grids (for internal use, not saved, vectors only)
            double *xgrid, *ygrid, *zgrid;

            // Some handy indices
            int X_IND, Y_IND, Z_IND;

            // Gradient object
            Grad * gradient_op;
            Trans1D *X_xform, *Y_xform, *Y_xform_bar;
            Trans1D *uX_xform, *uY_xform, *uY_xform_bar;
            double norm_3d, norm_3d_bar;
            double norm_x, norm_y, norm_z;

            // Nonlinear Fields
            vector<DTArray *> q;
            vector<DTArray *> psi;
            vector<DTArray *> u;
            vector<DTArray *> v;
            vector<DTArray *> qx;
            vector<DTArray *> qy;

            // Background State
            vector<DTArray *> ub;
            vector<DTArray *> vb;
            vector<DTArray *> qxb;
            vector<DTArray *> qyb;
            vector<DTArray *> qb;
            vector<DTArray *> psib;

            // Zonal mean fields
            vector<DTArray *> qbar;
            vector<DTArray *> qbar_flux;
            vector<DTArray *> qbary;
            vector<DTArray *> ubar;
            vector<DTArray *> psibar;
            vector<DTArray *> meanvq;

            // Forcing fields
            vector<DTArray *> forcing;
            vector<DTArray *> forcing_bar;
            bool do_forcing;
            double force_time;

            // Some work arrays
            vector<DTArray *> work;
            vector<DTArray *> work_bar;
            double ** yz_slice;
            double ** yz_slice_red;

            // Parameters for timestep
            double dx, dy, dz, t, next_plot_time;
            double w0,w1,w2;
            double dt0;
            double dt1;
            double dt2;
            bool do_plot;

            double U0;
            double V0;

            // Some problem parameters
            double g, H0, beta, N0, f0, tf, kappa;
            double Lr;
            int Nx, Ny, Nz;
            double Lx, Ly, Lz;
            bool restarting;
            TArrayn::S_EXP type_y, type_y_bar, type_z, type_y_u;

            // Filter parameters
            double fstrength, fcutoff, forder; 

            // Diagnostics variables
            FILE * diagnostics;
            ifstream diagnostics_tmp;
            string diag_tmp;
            double max_q, min_q, q_pert_norm, q_norm;
            int iterct;
            double start_clock_time, 
                   curr_clock_time, 
                   start_step_time,
                   step_time;
            bool compute_norms, compute_spectra, compute_aniso;
            double next_diag_time, tdiag;
            double KE, PE, vol, rho0, PE_scale,
                   KE_aniso, PE_aniso, EN_aniso,
                   *KE_aniso_array, *PE_aniso_array, *EN_aniso_array;
            double TE, Bu, Bu_x, Bu_y, KE_x, KE_y;
            double *level_mass;
            double init_mass, curr_mass, curr_circ_N, curr_circ_S;
            double net_transport;
            double enstrophy;
            double include_bg_in_spectra;
            double psibar_bar;

            // Spectra and other vector diagnostics
            int offset_from, offset_to, N_spect, Nx_spect, Ny_spect;
            int prev_chain_write_count, chain_write_count;
            MPI_Status status;
            MPI_File spect_cnt_file;

            // KE spect variables
            double **KE_2d_spect, *KE_1d_spect, *KE_x_spect, *KE_y_spect; 
            MPI_File KE_1d_spect_temp_file, KE_x_spect_temp_file,
                     KE_y_spect_temp_file, KE_2d_spect_temp_file,
                     KE_aniso_temp_file;
            MPI_File KE_1d_spect_final_file, KE_x_spect_final_file,
                     KE_y_spect_final_file, KE_2d_spect_final_file,
                     KE_aniso_final_file;

            // PE spect variables
            double **PE_2d_spect, *PE_1d_spect, *PE_x_spect, *PE_y_spect; 
            MPI_File PE_1d_spect_temp_file, PE_x_spect_temp_file,
                     PE_y_spect_temp_file, PE_2d_spect_temp_file,
                     PE_aniso_temp_file;
            MPI_File PE_1d_spect_final_file, PE_x_spect_final_file,
                     PE_y_spect_final_file, PE_2d_spect_final_file,
                     PE_aniso_final_file;

            // Enstrophy spect variables
            double **EN_2d_spect, *EN_1d_spect, *EN_x_spect, *EN_y_spect; 
            MPI_File EN_1d_spect_temp_file, EN_x_spect_temp_file,
                     EN_y_spect_temp_file, EN_2d_spect_temp_file,
                     EN_aniso_temp_file;
            MPI_File EN_1d_spect_final_file, EN_x_spect_final_file,
                     EN_y_spect_final_file, EN_2d_spect_final_file,
                     EN_aniso_final_file;

            MPI_File level_mass_temp_file, level_mass_final_file;
            MPI_Offset old_chain_len;

            MPI_File ubar_temp_file, ubar_final_file,
                     qbar_temp_file, qbar_final_file,
                     psibar_temp_file, psibar_final_file;
            double *temp_bar;

            string method;

            int my_rank, rank, num_procs;

            bool use_zonal_decomp;


            // Forcing variables
            double (*forcing_field_1)(int x, int y, int z, solnclass & soln);
            double (*forcing_field_2)(int x, int y, int z, solnclass & soln);
            double (*forcing_field_3)(int y, int z, solnclass & soln);
            double (*forcing_field_4)(int y, int z, solnclass & soln);
            double (*duSdt)          (int z, solnclass & soln);
            double (*duNdt)          (int z, solnclass & soln);
            double (*dalautdt)       (int z, solnclass & soln);
            double (*dblautdt)       (int z, solnclass & soln);
            double (*dclautdt)       (int z, solnclass & soln);

            vector<DTArray *> qlaut;
            vector<DTArray *> qlauty;
            vector<DTArray *> ulaut;
            vector<DTArray *> psilaut;

            double *uS, *uN, *alaut, *blaut, *claut;

            // Derivative flags
            vector<bool> compute_u_derivs, compute_v_derivs, compute_q_derivs;
            vector<DTArray *> u_derivs,  u_derivs_bar,  u_derivs_laut;   // 1st order u derivatives
            vector<DTArray *> u_derivs2, u_derivs2_bar, u_derivs2_laut;  // 2nd order u derivatices (non-mixed)
            vector<DTArray *> v_derivs,  v_derivs_bar,  v_derivs_laut;
            vector<DTArray *> q_derivs,  q_derivs_bar,  q_derivs_laut;

            // Constructor method
            solnclass(int sx, int sy, int sz, double lx, double ly, double lz,
                    string const &flux_method, string const &ygrid_type, string const &zgrid_type,
                    bool zonal_decomp);

            // Initialize various constants
            void initialize_constants();

            // Compute arbitrary derivative
            void compute_deriv(string const var, string const der ) ;

            // Compute requested derivatives
            void compute_requested_derivatives();

            // Sum-reduce yz_slice
            void sum_reduce_yz_slice();

            // Load background profiles
            void initialize_background_states(string const &ub_fn, string const &vb_fn, string const &qxb_fn, 
                    string const &qyb_fn, string const &psib_fn);

            // Load background profile for computing norms
            void initialize_background_q(string const &qb_fn);

            // Load forcing function if needed
            void initialize_forcing();

            void load_forcing(string const &qforce);

            // Save state
            void write_outputs(int iter, bool write_psi, bool write_vels);

            // Initialize the diagnostics file
            void initialize_diagnostics_files();

            void stitch_diagnostics();

            void initialize_chain_diagnostics_files();

            void initialize_spectra_files();

            void compute_laut_fields();

            void update_energy_diagnostics();

            void stitch_chain_diagnostics();

            void stitch_spectra();

            void do_stitching();

            void close_chain_diagnostics();

            void close_spect();

            // Write initial diagnostics
            void write_initial_diagnostics();

            // Update after step
            void update_after_step(int plot_count);

            void dump_if_needed(int plot_count);

            void finalize(int plot_count, double final_time);

    };

    // Transform information
    struct Transgeom {

        private:   
            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            TArrayn::S_EXP type_y, type_y_bar, type_z;

            vector <CTArray *> qh, qh_bar_c;
            // K2 is actually the inverse laplacian
            // K2h is the forward horizontal laplacian
            vector <DTArray *> K2, K2h, K2_bar, qh_bar_r;

            GeneralArrayStorage<3> spec_ordering;
            TinyVector<int,3> spec_lbound, spec_extent;

            GeneralArrayStorage<3> mspec_ordering;
            TinyVector<int,3> mspec_lbound, mspec_extent;

            double norm_3d, norm_3d_bar;
            double norm_x, norm_y, norm_z;
            double f0, N0;

            bool is_channel;

            int Nx, Ny, Nz;
            double Lx, Ly, Lz;

            Trans1D *X_xform,
                    *Y_xform;
            TransWrapper *XYZ_xform;

            Trans1D *Y_xform_bar;
            TransWrapper *XYZ_xform_bar;

            Array<double,1> *kvec;
            Array<double,1> *lvec;
            Array<double,1> *mvec;
            Array<double,1> *kvec_bar;
            Array<double,1> *lvec_bar;
            Array<double,1> *mvec_bar;

            void initialize(solnclass &soln, string const &ygrid_type,
                    string const &zgrid_type) {

                Nx = soln.Nx;
                Ny = soln.Ny;
                Nz = soln.Nz;

                Lx = soln.Lx;
                Ly = soln.Ly;
                Lz = soln.Lz;

                f0 = soln.f0;
                N0 = soln.N0;

                norm_x = (2.*M_PI/Lx);

                if (ygrid_type == "PERIODIC") {
                    type_y = FOURIER;
                    type_y_bar = FOURIER;
                    norm_y = 2.*M_PI/Ly;
                } else if (ygrid_type == "FREE_SLIP") {
                    type_y = SINE;
                    type_y_bar = COSINE;
                    norm_y = M_PI/Ly;
                } else {
                    if (master()) fprintf(stderr,"Invalid option %s received for type_y\n",ygrid_type.c_str());
                    MPI_Finalize(); exit(1);
                }

                if (zgrid_type == "FOURIER") {
                    type_z = FOURIER;
                    norm_z = (2.*M_PI/Lz);
                } else if (zgrid_type == "REAL") { 
                    type_z = COSINE;
                    norm_z = (1.*M_PI/Lz);
                } else if (zgrid_type == "CHEB") {
                    type_z = NONE;
                    norm_z = (2./Lz);
                } else if (zgrid_type == "NONE") {
                    type_z = NONE;
                    norm_z = 0.;
                } else {
                    if (master()) fprintf(stderr,"Invalid option %s received for type_z\n",zgrid_type.c_str());
                    MPI_Finalize(); exit(1);
                }

                norm_3d = (*XYZ_xform).norm_factor();

                spec_ordering.ordering() = (*XYZ_xform).get_complex_temp()->ordering();
                spec_lbound = (*XYZ_xform).get_complex_temp()->lbound();
                spec_extent = (*XYZ_xform).get_complex_temp()->extent();

                qh.resize(1);
                K2.resize(1);
                K2h.resize(1);

                qh[0] = new CTArray(spec_lbound,spec_extent,spec_ordering);
                K2[0] = new DTArray(spec_lbound,spec_extent,spec_ordering);
                K2h[0] = new DTArray(spec_lbound,spec_extent,spec_ordering);

                norm_3d_bar = (*XYZ_xform_bar).norm_factor();
                qh_bar_r.resize(1);
                qh_bar_c.resize(1);
                if (is_channel) {
                    mspec_ordering.ordering() = (*XYZ_xform_bar).get_real_temp()->ordering();
                    mspec_lbound = (*XYZ_xform_bar).get_real_temp()->lbound();
                    mspec_extent = (*XYZ_xform_bar).get_real_temp()->extent();

                    qh_bar_r[0] = new DTArray(mspec_lbound,mspec_extent,mspec_ordering);
                } else {
                    mspec_ordering.ordering() = (*XYZ_xform_bar).get_complex_temp()->ordering();
                    mspec_lbound = (*XYZ_xform_bar).get_complex_temp()->lbound();
                    mspec_extent = (*XYZ_xform_bar).get_complex_temp()->extent();

                    qh_bar_c[0] = new CTArray(mspec_lbound,mspec_extent,mspec_ordering);
                }

                K2_bar.resize(1);
                K2_bar[0] = new DTArray(mspec_lbound,mspec_extent,mspec_ordering);

                // Define wavenumbers
                if (Nz > 1) {
                    *K2[0]     = -1.0/(pow((*kvec)(ii),2) + pow((*lvec)(kk),2)     + pow((*mvec)(jj),2)     + 1e-100)/norm_3d;
                    *K2_bar[0] = -1.0/(0*ii               + pow((*lvec_bar)(kk),2) + pow((*mvec_bar)(jj),2) + 1e-100)/norm_3d_bar;
                } else {
                    *K2[0]     = -1.0/(pow((*kvec)(ii),2) + pow((*lvec)(kk),2)     + 1.0/pow(soln.Lr,2.0))/norm_3d;
                    *K2_bar[0] = -1.0/(0*ii + 0*jj        + pow((*lvec_bar)(kk),2) + 1.0/pow(soln.Lr,2.0))/norm_3d_bar;
                }
                *K2h[0] = -1.0*(pow((*kvec)(ii),2) + pow((*lvec)(kk),2) + 0*jj )/norm_3d;

                // Change wavenumber at (0,0,0) from Inf to zero: Assumes mean of zero
                if ( (Nz > 1) or (f0 == 0.)) {
                    if (K2[0]->lbound(firstDim) == 0 && K2[0]->lbound(secondDim) == 0 && K2[0]->lbound(thirdDim) == 0 )
                    {  (*K2[0])(0,0,0) = 0.0; }

                    // Since each processor does its own inversion, we need to zero out each corresponding entry.
                    (*K2_bar[0])(soln.rank,0,0) = 0.0;
                }
            }
    };


    struct fluxclass {

        private:
            TinyVector<int,3> local_lbound, local_extent;
            GeneralArrayStorage<3> local_storage;

            // Blitz index placeholders
            blitz::firstIndex ii;
            blitz::secondIndex jj;
            blitz::thirdIndex kk;

        public:
            vector<DTArray *> q;

            // Constructor method
            fluxclass(int Nx, int Ny, int Nz) {

                local_lbound = alloc_lbound(Nx,Nz,Ny);
                local_extent = alloc_extent(Nx,Nz,Ny);
                local_storage = alloc_storage(Nx,Nz,Ny);

                q.resize(3);

                q[0] = new DTArray(local_lbound,local_extent,local_storage);
                q[1] = new DTArray(local_lbound,local_extent,local_storage);
                q[2] = new DTArray(local_lbound,local_extent,local_storage);

                *q[0] = 0*ii + 0*jj + 0*kk;
                *q[1] = 0*ii + 0*jj + 0*kk;
                *q[2] = 0*ii + 0*jj + 0*kk;
            }

    };



    // Class to define flux function
    struct methodclass {

        void (*flux)(solnclass & soln, DTArray & flux, Transgeom & Sz); 

        void (*force_evolve)(solnclass & soln, Transgeom & Sz); 

    };

};

using namespace qg_functions;

// Declare some functions
void compute_dt(qg_functions::solnclass & soln);

void compute_weights2(qg_functions::solnclass & soln);

void compute_weights3(qg_functions::solnclass & soln);

void write_grids(DTArray & tmp, double Lx, double Ly, double Lz,
        int Nx, int Ny, int Nz, qg_functions::Transgeom & Sz);

void vertical_solve(CTArray & psihat, CTArray & qhat, DTArray & K2, qg_functions::solnclass & soln);

void compute_vels_and_psi(qg_functions::solnclass & soln, qg_functions::Transgeom & Sz);

void nonlinear(qg_functions::solnclass & soln, DTArray & flux, qg_functions::Transgeom & Sz); 

void linear(qg_functions::solnclass & soln, DTArray & flux, qg_functions::Transgeom & Sz); 

void nonlinear_pert(qg_functions::solnclass & soln, DTArray & flux, qg_functions::Transgeom & Sz); 

void apply_filter(qg_functions::solnclass & soln, qg_functions::Transgeom & Sz);

void step_euler(qg_functions::solnclass & soln, qg_functions::fluxclass & flux, qg_functions::methodclass & method, qg_functions::Transgeom & Sz);

void step_ab2(qg_functions::solnclass & soln, qg_functions::fluxclass & flux, qg_functions::methodclass & method, qg_functions::Transgeom & Sz);

void step_ab3(qg_functions::solnclass & soln, qg_functions::fluxclass & flux, qg_functions::methodclass & method, qg_functions::Transgeom & Sz);

void initialize_diagnostics_files();

void filter_bar(DTArray & source, TransWrapper & tform, 
        S_EXP dim2_type, S_EXP dim3_type,
        double cutoff, double order, double strength);
#endif
