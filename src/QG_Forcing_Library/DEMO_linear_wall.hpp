#include "../qg_functions.hpp"

#ifndef DEMO_LINEAR_WALL_HPP // Prevent double inclusion
#define DEMO_LINEAR_WALL_HPP 1

namespace linear_wall {
    double (forcing_field_1) (int x, int y, int z, solnclass & soln);
    double (forcing_field_2) (int x, int y, int z, solnclass & soln);
    double (forcing_field_3) (int y, int z, solnclass & soln);
    double (forcing_field_4) (int y, int z, solnclass & soln);
    double (duSdt)           (int z, solnclass & soln);
    double (duNdt)           (int z, solnclass & soln);
    double (dalautdt)        (int z, solnclass & soln);
    double (dblautdt)        (int z, solnclass & soln);
    double (dclautdt)        (int z, solnclass & soln);

    extern double uN_star, r, uS_star, day;
}

#endif
