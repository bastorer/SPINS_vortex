#include "DEMO_zonal_launcher.hpp"
 
double zonal_launcher::day    = 3600. * 24;
double zonal_launcher::r      = 1. / (200 * day);   //  200-day e-folding scale
double zonal_launcher::u_star = 0.05 * r;           //  0.05 m/s per day at equilibrium
double zonal_launcher::L      = 10.e3;              // 10km half-width

// Localized jet-like forcing test
double zonal_launcher::forcing_field_1(int xi, int yi, int zi, solnclass & soln) { return 0.; }

double zonal_launcher::forcing_field_2(int xi, int yi, int zi, solnclass & soln) {
    // d/dy { F_{m,x} + D_{m,x} }
    double Ly = soln.Ly, Lx = soln.Lx;
    double xt = soln.xgrid[xi] / L;
    double yt = soln.ygrid[yi] / L;

    double tmp1 =  -2 * (yt/L) * u_star * exp( - pow( yt, 2) - pow( xt, 2));

    // Damping
    double tmp2 = (*soln.u_derivs[soln.Y_IND])(xi, zi, yi);
    double tmp3 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp4 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);

    return tmp1 - r*(tmp2 + tmp3 + tmp4);
}

double zonal_launcher::forcing_field_3(int yi, int zi, solnclass & soln) {
    // < d/dy { F_{m,x} + D_{m,x} } >
    double Lx = soln.Lx, Ly = soln.Ly;
    double C = pow(M_PI, 0.5)*(L/Lx)*erf(0.5*Lx/L);
    double yt = soln.ygrid[yi] / L;
    double tmp1 = -2 * (yt/L) * C * u_star * exp( - pow( yt, 2) );

    // Damping
    double tmp2 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp3 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);

    return tmp1 - r*(tmp2 + tmp3);
}

double zonal_launcher::forcing_field_4(int yi, int zi, solnclass & soln) { 
    return 0; 
}

double zonal_launcher::duSdt(int zi, solnclass & soln) {
    // d/dt u_S
    double tmp1 = 0.;
    return tmp1 - r*soln.uS[zi];
}

double zonal_launcher::duNdt(int zi, solnclass & soln) {
    // d/dt u_N
    double tmp1 = 0.;
    return tmp1 - r*soln.uN[zi];
}

double zonal_launcher::dalautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double C = pow(M_PI, 0.5)*(L/Lx)*erf(0.5*Lx/L);
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * C * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * C * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    dFdyy_Ly += -r*( duNdt(zi,soln)/(Lr*Lr) - dFdyy_Ly);

    return ( (duNdt(zi,soln) - duSdt(zi,soln))/(Lr*Lr) - (dFdyy_Ly - dFdyy_0) )/ (6*Ly);
}

double zonal_launcher::dblautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double C = pow(M_PI, 0.5)*(L/Lx)*erf(0.5*Lx/L);
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * C * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * C * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    
    return 0.5*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0);
}

double zonal_launcher::dclautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double C = pow(M_PI, 0.5)*(L/Lx)*erf(0.5*Lx/L);
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * C * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * C * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    dFdyy_Ly += -r*( duNdt(zi,soln)/(Lr*Lr) - dFdyy_Ly);
    
    return (duNdt(zi,soln) - duSdt(zi,soln))/Ly - Ly/(6*Lr*Lr)*(duNdt(zi,soln)+ 2*duSdt(zi,soln)) + (Ly/6)*(2*dFdyy_0 + dFdyy_Ly);
}

