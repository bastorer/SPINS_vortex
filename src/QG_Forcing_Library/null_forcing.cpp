#include "null_forcing.hpp"

// Linear internal forcing with Rayleigh damping
double null_forcing::forcing_field_1(int xi, int yi, int zi, solnclass & soln) {
    // d/dx { F_{m,y} + D_{m,y} }
    return 0.;
}

double null_forcing::forcing_field_2(int xi, int yi, int zi, solnclass & soln) {
    // d/dy { F_{m,x} + D_{m,x} }
    return 0.;
}

double null_forcing::forcing_field_3(int yi, int zi, solnclass & soln) {
    // < d/dy { F_{m,x} + D_{m,x} } >
    return 0.;
}

double null_forcing::forcing_field_4(int yi, int zi, solnclass & soln) {
    // d/d/ { F_laut_{m,x} + D_laut_{m,x} }
    return 0.;
}

double null_forcing::dalautdt(int zi, solnclass & soln) {
    // d/dt u_S
    return 0.;
}

double null_forcing::dblautdt(int zi, solnclass & soln) {
    // d/dt u_S
    return 0.;
}

double null_forcing::dclautdt(int zi, solnclass & soln) {
    // d/dt u_S
    return 0.;
}

double null_forcing::duSdt(int zi, solnclass & soln) {
    // d/dt u_S
    return 0.;
}

double null_forcing::duNdt(int zi, solnclass & soln) {
    // d/dt u_N
    return 0.;
}

