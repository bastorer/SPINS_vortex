#include "../qg_functions.hpp"

#ifndef PERIODIC_MERIDIONAL_LAUNCHER_HPP // Prevent double inclusion
#define PERIODIC_MERIDIONAL_LAUNCHER 1

namespace periodic_meridional_launcher {
    double (forcing_field_1) (int xi, int yi, int zi, solnclass & soln);
    double (forcing_field_2) (int xi, int yi, int zi, solnclass & soln);
    double (forcing_field_3) (int yi, int zi, solnclass & soln);
    double (forcing_field_4) (int yi, int zi, solnclass & soln);
    double (duSdt)           (int zi, solnclass & soln);
    double (duNdt)           (int zi, solnclass & soln);
    double (dalautdt)        (int zi, solnclass & soln);
    double (dblautdt)        (int zi, solnclass & soln);
    double (dclautdt)        (int zi, solnclass & soln);

    extern double u_star, L, T;
}

#endif
