#include "../qg_functions.hpp"

#ifndef QG_NULL_FORCING_HPP // Prevent double inclusion
#define QG_NULL_FORCING_HPP 1

namespace null_forcing {
    double (forcing_field_1) (int xi, int yi, int zi, solnclass & soln);
    double (forcing_field_2) (int xi, int yi, int zi, solnclass & soln);
    double (forcing_field_3) (int yi, int zi, solnclass & soln);
    double (forcing_field_4) (int yi, int zi, solnclass & soln);
    double (duSdt)           (int zi, solnclass & soln);
    double (duNdt)           (int zi, solnclass & soln);
    double (dalautdt)        (int zi, solnclass & soln);
    double (dblautdt)        (int zi, solnclass & soln);
    double (dclautdt)        (int zi, solnclass & soln);

}

#endif
