#include "DEMO_linear_wall.hpp"

double linear_wall::day = 3600. * 24;
double linear_wall::r   = 0.2 / day;      //  5-day e-folding time
double linear_wall::uN_star = - r * 0.4;  // -0.4 m/s at equilibration
double linear_wall::uS_star =   r * 0.1;  //  0.1 m/s at equilibration

// Linear internal forcing with Rayleigh damping
double linear_wall::forcing_field_1(int xi, int yi, int zi, solnclass & soln) {
    // d/dx { F_{m,y} + D_{m,y} }
    return 0.;
}

double linear_wall::forcing_field_2(int xi, int yi, int zi, solnclass & soln) {
    // d/dy { F_{m,x} + D_{m,x} }
    double tmp1 =  (uN_star - uS_star)/soln.Ly;
    double tmp2 = (*soln.u_derivs[soln.Y_IND])(xi, zi, yi);
    double tmp3 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp4 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);
    if (soln.Nz > 1) { tmp1 *= exp( -pow(8*(soln.zgrid[zi]/soln.Lz + 0.25), 2.0)); }
    return tmp1 - r*(tmp2 + tmp3 + tmp4);
}

double linear_wall::forcing_field_3(int yi, int zi, solnclass & soln) {
    // < d/dy { F_{m,x} + D_{m,x} } >
    double tmp1 =  (uN_star - uS_star)/soln.Ly;
    double tmp2 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp3 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);
    if (soln.Nz > 1) { tmp1 *= exp( -pow(8*(soln.zgrid[zi]/soln.Lz + 0.25), 2.0)); }
    return tmp1 - r*(tmp2 + tmp3);
}

double linear_wall::forcing_field_4(int yi, int zi, solnclass & soln) {
    // d/dy { F_laut_{m,x} + D_laut_{m,x} }
    double Ly = soln.Ly;
    double Lr = soln.Lr; 
    double yt = soln.ygrid[yi] + Ly/2.;

    double ahat = dalautdt(zi, soln);
    double bhat = dblautdt(zi, soln);
    double chat = dclautdt(zi, soln);

    return (3*ahat*pow(yt,2) + 2*bhat*yt + chat);
}

double linear_wall::duSdt(int zi, solnclass & soln) {
    // d/dt u_S
    double tmp1 = uS_star;
    if (soln.Nz > 1) { tmp1 *= exp( -pow(8*(soln.zgrid[zi]/soln.Lz + 0.25), 2.0)); }
    return tmp1 - r*soln.uS[zi];
}

double linear_wall::duNdt(int zi, solnclass & soln) {
    // d/dt u_N
    double tmp1 = uN_star;
    if (soln.Nz > 1) { tmp1 *= exp( -pow(8*(soln.zgrid[zi]/soln.Lz + 0.25), 2.0)); }
    return tmp1 - r*soln.uN[zi];
}


double linear_wall::dalautdt(int zi, solnclass & soln) {
    double Ly = soln.Ly;
    double Lr = soln.Lr;

    double tmp1 = (*soln.u_derivs2_bar[soln.Y_IND])(soln.rank, zi, 0);
    double tmp2 = (*soln.u_derivs2_laut[soln.Y_IND])(soln.rank, zi, 0);
    double dFdyy_0 = -r * ( tmp1 + tmp2 ) * 0;

    double tmp3 = (*soln.u_derivs2_bar[soln.Y_IND])(soln.rank, zi, soln.Ny-1);
    double tmp4 = (*soln.u_derivs2_laut[soln.Y_IND])(soln.rank, zi, soln.Ny-1);
    double dFdyy_Ly = -r * ( tmp3 + tmp4 ) * 0;

    // Rayleigh term
    //dFdyy_0  += -r * duSdt(zi,soln)/(Lr*Lr);
    //dFdyy_Ly += -r * duNdt(zi,soln)/(Lr*Lr);

    return ( (duNdt(zi,soln) - duSdt(zi,soln))/(Lr*Lr) - (dFdyy_Ly - dFdyy_0) )/ (6*Ly);
}

double linear_wall::dblautdt(int zi, solnclass & soln) {
    double Ly = soln.Ly;
    double Lr = soln.Lr;

    double tmp1 = (*soln.u_derivs2_bar[soln.Y_IND])(soln.rank, zi, 0);
    double tmp2 = (*soln.u_derivs2_laut[soln.Y_IND])(soln.rank, zi, 0);
    double dFdyy_0 = -r * ( tmp1 + tmp2 ) * 0;

    // Rayleigh term (-r*u_yy)
    //dFdyy_0  += -r * duSdt(zi,soln)/(Lr*Lr);

    return 0.5*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0);
}

double linear_wall::dclautdt(int zi, solnclass & soln) {
    double Ly = soln.Ly;
    double Lr = soln.Lr;

    double tmp1 = (*soln.u_derivs2_bar[soln.Y_IND])(soln.rank, zi, 0);
    double tmp2 = (*soln.u_derivs2_laut[soln.Y_IND])(soln.rank, zi, 0);
    double dFdyy_0 = -r * ( tmp1 + tmp2 ) * 0;

    double tmp3 = (*soln.u_derivs2_bar[soln.Y_IND])(soln.rank, zi, soln.Ny-1);
    double tmp4 = (*soln.u_derivs2_laut[soln.Y_IND])(soln.rank, zi, soln.Ny-1);
    double dFdyy_Ly = -r * ( tmp3 + tmp4 ) * 0;

    // Rayleigh term
    //dFdyy_0  += -r * duSdt(zi,soln)/(Lr*Lr);
    //dFdyy_Ly += -r * duNdt(zi,soln)/(Lr*Lr);

    return (duNdt(zi,soln) - duSdt(zi,soln))/Ly - Ly/(6*Lr*Lr)*(duNdt(zi,soln)+ 2*duSdt(zi,soln)) + (Ly/6)*(2*dFdyy_0 + dFdyy_Ly);
}

