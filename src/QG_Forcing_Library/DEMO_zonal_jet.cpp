#include "DEMO_zonal_jet.hpp"
 
double zonal_jet::day    = 3600. * 24;
double zonal_jet::r      = 1. / (50 *day);  // 50-day e-folding scale
double zonal_jet::u_star = 0.05 * r;        // 0.05 m/s at equilibrium
double zonal_jet::L      = 10.e3;           // 10km half-width

// Localized jet-like forcing test
double zonal_jet::forcing_field_1(int xi, int yi, int zi, solnclass & soln) { return 0.; }

double zonal_jet::forcing_field_2(int xi, int yi, int zi, solnclass & soln) {
    // d/dy { F_{m,x} + D_{m,x} }
    double Ly = soln.Ly, Lx = soln.Lx;
    double yt = soln.ygrid[yi] / L;

    double tmp1 =  -2 * (yt/L) * u_star * exp( -pow( yt, 2) );

    // Damping
    double tmp2 = (*soln.u_derivs[soln.Y_IND])(xi, zi, yi);
    double tmp3 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp4 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);

    return tmp1 - r*(tmp2 + tmp3 + tmp4);
}

double zonal_jet::forcing_field_3(int yi, int zi, solnclass & soln) {
    // < d/dy { F_{m,x} + D_{m,x} } >
    double yt = soln.ygrid[yi] / L;
    double tmp1 = -2 * (yt/L) * u_star * exp( - pow( yt, 2) );

    // Damping
    double tmp2 = (*soln.u_derivs_bar[soln.Y_IND])(soln.rank, zi, yi);
    double tmp3 = (*soln.u_derivs_laut[soln.Y_IND])(soln.rank, zi, yi);

    return tmp1 - r*(tmp2 + tmp3);
}

double zonal_jet::forcing_field_4(int yi, int zi, solnclass & soln) { 
    return 0; 
}

double zonal_jet::duSdt(int zi, solnclass & soln) {
    // d/dt u_S
    double tmp1 = 0.;
    return tmp1 - r*soln.uS[zi];
}

double zonal_jet::duNdt(int zi, solnclass & soln) {
    // d/dt u_N
    double tmp1 = 0.;
    return tmp1 - r*soln.uN[zi];
}

double zonal_jet::dalautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    dFdyy_Ly += -r*( duNdt(zi,soln)/(Lr*Lr) - dFdyy_Ly);

    return ( (duNdt(zi,soln) - duSdt(zi,soln))/(Lr*Lr) - (dFdyy_Ly - dFdyy_0) )/ (6*Ly);
}

double zonal_jet::dblautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    
    return 0.5*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0);
}

double zonal_jet::dclautdt(int zi, solnclass & soln) {
    double Lr = soln.Lr;
    double Lx = soln.Lx, Ly = soln.Ly;
    double y0  = soln.ygrid[0]         / L;
    double yLy = soln.ygrid[soln.Ny-1] / L;
    double dFdyy_0  = -2 * (u_star/L/L) * (1 - 2 * y0*y0   / L) * exp( - pow( y0,  2) );
    double dFdyy_Ly = -2 * (u_star/L/L) * (1 - 2 * yLy*yLy / L) * exp( - pow( yLy, 2) );

    // Rayleigh term (-r*u_yy)
    dFdyy_0  += -r*( duSdt(zi,soln)/(Lr*Lr) - dFdyy_0 );
    dFdyy_Ly += -r*( duNdt(zi,soln)/(Lr*Lr) - dFdyy_Ly);
    
    return (duNdt(zi,soln) - duSdt(zi,soln))/Ly - Ly/(6*Lr*Lr)*(duNdt(zi,soln)+ 2*duSdt(zi,soln)) + (Ly/6)*(2*dFdyy_0 + dFdyy_Ly);
}

