#include "DEMO_periodic_meridional_launcher.hpp"

double periodic_meridional_launcher::u_star = 0.05/(3600*24); // 0.05 m/s per day
double periodic_meridional_launcher::T = 20*3600*24; // 20 day period 
double periodic_meridional_launcher::L = 10.e3; // 10km half-width

double periodic_meridional_launcher::forcing_field_1(int xi, int yi, int zi, solnclass & soln) {
    // d/dx { F_{m,y} + D_{m,y} }
    double xt = soln.xgrid[xi]/L;
    double yt = soln.ygrid[yi]/L;
    return -2 * (xt/L) * u_star * exp( - pow( yt, 2) - pow( xt, 2)) * sin( 2 * M_PI * soln.t / T);
}

double periodic_meridional_launcher::forcing_field_2(int xi, int yi, int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::forcing_field_3(int yi, int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::forcing_field_4(int yi, int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::duSdt(int zi, solnclass & soln) { return 0; } 

double periodic_meridional_launcher::duNdt(int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::dalautdt(int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::dblautdt(int zi, solnclass & soln) { return 0; }

double periodic_meridional_launcher::dclautdt(int zi, solnclass & soln) { return 0; }
