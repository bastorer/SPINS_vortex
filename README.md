## Welcome to SPINS\_vortex!

This repository is designed to serve as a branch of the master SPINS repository (https://git.uwaterloo.ca/SPINS/SPINS\_main, or clone from https://git.uwaterloo.ca/SPINS/SPINS_main.git).

This branch provides some development with respect to geophysical fluid applications, as well as some more generic developments. As features are finalized, they will be merged into the master repository, but in-progress work (which, if I'm honest, is most of it) is kept here.

To add this repository as a branch to your own SPINS installation, the following series of commands *should* work.

1. git remote add vortex https://git.uwaterloo.ca/bastorer/SPINS_vortex.git
  (“git remote -v” should now list vortex as one of the remotes.)
2. git fetch vortex
3. git checkout -b vortex vortex/master
